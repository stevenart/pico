(function() {
	tinymce.addI18n('fr_FR',{
		"AI Assistant": "Assistant IA",
		"Generate": "Générer",
		"What do you want me to do?": "Que souhaitez-vous que je fasse ?",
		"AI actions requires editor content or selection. You should insert and/or select content first.": "Les fonctionnalités IA modifient le contenu ou la sélection. Vous devez d'abord entrer du contenu.",
		"An unknown error has occured. Please try again or contact support.": "Une erreur est survenue. Merci de réessayer et de contacter le support si l'erreur persiste.",
		"No assistant has been configured for WYSIWYG editors.": "Aucun assistant n'a été configuré pour ce type d'éditeur.",
	});
	tinymce.PluginManager.add('aibot', function (editor) {
		editor.ui.registry.addIcon(
			'aibot',
			'<svg width="24" height="24" focusable="false"><path fill-rule="evenodd" clip-rule="evenodd" d="M5 3a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h14a3 3 0 0 0 3-3V6a3 3 0 0 0-3-3H5Zm6.8 11.5.5 1.2a68.3 68.3 0 0 0 .7 1.1l.4.1c.3 0 .5 0 .7-.3.2-.1.3-.3.3-.6l-.3-1-2.6-6.2a20.4 20.4 0 0 0-.5-1.3l-.5-.4-.7-.2c-.2 0-.5 0-.6.2-.2 0-.4.2-.5.4l-.3.6-.3.7L5.7 15l-.2.6-.1.4c0 .3 0 .5.3.7l.6.2c.3 0 .5 0 .7-.2l.4-1 .5-1.2h3.9ZM9.8 9l1.5 4h-3l1.5-4Zm5.6-.9v7.6c0 .4 0 .7.2 1l.7.2c.3 0 .6 0 .8-.3l.2-.9V8.1c0-.4 0-.7-.2-.9a1 1 0 0 0-.8-.3c-.2 0-.5.1-.7.3l-.2 1Z"></path></svg>'
		);
		editor.ui.registry.addButton('aibot', {
			icon: 'aibot',
			tooltip: 'AI Assistant',
			onAction: function () {
				let aibot = new AIBot('wysiwyg', function(config){
					let content = tinymce.activeEditor.selection.getContent();
					let contentReplace = false;
					if (content=='') {
						content = tinymce.activeEditor.getContent();
						contentReplace = true;
					}
					if (content=='') {
						tinymce.activeEditor.notificationManager.open({
							text: 'AI actions requires editor content or selection. You should insert and/or select content first.',
							type: 'error'
						});
					} else {
						return editor.windowManager.open({
							title: 'AI Assistant',
							body: {
								type: 'panel',
								items: [
									{
										type: 'selectbox',
										name: 'prompt',
										label: 'What do you want me to do?',
										items: config.tinymce,
									},
								]
							},
							buttons: [
								{
									type: 'cancel',
									text: 'Close',
								},
								{
									type: 'submit',
									text: 'Generate',
									primary: true,
								}
							],
							onSubmit: function(api) {
								api.block('Loading...');
								const data = api.getData();
								let prompt = data.prompt + "\n" + content;
								aibot.chat(prompt, function(response) {
									let reply = response.message.content;
									if (reply) {
										if (contentReplace) {
											editor.setContent(reply);
										} else {
											editor.insertContent(reply);
										}
										editor.undoManager.add();
									}
									api.close();
								}, function(error, errorDetails){
									console.log('[aibot:tinymce_plugin] ' + error, errorDetails);
									api.close();
									tinymce.activeEditor.notificationManager.open({
										text: 'An unknown error has occured. Please try again or contact support.',
										type: 'error'
									});
								});
							}
						});
					}
				}, function(e){
					tinymce.activeEditor.notificationManager.open({
						text: 'No assistant has been configured for WYSIWYG editors.',
						type: 'error'
					});
				});
			}
		});
		return {
			getMetadata: function () {
				return {
					author : 'Nicolas Stevenart',
					url : 'https://picophp.micromegas.me/',
					name : 'Pico AI Bot TinyMCE Plugin',
					version : '1.0'
				}
			}
		}
	});
})();