(function() {
	tinymce.PluginManager.add('picocdn', (editor, url) => {
		editor.ui.registry.addButton('picocdn', {
			icon: 'image',
			title: "Insert/edit image",
			onAction: () => {
				let thumbnail = tinymce.activeEditor.getParam('picocdn_thumbnail');
				if (typeof thumbnail === 'undefined') {
					thumbnail = '';
				}
				tinymce.activeEditor.windowManager.openUrl({
					height: 500,
					title: "Insert/edit image",
					url: pico.href(';resources/tinymce/picocdn.php')+'?lang='+window.location.pathname.slice(1,3)+'&thumb='+thumbnail,
					width: 500
				});

			}
		});
		return {
			getMetadata: () => ({
				author : 'Nicolas Stevenart',
				url : 'https://picophp.micromegas.me/',
				name : 'Pico CDN TinyMCE Plugin',
				version : '1.0'
			})
		};
	});
})();
