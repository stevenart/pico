<?php

require_once '../../../core/autoload.php';
global $pico;
$pico = Pico::singleton();

if (CMSUserModel::logged()===false) {
	header("HTTP/1.0 401 Authorization Required"); 
	
} else {
	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])=='xmlhttprequest' && isset($_POST['id'])) {
		$file = $pico->db->row('cdn', '`id`='.intval($_POST['id']));
		$thumb = empty($_POST['thumb']) ? 'wysiwyg' : $_POST['thumb'];
		$width = $file['width'];
		$height = $file['height'];
		$ratio = $file['ratio'];
		if ($thumb) {
			$thumbFile = __ROOT.'/cdn/'.$file['id'].'_'.$file['key'].'_'.$thumb.'.'.$file['extension'];
			if (file_exists($thumbFile)) {
				$imageSize = getimagesize($thumbFile);
				$width = $imageSize[0];
				$height = $imageSize[1];
				$ratio = $imageSize[0] / $imageSize[1];
			}
		}
		$type = 'file';		
		if (in_array($file['extension'], Thumbnail::$driverExtensions)) {
			$type = 'image';
		} elseif (in_array($file['extension'], array('svg'))) {
			$type = 'vector';
			$thumb = ''; 
		} else {
			$thumb = ''; 
		}
		$url = $pico->cdnHref($file, $thumb, false, true);
		echo json_encode(array(
			'alt'			=> empty($file[$pico->i18n->lang().'-alt']) ? $file['name'] : $file[$pico->i18n->lang().'-alt'],
			'extension' 	=> $file['extension'],
			'name'			=> $file['name'],
			'width'			=> $width,
			'height'		=> $height,
			'ratio'			=> $ratio,
			'type'			=> $type,
			'url'			=> $url,
		));
	} else {
		if (isset($_GET['lang'])) {
			$pico->i18n->lang($_GET['lang']);
		}
		$dummy = new Model(array(
			'name'=>'dummy', 
			'detached'=>1,
			'fields'=>array(
				'id'=>array('type'=>'integer','checks'=>array('required')),
				'file'=>array('type'=>'cdn','max'=>'1','multiple'=>0,'browse'=>true),
			)
		));
		$_SESSION['pico__defer'] = true;
		$pico->variable('_pico', [
			'cdnDriverExtensions' => Thumbnail::$driverExtensions,
			'gdpr' => false,
		], false, true);
		echo $pico->twig->render('picocdn.twig', array(
			'form' => $dummy->form(array(
				'actions' => array(
					'insert'=>array('type'=>'link','css'=>'button isUpdate isPrimary isDisabled','text'=>i18n('CMS.ACTION_INSERT_CDN'))
				),
				'css' => 'iframe',
			)),
			'lang' => $pico->i18n->lang(),
			'thumb' => isset($_GET['thumb']) ? $_GET['thumb'] : 'wysiwyg',
		));
		$_SESSION['pico__defer'] = false;
	}
}
die();