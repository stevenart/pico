<?php

require_once '../core/autoload.php';


/*  HTTP Strict Transport Security */
if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS']!='off')) {
	header('Strict-Transport-Security: max-age=500; includeSubDomains; preload');
} 


/* XSS-Protection */
header("X-XSS-Protection: 1; mode=block");


/* Pico */
global $pico;
if (isset($_GET['__pico_file'])) {
	$picoFile = $_GET['__pico_file'];
	unset($_GET['__pico_file']);
} else {
	$picoFile = false;
}
if (isset($_GET['__pico_rewrite'])) {
	$rewrite = $_GET['__pico_rewrite'];
	unset($_GET['__pico_rewrite']);
} else {
	$rewrite = '';
}
if (isset($_GET['__pico_root'])) {
	unset($_GET['__pico_root']);
}
if ($picoFile && (appfile('public/'.$picoFile))) {
	/* Direct access */
	require(appfile('public/'.$picoFile));
} else {
	/* Request start */
	$pico = Pico::singleton();
	if (file_exists(__ROOT.'/cache/maintenance')) {
		$pico->fatal('/cache/maintenance file present. Site access locked.');
	}
	$pico->start($rewrite);
}