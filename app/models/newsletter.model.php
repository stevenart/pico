<?php

class NewsletterModel extends Model {


	public function renderContent(array $sending, $index=0) {
		global $pico;
		if ($this->fetched($index)) {
		
			switch($this->value('template', $index)) {

				/* You can define special content based on template here. HTML must be returned. */

				default:
					return $this->value('content', $index);

			}
		}
		return false;

	}
	


}
