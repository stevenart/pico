<?php

class ContactModel extends Model {


	public function send($email=NULL) {
		global $pico;
		if ($this->fetched()) {
			$mail = new Mail();
			if (is_string($email) && Check::email($email)) {
				$mail->addTo($email, true);
			}
			$mail->addReplyTo(array(
				'name' => $this->value('name'),
				'email' => $this->value('email'),
			));
			$subject = i18n('APP.TITLE_EMAIL_CONTACT', NULL, $this->value('lang'));
			$mail->setSubject($subject);
			$mail->setBodyTemplate('emails/contact.twig', array(
				'contact' => $this->one(),
				'fields' => $this->getFields(),
				'signature' => false,
				'subject' => $subject,
			));
			return $mail->send();
		}
		return false;

	}
	


}
