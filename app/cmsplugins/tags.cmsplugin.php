<?php

class TagsCMSPlugin extends CMSPlugin {


	static public function getPluginConfiguration() {
		$config = Tools::arrayMergeConfig(parent::getPluginConfiguration(), array(
			'actions'		=> array(
				'index'	=> array(
					'order' => '`name` ASC',
					'sortable' => true,
					'star' => true,
				)
			),
			'icon'			=> 'tag',
			'model' 		=> 'Tag',
			'title'			=> i18n('CMS.LABEL_PLUGIN_TAGS'),
		));
		unset($config['actions']['preview']);
		return $config;
	}


}