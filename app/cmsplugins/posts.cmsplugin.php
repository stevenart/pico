<?php

class PostsCMSPlugin extends CMSPlugin {


	static public function getPluginConfiguration() {
		return Tools::arrayMergeConfig(parent::getPluginConfiguration(), array(
			'actions' 		=> array(
				'index' => array(
					'order' => '`__draft` DESC, IFNULL(`publication`,`__created`) DESC',
				),
				'create' => array(
					'form'		=> array(
						'fsclosed' => array('seo'),
					),
				),
				'preview' => array(
					'url' 		=> '*posts/preview',
				),
				'update' => array(
					'form'		=> array(
						'fsclosed' => array('seo'),
					),
				),
			),
			'draft'			=> true,
			'icon'			=> 'feed',
			'model' 		=> 'Post',
			'search'		=> array('title', 'content', 'metaTitle', 'metaDescription'),
			'title'			=> i18n('CMS.LABEL_PLUGIN_POSTS')
		));
	}


}