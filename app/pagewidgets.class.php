<?php

/**
 * Note: widgets starting with "_" are implicit, which means there are always executed
 * on all pages without any interaction from CMS. This helps executing code on every pages 
 * without altering PageController.
 */
class PageWidgets {
	

	static public function contactForm(&$page) {
		global $pico;
		$widgetData = array();
		if ($pico->session('contactFormSuccess')) {
			$widgetData['formSuccess'] = true;
		} else {
			$contact = Model::factory('Contact');
			$canSave = $contact->loadForm();
			if ($canSave===true) {
				$contact->value('lang', 0, $pico->i18n->lang());
				$contact->save();
				if ($contact->send()) {
					$pico->session('contactFormSuccess', true);
					$pico->url('#');
				}
			} else {
				$widgetData['form'] = $contact->form(array(
					'actions' => array(
						'submit' => array('type'=>'submit', 'css'=>'primary', 'text'=>i18n('APP.ACTION_SUBMIT'))
					),
					'captcha' => true,
					'css'		=> 'labels-left',
					'errors' => $canSave,
					'fields' => array('lang'),
					'include' => false,
				));
			}
		}
		return $widgetData;
	}


}