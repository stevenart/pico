<?php

class Hooks {


	/**
	 * Executed before any CDN request (GET or POST). Usefull to limit access.
	 */
	static public function cdnAccess() {
		// if (!UserModel::logged()) $pico->_401(); 
	}


	/**
	 * Changes core plugin configuration.
	 *
	 * @param string $pluginName The plugin name
	 * @param array $pluginConfiguration Reference to the plugin configuration
	 */
	static public function cmsPluginConfiguration(string $pluginName, array &$pluginConfiguration) {
		// $pluginConfiguration['config'] = 'new value';
	}


	/**
	 * Changes to the default _pico variable passed to every views.
	 * 
	 * @access public
	 * @static
	 * @param array $picoVariable Reference to the _pico variable passed to every view.
	 * @return void
	 */
	static public function controllerPicoVariable(array &$picoVariable) {
		// $picoVariable['my_var'] = 'my value';
	}


	/**
	 * Return current environment
	 *
	 * @return array The environement
	 */
	static public function env() {
		if ((!isset($_SERVER['SERVER_PORT']) || (intval($_SERVER['SERVER_PORT'])==8888))) {
			return 'local';
		} else {
			return 'prod';
		}
	}


	/**
	 * Changes the definition of models on load.
	 * 
	 * @access public
	 * @static
	 * @param array $definition Reference to the model definition.
	 * @return void
	 */
	static public function modelDefinition(string $modelName, array &$definition) {
		//$definition['fields']['__newautofield'] = array('name'=>'__newautofield','type'=>'boolean','default'=>0);
	}


	/**
	 * Changes the type of fields allowed in models.
	 * 
	 * @access public
	 * @static
	 * @param array $fields Type of fields allowed.
	 * @return void
	 */
	static public function modelFields(array &$fields) {
	/*	
		$fields['newfieldtype'] = [
			'escape' => true,
			'sql' => '`%name` JSON %null',
			'sql_default' => array('name'=>'newfieldtype', 'null'=>'NULL'),
		];
	*/
	}


	/**
	 * Changes the url returned by any call of Pico::href().
	 *
	 * @param string $href Reference to the resulting URL
	 * @param string $href Original href parameter
	 * @param bool $includeHost Original includeHost parameter
	 * @param bool $disableCache Original disableCache parameter
	 */
	static public function picoHref(string &$url, string $href, bool $includeHost, bool $disableCache) {
		// $url = str_replace($domain1, $domain2, $url);
	}


	/**
	 * Changes the sitemap.xml entries
	 *
	 * @param array $urls The urls entries
	 * @param null|string $lang The language
	 */
	static public function sitemap(array &$urls, ?string $lang = NULL) {
		// $urls[] = array();
	}


}