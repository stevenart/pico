<?php

class Page_PostsController extends PageController {


	public static function _sitemap(string $pageUri, ?string $lang = NULL) {
		global $pico;
		if (is_null($lang)) {
			$lang = $pico->i18n->lang();
		}
		$urls = array();
		$post = Model::factory('Post');
		$post->fetch(self::baseCondition($lang));
		foreach($post->values() as $item) {
			$urls[] = array(
				'loc' => $pico->href(';'.$lang.'/'.$pageUri.'/'.$item['uri']),
				'changefreq' => 'yearly',
				'lastmod' => substr($item['__timestamp'], 0, 10),
				'priority' => 0.4,
			);
		}
		return $urls;
	}


	public static function baseCondition(?string $lang = NULL) {
		global $pico;
		if (is_null($lang)) {
			$lang = $pico->i18n->lang();
		}
		return '`__draft`=0 AND `lang`="'.$lang.'" AND (`publication` IS NULL OR `publication`<=CURRENT_DATE)';
	} 

	
	public function _() {	
		global $pico;

		/* Tags */
		$tag = Model::factory('Tag');
		$tag->fetch('`__draft`=0 AND EXISTS(SELECT * FROM `posts` WHERE '.self::baseCondition().' AND FIND_IN_SET(`tags`.`id`,`posts`.`tags`))');
		$this->variable('tags', $tag->values());

		/* Posts */
		$post = Model::factory('Post');
		$this->variable('fields', $post->getFields());

		
		if (isset($pico->urlParts[3]) && ($pico->urlParts[3]=='preview')) {

			/* Preview */
			if (CMSController::isLogged() && $post->fetchPrimary($pico->urlParts[4])) {
				$this->variable('post', $post->one(NULL, array(
					'tags'=>array('tags','id'),
					'author'=>array('cmsusers','id'),
				)));
			} else {
				$pico->url('-');
			}

		} elseif (isset($pico->urlParts[3]) && ($pico->urlParts[3]!='tag')) {

			/* This is a single post page */
			$post->fetch('`__draft`=0 AND `uri`="'.safer($pico->urlParts[3], 'db').'"');
			if (!$post->fetched()) {
				$pico->url('<');
			}
			$this->variable('post', $post->one(NULL, array(
				'tags'=>array('tags','id'),
				'author'=>array('cmsusers','id'),
			)));

			/* Metas */
			if ($post->value('metaTitle')) {
				$pico->meta('title', $post->value('metaTitle'));
			} else {
				$pico->meta('title', $post->value('title').' - '.$pico->meta('title'));
			}
			if ($post->value('metaDescription')) {
				$pico->meta('description', $post->value('metaDescription'));
			}
			if ($post->value('metaKeywords')) {
				$pico->meta('keywords', $post->value('metaKeywords'));
			}
			if ($post->value('metaCanonical')) {
				if ($post->value('metaCanonical')=='yes') {
					$pico->meta('canonical', $pico->href('~'));
				} elseif ($post->value('metaCanonical')=='no') {
					$pico->meta('canonical', NULL, true);
				} elseif ($post->value('metaCanonical')=='manual') {
					$pico->meta('canonical', $pico->href($post->value('metaCanonicalManual')));
				}
			}

		} else {

			/* This is a posts listing page */

			$condition = self::baseCondition();
			if (isset($pico->urlParts[3]) && ($pico->urlParts[3]=='tag')) {
				if (isset($pico->urlParts[4]) && $tag->existsField('uri', $pico->urlParts[4])) {
					$tag->fetchOne('uri', $pico->urlParts[4]);
					$this->variable('_tag', $tag->one()); 
					$condition.= ' AND (FIND_IN_SET('.$tag->value('id').',`tags`))';
				} else {
					$pico->url('-');
				}
			}

			$pagination = array(
				'pcount' => 1,
				'pnumber' => 1,
				'psize' => 5,
				'items' => $post->count($condition),
			);
			$pagination['pcount'] = ceil($pagination['items']/$pagination['psize']);
			if (isset($_GET['page'])) {
				$_GET['page'] = intval($_GET['page']);
				if (($_GET['page']>0) && ($_GET['page']<=$pagination['pcount'])) {
					$pagination['pnumber'] = $_GET['page'];
				}	
			}
			$limit = (($pagination['pnumber']-1)*$pagination['psize']).','.$pagination['psize'];
			
			$post->fetch($condition, 'IFNULL(`publication`,`__created`) DESC', $limit);
			$this->variable('pagination', $pagination);
			$this->variable('posts', $post->values(NULL, array(
				'tags'=>array('tags','id'),
				'author'=>array('cmsusers','id'),
			)));

		}
	
	}
		
}