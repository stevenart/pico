<?php

class Thumbnail {


	public static $driver = 'gd';
	public static $driverExtensions = ['gif','jpg','jpeg','png','webp'];


	static public function cms($source, $destination) {
		Intervention\Image\ImageManager::{self::$driver}()
			->read($source)
			->cover(100, 100)
			->save($destination);
	}


	static public function cmsZoom($source, $destination) {
		Intervention\Image\ImageManager::{self::$driver}()
			->read($source)
			->scaleDown(1280, 720)
			->save($destination);
	}


	static public function newsletter($source, $destination) {
		Intervention\Image\ImageManager::{self::$driver}()
			->read($source)
			->scaleDown(600, 600)
			->save($destination);
	}


	static public function test_webp($source, $destination) {
		Intervention\Image\ImageManager::{self::$driver}()
			->read($source)
			->cover(300, 300)
			->toWebp()
			->save($destination);
	}

	

	static public function wysiwyg($source, $destination) {
		Intervention\Image\ImageManager::{self::$driver}()
			->read($source)
			->scaleDown(1920, 1080)
			->save($destination);
	}


}