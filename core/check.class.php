<?php

class Check {


	static public function after($value, $date) {
		return (self::before($date, $value));
	}


	static public function before($value, $date) {
		if ( (self::date($value)===false) || (self::date($date)===false) ) return false;
		$value = intval(str_replace('-','',$value));
		$date = intval(str_replace('-','',$date)); 
		if ($value<$date) return true;
		return false;
	}
	

	static public function checkValue($value, $rule) {
		$rule = str_replace(', ', ',', $rule);
		$pos = strpos($rule, '(');
		if (!Check::required($value)) {
			if ($rule=='required') {
				return false;
			} elseif (substr($rule, 0, 8)!='required') {
				return true;
			}
		}
		if ($pos===false) {
			if (method_exists('AppCheck',$rule)) {
				return AppCheck::$rule($value);
			} elseif (method_exists('Check',$rule)) {
				return Check::$rule($value);
			} else {
				return false;
			}
		} else {
			$param = substr($rule, $pos+1, -1);
			if (strpos($param, ',')!==false) {
				$param = explode(',', $param);
				foreach($param as $paramIndex => $paramValue) {
					$param[$paramIndex] = trim($paramValue, " '\"\n\r\t\v\0");
					if (substr($paramValue, 0, 1)=='$') unset($param[$paramIndex]);
				}
			}
			$rule = substr($rule, 0, $pos);
			if (method_exists('AppCheck',$rule)) {
				return AppCheck::$rule($value, $param);
			} elseif (method_exists('Check',$rule)) {
				return Check::$rule($value, $param);
			} else {
				return false;
			}
		}
	}
	

	static public function computerfriendly($value, $case='lower') {
		global $pico;
		return ($value==Tools::computerFriendly($value, $case));
	}
	

	static public function date($value) {
		if (!(strpos($value, '-')==strrpos($value, '-'))) {
			list($date_AAAA, $date_MM, $date_DD) = explode('-', $value, 3);
			if (isset($date_AAAA) && isset($date_MM) && isset($date_DD)) {
				if (is_numeric($date_AAAA) && is_numeric($date_MM) && is_numeric($date_DD)) 
					return checkdate($date_MM, $date_DD, $date_AAAA);
			} 
		}
		return false;
	}

	
	static public function datetime($value) {
		list($date, $time) = explode(" ", $value, 2);
		if (isset($date) && isset($time)) {
			return (Check::date($date) && Check::time($time));
		}
		return false;
	}


	static public function different($value, $original, $forceType=NULL) {
		if ($forceType=='float') {
			$value = floatval($value);
			$original = floatval($original);
		} elseif ($forceType=='int' || $forceType=='integer') {
			$value = intval($value);
			$original = intval($original);
		}
		return ($value!=$original);
	}
	

	static public function email($value) {
		return (preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]+)$/i', $value)>0);
	}
	

	static public function emails($value, $separator="\n") {
		if (substr($separator, 0, 2)=='\u') {
			/* Decode unicode characters to avoid yaml parsing error. (\u002c = ",") */
			$separator = json_decode('"'.$separator.'"');
		}
		$emails = explode($separator, $value);
		foreach ($emails as $email) {
			$email = trim($email, " \r\t\v");
			if (!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]+)$/i', $email)) {
				return false;
			}
		}
		return true;
	}

	
	static public function equal($value, $original) {
		return (floatval($value)==floatval($original));
	}


	static public function extensions($value, $extensions) {
		global $pico;
		if (!is_array($value)) {
			$ids = explode(',', $value);
		} else {
			$ids = $value;
		}
		if (!is_array($extensions)) {
			$extensions = explode(',', $extensions);
		}
		foreach($extensions as $index => $ext) {
			$extensions[$index] = trim($ext, " \n\r\t\v\0'\"");
		}
		foreach ($ids as $id) {
			if (isset($pico->cdn[$id]) && !in_array($pico->cdn[$id]['extension'], $extensions)) {
				return false;
			}
		}
		return true;
	}
	

	static public function future($value) {
		return (self::after($value, date('Y-m-d')));
	}
	

	static public function integer($value) {
		return (is_numeric(str_replace(array(',','.'), array('_'), $value)));
	}


	static public function images($value) {
		return (self::extensions($value, array_merge(Thumbnail::$driverExtensions, ['svg'])));
	}


	static public function length($value, $len) {
		return (strlen($value)==$len);
	}
	

	static public function longer($value, $len) {
		return (strlen($value)>$len);
	}


	static public function max($value, $max) {
		return (floatval($value)<=floatval($max));
	}
	

	static public function min($value, $min) {
		return (floatval($value)>=floatval($min));	
	}


	static public function none($value, $params=NULL) {
		global $pico;
		if (!is_array($params) || count($params)<3) return false;
		if ($pico->db->exists($params[0], '('.$params[1].'="'.$params[2].'")')) {
			return false;
		} else {
			return true;
		}
	}
	
	
	static public function notparent($value, $params=NULL) {
		global $pico;
		if (!is_array($params) || count($params)<2) {
			return true;
		}
		if (!isset($params[2])) {
			$params[2] = 'parent';
		}
		if ($pico->db->exists($params[0], '`'.$params[2].'`='.intval($params[1]))) {
			return false;
		} else {
			return true;
		}
	}

	
	static public function number($value) {
		return (is_numeric(str_replace(",", ".", $value)));
	}


	static public function float($value) {
		return (is_numeric(str_replace(",", ".", $value)));
	}

	
	static public function parseRule($rule, $values) {
		if (is_array($values)) {
			$keys = array_keys($values);
			usort($keys, function($a, $b) {
				return strlen($b) - strlen($a);
			});
			foreach($keys as $key) {
				$rule = str_replace('$'.$key, $values[$key], $rule);
			}
		}
		return $rule;
	}


	static public function password($value) {
		return strlen($value)>5;
	}
	

	static public function past($value) {
		return (self::before($value, date('Y-m-d')));
	}
	

	static public function references($value, $data) {
		global $pico;
		return $pico->db->exists(trim($data[0]), '`'.trim($data[1]).'`="'.$value.'"');
	}
	

	static public function required($value) {
		if (is_array($value)) {
			return (!empty($value));
		} else {
			return (!is_null($value) && (trim($value)!=''));
		}
	}


	static public function requiredif($value, $params=NULL) {
		if (!is_array($params) && !isset($params[1])) return false;
		if (isset($params[1]) && !isset($params[0])) $params[0] = NULL;
		if (in_array($params[0], array_slice($params, 1))) {
			return self::required($value);
		} else {
			return true;
		}
	}
	
	
	static public function requiredifnot($value, $params=NULL) {
		if (!is_array($params) && !isset($params[1])) return false;
		if (isset($params[1]) && !isset($params[0])) $params[0] = NULL;
		if (in_array($params[0], array_slice($params, 1))) {
			return true;
		} else {
			return self::required($value);
		}
	}


	static public function requiredoptions($value, $params=NULL) {
		if (!is_array($value)) {
			$value = explode(',', $value);
		}
		if (!is_array($params)) {
			$params = explode(',', $params);
		}
		foreach($params as $p) {
			if (!in_array($p, $value)) {
				return false;
			}
		}
		return true;
	}


	static public function same($value, $original) {
		return ($value==$original);
	}


	static public function shorter($value, $len) {
		return (strlen($value)<$len);
	}

	
	static public function time($value) {
		if (!(strpos($value, ":")==strrpos($value, ":"))) {
			list($time_hour, $time_min, $time_sec) = explode(":", $value, 3);
			if (isset($time_hour) && isset($time_min) && isset($time_sec)) {
				if ((intval($time_min)<0) || (intval($time_min)>59)) return false;
				if ((intval($time_sec)<0) || (intval($time_sec)>59)) return false;
				return true;
			} 
		}
		return false;
	}


	static public function unique($value, $params) {
		global $pico;
		/* 
			Params are : table,field,$id
			Ex.: unique(partners,email,$id) checks if there is not a row in 'partners' table 
			with 'email' field of $value. Optional $id ignore if it's the same id (update).
		*/
		if (is_array($params) && (count($params)>1)) {
			$condition = '`'.$params[1].'`="'.safer($value, 'db').'"';
			if (isset($params[2])) {
				$condition.= ' AND (`id`<>'.intval($params[2]).')';
			}
			if (!$pico->db->exists($params[0], $condition)) {
				return true;
			}
		}
		return false;
	}
	

	static public function url($value) {
		return (substr($value, 0, 1)=='/') || (preg_match('/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:.[A-Z0-9][A-Z0-9_-]*)+):?(d+)?\/?/i', $value)>0);
	}


	static public function vatnumber($value) {
		global $pico;
		$value = str_replace(array(' ', '.', '-', '/'), '', $value);
		$vat_no = substr($value, 2);
		$state_code = strtoupper(substr($value, 0, 2));
		try {
			$soap = new SoapClient("http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl");
			$check = $soap->checkVat(array(
				'countryCode' => $state_code,
				'vatNumber' => $vat_no
			));
			if (isset($check->valid)) {
				return $check->valid;
			} else {
				return false;
			}
		} catch (\Throwable $err) {
			return false;
		}
	}


}