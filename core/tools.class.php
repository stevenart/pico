<?php

class Tools {


	public static function apiCall($url, $returnType='raw', $params=NULL, $post=false, $headers=NULL, $timeouts='long') {
		$curlObject = curl_init(); 
		curl_setopt($curlObject, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($curlObject, CURLOPT_FORBID_REUSE, true); 
		curl_setopt($curlObject, CURLOPT_SSL_VERIFYPEER, false);
		if (is_array($timeouts) && (count($timeouts)==2)) {
			curl_setopt($curlObject, CURLOPT_CONNECTTIMEOUT, $timeouts[0]);
			curl_setopt($curlObject, CURLOPT_TIMEOUT, $timeouts[1]);
		} elseif ($timeouts=='short') {
			curl_setopt($curlObject, CURLOPT_CONNECTTIMEOUT, 1);
			curl_setopt($curlObject, CURLOPT_TIMEOUT, 2);
		} elseif ($timeouts=='long') {
			curl_setopt($curlObject, CURLOPT_CONNECTTIMEOUT, 30);
			curl_setopt($curlObject, CURLOPT_TIMEOUT, 60);
		}
		if (is_array($headers)) {
			curl_setopt($curlObject, CURLOPT_HTTPHEADER, $headers);
		}
		if (is_array($params)) {
			$params = Tools::paramsArrayToString($params, false);
			if ($post) {
				curl_setopt($curlObject, CURLOPT_POST, 1);
				curl_setopt($curlObject, CURLOPT_POSTFIELDS, $params); 	
			} else {
				$url = $url.(strpos($url, '?') ? '&' : '?').$params;
			}
		}
		curl_setopt($curlObject, CURLOPT_URL, $url);
		$return = array();
		$return['result'] = curl_exec($curlObject);
		switch($returnType) {
			case 'json':
				$return['result'] = json_decode($return['result'], true);
				break;
		}
		$return['info'] = curl_getinfo($curlObject);
		$return['code'] = curl_getinfo($curlObject, CURLINFO_HTTP_CODE);
		curl_close($curlObject);
		return $return;
	}


	public static function arrayInsertAfter($array, $after, $newKey, $newValue) {
		$result = array();
		foreach($array as $key => $value) {
			$result[$key] = $value;
			if ($key==$after) {
				$result[$newKey] = $newValue;
			}
		}
		return $result;
	}


	public static function arrayInsertBefore($array, $before, $newKey, $newValue) {
		$result = array();
		foreach($array as $key => $value) {
			if ($key==$before) {
				$result[$newKey] = $newValue;
			}
			$result[$key] = $value;
		}
		return $result;
	}


	public static function arrayMergeConfig(array $array1, array $array2) {
		foreach($array2 as $key => $value) {
			if (array_key_exists($key, $array1) && is_array($value)) {
				if (is_array($array1[$key])) {
					$array1[$key] = Tools::arrayMergeConfig($array1[$key], $array2[$key]);
				} else {
					$array1[$key] = $array2[$key];
				}
			} else {
				$array1[$key] = $value;
			}
		}
		return $array1;
	}
	

	/**
	 * Sorts an array.
	 *
	 *		$example = array(
	 *			'12'=>array('name'=>array('firstname'=>'Zoiberg')),
	 *			'13'=>array('name'=>array('firstname'=>'Fry'))
	 *		);
	 *		arraySort($example, array('name','firstname'));
	 *		print_r($example);
	 *
	 *		> Array (
	 *		> 	[13] => Array ( [name] => Array ( [firstname] => Fry ) )
	 *		> 	[12] => Array ( [name] => Array ( [firstname] => Zoiberg ) )
	 *		> )
	 *
	 * @access public
	 * @param array $array Reference to array to sort.
	 * @param mixed $multiKey Key or array path if you need to sort on some child element (optional).
	 * @param bool $asc True for asc, false for desc.
	 * @param $orderFunction Callable to use for comparaison (default: strcasecmp).
	 * @return void
	 */
	public static function arraySort(&$array, $multiKey=NULL, $asc=true, $orderFunction='strnatcmp') {
		if (!is_null($multiKey)) {
			if (is_int($multiKey)) {
				if (is_array($multiKey)) $multiKey = implode('][', $multiKey);
				$multiKey = '['.$multiKey.']';
			} else {
				if (is_array($multiKey)) $multiKey = implode('"]["', $multiKey);
				$multiKey = '["'.$multiKey.'"]';
			}
		} else {
			$multiKey = '';
		}
		if (!is_string($orderFunction) || !is_callable($orderFunction)) {
			return false;
		}
		uasort($array,
			function($a, $b) use ($asc, $orderFunction, $multiKey) {
				if ($multiKey) {
					$a = eval('return $a'.$multiKey.';');
					$b = eval('return $b'.$multiKey.';');
				}
				return ($asc ? 1 : -1) * call_user_func($orderFunction, $a, $b);
			}
		);
		return true;
	}
	

	public static function computerFriendly($value, $case='lower', $maxlength=NULL, $allow='[^0-9a-zA-Z_\-\.]') {
		$before = array('\'', '"', '`', '«', '»', '‘', '’', '“', '”', '‚', '„', '‛', '‟', '′', '″', '`', '´', ' ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Ā', 'Ą', 'Ă', 'Æ', 'Ç', 'Ć', 'Č', 'Ĉ', 'Ċ', 'Ď', 'Đ', 'È', 'É', 'Ê', 'Ë', 'Ē', 'Ě', 'Ĕ', 'Ė', 'Ĝ', 'Ğ', 'Ġ', 'Ģ', 'Ĥ', 'Ħ', 'Ì', 'Í', 'Î', 'Ï', 'Ī', 'Ĩ', 'Ĭ', 'Į', 'İ', 'Ĳ', 'Ĵ', 'Ķ', 'Ł', 'Ľ', 'Ĺ', 'Ļ', 'Ŀ', 'Ñ', 'Ń', 'Ň', 'Ņ', 'Ŋ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ō', 'Ő', 'Ŏ', 'Œ', 'Ŕ', 'Ř', 'Ŗ', 'Ś', 'Š', 'Ş', 'Ŝ', 'Ș', 'Ť', 'Ţ', 'Ŧ', 'Ț', 'Ù', 'Ú', 'Û', 'Ü', 'Ū', 'Ů', 'Ű', 'Ŭ', 'Ũ', 'Ų', 'Ŵ', 'Ý', 'Ŷ', 'Ÿ', 'Ź', 'Ż', 'á', 'à', 'â', 'ä', 'ã', 'å', 'ā', 'ă', 'æ', 'ç', 'ć', 'č', 'ĉ', 'ċ', 'ď', 'đ', 'è', 'é', 'ê', 'ë', 'ē', 'ę', 'ě', 'ĕ', 'ė', 'ƒ', 'ĝ', 'ğ', 'ġ', 'ģ', 'ĥ', 'ħ', 'ì', 'í', 'î', 'ï', 'ī', 'ĩ', 'ĭ', 'į', 'ı', 'ĳ', 'ĵ', 'ķ', 'ĸ', 'ł', 'ľ', 'ĺ', 'ļ', 'ŀ', 'ñ', 'ń', 'ň', 'ņ', 'ŉ', 'ŋ', 'ò', 'ô', 'õ', 'ö', 'ø', 'ō', 'ő', 'œ', 'ŕ', 'ř', 'ŗ', 'ś', 'š', 'ş', 'ŝ', 'ș', 'ť', 'ţ', 'ŧ', 'ț', 'ù', 'ú', 'û', 'ü', 'ū', 'ů', 'ű', 'ŭ', 'ų', 'ŵ', 'ý', 'ÿ', 'ŷ', 'ž', 'ż', 'ź');
		$after = array('-' , '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '_', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'C', 'C', 'C', 'C', 'D', 'D', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'G', 'G', 'G', 'G', 'H', 'H', 'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I', 'I', 'J', 'K', 'L', 'L', 'L', 'L', 'L', 'N', 'N', 'N', 'N', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'R', 'R', 'R', 'S', 'S', 'S', 'S', 'S', 'T', 'T', 'T', 'T', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'U', 'W', 'Y', 'Y', 'Y', 'Z', 'Z', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'c', 'c', 'c', 'c', 'd', 'd', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'f', 'g', 'g', 'g', 'g', 'h', 'h', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'j', 'j', 'k', 'k', 'l', 'l', 'l', 'l', 'l', 'n', 'n', 'n', 'n', 'n', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'r', 'r', 'r', 's', 's', 's', 's', 's', 't', 't', 't', 't', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'w', 'y', 'y', 'y', 'z', 'z', 'z');
    	$clean = str_replace($before, $after, $value);	
		$clean = preg_replace('/'.$allow.'/u', '', $clean);
		if (is_string($case)) {
			if ($case=='upper') $clean = mb_convert_case($clean, MB_CASE_UPPER);
			elseif ($case=='lower') $clean = mb_convert_case($clean, MB_CASE_LOWER);
			elseif ($case=='ucfirst') $clean = mb_convert_case($clean, MB_CASE_TITLE);
		}
		if (!is_null($maxlength) && (mb_strlen($clean)>intval($maxlength))) {
			$clean = mb_substr($clean, 0, intval($maxlength));
		}
		return $clean;
	}		


	public static function generateSecret($length=8, $strength=1, $prefix='', $postfix='') {
		$chars = '1234567890';
		$secret = '';
		if ($strength>0) $chars.= 'abcdefghijklmnopqrstuvwxyz';
		if ($strength>1) $chars.= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		if ($strength>2) $chars.= '#@&(!)_$%?';
		while(($length--)>0) $secret.=$chars[rand(0,(strlen($chars)-1))];
		return $prefix.$secret.$postfix;
	}


	public static function generateURI($value) {
		if (is_array($value)) $value = implode('-', $value);
		$uri = str_replace('--', '-', trim(str_replace(array('_','.'), '-', Tools::computerFriendly($value, 'lower', 250))));
		if (substr($uri, -1)=='-') $uri = substr($uri, 0, -1);
		return $uri;
	}


	public static function getUploadMaxSize() {
		$upload = Tools::toBytes(ini_get('upload_max_filesize'));
		$post = Tools::toBytes(ini_get('post_max_size'));
		return ($upload<$post) ? $upload : $post;
	}


	public static function paramsArrayToString($params, $includeFirst=true) {
		if ( count($params)==0 ) return '';
		$url = ($includeFirst ? '?' : '');
		foreach($params as $name => $value) {
			$url.= urlencode($name) . '=' . urlencode($value) . '&';
		}
		return substr($url, 0, -1);
	}


	public static function paramsStringToArray($url) {
		if (in_array( substr($url,0,1), array('?','#','&') )) $url = substr($url,1);
		if (!$url) return array();
		$params = explode('&', $url);
		$cleanParams = array();
		foreach($params as $param) {
			if (strpos($param, '=')) {
				list($name,$value) = explode('=', $param);
				$cleanParams[urldecode($name)] = urldecode($value);
			} else {
				$cleanParams[urldecode($param)] = 1;
			}
		}
		return $cleanParams;
	}


	public static function spamScore(string $text, string &$details, bool $cleanText=true) {
		global $pico;
		$spamScore = 0;
		$details = "";

		// Cleaning text
		$cleanedText = $text;
		if (stripos($cleanedText, '</body>')!==false) {
			preg_match('/<body [^>]*>(.*?)<\/body>/s', $cleanedText, $match);
			if (isset($match[1])) {
				$cleanedText = $match[1];
			}
		}
		$cleanedText = strip_tags($cleanedText, '<a>');
		$cleanedText = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $cleanedText);
		$cleanedText = str_replace(array("\n", "\r", "\t"), array(' ', ' ', ''), $cleanedText);
		$plainText = strip_tags($cleanedText);
		if ($cleanText) {
			$text = $cleanedText;
		}

		// Language
		if (strlen(str_replace(' ', '', $plainText))>300) {
			$languageDetector = new LanguageDetector\LanguageDetector();
			$language = $languageDetector->evaluate($plainText)->getLanguage();
			if (in_array($language, $pico->i18n->langs(NULL, false))) {
				if ($language == 'en') {
					$spamScore+= 2; 
					$details.= "Language=en [+2]\n";
				} else {
					$spamScore+= 0; 
					$details.= "Language=$language [+0]\n";
				}
			} else {
				$spamScore+= 4; 
				$details.= "Language=$language (not website language) [+4]\n";
			}
		} else {
			$spamScore+= 2;
			$details.= "Language not detectable [+2]\n";
		}

		// URL detection
		$text = str_replace($pico->href(';'), '', $text);
		$urlPattern = '/\b(?:https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|]/i';
		if (preg_match($urlPattern, $text)) {
			$spamScore+= 2;
			$details.= "URL present [+2]\n";
		}
		$urlDomains = ['bit.ly', 'bour.so'];
		foreach($urlDomains as $urlDomain) {
			if (stripos($text, $urlDomain)!==false) {
				$spamScore+= 2;
				$details.= "Blacklisted domain $urlDomain present [+2]\n";
			}
		}

		// Spamlist
		if (file_exists(__ROOT.'/core/spamlist.txt')) {
			$spamList = file(__ROOT.'/core/spamlist.txt');
			foreach($spamList as $spamRegex) {
				// Remove comments and whitespace before and after a keyword
				$spamRegex = preg_replace('/(^\s+|\s+$|\s*#.*$)/i', "", $spamRegex);
				if (empty($spamRegex)) continue;
				// Check if match
				try {
					if (preg_match("/$spamRegex/i", $text)) {
						$spamScore+= 1;
						$details.= "Regex match $spamRegex [+1]\n";
					}
				}
				catch (\Throwable $e) {
					// Ignore this (probably invalid) regex
				}
			}
		}
		return $spamScore;
	}
	

	public static function toBytes($value) {
		$value = trim($value);
		$last = strtolower($value[strlen($value)-1]);
		if (!is_numeric($last)) {
			$value = substr($value, 0, -1);
		}
		switch($last) {
			case 'g':
				$value *= 1024;
			case 'm':
				$value *= 1024;
			case 'k':
				$value *= 1024;
		}
		return $value;
	}
	

}