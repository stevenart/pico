<?php

class EmailsCMSPlugin extends CMSPlugin {


	static public function getPluginConfiguration() {
		global $pico;
		$tabs = array();
		foreach($pico->db->column('emails', 'DISTINCT(DATE_FORMAT(`__created`,"%Y-%m"))', NULL, '`__created` DESC') as $month) {
			$tabs['m'.$month] = array(
				'condition' => '`spam`=0 AND `__created` LIKE "'.$month.'-%"',
				'id' 		=> 'm'.$month,
				'text' 		=> date('m/Y', strtotime($month.'-01')),
			);
		} 
		$tabs['spam'] = array(
			'condition' => '`spam`=1',
			'id' 		=> 'spam',
			'text' 		=> '— '.i18n('EMAIL.FIELD_SPAM'),
		);
		$config = Tools::arrayMergeConfig(parent::getPluginConfiguration(), array(
			'actions'		=> array(
				'index'	=> array(
					'order' => '`__timestamp` DESC',
					'tabs' => $tabs,
				),
				'config'	=> array(
					'form'		=> array(
						'actions'	=> array(
							'test' => array('type'=>'link','href'=>'#','text'=>i18n('CMS.ACTION_EMAIL_TEST'),'css'=>'button isLeft isCreate'),
							'cancel' => array('type'=>'link','href'=>'_index','text'=>i18n('PICO.ACTION_CANCEL'),'css'=>'button'),
							'submit' => array('type'=>'submit', 'text'=>i18n('CMS.ACTION_SAVE'), 'css'=>'isUpdate isPrimary'),
						),
						'css'		=> 'labels-left'
					),
					'icon' 		=> 'gear',
					'text' 		=> i18n('CMS.ACTION_CONFIG'),
					'title' 	=> i18n('CMS.ACTION_CONFIG'),
				),
				'spam'	=> array(
					'css' 		=> 'isWarning',
					'icon' 		=> 'thumbs-down',
					'item' 		=> true,
					'text' 		=> i18n('CMS.ACTION_SPAM'),
				),
				'unspam'	=> array(
					'css' 		=> 'isCreate',
					'icon' 		=> 'thumbs-up',
					'item' 		=> true,
					'text' 		=> i18n('CMS.ACTION_UNSPAM'),
				),
			),
			'defaultAction' => 'read',
			'icon'			=> 'mail',
			'model' 		=> 'Email',
			'title'			=> i18n('CMS.LABEL_PLUGIN_EMAILS'),
			'type'			=> 'core',
			'view'			=> 'cms/emails.twig',
		));
		unset($config['actions']['create']);
		unset($config['actions']['delete']);
		unset($config['actions']['duplicate']);
		unset($config['actions']['update']);
		if (method_exists('Hooks', 'cmsPluginConfiguration')) {
			Hooks::cmsPluginConfiguration('emails', $config);
		}
		return $config;
	}


	public function ajax() {
		global $pico;
		if ($this->path(2) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])=='xmlhttprequest') {
			if ($pico->debugBar) {
				$pico->debugBar['messages']->addMessage($_SERVER['REQUEST_URI'] ?? '?', 'request');
				$pico->debugBar->sendDataInHeaders();
			}
			$result = ['success' => false];
			switch ($this->path(2)) {
				
				case 'test':	
					$config = Model::factory('EmailConfig');
					$canTest = $config->loadForm();
					if ($canTest===true) {
						if (isset($_POST['test_email']) && Check::email($_POST['test_email'])) {
							$config->value('toEmail', 0, $_POST['test_email']);
						}
						$mail = new Mail($config->one());
						$subject = i18n('CMS.TITLE_EMAIL_TEST');
						$mail->setSubject($subject);
						$mail->setBodyTemplate('emails/test.twig', array(
							'config' => $config->one(['method', 'fromEmail', 'toEmail']),
							'fields' => $config->getFields(),
							'signature' => false,
							'subject' => $subject,
						));
						$result['emailId'] = $mail->send(antiSpam: false, returnEmailId: true, debug: true);
						$email = Model::factory('Email');
						if ($email->fetchPrimary(intval($result['emailId']))) {
							$result['sent'] = $email->value('sent');
							$result['errorMessage'] = $email->value('errorMessage');
							$result['debugLog'] = $mail->debugLog();
							$result['success'] = true;
						} else {
							$result['error'] = 'unknown';
						}
					} else {
						$result['error'] = 'invalid';
						$result['errorFields'] = array_keys($canTest);
					}
					if (isset($result['error'])) {
						$result['errormsg'] = i18n('CMS.MESSAGE_EMAIL_TEST_ERROR_'.$result['error']);
					}
					break;

				default:
					parent::ajax();
					break;

			}
			echo json_encode($result);
			die();
		} else {
			$pico->url(':');
		}
	}


	public function config() {
		global $pico;
		$c = static::getPluginConfiguration();
		if (isset($c['actions'][$this->path(1)])) {
			$config = Model::factory('EmailConfig');
			$canSave = $config->loadForm();
			if ($canSave===true) {
				$config->save();
				$pico->url('_index');
			} elseif ($canSave===false) {
				$config->fetchCurrent();
			}
			if (!isset($c['actions'][$this->path(1)]['form'])) {
				$c['actions'][$this->path(1)]['form'] = array();
			}
			if ($pico->config['mail']['replace_all_email']) {
				$this->variable('forceTestEMail', $pico->config['mail']['replace_all_email']);
			}
			$c['actions'][$this->path(1)]['form']['errors'] = $canSave;
			$this->variable('form', $config->form($c['actions'][$this->path(1)]['form']));
		} else {
			$pico->url('_index');
		}
	}


	public function index() {
		global $pico;
		$config = Model::factory('EmailConfig');
		if ($config->fetchCurrent()) {
			$this->variable('emailconfig', $config->one());
		}
		parent::index();
	}


	public function preview() {
		global $pico;
		$c = static::getPluginConfiguration();
		if (isset($c['actions'][$this->path(1)])) {
			$model = Model::factory($c['model']);
			if ($this->path(2) && $model->fetchPrimary($this->path(2))) {
				echo $model->value('message');
				die();
			}
		} 
		$pico->url('_index');
	}


	public function spam() {
		global $pico;
		$c = static::getPluginConfiguration();
		if (isset($c['actions'][$this->path(1)])) {
			$model = Model::factory($c['model']);
			if ($this->path(2) && $model->fetchPrimary($this->path(2))) {
				$model->value('spam', 0, 1);
				$model->save($this->path(2));
				$pico->url('_index');
			} else {
				$pico->url('_index');
			}
		} else {
			$pico->url('_index');
		}
	}


	public function unspam() {
		global $pico;
		$c = static::getPluginConfiguration();
		if (isset($c['actions'][$this->path(1)])) {
			$model = Model::factory($c['model']);
			if ($this->path(2) && $model->fetchPrimary($this->path(2))) {
				$model->value('spam', 0, 0);
				$model->save($this->path(2));
				$pico->url('_index');
			} else {
				$pico->url('_index');
			}
		} else {
			$pico->url('_index');
		}
	}


}