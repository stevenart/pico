<?php

class SubscribersCMSPlugin extends CMSPlugin {


	static public function getPluginConfiguration() {
		$config = Tools::arrayMergeConfig(parent::getPluginConfiguration(), array(
			'actions'		=> array(
				'create' => array(
					'form' => array(
						'fields' => array('secret'),
						'include' => false,
					),
				),
				'delete' => array(
					'form' => array(
						'fields' => array('secret'),
						'include' => false,
					),
				),
				'export' => array(
					'css' 		=> '',
					'icon' 		=> 'download',
					'item' 		=> false,
					'target'	=> '_blank',
					'text' 		=> i18n('CMS.ACTION_EXPORT'),
				),
				'import' => array(
					'css' 		=> '',
					'icon' 		=> 'upload',
					'item' 		=> false,
					'text' 		=> i18n('CMS.ACTION_IMPORT'),
				),
				'index'	=> array(
					'order' => '`email` ASC',
				),
				'update' => array(
					'form' => array(
						'fields' => array('secret', 'subscribed'),
						'include' => false,
					),
				),
			),
			'icon'			=> 'user-id',
			'model' 		=> 'Subscriber',
			'title'			=> i18n('CMS.LABEL_PLUGIN_SUBSCRIBERS'),
			'view'			=> 'cms/subscribers.twig',
		));
		unset($config['actions']['duplicate']);
		unset($config['actions']['preview']);
		return $config;
	}


	public function export() {
		global $pico;
		$data = $pico->db->all('subscribers', NULL, '`subscribed`=1', '`email` ASC');
		$langs = $pico->i18n->langs();
		$csv = 'EMAIL;NOM;';
		$csv.= "\n";
		foreach($data as $row) {
			$csv.= $row['email'].';'.$row['name'].';';
			$csv.= "\n";
		}
		$csv = str_replace("\n", "\r\n", $csv);
		header("Content-Encoding: UTF-8");
		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=mailinglist.csv");
		echo "\xEF\xBB\xBF";
		echo $csv;
		die();
	}


	public function import() {
		global $pico;

		$import = Model::factory('SubscribersImport');
		$canImport = $import->loadForm();
		if ($canImport===true) {
			$file = Model::factory('Cdn');
			$file->fetchPrimary($import->value('file'));
			if ($file->value('extension')=='csv') {
				$csv = file_get_contents(__ROOT.'/cdn/'.$file->value('id').'_'.$file->value('key').'.csv');
				$csv = explode("\n", str_replace("\r\n", "\n", $csv));
				$data = array();
				foreach($csv as $row) {
					$row = explode(';', $row);
					if (Check::email($row[0])) {
						$data[] = array(
							'email' => $row[0], 
							'name' => $row[1],
							'lang' => $import->value('lang'),
						);
					}
				}
				if (count($data)>0) {
					$subscriber = Model::factory('Subscriber');
					$subscriber->load($data, true);
					if ($subscriber->save()) {
						$this->variable('results', array(
							'uploaded' => count($csv),
							'saved' => count($data),
						));
					}
				}
			}
			$file->delete($import->value('file'));
		} else {
			$this->variable('form', $import->form(array(
				'actions'	=> array(
					'cancel' => array('type'=>'link','href'=>'_index','text'=>i18n('PICO.ACTION_CANCEL'),'css'=>'button'),
					'submit' => array('type'=>'submit', 'text'=>i18n('CMS.ACTION_IMPORT'), 'css'=>'add'),
				),
				'fields' 	=> array('file', 'lang'),
				'fieldsets' => false,
				'include'	=> true,
				'css'		=> 'labels-left'
			)));
		}
	}


}