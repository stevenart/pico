<?php

class EmailRulesCMSPlugin extends CMSPlugin {


	static public function getPluginConfiguration() {
		global $pico;
		$config = Tools::arrayMergeConfig(parent::getPluginConfiguration(), array(
			'actions'		=> array(
				'apply' => array(
					'css' 		=> 'warning',
					'icon' 		=> 'media-play',
					'item' 		=> true,
					'text' 		=> i18n('CMS.ACTION_EMAILRULE_APPLY'), 
					'title' 	=> i18n('CMS.TITLE_EMAILRULE_APPLY'), 
				),
				'index'	=> array(
					'order' => '`__timestamp` DESC',
					'sortable' => true,
				)
			),
			'defaultAction' => 'read',
			'icon'			=> 'network-1',
			'model' 		=> 'EmailRule',
			'parentPlugin'	=> 'emails',
			'title'			=> i18n('CMS.LABEL_PLUGIN_EMAILRULES'),
			'type'			=> 'core',
			'view'			=> 'cms/emailrules.twig',
		));
		unset($config['actions']['preview']);
		if (method_exists('Hooks', 'cmsPluginConfiguration')) {
			Hooks::cmsPluginConfiguration('emailrules', $config);
		}
		return $config;
	}


	public function apply() {
		global $pico;
		$c = static::getPluginConfiguration();
		if (isset($c['actions'][$this->path(1)])) {
			$model = Model::factory($c['model']);
			if ($this->path(2) && $model->exists($this->path(2))) {
				$canApply = $model->loadForm();
				if ($canApply===true) {
					$model->fetchPrimary($this->path(2));
					$model->applyAll();
					$this->variable('success', true);
				} else {
					$this->variable('form', $model->form(array(
						'actions'	=> array(
							'cancel' => array('type'=>'link','href'=>'_index','text'=>i18n('PICO.ACTION_CANCEL'),'css'=>'button'),
							'submit' => array('type'=>'submit', 'text'=>i18n('CMS.ACTION_EMAILRULE_APPLY'), 'css'=>'warning'),
						),
						'fields' 	=> array(),
						'fieldsets' => false,
						'include'	=> true,
					)));
					$model->fetchPrimary($this->path(2));
					$this->variable('title', $model->value('title'));
					$this->variable('count', $model->countAll(false));
				}
			} else {
				$pico->url('_index');
			}
		} else {
			$pico->url('_index');
		}
	}


}