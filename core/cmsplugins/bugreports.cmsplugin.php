<?php

class BugReportsCMSPlugin extends CMSPlugin {


	static public function getPluginConfiguration() {
		global $pico;

		/* Tabs */
		$statuses = $pico->db->pairs('bugreports', 'IFNULL(`parent`, `id`)', 'status', NULL, '`__created` ASC');
		$opens = array(-1);
		foreach( $statuses as $id => $status) {
			if ($status=='open') {
				$opens[] = $id;
			}
		}
		$tabs = array(
			'open' => array(
				'condition' => '`id` IN ('.implode(',', $opens).')',
				'id' => 'open',
				'text' => i18n('BUGREPORT.FIELD_STATUS_OPEN'),
			),
			'closed' => array(
				'condition' => '`id` NOT IN ('.implode(',', $opens).')',
				'id' => 'closed',
				'text' => i18n('BUGREPORT.FIELD_STATUS_CLOSED'),
			),
		);

		$config = Tools::arrayMergeConfig(parent::getPluginConfiguration(), array(
			'actions' 		=> array(
				'create' => array(
					'form' => array(
						'fields' => array('parent', 'author'),
						'fieldsets' => false,
						'include' => false,
					),
				),
				'index' => array(
					'condition'	=> '`parent` IS NULL',
					'order'	=> '`__timestamp` DESC',
					'tabs' => $tabs,
				),
				'update' => array(
					'form' => array(
						'fields' => array('parent', 'author', 'title'),
						'include' => false,
					),
				),
			),
			'defaultAction' => 'read',
			'icon'			=> 'pill',
			'model' 		=> 'BugReport',
			'title'			=> i18n('CMS.LABEL_PLUGIN_BUGREPORTS'),
			'type'			=> 'core',
			'view'			=> 'cms/bugreports.twig',
		));
		unset($config['actions']['update']);
		unset($config['actions']['delete']);
		unset($config['actions']['preview']);
		if (method_exists('Hooks', 'cmsPluginConfiguration')) {
			Hooks::cmsPluginConfiguration('bugreports', $config);
		}
		return $config;
	}


	public function read() {
		global $pico;
		$c = static::getPluginConfiguration();
		if (isset($c['actions'][$this->path(1)])) {
			$model = Model::factory($c['model']);
			if (!is_null($this->path(2)) && $model->fetchPrimary($this->path(2))) {
				$this->variable('fields', $model->getFields());
				$status = $model->value('status');

				/* Parent */
				$reports = array();
				$parent = $model->one(NULL, array(
					'__user' => array('cmsusers', 'id'),
					'attachments' => array('cdn', 'id'),
				));
				$reports[] = $parent;

				/* Children */
				$model->clear();
				$model->fetch('`parent`='.$parent['id'], '`__created` ASC');
				$reports = array_merge($reports, $model->values(NULL, array(
					'__user' => array('cmsusers', 'id'),
					'attachments' => array('cdn', 'id'),
				)));

				$this->variable('reports', $reports);
				$this->variable('status', $reports[count($reports)-1]['status']);

				/* Form */
				$newReport = Model::factory($c['model']);
				$canSave = $newReport->loadForm();
				if ($canSave===true) {
					if (
						$newReport->value('message') || 
						$newReport->value('attachments') || 
						($newReport->value('status')!=$parent['status'])
					) {
						$newReport->value('parent', 0, $parent['id']);
						$newReport->value('title', 0, $parent['title']);
						$newReport->save();
						/* Updating __timestamp from parent */
						$model->fetchPrimary($parent['id']);
						$model->save($parent['id']);
						$pico->url('~');
					}
				}
				unset($c['actions']['create']['form']['actions']['cancel']);
				$c['actions']['create']['form']['actions']['submit']['text'] = i18n('CMS.ACTION_SEND');
				$c['actions']['create']['form']['errors'] = $canSave;
				$c['actions']['create']['form']['fields'] = array_merge(
					$c['actions']['create']['form']['fields'],
					array('title')
				);
				$this->variable('form', $newReport->form($c['actions']['create']['form']));

				
			} else {
				$pico->url('_index');
			}
		} else {
			$pico->url('_index');
		}
	}



}