<?php

class CmsusersCMSPlugin extends CMSPlugin {


	static public function getPluginConfiguration() {
		global $pico;
		$config = Tools::arrayMergeConfig(parent::getPluginConfiguration(), array(
			'actions' 		=> array(
				'create' => array(
					'form' => array(
						'fields' => array('password', 'secret', 'code', 'logged'),
						'include' => false,
					),
				),
				'index' => array(
					'order' => '`name` ASC',
				),
				'read'	 => array(
					'fields' => array('password', 'secret', 'code'),
					'include' => false,
				),
				'update' => array(
					'form' => array(
						'fields' => array('password', 'secret', 'code', 'logged'),
						'include' => false,
					),
				)
			),
			'icon'			=> 'user-group',
			'model' 		=> 'CMSUser',
			'title'			=> i18n('CMS.LABEL_PLUGIN_CMSUSERS'),
			'type'			=> 'core',
			'view'			=> 'cms/cmsusers.twig',
		));
		if ($pico->config['cms']['root_user_domain']) {
			if (isset($config['actions']['index']['condition'])) {
				$config['actions']['index']['condition'].= ' AND ';
			} else {
				$config['actions']['index']['condition'] = '';
			}
			$config['actions']['index']['condition'].= '(`email` NOT LIKE "%@'.safer($pico->config['cms']['root_user_domain'], 'db').'")';
		}
		unset($config['actions']['duplicate']);
		unset($config['actions']['preview']);
		if (method_exists('Hooks', 'cmsPluginConfiguration')) {
			Hooks::cmsPluginConfiguration('cmsusers', $config);
		}
		return $config;
	}


}