<?php

class NewslettersCMSPlugin extends CMSPlugin {


	static public function getPluginConfiguration() {
		global $pico;

		/* Tabs */
		$tabs = [
			'_drafts' => [
				'condition' => '`dateSent` IS NULL',
				'id' => '_drafts',
				'text' => i18n('CMS.LABEL_NEWSLETTERS_DRAFTS'),
				'shortcuts' => ['preview', 'test', 'send']
			],
			'_sent' => [
				'condition' => '`dateSent` IS NOT NULL',
				'id' => '_sent',
				'text' => i18n('CMS.LABEL_NEWSLETTERS_SENT'),
				'defaultAction' => 'read',
				'shortcuts' => ['preview', 'test']
			]
		];
		$incomplete = $pico->db->column('newslettersents', 'DISTINCT `newsletter`', '`sent` IS NULL');
		if (count($incomplete)>0) {
			$tabs = ['_incomplete' => [
				'condition' => '`id` IN (SELECT DISTINCT `newsletter` FROM `newslettersents` WHERE `sent` IS NULL)',
				'id' => '_incomplete',
				'text' => i18n('CMS.LABEL_NEWSLETTERS_INCOMPLETE'),
				'shortcuts' => ['send'],
				'defaultAction' => 'send',
			]] + $tabs;
			$pico->session('newsletters_tab', '_incomplete');
		}

		return Tools::arrayMergeConfig(parent::getPluginConfiguration(), array(
			'actions' 		=> array(
				'index' => array(
					'order' => '`__timestamp` DESC',
					'tabs' => $tabs,
				),
				'create' => array(
					'form' => array(
						'fields' => array('dateSent'),
						'include' => false,
					)
				),
				'test' => array(
					'icon' 		=> 'crosshair',
					'item' 		=> true,
					'text' 		=> i18n('CMS.ACTION_TEST'),
					'url'		=> ':',
				),
				'update' => array(
					'form' => array(
						'fields' => array('dateSent'),
						'include' => false,
					)
				),
				'send' => array(
					'icon' 		=> 'rocket',
					'item' 		=> true,
					'text' 		=> i18n('CMS.ACTION_SEND'),
					'url'		=> ':',
				),
			),
			'icon'			=> 'pamphlet',
			'model' 		=> 'Newsletter',
			'shortcuts'		=> [],
			'title'			=> i18n('CMS.LABEL_PLUGIN_NEWSLETTERS'),
			'view'			=> 'cms/newsletters.twig',
		));
	}


	public function ajax() {
		global $pico;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])=='xmlhttprequest') {
			if ($pico->debugBar) {
				$pico->debugBar['messages']->addMessage($_SERVER['REQUEST_URI'] ?? '?', 'request');
				$pico->debugBar->sendDataInHeaders();
			}
			$c = static::getPluginConfiguration();
			$result = array();
			switch ($this->path(2)) {
				
				case 'sendnext':	
					if ($this->path(3)) {
						$model = Model::factory($c['model']);
						if ($model->fetchPrimary($this->path(3))) {
							$result['present'] = true;
							$next = Model::factory('NewsletterSent');
							if ($next->fetch('`sent` IS NULL AND `newsletter`='.$model->value('id'), NULL, 1)) {
								$mail = new Mail();
								$mail->addTo(array(
									'name'=>$next->value('name'),
									'email'=>$next->value('email'),
								));
								$mail->setSubject($model->value('title'));
								$mail->setBodyTemplate('emails/newsletters_'.$model->value('template').'.twig', array(
									'subject' => $model->value('title'),
									'content' => $model->renderContent($next->one()),
									'sending' => $next->one(),
									'unsubscribe' => $pico->href(':subscription/unsubscribe?id='.$next->value('subscriber').'&key='.$next->value('secret')),
								));
								if ($mail->send()) {
									$next->value('sent', 0, date('Y-m-d H:i:s'));
									$result['success'] = $next->save($next->value('id'));
								}
								$result['done'] = false;
							} else {
								$result['success'] = true;
								$result['done'] = true;
							}
							/* Percentage */
							$totalcount = $next->count('`newsletter`='.$model->value('id'));
							$totalsent = $next->count('`sent` IS NOT NULL AND `newsletter`='.$model->value('id'));
							$result['percent'] = round(($totalsent/$totalcount)*100);
						}
					}
					break;

				default:
					parent::ajax();
					break;
			}
			echo json_encode($result);
			die();
		}
		$pico->url('_');
	}


	public function delete() {
		global $pico;
		$c = static::getPluginConfiguration();
		if (isset($c['actions'][$this->path(1)])) {
			$model = Model::factory($c['model']);
			if ($model->fetchPrimary($this->path(2)) && ($model->value('dateSent'))) {
				$this->variable('errorCantUpdateOrDelete', true);
			} else {
				parent::delete();
			}
		}	
	}


	public function index() {
		global $pico;
		$incomplete = $pico->db->column('newslettersents', 'DISTINCT `newsletter`', '`sent` IS NULL');
		$this->variable('incomplete', $incomplete);
		parent::index();
	}


	public function preview() {
		global $pico;
		$c = static::getPluginConfiguration();
		if (isset($c['actions'][$this->path(1)])) {
			$model = Model::factory($c['model']);
			if (!is_null($this->path(2)) && $model->exists($this->path(2))) {
				$model->fetchPrimary($this->path(2));
				if ($model->value('template')) {
					$sending = [
						'newsletter' => $model->value('id'), 
						'name' => 'John Doe', 
						'subscriber' => 0, 
						'secret' => 'test',
					];
					$html = $pico->twig->render('emails/newsletters_'.$model->value('template').'.twig', array(
						'subject' => $model->value('title'),
						'content' => $model->renderContent($sending),
						'sending' => $sending,
						'unsubscribe' => $pico->href(':subscription/unsubscribe?id='.$sending['subscriber'].'&key='.$sending['secret']),
					));
					echo $html;
					die();
				}
			}
		}
		$pico->url('_index');
	}


	public function send() {
		global $pico;
		$c = static::getPluginConfiguration();
		if (isset($c['actions'][$this->path(1)])) {
			$model = Model::factory($c['model']);
			if (!is_null($this->path(2)) && $model->exists($this->path(2))) {
				$model->fetchPrimary($this->path(2));
				$this->variable('newsletter', $model->one());
				if ($model->value('dateSent')) {
					$this->variable('sent', $model->value('dateSent'));
					$this->variable('sending', $pico->db->exists('newslettersents', '`sent` IS NULL AND `newsletter`='.$model->value('id')));
				} else {
					$tokens = $pico->session('tokens');
					$subscribers = $pico->db->all('subscribers', NULL, '`lang`="'.$model->value('lang').'" AND `subscribed`=1');
					$this->variable('subscribers_count', count($subscribers));
					if (isset($_POST['newsletter_confirm_present']) && ($_POST['newsletter_confirm_present']==$tokens['form'])) {
						$sendings = array();
						foreach($subscribers as $foo) {
							$sendings[] = array(
								'newsletter' => $model->value('id'),
								'name' => $foo['name'],
								'email' => $foo['email'],
								'subscriber' => $foo['id'],
								'secret' => $foo['secret'],
							);
						}
						$sent = Model::factory('NewsletterSent');
						$sent->load($sendings, true);
						if ($sent->save()) {
							$model->value('dateSent', 0, date('Y-m-d H:i:s'));
							$model->save($model->value('id'));
							$this->variable('sending', true);
						} 
					} 
				}
			} else {
				$pico->url('_index');
			}
		} else {
			$pico->url('_index');
		}
	}


	public function test() {
		global $pico;
		$c = static::getPluginConfiguration();
		if (isset($c['actions'][$this->path(1)])) {
			$model = Model::factory($c['model']);
			if (!is_null($this->path(2)) && $model->exists($this->path(2))) {
				$model->fetchPrimary($this->path(2));
				$test = Model::factory('NewsletterTest');
				/* Existsing tests */
				$test->fetch('`newsletter`='.$model->value('id'), '`__created` DESC');
				$this->variable('previous_tests', $test->values());
				$test->clear();
				/* New test */
				$canTest = $test->loadForm();
				if ($canTest===true) {
					$test->value('newsletter', 0, $model->value('id'));
					$mail = new Mail();
					$mail->addTo($test->value('email'));
					$mail->setSubject('[TEST] '.$model->value('title'));
					$sending = [
						'newsletter' => $model->value('id'), 
						'name' => 'John Doe', 
						'subscriber' => 0, 
						'secret' => 'test',
					];
					$mail->setBodyTemplate('emails/newsletters_'.$model->value('template').'.twig', array(
						'subject' => $model->value('title'),
						'content' => $model->renderContent($sending),
						'sending' => $sending,
						'unsubscribe' => $pico->href(':subscription/unsubscribe?id='.$sending['subscriber'].'&key='.$sending['secret']),
					));
					if ($mail->send()) {
						$this->variable('success', $test->save());
					}
				} else {
					$this->variable('form', $test->form(array(
						'actions'	=> array(
							'cancel' => array('type'=>'link','href'=>'_index','text'=>i18n('PICO.ACTION_CANCEL'),'css'=>'button'),
							'submit' => array('type'=>'submit', 'text'=>i18n('CMS.ACTION_TEST'), 'css'=>'add'),
						),
						'errors' 	=> $canTest,
						'fields' 	=> array('email'),
						'fieldsets' => false,
						'include'	=> true,
						'css'		=> 'labels-left'
					)));
				}

			} else {
				$pico->url('_index');
			}
		} else {
			$pico->url('_index');
		}
	}


	public function update() {
		global $pico;
		$c = static::getPluginConfiguration();
		if (isset($c['actions'][$this->path(1)])) {
			$model = Model::factory($c['model']);
			if ($model->fetchPrimary($this->path(2)) && ($model->value('dateSent'))) {
				$this->variable('errorCantUpdateOrDelete', true);
			} else {
				parent::update();
			}
		}	
	}


}