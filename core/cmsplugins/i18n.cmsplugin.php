<?php

class I18nCMSPlugin extends CMSPlugin {

	static public $cmsSections = array(
		'CDN',
		'CMS', 
		'CMSUSER', 
		'I18NIMPORT',
		'NEWSLETTER', 
		'NEWSLETTERSENT', 
		'NEWSLETTERTEST', 
		'SUBSCRIBERSIMPORT'
	);

	static public function getPluginConfiguration() {
		global $pico;

		/* Fields */
		$fields = array('section','key');
		$langs = $pico->i18n->langs();
		foreach($langs as $lang) {
			$fields[] = $lang.'-text';
		}

		/* Tabs */
		$tabs = array();
		if (count($langs)>1) {
			foreach($pico->i18n->langs() as $lang) {
				$missingCondition = '`'.$lang.'-text` IS NULL';
				if (!in_array($lang, $pico->i18n->cmsLangs())) {
					$missingCondition.= ' AND (`section` NOT IN ("'.implode('","', I18nCMSPlugin::$cmsSections).'"))';
				}
				$tabs['_missing_'.$lang] = array(
					'condition' => $missingCondition,
					'id' => '_missing_'.$lang,
					'text' => i18n('CMS.LABEL_I18N_MISSING_TAB').' ('.strtoupper($lang).')',
				);
			}
		} else {
			$lang = reset($langs);
			$tabs['_missing'] = array(
				'condition' => '`'.$lang.'-text` IS NULL',
				'id' => '_missing',
				'text' => i18n('CMS.LABEL_I18N_MISSING_TAB'),
			);
		}
		$sections = $pico->db->column('_i18n', 'DISTINCT `section`', NULL, '`section` ASC');
		foreach($sections as $s) {
			$tabs[$s] = array(
				'condition' => '`section`="'.safer($s, 'db').'"',
				'id' => $s,
				'text' => $s,
			);
		} 
		if (count($tabs)==0) {
			$tabs = false;
		}

		$config = Tools::arrayMergeConfig(parent::getPluginConfiguration(), array(
			'actions' 		=> array(
				'index' => array(
					'order' => '`key` ASC',
					'tabs' => $tabs,
				),
				'export' => array(
					'css' 		=> '',
					'icon' 		=> 'download',
					'item' 		=> false,
					'target'	=> '_blank',
					'text' 		=> i18n('CMS.ACTION_EXPORT'),
					'tooltiped' => true,
				),
				'import' => array(
					'css' 		=> '',
					'icon' 		=> 'upload',
					'item' 		=> false,
					'text' 		=> i18n('CMS.ACTION_IMPORT'),
					'tooltiped' => true,
				),
				'auto' => array(
					'css' 		=> '',
					'icon' 		=> 'gear',
					'item' 		=> false,
					'text' 		=> i18n('CMS.ACTION_I18NAUTO'),
				),
				'update' => array(
					'form' => array(
						'disabled' => array('section','key'),
						'fields' => $fields,
						'include' => true,
					),
				),
			),
			'icon'			=> 'code',
			'model' 		=> 'I18n',
			'templateItem' 	=> '
				{{ item.section }}.<strong>{{ item.key }}</strong><br />
				{% for lang in _pico.langsAll %}
					{% if ((tab[:9]!="_missing_") or (lang==tab[-2:])) and ((item.section not in i18nCmsSections) or (lang in ["'.implode('","', $pico->i18n->cmsLangs()).'"])) %}
						{% if loop.length>1 %}
							<span class="tag isLight">{{ lang|upper }}</span>
						{% endif %}
						{% if item[lang~"-text"] %}
							<small data-lang="{{ lang }}">
								{% if _pico.langs.length>0 %}[{{ lang|upper }}]{% endif %}
								{{ item[lang~"-text"]|summary(100, "...") }}
							</small>
						{% else %}
							<small data-lang="{{ lang }}" class="isWarning">
								{% if _pico.langs.length>0 %}[{{ lang|upper }}]{% endif %}
								{{ "CMS.MESSAGE_MISSING_I18N"|i18n }}
							</small>
						{% endif %}
						<br />
					{% endif %}
				{% endfor %}
			', // More specific templateItem based on tabs
			'title'			=> i18n('CMS.LABEL_PLUGIN_I18N'),
			'type'			=> 'core',
			'view'			=> 'cms/i18n.twig',
		));
		unset($config['actions']['create']);
		unset($config['actions']['duplicate']);
		unset($config['actions']['preview']);
		if (method_exists('Hooks', 'cmsPluginConfiguration')) {
			Hooks::cmsPluginConfiguration('i18n', $config);
		}
		return $config;
	}
	

	public function ajax() {
		global $pico;
		if ($this->path(2) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])=='xmlhttprequest') {
			if ($pico->debugBar) {
				$pico->debugBar['messages']->addMessage($_SERVER['REQUEST_URI'] ?? '?', 'request');
				$pico->debugBar->sendDataInHeaders();
			}
			$result = array();
			switch ($this->path(2)) {
				
				case 'edit':	
					if (isset($_POST['item'])) {
						$i18n = Model::factory('I18n');
						if ($i18n->loadForm()) {
							$result['success'] = $i18n->save($_POST['item']);
						} elseif ($i18n->fetchPrimary($_POST['item'])) {
							$fields = array();
							foreach($pico->i18n->langs() as $lang) {
								if (in_array($lang, $pico->i18n->cmsLangs()) || !in_array($i18n->value('section'), I18nCMSPlugin::$cmsSections)) {
									$fields[] = $lang.'-text';
								}
							}
							$result['form'] = $i18n->form(array(
								'actions' => array(
									'submit' => array('type'=>'submit', 'css'=>'button isUpdate isPrimary', 'text'=>i18n('CMS.ACTION_SAVE')),
								),
								'action' => '#',
								'ajax' => true,
								'fields' => $fields,
								'include' => true,
								'fieldsets' => false,
							));
						}
					}	
					break;

				default:
					parent::ajax();
					break;

			}
			echo json_encode($result);
			die();
		} else {
			$pico->url(':');
		}
	}


	public function auto() {
		global $pico;
		$missing = array(
			'before'=>array(), 
			'after'=>array(),
			'cms_before'=>0, 
			'cms_after'=>0,
			'cms'=>false
		);
		foreach($pico->i18n->langs() as $lang) {
			$missing['before'][$lang] = $pico->db->count('_i18n', "`$lang-text` IS NULL");
			$pico->db->script('auto_i18n_'.$lang);
			$missing['after'][$lang] = $pico->db->count('_i18n', "`$lang-text` IS NULL");
		}
		foreach($pico->i18n->cmsLangs() as $cmsLang) {
			$missing['cms_before'] = $pico->db->count('_i18n', "`$cmsLang-text` IS NULL");
			$pico->db->script('cms_i18n_'.$cmsLang);
			$missing['cms_after'] = $pico->db->count('_i18n', "`$cmsLang-text` IS NULL");
			if ($missing['cms_before']!=$missing['cms_after']) {
				$missing['cms'] = true;
			}
		}
		$this->variable('missing', $missing);
	}


	public function export() {
		global $pico;
		/**
		 * NOTE: 
		 * This will generate a correct file if you open it directly with Excel,
		 * but NOT if you import within Excel (don't ask).
		 */
		$data = $pico->db->all('_i18n', '*', '`section` NOT IN ("'.implode('","', I18nCMSPlugin::$cmsSections).'")', '`section` ASC, `key` ASC');
		$langs = $pico->i18n->langs();
		$csv = 'SECTION;KEY;';
		foreach($langs as $lang) {
			$csv.= strtoupper($lang).';';
		}
		$csv.= "\n";
		foreach($data as $row) {
			$csv.= $row['section'].';'.$row['key'].';';
			foreach($langs as $lang) {
				$csv.= '"'.str_replace('"', '&quot;', $row[$lang.'-text']).'";';
			}
			$csv.= "\n";
		}
		header("Content-Encoding: UTF-8");
		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=i18n_export.csv");
		echo "\xEF\xBB\xBF";
		echo $csv;
		die();
	}


	public function import() {
		global $pico;
		$import = Model::factory('I18nImport');
		$canImport = $import->loadForm();
		if ($canImport===true) {
			$file = Model::factory('Cdn');
			$file->fetchPrimary($import->value('file'));
			if ($file->value('extension')=='csv') {
				$sections = $pico->db->column('_i18n', 'DISTINCT `section`');
				$keys = $pico->db->column('_i18n', 'DISTINCT `key`');
				$csv = file_get_contents(__ROOT.'/cdn/'.$file->value('id').'_'.$file->value('key').'.csv');
				$csv = explode("\n", str_replace("\r\n", "\n", $csv));
				$data = array();
				$headers = explode(';', $csv[0]);
				if ((substr($headers[0], -7)=='SECTION') && ($headers[1]=='KEY')) {
					$ci = 2;
					$columns = array();
					while (in_array(strtolower($headers[$ci]), $pico->i18n->langs())) {
						$columns[trim(strtolower($headers[$ci])).'-text'] = $ci;
						$ci++;
					}		
					$ri = 0;
					foreach($csv as $row) {
						if ($ri++>0) {
							$rowData = str_getcsv($row, ';');
							if (in_array($rowData[0], $sections) && in_array($rowData[1], $keys) && (trim($rowData[2])!='')) {
								$updatedRow = array(
									'section' => $rowData[0], 
									'key' => $rowData[1],
								);
								foreach($columns as $field => $number) {
									if ($rowData[$number]!='') {
										$updatedRow[$field] = $rowData[$number];
									}
								}
								$data[] = $updatedRow;
							}
						}
					}
				}
				if (count($data)>0) {
					$i18n = Model::factory('I18n');
					$i18n->load($data, true);
					if ($i18n->save()) {
						$this->variable('results', array(
							'uploaded' => count($csv),
							'saved' => count($data),
						));
					}
				}
			}
			$file->delete($import->value('file'));
		} else {
			$this->variable('form', $import->form(array(
				'actions'	=> array(
					'cancel' => array('type'=>'link','href'=>'_index','text'=>i18n('PICO.ACTION_CANCEL'),'css'=>'button'),
					'submit' => array('type'=>'submit', 'text'=>i18n('CMS.ACTION_IMPORT'), 'css'=>'add'),
				),
				'fields' 	=> array('file'),
				'fieldsets' => false,
				'include'	=> true,
				'css'		=> 'labels-left'
			)));
		}
	}

}