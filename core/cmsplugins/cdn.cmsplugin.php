<?php

class CdnCMSPlugin extends CMSPlugin {


	static public function getPluginConfiguration() {
		global $pico;
		
		/* Tabs */
		$months = $pico->db->sql('SELECT YEAR(`__timestamp`) as "year", MONTH(`__timestamp`) as "month" FROM `cdn` GROUP BY CONCAT(YEAR(`__timestamp`),"-",MONTH(`__timestamp`)) ORDER BY `year` DESC, `month` DESC');
		$tabs = array();
		foreach($months as $m) {
			$dbm = $m['year'].'-'.str_pad($m['month'], 2, '0', STR_PAD_LEFT);
			$tabs[$dbm] = array(
				'condition' => '`__timestamp` LIKE "'.$dbm.'-%"',
				'id' => $dbm,
				'text' => date('m/Y', strtotime($dbm.'-01')),
			);
		} 
		if (count($tabs)==0) {
			$tabs = false;
		}

		/* Fields */
		$fields = array('name');
		foreach($pico->i18n->langs() as $lang) {
			$fields[] = $lang.'-alt';
		}
		$fields[] = 'color';

		$config = Tools::arrayMergeConfig(parent::getPluginConfiguration(), array(
			'actions' 		=> array(
				'download'	=> array(
					'icon' 		=> 'download',
					'item' 		=> true,
					'target'	=> '_blank',
					'text' 		=> i18n('CMS.ACTION_DOWNLOAD'),
				),
				'index'	=> array(
					'order' => '`__timestamp` DESC',
					'tabs' => $tabs,
				),
				'thumbnails' => array(
					'css' 		=> '',
					'icon' 		=> 'trash',
					'item' 		=> false,
					'text' 		=> i18n('CMS.ACTION_THUMBNAILS'),
				),
				'update' => array(
					'form' => array(
						'fields' => $fields,
						'include' => true,
					),
				),
				'upload' => array(
					'css' 		=> 'isCreate isPrimary',
					'form'		=> array(
						'actions'	=> array(
							'cancel' => array('type'=>'link','href'=>'_index','text'=>i18n('PICO.ACTION_CANCEL'),'css'=>'button'),
							'submit' => array('type'=>'submit', 'text'=>i18n('CMS.ACTION_SAVE'), 'css'=>'isCreate'),
						),
						'css'		=> 'labels-left'
					),
					'icon' 		=> 'plus',
					'item' 		=> false,
					'text' 		=> i18n('CMS.ACTION_CREATE'), 
					'title' 	=> i18n('CMS.TITLE_CREATE'), 
				),
			),
			'defaultAction' => 'read',
			'icon'			=> 'cloud',
			'model' 		=> 'Cdn',
			'shortcuts' 	=> array('link', 'download'),
			'title'			=> i18n('CMS.LABEL_PLUGIN_CDN'),
			'type'			=> 'core',
			'view'			=> 'cms/cdn.twig',
		));
		unset($config['actions']['create']);
		unset($config['actions']['duplicate']);
		unset($config['actions']['preview']);
		if (method_exists('Hooks', 'cmsPluginConfiguration')) {
			Hooks::cmsPluginConfiguration('cdn', $config);
		}
		return $config;
	}


	public function ajax() {
		global $pico;
		if ($this->path(2) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])=='xmlhttprequest') {
			if ($pico->debugBar) {
				$pico->debugBar['messages']->addMessage($_SERVER['REQUEST_URI'] ?? '?', 'request');
				$pico->debugBar->sendDataInHeaders();
			}
			$result = array();
			switch ($this->path(2)) {
				
				case 'browser':	
					if (isset($_POST['query'])) {
						$pluginConfig = self::getPluginConfiguration();
						$model = Model::factory($pluginConfig['model']);
						$condition = '(1=1)';
						if ($_POST['extensions']) {
							$extensions = json_decode($_POST['extensions'], true);
							if (is_array($extensions)) {
								$condition.= ' AND `extension` IN ("'.implode('","', $extensions).'")';
							}
						}
						$resultsData = array(
							'config' => $pluginConfig,
							'fields' => $model->getFields(),
							'templateItem' => $model->getTemplateItem(),
							'type' => $pluginConfig['type'],
						);
						if (strlen($_POST['query'])>2) {
							$model->search(is_array($pluginConfig['search']) ? $pluginConfig['search'] : NULL, $_POST['query'], $condition);
							$resultsData['results'] = $model->values();
							$result['results'] = $pico->twig->render('cms/cdn._search.twig', $resultsData);
						} elseif (strlen($_POST['query'])>0) {
							$resultsData['help'] = true;
							$result['results'] = $pico->twig->render('cms/cdn._search.twig', $resultsData);
						} else {
							$model->fetch($condition, '`__timestamp` DESC', 50);
							$resultsData['results'] = $model->values();
							$result['results'] = $pico->twig->render('cms/cdn._search.twig', $resultsData);
						}
						$result['success'] = true;
					} else {
						$result['success'] = false;
					}
					break;

				case 'legend':	
					if (isset($_POST['item'])) {
						$cdn = Model::factory('Cdn');
						if ($cdn->loadForm()) {
							$result['success'] = $cdn->save($_POST['item']);
						} elseif ($cdn->fetchPrimary($_POST['item'])) {
							$result['form'] = $cdn->form(array(
								'actions' => array(
									'submit' => array('type'=>'submit', 'css'=>'button isUpdate isPrimary', 'text'=>i18n('CMS.ACTION_SAVE')),
								),
								'action' => '#',
								'ajax' => true,
								'fields' => array('key', 'name', 'extension', 'size', 'info', 'width', 'height', 'ratio', 'color', 'lightness'),
								'include' => false,
								'fieldsets' => false,
							));
						}
					}	
					break;

				case 'link':	
					if (isset($_POST['item'])) {
						$result['url'] = CoreTwig::cdn(intval($_POST['item']));
						$result['success'] = true;
					}	
					break;

				default:
					parent::ajax();
					break;

			}
			echo json_encode($result);
			die();
		} else {
			$pico->url(':');
		}
	}


	public function download() {
		global $pico;
		$pico->url(CoreTwig::cdn(intval($this->path(2)), '', true));
	}


	public function thumbnails() {
		global $pico;
		$thumbnails = glob(__ROOT."/cdn/*_*_*.*");
		array_map('unlink', $thumbnails);
		$this->variable('count', count($thumbnails));
	}

	
	public function update() {
		global $pico;
		$c = static::getPluginConfiguration();
		if (isset($c['actions'][$this->path(1)])) {
			$model = Model::factory($c['model']);
			if (!is_null($this->path(2)) && $model->exists($this->path(2))) {
				$rootData = NULL;
				if ($model->primary()[0]=='id') {
					$rootData = array('id' => $this->path(2));
				}
				$canSave = $model->loadForm(NULL, true, $rootData);
				if ($canSave===true) {
					$ext = '.'.$pico->db->value('cdn', '`extension`', '`id`='.$model->value('id'));
					if ($ext != substr($model->value('name'), -1 * strlen($ext))) {
						$model->value('name', 0, $model->value('name').$ext);
					}
					$model->save($this->path(2));
					if (isset($_GET['continue'])) {
						$pico->url('~');
					} else {
						$pico->url('_index');
					}
				} elseif ($canSave===false) {
					$model->fetchPrimary($this->path(2));
				}
				if (!isset($c['actions'][$this->path(1)]['form'])) {
					$c['actions'][$this->path(1)]['form'] = array();
				}
				if (isset($c['draft']) && ($c['draft'])) {
					$c['actions'][$this->path(1)]['form']['internalRemove'] = '__draft';
				}
				$c['actions'][$this->path(1)]['form']['errors'] = $canSave;
				$this->variable('_id', $this->path(2));
				$this->variable('form', $model->form($c['actions'][$this->path(1)]['form']));
			} else {
				$pico->url('_index');
			}
		} else {
			$pico->url('_index');
		}
	}


	public function upload() {
		global $pico;
		$c = static::getPluginConfiguration();
		if (isset($c['actions'][$this->path(1)])) {
			$dummy = new Model(array(
				'name'=>'dummy', 
				'table'=>'dummy',
				'fields'=>array(
					'id'=>array('type'=>'integer','checks'=>array('required')),
					'file'=>array(
						'type' => 'cdn',
						'max' => '1',
						'multiple' => '0',
						'browse' => '0',
					),
				)
			));
			$this->variable('form', $dummy->form(array(
				'actions' => array(
					'insert'=>array('type'=>'link','css'=>'button isAdd isDisabled','text'=>i18n('CMS.ACTION_INSERT_CDN'))
				),
			)));
		}
	}


}