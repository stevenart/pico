<?php

class PlausibleCMSPlugin extends CMSPlugin {


	static public function getPluginConfiguration() {
		$config = Tools::arrayMergeConfig(parent::getPluginConfiguration(), array(
			'actions' => array(
				'index' => array(
					'text' 		=> i18n('CMS.ACTION_BACK'), 
				),
			),
			'icon'			=> 'graph-line',
			'title'			=> i18n('CMS.LABEL_PLUGIN_PLAUSIBLE'),
			'type'			=> 'core',
			'view'			=> 'cms/plausible.twig',
		));
		if (method_exists('Hooks', 'cmsPluginConfiguration')) {
			Hooks::cmsPluginConfiguration('plausible', $config);
		}
		unset($config['actions']['create']);
		unset($config['actions']['delete']);
		unset($config['actions']['duplicate']);
		unset($config['actions']['read']);
		unset($config['actions']['preview']);
		unset($config['actions']['update']);
		return $config;
	}


}