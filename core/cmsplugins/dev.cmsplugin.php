<?php

class DevCMSPlugin extends CMSPlugin {


	static public function getPluginConfiguration() {
		$config = Tools::arrayMergeConfig(parent::getPluginConfiguration(), array(
			'actions' => array(
				'index' => array(
					'text' 		=> i18n('CMS.ACTION_BACK'), 
				),
			),
			'icon'			=> 'gear',
			'title'			=> i18n('CMS.LABEL_PLUGIN_DEV'),
			'type'			=> 'core',
			'view'			=> 'cms/dev.twig',
		));
		unset($config['actions']['create']);
		unset($config['actions']['delete']);
		unset($config['actions']['duplicate']);
		unset($config['actions']['read']);
		unset($config['actions']['preview']);
		unset($config['actions']['update']);
		if (method_exists('Hooks', 'cmsPluginConfiguration')) {
			Hooks::cmsPluginConfiguration('dev', $config);
		}
		return $config;
	}


	public function picodminer() {
		global $pico;
		if (isset($_POST['logout'])) {
			$pico->url(':cms/dev/index');
		}
		require_once __ROOT.'/core/adminer.php';
		die();
	}


	public function filemanager() {
		define('FM_EMBED', true);
        define('FM_ROOT_PATH', __ROOT); 
        define('FM_ROOT_URL', __URL);
        define('FM_SELF_URL', '/fr/cms/dev/filemanager'); 
        define('FM_ICONV_INPUT_ENC', 'UTF-8');
        define('FM_DATETIME_FORMAT', 'd/m/Y H:i:s');
        define('FM_USE_HIGHLIGHTJS', false);
		require_once __ROOT.'/core/filemanager.php';
		die();
	}


	public function index() {
		global $pico;
		$this->variable('config', $pico->db->configuration);

		/* Scripts */
		$scripts = array();
		foreach(['core', 'app'] as $prefix) {
			foreach(glob(__ROOT.'/'.$prefix.'/scripts/sql/*.sql') as $script) {
				$scriptName = substr(basename($script), 0, -4);
				if (!(substr($scriptName, 0, 9)=='cms_i18n_') && !(substr($scriptName, 0, 10)=='auto_i18n_')) {
					$scripts[basename($script)] = array(
						'type' => 'sql',
						'mtime' => filemtime($script),
						'name' => $scriptName,
						'size' => filesize($script),
					);
				}
			}
		}
		foreach(['core', 'app'] as $prefix) {
			foreach(glob(__ROOT.'/'.$prefix.'/scripts/php/*.php') as $script) {
				$scriptName = substr(basename($script), 0, -4);
				$scripts[basename($script)] = array(
					'type' => 'php',
					'mtime' => filemtime($script),
					'name' => $scriptName,
					'size' => filesize($script),
				);
			}
		}
		Tools::arraySort($scripts, 'mtime', false);
		$this->variable('scripts', $scripts);

		/* ScriptRuns */
		$model = Model::factory('ScriptRun');
		$model->fetch(NULL, '`__created` DESC', 200);
		$this->variable('scriptRunTemplate', $model->getTemplateItem());
		$this->variable('scriptruns', $model->values());
	}


	public function php() {
		phpinfo();
		die();
	}


	public function p0wnyshell() {
		require_once __ROOT.'/core/p0wnyshell.php';
		die();
	}

	public function run() {
		global $pico;
		if ($this->path(2, ['php', 'sql']) && !is_null($this->path(3))) {
			$scriptName = Tools::computerFriendly($this->path(3), false);
			$scriptFile = appfile('scripts/'.$this->path(2).'/'.$scriptName.'.'.$this->path(2));
			if (file_exists($scriptFile)) {
				$scriptIntro = '';
				$i = 0;
				$foo = fopen($scriptFile, 'r');
				while (!feof($foo) && ($i++<10))  {
					$scriptIntro.= fgets($foo);
				}
				if ($i>9) {
					$scriptIntro.= '[...]';
				}
				fclose($foo);
				$model = Model::factory('ScriptRun');
				$canRun = $model->loadForm();
				if ($canRun) {
					$ok = false;
					switch ($this->path(2)) {
						case 'php':
							$output = shell_exec('$(which php) '.$scriptFile);
							$ok = true;
							break;
						case 'sql':
							$output = NULL;
							$ok = $pico->db->script($scriptName);
							break;
					}
					if ($ok!==false) {
						$ok = true;
					}
					$id = $model->nextId();
					$model->load(array(
						'id' => $id,
						'mtime' => date('Y-m-d H:i:s', filemtime($scriptFile)),
						'type' => $this->path(2),
						'name' => $scriptName,
						'size' => filesize($scriptFile),
						'intro' => $scriptIntro,
						'ok' => $ok,
						'output' => $output,
					));
					$model->save($id);
					$pico->url(':cms/dev/script/'.$id);
				} else {
					$model->load(array(
						'mtime' => date('d/m/Y', filemtime($scriptFile)),
						'name' => 'scripts/'.$this->path(2).'/'.$scriptName.'.'.$this->path(2),
						'size' => CoreTwig::bytesize(filesize($scriptFile)),
						'intro' => $scriptIntro,
					));
					$this->variable('form', $model->form(array(
						'actions'	=> array(
							'cancel' => array('type'=>'link','href'=>'_index','text'=>i18n('PICO.ACTION_CANCEL'),'css'=>'button'),	
							'submit' => array('type'=>'submit', 'text'=>i18n('CMS.ACTION_RUN'), 'css'=>'isWarning isPrimary'),
						),
						'disabled' 	=> true,
						'fields' 	=> array('mtime', 'name', 'size', 'intro'),
						'fieldsets' => false,
					)));
				}
			}
		} else {
			$pico->url('_index');
		}
	}


	public function script() {
		global $pico;
		$c = static::getPluginConfiguration();
		$model = Model::factory('ScriptRun');
		if (!is_null($this->path(2)) && $model->fetchPrimary($this->path(2))) {
			$fields = $model->getFields();
			$fields = array_diff_key($fields, array_flip(array('id', '__draft', '__ordering', '__starred', '__timestamp')));
			$this->variable('fields', $fields);
			$fieldsets = $model->getFieldsets();
			$this->variable('fieldsets', $fieldsets);
			$model->fetchPrimary($this->path(2));
			$this->variable('_id', $this->path(2));
			$this->variable('item', $model->one(array_keys($fields)));
			$this->variable('legendsModel', 'ScriptRun');
		} else {
			$pico->url('_index');
		}
	}


}