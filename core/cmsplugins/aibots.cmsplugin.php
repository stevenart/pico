<?php

class AiBotsCMSPlugin extends CMSPlugin {


	static public function getPluginConfiguration() {
		$config = Tools::arrayMergeConfig(parent::getPluginConfiguration(), array(
			'actions'		=> array(
				'index'	=> array(
					'order' => '`__timestamp` DESC',
					'sortable' => true,
				),
				'run'	=> array(
					'icon' 		=> 'media-play',
					'item' 		=> true,
					'text' 		=> i18n('CMS.ACTION_AIBOTS_RUN'), 
					'title' 	=> i18n('CMS.TITLE_AIBOTS_RUN'), 
				),
			),
			'svg'			=> '<svg width="24" height="24" focusable="false"><path class="colorFill" fill-rule="evenodd" clip-rule="evenodd" d="M5 3a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h14a3 3 0 0 0 3-3V6a3 3 0 0 0-3-3H5Zm6.8 11.5.5 1.2a68.3 68.3 0 0 0 .7 1.1l.4.1c.3 0 .5 0 .7-.3.2-.1.3-.3.3-.6l-.3-1-2.6-6.2a20.4 20.4 0 0 0-.5-1.3l-.5-.4-.7-.2c-.2 0-.5 0-.6.2-.2 0-.4.2-.5.4l-.3.6-.3.7L5.7 15l-.2.6-.1.4c0 .3 0 .5.3.7l.6.2c.3 0 .5 0 .7-.2l.4-1 .5-1.2h3.9ZM9.8 9l1.5 4h-3l1.5-4Zm5.6-.9v7.6c0 .4 0 .7.2 1l.7.2c.3 0 .6 0 .8-.3l.2-.9V8.1c0-.4 0-.7-.2-.9a1 1 0 0 0-.8-.3c-.2 0-.5.1-.7.3l-.2 1Z"></path></svg>',
			'model' 		=> 'AiBot',
			'shortcuts'		=> ['run'],
			'title'			=> i18n('CMS.LABEL_PLUGIN_AIBOTS'),
			'type'			=> 'core',
			'view'			=> 'cms/aibots.twig',
		));
		unset($config['actions']['preview']);
		if (method_exists('Hooks', 'cmsPluginConfiguration')) {
			Hooks::cmsPluginConfiguration('aibots', $config);
		}
		return $config;
	}


	public function ajax() {
		global $pico;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])=='xmlhttprequest') {
			if ($pico->debugBar) {
				$pico->debugBar['messages']->addMessage($_SERVER['REQUEST_URI'] ?? '?', 'request');
				$pico->debugBar->sendDataInHeaders();
			}
			$c = static::getPluginConfiguration();
			$result = ['success' => false];
			switch ($this->path(2)) {	

				case 'chat':	
					if (isset($_POST['key']) && isset($_POST['messages'])) {
						$aibot = Model::factory('AIBot');
						if ($aibot->fetchOne('key', safer($_POST['key'], 'db'))) {
							$messages = json_decode($_POST['messages'], true);
							set_time_limit(0);
							$result['response'] = $aibot->chat($messages);
							$result['success'] = true;
						}
					}
					break;

				case 'config':
					if (isset($_POST['key'])) {
						$aibot = Model::factory('AIBot');
						if ($aibot->fetchOne('key', safer($_POST['key'], 'db'))) {
							$result['config'] = $aibot->one();
							if ($result['config']['predefined']) {
								$result['config']['predefined'] = json_decode($result['config']['predefined'], true);
								foreach($result['config']['predefined'] as $foo) {
									$result['config']['tinymce'][] = [
										'text' => $foo['title'],
										'value' => $foo['prompt'],
									];
								}
							}
							$result['success'] = true;
						}
					}	
					break;

				case 'stream':	
					if (isset($_POST['key']) && isset($_POST['messages'])) {
						$aibot = Model::factory('AIBot');
						if ($aibot->fetchOne('key', safer($_POST['key'], 'db'))) {
							$messages = json_decode($_POST['messages'], true);
							set_time_limit(0);
							ob_implicit_flush(1);
							ob_end_flush();
							foreach ($aibot->stream($messages) as $response) {
								$result['response'] = $response;
								$result['success'] = true;
								echo json_encode($result)."\n";
							}
						}
					}
					break;

				default:
					parent::ajax();
					break;

			}
			echo json_encode($result);
			die();
		} else {
			$pico->url(':');
		}
	}


	public function run() {
		global $pico;
		$c = static::getPluginConfiguration();
		if (isset($c['actions'][$this->path(1)])) {
			$model = Model::factory($c['model']);
			if (!is_null($this->path(2)) && $model->fetchPrimary($this->path(2))) {
				$this->variable('aibot', $model->one());
			} else {
				$pico->url('_index');
			}
		} else {
			$pico->url('_index');
		}
	}


}