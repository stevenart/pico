<?php

class PagesCMSPlugin extends CMSPlugin {


	static public function getPluginConfiguration() {
		global $pico;
		$config = Tools::arrayMergeConfig(parent::getPluginConfiguration(), array(
			'actions' 		=> array(
				'create' => array(
					'form' => array(
						'fields' => array('uri'),
						'fsclosed' => array('seo'),
						'include' => false,
						'internal' => array('id', 'uri', '__ordering', '__starred', '__timestamp', '__created', '__user'),
					),
				),
				'index' => array(
					'sections' => 'navigation',
					'tabs' => 'lang',
					'parentable' => 'parent',
					'sortable' => true,
				),
				'preview' => array(
					'url' 		=> ':page',
				),
				'update' => array(
					'form' => array(
						'fields' => array('uri'),
						'fsclosed' => array('seo'),
						'include' => false,
						'internal' => array('id', 'uri', '__ordering', '__starred', '__timestamp', '__created', '__user'),
					),
				),
			),
			'icon'			=> 'document',
			'model' 		=> 'Page',
			'title'			=> i18n('CMS.LABEL_PLUGIN_PAGES'),
			'view'			=> 'cms/pages.twig',
		));
		if (method_exists('Hooks', 'cmsPluginConfiguration')) {
			Hooks::cmsPluginConfiguration('pages', $config);
		}
		return $config;
	}


	public function ajax() {
		global $pico;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])=='xmlhttprequest') {
			if ($pico->debugBar) {
				$pico->debugBar['messages']->addMessage($_SERVER['REQUEST_URI'] ?? '?', 'request');
				$pico->debugBar->sendDataInHeaders();
			}
			$result = array();
			switch ($this->path(2)) {
				
				case 'parents':
					if (isset($_POST['navigation'])) {
						if (!empty($_POST['exclude'])) {
							$exclude = explode(',', $_POST['exclude']);
							array_walk($exclude, 'intval');
						}
						$result['parents'] = $pico->db->pairs('pages', 'CONCAT("p", `id`)', '`name`', 
							'`parent` IS NULL '.
							(!empty($_POST['lang']) ? ' AND `lang`="'.safer($_POST['lang'], 'db').'"' : '').
							(isset($exclude) ? ' AND `id` NOT IN ('.implode(',', $exclude).') ' : '').
							'AND `navigation`="'.safer($_POST['navigation'], 'db').'"', 
							'`__ordering` ASC, `id` ASC'
						);
					}
					break;

				default:	
					parent::ajax();
					break;
			}
			echo json_encode($result);
			die();
		} else {
			$pico->url(':');
		}
	}
	

}