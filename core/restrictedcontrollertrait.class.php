<?php


trait RestrictedControllerTrait {


	protected $restrictedAuthorized;


	public function _authorized() {
		global $pico;
		$this->restrictedAuthorized = false;
		if (!$this->restrictedSession('userEmail') && $this->restrictedCookie('userEmail')) {
			$this->restrictedSession('userEmail', $this->restrictedCookie('userEmail'));
			$this->restrictedSession('token', $this->restrictedCookie('token'));
		}
		$this->restrictedAuthorized = Model::factory($this->restrictedProp('model'));
		if ($this->restrictedSession('userEmail') && $this->restrictedSession('token')) {
			if ($this->restrictedAuthorized->fetchOne($this->restrictedProp('emailField'), $this->restrictedSession('userEmail'))) {
				$validToken = NULL;
				$twoFactorActivated = $this->restrictedProp('twoFactor');
				if (is_string($twoFactorActivated)) {
					$twoFactorActivated = $pico->db->value(
						$this->restrictedAuthorized->table(), 
						$twoFactorActivated, 
						'`'.$this->restrictedProp('emailField').'`="'.safer($this->restrictedAuthorized->value($this->restrictedProp('emailField')), 'db').'"'
					);
				}
				if ($twoFactorActivated) {
					$validToken = $this->restrictedToken(
						$this->restrictedAuthorized->value($this->restrictedProp('passwordField')),
						$this->restrictedAuthorized->value($this->restrictedProp('codeField')),
					);
				} else {
					$validToken = $this->restrictedToken($this->restrictedAuthorized->value($this->restrictedProp('passwordField')));
				}
				if ($this->restrictedSession('token')!==$validToken) {
					$this->restrictedAuthorized->clear();
				}
			}
		}
		if ($this->restrictedAuthorized->fetched()===1) {
			$pico->variable('_logged', $this->restrictedAuthorized->one());
			if (method_exists($this->restrictedAuthorized, 'restrictedAuthorizedCallback')) {
				$this->restrictedAuthorized->restrictedAuthorizedCallback();
			}
			if (method_exists($this, 'restrictedAuthorizedCallback')) {
				$this->restrictedAuthorizedCallback();
			}
		} else {
			$this->restrictedAuthorized = false;
		}
		return $this->restrictedAuthorized;
	}
	

	public function _login() {	
		global $pico;
		if (CoreTwig::viewExists($this->restrictedProp('view'))) {
			$model = Model::factory($this->restrictedProp('model'));
			$model->checks($this->restrictedProp('emailField'), 'unique', false); // Remove unique test since we are not adding a user
			if (isset($_GET['reset']) && isset($_GET['key']) && $this->restrictedProp('forgot')) {
				$this->variable('restrictedControllerAction', 'reset');
				if ($model->fetchOne($this->restrictedProp('emailField'), urldecode($_GET['reset']))) {
					$modelKey = substr($model->value($this->restrictedProp('passwordField')), 0, 10);
					if ($modelKey===$_GET['key']) {
						$showForm = true;
						$canReset = $model->loadForm();
						if ($canReset===true) {
							$model->value($this->restrictedProp('emailField'), 0, urldecode($_GET['reset']));
							if ($model->save()) {
								if (method_exists($model, 'restrictedResetCallback')) {
									$model->restrictedResetCallback();
								}
								$this->variable('restrictedControllerSuccess', 'reset'); 
								$showForm = false; 
							} else {
								$this->variable('restrictedControllerError', 'fatal'); 
								$showForm = false; 
							}
						} elseif (is_array($canReset)) {
							$this->variable('restrictedControllerError', 'password'); 
						}
						if ($showForm) {
							$this->variable('restrictedControllerForm', $model->form(array(
								'actions' => array(
									'submit' => array('type'=>'submit', 'css'=>'isPrimary isBrand', 'text'=>i18n($this->restrictedProp('i18nSection').'.ACTION_RESTRICTED_SAVE')),
								),
								'fields' => array('password'),
								'fieldsets' => false
							)));	
						}
					} else {
						$this->variable('restrictedControllerError', 'reset'); 
					}
				}
			} elseif (isset($_GET['forgot']) && $this->restrictedProp('forgot')) {
				$this->variable('restrictedControllerAction', 'forgot');
				$showForm = true;
				$canForgot = $model->loadForm();
				if ($canForgot===true) {
					if ($model->fetchOne($this->restrictedProp('emailField'), $model->value($this->restrictedProp('emailField')))) {
						if (method_exists($model, 'restrictedSendResetLink')) {
							$resetOk = $model->restrictedSendResetLink();
						} else {
							$modelKey = substr($model->value($this->restrictedProp('passwordField')), 0, 10);
							$link = $pico->href('~?reset='.urlencode($model->value($this->restrictedProp('emailField'))).'&key='.$modelKey);
							$domain = CoreTwig::urlDomain($pico->href(';'));
							$mail = new Mail();
							$mail->addTo($model->value($this->restrictedProp('emailField')));
							$subject = i18n($this->restrictedProp('i18nSection').'.TITLE_MAIL_RESTRICTED_RESET');
							$mail->setSubject($subject);
							$mail->setBodyTemplate('emails/_email.twig', array(
								'subject' => $subject,
								'content' => nl2br(i18n($this->restrictedProp('i18nSection').'.MESSAGE_MAIL_RESTRICTED_RESET', array(
									$domain, 
									$model->value($this->restrictedProp('emailField')),
								))),
								'actionUrl' => $link,
								'actionText' => i18n($this->restrictedProp('i18nSection').'.ACTION_MAIL_RESTRICTED_RESET'),
								'signature' => i18n($this->restrictedProp('i18nSection').'.MESSAGE_MAIL_SIGNATURE', array($domain)),
							));
							$resetOk = $mail->send(true, NULL, false);
						}
						if ($resetOk) {
							$this->variable('restrictedControllerSuccess', 'send'); 
						} else {
							$this->variable('restrictedControllerError', 'send'); 
						}
						$showForm = false;
					} else {
						$this->variable('restrictedControllerError', 'forgot'); 
					}
				} elseif (is_array($canForgot)) {
					$this->variable('restrictedControllerError', 'forgot'); 
				}
				if ($showForm) {
					$this->variable('restrictedControllerForm', $model->form(array(
						'actions' => array(
							'submit' => array('type'=>'submit', 'css'=>'isBrand', 'text'=>i18n($this->restrictedProp('i18nSection').'.ACTION_RESTRICTED_RESET')),
							'back' => array('href'=>'~', 'css'=>'isInvisible isLeft isLight', 'text'=>i18n($this->restrictedProp('i18nSection').'.ACTION_RESTRICTED_LOGIN'))
						),
						'fields' => array('email'),
						'fieldsets' => false
					)));	
				}

			} elseif (isset($_GET['twofactor']) && $this->restrictedProp('twoFactor')) {
				$this->variable('restrictedControllerAction', 'twofactor');
				if ($model->fetchOne($this->restrictedProp('emailField'), $this->restrictedSession('userEmail'))) {

					$twoFactorActivated = $this->restrictedProp('twoFactor');
					if (is_string($twoFactorActivated)) {
						$twoFactorActivated = $pico->db->value(
							$model->table(), 
							$twoFactorActivated, 
							'`'.$this->restrictedProp('emailField').'`="'.safer($model->value($this->restrictedProp('emailField')), 'db').'"'
						);
					}
					if ($twoFactorActivated) {
						$tempToken = $this->restrictedSession('tempToken');
						if ($tempToken===$this->restrictedToken($model->value($this->restrictedProp('passwordField')))) {
							$domain = CoreTwig::urlDomain($pico->href(';'));
							$twofactor = new RobThree\Auth\TwoFactorAuth(
								new RobThree\Auth\Providers\Qr\BaconQrCodeProvider(format: 'svg'),
								$domain
							);
							$firstTime = false;
							$secret = $model->value($this->restrictedProp('secretField'));
							if (is_null($secret)) {
								$firstTime = true;
								$secret = $this->restrictedSession('secret');
								if (!$secret) {
									$secret = $twofactor->createSecret();
									$this->restrictedSession('secret', $secret);
								}
								$this->variable('restrictedControllerSecret', $secret);
								$this->variable('restrictedControllerQRCode', $twofactor->getQRCodeImageAsDataUri($domain, $secret));
							}
							$canTwoFactor = $model->loadForm();
							if ($canTwoFactor===true) {
								$code = $model->value($this->restrictedProp('codeField'));
								if (!$code || ($twofactor->verifyCode($secret, $code)!==true)) {
									$this->variable('restrictedControllerError', 'twofactor'); 
								} else {
									$model->value($this->restrictedProp('emailField'), 0, $this->restrictedSession('userEmail'));
									if ($firstTime) {
										$model->value($this->restrictedProp('secretField'), 0, $secret);
									}
									$model->value($this->restrictedProp('codeField'), 0, $code);
									if ($model->save()) {
										$model->fetchOne($this->restrictedProp('emailField'), $this->restrictedSession('userEmail'));
										$this->restrictedSession('userEmail', $model->value($this->restrictedProp('emailField')));
										$this->restrictedCookie('userEmail', $model->value($this->restrictedProp('emailField')));
										$token = $this->restrictedToken(
											$model->value($this->restrictedProp('passwordField')),
											$code,
										);
										$this->restrictedSession('token', $token);
										$this->restrictedCookie('token', $token);
										if (method_exists($model, 'restrictedLoginCallback')) {
											$model->restrictedLoginCallback();
										}
										$pico->url('~');
									} else {
										$this->variable('restrictedControllerError', 'fatal'); 
									}
								}
							} elseif (is_array($canTwoFactor)) {
								$this->variable('restrictedControllerError', 'twofactor'); 
							}
							$model->clear();
							$this->variable('restrictedControllerForm', $model->form(array(
								'actions' => [
									'submit' => ['type'=>'submit', 'css'=>'isPrimary isBrand', 'text'=>i18n($this->restrictedProp('i18nSection').'.ACTION_RESTRICTED_LOGIN')],
									'cancel' => ['href'=>':cms', 'css'=>'isInvisible isLight isRight', 'text'=>i18n('PICO.ACTION_CANCEL')],
								],
								'fields' => [$this->restrictedProp('codeField')],
								'fieldsets' => false
							)));
						} else {
							$this->variable('restrictedControllerError', 'fatal'); 
						}
					} else {
						$this->variable('restrictedControllerError', 'fatal'); 
					}
				} else {
					$this->variable('restrictedControllerError', 'fatal'); 
				}

			} else {
				$this->variable('restrictedControllerAction', 'login');
				$canLogin = $model->loadForm();
				if ($canLogin===true) {
					if ($model->value($this->restrictedProp('emailField')) && $model->value($this->restrictedProp('passwordField'))) {
						$validPassword = $pico->db->value(
							$model->table(), 
							$this->restrictedProp('passwordField'), 
							'`'.$this->restrictedProp('emailField').'`="'.safer($model->value($this->restrictedProp('emailField')), 'db').'"'
						);
						if ($model->value($this->restrictedProp('passwordField'))===$validPassword) {
							$twoFactorActivated = $this->restrictedProp('twoFactor');
							if (is_string($twoFactorActivated)) {
								$twoFactorActivated = $pico->db->value(
									$model->table(), 
									$twoFactorActivated, 
									'`'.$this->restrictedProp('emailField').'`="'.safer($model->value($this->restrictedProp('emailField')), 'db').'"'
								);
							}
							if ($twoFactorActivated) {
								$this->restrictedSession('userEmail', $model->value($this->restrictedProp('emailField')));
								$this->restrictedSession('tempToken', $this->restrictedToken($validPassword));
								$pico->url('#twofactor');
							} else {
								$model->fetchOne($this->restrictedProp('emailField'), $model->value($this->restrictedProp('emailField')));
								$this->restrictedSession('userEmail', $model->value($this->restrictedProp('emailField')));
								$this->restrictedCookie('userEmail', $model->value($this->restrictedProp('emailField')));
								$token = $this->restrictedToken($model->value($this->restrictedProp('passwordField')));
								$this->restrictedSession('token', $token);
								$this->restrictedCookie('token', $token);
								if (method_exists($model, 'restrictedLoginCallback')) {
									$model->restrictedLoginCallback();
								}
								$pico->url('#');
							}
						} else {
							$this->variable('restrictedControllerError', 'password'); 
						}
					} else {
						$this->variable('restrictedControllerError', 'required'); 
					}
				} elseif (is_array($canLogin)) {
					$this->variable('restrictedControllerError', 'required'); 
				}
				$actions = [
					'submit' => ['type'=>'submit', 'css'=>'isBrand isPrimary', 'text'=>i18n($this->restrictedProp('i18nSection').'.ACTION_RESTRICTED_LOGIN')]
				];
				if ($this->restrictedProp('forgot')) {
					$actions['forgot'] = ['href'=>'#forgot', 'css'=>'isInvisible isLight isRight', 'text'=>i18n($this->restrictedProp('i18nSection').'.ACTION_RESTRICTED_FORGOT')];
				}
				$this->variable('restrictedControllerForm', $model->form(array(
					'actions' => $actions,
					'fields' => [$this->restrictedProp('emailField'), $this->restrictedProp('passwordField')],
					'fieldsets' => false
				)));	
			}
			$this->variable('restrictedI18nSection', $this->restrictedProp('i18nSection'));
			$this->view = $this->restrictedProp('view');
		} else {
			$pico->_401();
		}
	}


	public function _logout() {
		global $pico;
		$this->restrictedSession('password', NULL, true);
		$this->restrictedCookie('password', NULL, true);
		$this->restrictedSession('userEmail', NULL, true);
		$this->restrictedCookie('userEmail', NULL, true);
		$this->restrictedSession('token', NULL, true);
		$this->restrictedCookie('token', NULL, true);
		if (is_object($this->restrictedAuthorized)) {
			if (method_exists($this->restrictedAuthorized, 'restrictedLogoutCallback')) {
				$this->restrictedAuthorized->restrictedLogoutCallback();
			}
		}
		$pico->url(':');
	}	


	/**
	 * This function returns the a property for the object. 
	 * The reason for this is because you cannot overwrite trait properties in class definition, and we dind't want to alter constructor.
	 *
	 * @param string $name The property name (allowed: model, emailField, passwordField, secretField, codeField, forgot, i18nSection, view)
	 *
	 * @return mixed Property value or NULL.
	 */
	private function restrictedProp(string $name) {
		switch ($name) {
			case 'prefix':
				return property_exists($this, 'restrictedPrefix') ? $this->restrictedPrefix : $this->name;
				break;
			case 'model':
				return property_exists($this, 'restrictedModel') ? $this->restrictedModel : NULL;
				break;
			case 'twoFactor':
				if (property_exists($this, 'restrictedTwoFactor')) {
					return $this->restrictedTwoFactor;
				} elseif (property_exists($this, 'restrictedModel')) {
					$model = Model::factory($this->restrictedModel);
					if (!is_null($model->field('twofactor'))) {
						return 'twofactor';
					}
				}
				break;
			case 'emailField':
				if (property_exists($this, 'restrictedEmailField')) {
					return $this->restrictedPasswordField;
				} elseif (property_exists($this, 'restrictedModel')) {
					$model = Model::factory($this->restrictedModel);
					if (!is_null($model->field('email'))) {
						return 'email';
					}
				}
				break;
			case 'passwordField':
				if (property_exists($this, 'restrictedPasswordField')) {
					return $this->restrictedPasswordField;
				} elseif (property_exists($this, 'restrictedModel')) {
					$model = Model::factory($this->restrictedModel);
					if (!is_null($model->field('password'))) {
						return 'password';
					}
				}
				break;
			case 'secretField':
				if (property_exists($this, 'restrictedSecretField')) {
					return $this->restrictedSecretField;
				} elseif (property_exists($this, 'restrictedModel')) {
					$model = Model::factory($this->restrictedModel);
					if (!is_null($model->field('secret'))) {
						return 'secret';
					}
				}
				break;
			case 'codeField':
				if (property_exists($this, 'restrictedCodeField')) {
					return $this->restrictedCodeField;
				} elseif (property_exists($this, 'restrictedModel')) {
					$model = Model::factory($this->restrictedModel);
					if (!is_null($model->field('code'))) {
						return 'code';
					}
				}
				break;
			case 'forgot':
				if (property_exists($this, 'restrictedForgot')) {
					return $this->restrictedForgot;
				} elseif (property_exists($this, 'restrictedModel')) {
					return true;
				} else {
					return false;
				}
				break;
			case 'i18nSection':
				return property_exists($this, 'restrictedI18nSection') ? $this->restrictedI18nSection : 'PICO';
				break;
			case 'view':
				return property_exists($this, 'restrictedView') ? $this->restrictedView : '_restricted.twig';
				break;
		}
		return NULL;
	}


	public function restrictedCookie($name, $value=NULL, $clear=false, $lifetime='30days') {
		global $pico;
		return $pico->cookie($this->restrictedProp('prefix').'Restricted'.ucfirst($name), $value, $clear, $lifetime);
	}


	private function restrictedSession($name, $value=NULL, $clear=false) {
		global $pico;
		return $pico->session($this->restrictedProp('prefix').'Restricted'.ucfirst($name), $value, $clear);
	}


	private function restrictedToken($password, $code='') {
		return md5($password.$code.date('Ym'));
	}

}