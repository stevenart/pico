class AIBot {

	config = {};

	key = '';

	maxTokens = 1000;

	messages = [];

	ready = false;


	constructor(key, onSuccess, onError) {
		let aibot = this;
		$.ajax({
			data: 'key='+key,
			dataType: 'json',
			method: 'POST',
			url: pico.href(':cms/aibots/ajax/config'),
			success: function(result) {
				if (result.success) {
					aibot.config = result.config;
					aibot.messages.push({
						'role': 'system',
						'content': result.config.system,
					});
					if (typeof onSuccess==='function') {
						onSuccess(aibot.config);
					}
					aibot.ready = true;
				} else {
					if (typeof onError==='function') {
						onError(result);
					} else {
						console.log('[AIBot:constructor] Failed to load bot "'+config+'"');
					}
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				if (typeof onError==='function') {
					onError('[AIBot:constructor] '+textStatus, jqXHR);
				} else {
					console.log('[AIBot:constructor] '+textStatus+': '+errorThrown);
				}
			}
		});
	};


	chat(prompt, onResponse, onError) {
		this.messages.push({
			'role': 'user',
			'content': prompt,
		});
		let aibot = this;
		$.ajax({
			data: 'key='+this.config.key+'&messages='+encodeURIComponent(JSON.stringify(this.messages)),
			dataType: 'json',
			method: 'POST',
			url: pico.href(':cms/aibots/ajax/chat'),
			success: function(result) {
				if (result.success) {
					let response = result.response.choices[0];
					response['done'] = true;
					onResponse(response);
					aibot.messages.push(response.message);
				} else {
					onError('[AIBot:chat] Server did not return success code', result);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				onError('[AIBot:chat] '+textStatus+': '+errorThrown, jqXHR);
			}
		});
	};


	async stream(prompt, onResponse, onError) {
		this.messages.push({
			'role': 'user',
			'content': prompt,
		});
		let body = new FormData();
		body.append('key', this.config.key);
		body.append('messages', JSON.stringify(this.messages));
	    const stream = await fetch(pico.href(':cms/aibots/ajax/stream'), {
	    	method: 'POST',
			headers: {
				'X-Requested-With': 'XMLHttpRequest',
			},
			body: body
	    });
	    const reader = stream.body.getReader();
		let i = 0;
		let message = false;
		while(i<this.maxTokens) {
			i = i + 1;
			const { done, value } = await reader.read();
			if (!done) {
				for (let chunk of this.parse(value)) {
					if (chunk[0] === 0x1E) {
						chunk = chunk.substr(1);
					}
					try {
						chunk = JSON.parse(chunk);
						let response = chunk.response.choices[0];
						if (response['finishReason']!==null) {
							response['done'] = true;
							i = this.maxTokens;
						} else if (i === this.maxTokens) {
							response['done'] = true;
							response['finishReason'] = 'maxTokens';
						}
						if (response.delta) {
							if (message===false) {
								message = response.delta;
							} else {
								message['content'] = message['content'] + response.delta['content'];
							}
						}
						onResponse(response);
					} catch (e) {
						onError('[AIBot:stream] Failed to Parse JSON', e);
						continue;
					}
				}
			}
		}
		this.messages.push(message);
	};


	parse(value) {
		return new TextDecoder().decode(value).trim().split(/\r?\n/);
	};


};