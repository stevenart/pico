$(document).ready(function(){


	/* [cmsItemFieldset] Open */
	$(document).on('click', '.cmsItemFieldset [data-action="open"]', function(e){
		if (!$(this).hasClass('isDisabled')) {
			$(this).parents('.cmsItemFieldset').removeClass('isClosed');
		}
		e.preventDefault();
	});


	/* [cmsItemField] Code modal */
	$(document).on('click', '.cmsItemField .code a[data-action="modal"]', function(e){
		if (!$(this).hasClass('isDisabled')) {
			$(this).parent().find('.modal').first().removeClass('isHidden');
		}
		e.preventDefault();
	});


	/* [cmsItems] Stop data-href on links */
	$(document).on('click', '.cmsItem[data-href] a, .cmsItem[data-href] .dropdown', function(e){
		e.stopPropagation();
	});


	/* [cmsItems] Search */
	$(document).on('click', '.cmsItems .cmsItemsTabs a[data-action="search"]', function(e){
		if (!$(this).hasClass('isDisabled')) {
			const cmsItems = $(this).parents('.cmsItems');
			cmsItems.addClass('isSearching');
			cmsItems.find('.cmsItemsTabs > nav a, .cmsItemsTabs > .dropdown').removeClass('isCurrent');
			$(this).addClass('isCurrent');
			cmsItems.find('.cmsItemsSearchQuery').focus();
		}
		e.stopPropagation();
		e.preventDefault();
	});
	const searchDelay = (function(){
		let timer = 0;
		return function(callback, ms){
			clearTimeout(timer);
			timer = setTimeout(callback, ms);
		};
	})();
	$('.cmsItems .cmsItemsSearchQuery')
		.keyup(function(e){ 
			var searchQuery = $.trim($(this).val());
			if (e.which==13) {
				if (searchQuery.length>2) {
					$(this).trigger('blur');
				}
			} else {
				const cmsItems = $(this).parents('.cmsItems');
				searchDelay(function(){
					cmsItems.find('.cmsItemsSearchResults').html('');
					if (searchQuery.length>2) {
						cmsItems.find('.cmsItemsSearchEmpty').addClass('isHidden');
						cmsItems.find('.cmsItemsSearchHelp').addClass('isHidden');
						$.ajax({
							data: 'query='+searchQuery,
							dataType: 'json',
							method: 'POST',
							url: pico.href('_/ajax/search'),
							success: function(result) {
								if (result.success) {
									if (result.count>0) {
										cmsItems.find('.cmsItemsSearchResults').html(result.html);
									} else {
										cmsItems.find('.cmsItemsSearchEmpty').removeClass('isHidden');
									}
								}
							}
						});
					} else {
						cmsItems.find('.cmsItemsSearchEmpty').addClass('isHidden');
						cmsItems.find('.cmsItemsSearchHelp').removeClass('isHidden');
					}
				}, 300);
			}
		})
		.trigger('keyup');
	

	/* [cmsItems] Sort */
	$(document).on('click', '.cmsItems a[data-action="sort"]', function(e){
		if (!$(this).hasClass('isDisabled')) {
			const cmsItems = $(this).parents('.cmsItems');
			cmsItems.addClass('isSorting');
			const cmsItemsList = cmsItems.find('.cmsItemsList');
			cmsItemsList.find('.cmsItem[data-href]').removeAttr('data-href');
			if (!cmsItemsList.attr('id')) {
				cmsItemsList.attr('id', 'sortable'+Math.random().toString().substring(3));
			}
			let selector = '#'+cmsItemsList.attr('id');
			if (cmsItemsList.find('.cmsItemsSection').length>0) {
				selector = selector + ' .cmsItemsSection';
			}
			sortable(selector, { 
				forcePlaceholderSize: true,
				items: '.cmsItem',
			});
		}
		e.preventDefault();
	});
	$(document).on('click', '.cmsItems a[data-action="sortsave"]', function(e){
		if (!$(this).hasClass('isDisabled')) {
			let ordering = [];
			const cmsItemsList = $(this).parents('.cmsItems').find('.cmsItemsList');
			$(this).parents('.cmsItems').find('.cmsItemsList .cmsItem[data-item]').each(function(value, element){
				ordering.push($(element).data('item'));
			});
			$.ajax({
				data: {'ordering': ordering},
				dataType: 'json',
				method: 'POST',
				url: pico.href('_/ajax/ordering'),
				success: function(result) {
					pico.url('#');
				}
			});
		}
		e.preventDefault();
	});
	$(document).on('click', '.cmsItems a[data-action="sortcancel"]', function(e){
		pico.url('#');
		e.preventDefault();
	});
	

	/* [cmsItems] Star */
	$(document).on('click', '.cmsItem a[data-action="star"]', function(e){
		if (!$(this).hasClass('isDisabled')) {
			let cmsItem = $(this).parents('.cmsItem');
			if (!$(cmsItem).hasClass('isDisabled')) {
				$.ajax({
					data: 'id='+cmsItem.data('item'),
					dataType: 'json',
					method: 'POST',
					url: pico.href('_/ajax/star'),
					success: function(result) {
						if (result.success) {
							cmsItem.toggleClass('isStarred', result.starred);
						}
					}
				});
			}
		}
		e.stopPropagation();
		e.preventDefault();
	});


	/* [form] Submit action */
	$(document).on('click', '.form .formActions a.action-submit', function(e){
		if (!$(this).hasClass('isLoading') && !$(this).hasClass('isDisabled')) {
			$(this).addClass('isLoading');
			$(this).parents('form').submit();
		}
		e.preventDefault();
	});


	/* [form] Continue action */
	$(document).on('click', '.form .formActions a.action-continue', function(e){
		if (!$(this).hasClass('isLoading') && !$(this).hasClass('isDisabled')) {
			$(this).addClass('isLoading');
			const form = $(this).parents('form');
			form.attr('action', form.attr('action')+(form.attr('action').indexOf('?') ? '&' : '?')+'continue=1');
			form.submit();
		}
		e.preventDefault();
	});


	/* [templateItem] Thumbnail zoom */
	$(document).on('click', '.templateItem a.thumbnail', function(e){
		if (!$(this).hasClass('isLoading') && !$(this).hasClass('isDisabled') ) {
			$('#templateItemZoomModal').removeClass('isHidden').addClass('isLoading');
			$('#templateItemZoomTitle').html($(this).attr('href').split('/').pop());
			$('#templateItemZoom').html('');
			let image = $('<img>');
			image.attr('src', $(this).attr('href'));
			image.appendTo('#templateItemZoom');
			$('#templateItemZoomModal').removeClass('isLoading isHidden');
		}
		e.preventDefault();
	});

});