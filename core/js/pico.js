var pico = {
	

	config: {
		'basepath':'', 
		'domain':'',
		'filesize_units':'B KB MB GB TB PB',
		'lang':'', 
		'locale':'', 
		'sessionId':''
	},
	

	bytesize: function(size) {
		var mod = 1024;
		size = parseInt(size);
		units = pico.config.filesize_units.split(' ');
		var i = 0;
		for (i = 0; size > mod; i++) {
			size = size / mod;
		} 
		return (Math.round(size * 100) / 100) + ' ' + units[i];
	},


	init: function(config) {
		$.extend(this.config, config);
	},
	

	href: function(href, includeHost, disableCache) {
		if (typeof includeHost=='undefined') includeHost = true;
		if (typeof disableCache=='undefined') disableCache = false;
		var host = ( includeHost ? this.config.basepath : '' ) + '/';
		if (typeof href=='undefined') return host;
		var url;
		switch(href.substr(0,1)) {
			case ';':
				url = host + href.substr(1);
				break;
			case ':':
				url = host + this.config.lang + '/' + href.substr(1);
				break;
			case '|':
				url = host + href.substr(1) + '/' + window.location.pathname.substr(4) + window.location.search;
				break;
			case '~':
				url = host + window.location.pathname.substr(1) + '/' + href.substr(1);
				break;
			case '#':
				url = host + window.location.pathname.substr(1) + ( href.substr(1).length>0 ? ( window.location.search == '' ? '?' : window.location.search+'&' ) + href.substr(1) : '');
				break;
			case '_':
				const parts = window.location.pathname.split('/');
				if (parts.length>3) {
					url = host + '/' + parts[1] + '/' + parts[2] + '/' + parts[3];
				} else if (parts.length>2) {
					url = host + '/' + parts[1] + '/' + parts[2];
				} else {
					url = host + '/' + parts[1];
				}
				url = url + '/' + href.substr(1);
				break;
			case '$':
				url = host + 'img.php?file=' + href.substr(1);
			default:
				url = href;
				break;	
		}
		url = url.replace(/\/\//g, '/');
		url = url.replace(':/', '://');
		return url;
	},


	inArray: function(needles, haystack, matchAll, startsWith) {
		/*
			If matchAll==true (default), returns true if ALL needles are found in haystack.
			If matchAll==false, returns true if ONE needle are found in haystack.
		*/
		if (typeof needles!=='object') {
			needles = (""+needles).split(',');
		}
		if (typeof haystack!=='object') {
			haystack = (""+haystack).split(',');
		}
		if (typeof matchAll==='undefined') {
			matchAll = true;
		}
		if (typeof startsWith==='undefined') {
			startsWith = false;
		}
		var result = matchAll;
		for (var i=0; i<needles.length; ++i) {
			if (startsWith) {
				var needleResult = false;
				for (var j=0; j<haystack.length; ++j) {
					if (haystack[j].startsWith(needles[i])) {
						needleResult = true;
						break;
					}
				}
				if (matchAll) {
					result = result && needleResult;
				} else  {
					result = result || needleResult;
				}
			} else {
				if (matchAll) {
					result = result && haystack.includes(needles[i]);
				} else  {
					result = result || haystack.includes(needles[i]);
				}
			}
		}
		return result;
	},


	isEmail: function(email) {
		const regex =  new RegExp('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]+)$', 'i');
		return email.toString().match(regex);
	},
	

	isTouch: function() {
		return ('ontouchstart' in window) || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0);
	},


	nl2br: function(str) {
		return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1<br>$2');
	},
	

	serializedAdd: function(element, value) {
		var cleanValues = [];
		var cleanValue = '';
		var newValue = '';
		if ( $('#'+element).val()=='' ) {
			newValue = ''+value; 	
		} else {
			newValue = $('#'+element).val()+','+value;
		} 
		var newValues = newValue.split(',');
		for (var i=0; i<newValues.length; ++i) {
			if (!pico.inArray(newValues[i], cleanValues)) {
				cleanValues.push(newValues[i]);
			}
		}
		$('#'+element).val(cleanValues.join(','));
		return cleanValues;
	},


	serializedCount: function(element) {
		var values = $('#'+element).val().split(',');
		var actualValues = [];
		for (var i=0; i < values.length; ++i) {
			if (values[i]!='') actualValues.push(values[i]);
		}
		return actualValues.length;
	},


	serializedCut: function(element, length) {
		var values = $('#'+element).val().split(',');
		var newValues = [];
		for (var i=0; i < values.length; ++i) {
			if (values[i]!='') newValues.push(values[i]);
		}
		newValues = newValues.slice(newValues.length-length);
		$('#'+element).val(newValues.join(','));
		return newValues;
	},


	serializedExists: function(element, value) {
		return pico.inArray(value, $('#'+element).val().split(','));
	},


	serializedFromDom: function(element, domElementId, valueAttr) {
		var values = [];
		var value = '';
		$('#'+element).val('');
		$('#'+domElementId).children().each(function() {
			value = $(this).attr(valueAttr);
			if (value!='') values.push(value);
		});
		$('#'+element).val(values.join(','));
		return values;
	},


	serializedRemove: function(element, value) {
		if ($('#'+element).val()=='') return;i
		var values = $('#'+element).val().split(',');
		var newValues = [];
		for (var i=0; i < values.length; ++i) {
			if (values[i]!=value) newValues.push(values[i]);
		}
		$('#'+element).val(newValues.join(','));
		return newValues;
	},


	stripTags: function(value) {
		return $.trim(value.replace(/<(\w+)[^>]*>.*<\/\1>/gi,''));
	},


	url: function(href, blank) {
		var url = this.href(href);
		if (typeof blank=='undefined') {
			if (url.indexOf('//') !== -1) {
				blank = (url.substr(0, pico.config.basepath.length)!=pico.config.basepath);
			} else {
				blank = false;
			}
		}
		if (blank) {
			window.open(url);
		} else {
			window.location.href = url;
		}
	}


};


$(document).ready(function(){


	/* Loading */
	$('html').addClass('loaded');


	/* data-href attribute */
	var ctrlPressed = false;
	$(window).keydown(function(e) {
		if ((e.which == 17) || (e.which == 91) || (e.which == 224)) {
			ctrlPressed = true;
		}
	}).keyup(function(e) {
		if ((e.which == 17) || (e.which == 91) || (e.which == 224)) {
			ctrlPressed = false;
		}
	});
	$(document).on('click', '[data-href]', function(e){
		pico.url($(this).data('href'), ctrlPressed ? true : undefined);	
		e.stopPropagation();
		e.preventDefault();
	});


	/* Dropdowns */
	$(document).on('click', '.dropdown .dropdownAction', function(e){
		const activate = !$(this).parent().hasClass('isActive');
		$('.dropdown').removeClass('isActive');
		if (activate) {
			$(this).parent().addClass('isActive');
		}
	});
	$(document).on('click', '.dropdown .dropdownOverlay', function(e){
		$(this).parent().removeClass('isActive');
	});


	/* Inputs */
	$(document).on('click', '.input > i', function(e){
		$(this).parent()
			.find('input[type="color"], input[type="date"], input[type="datetime-local"], input[type="email"], input[type="file"], input[type="number"], input[type="password"], input[type="search"], input[type="tel"], input[type="text"], input[type="time"], input[type="url"], select, textarea')
			.first().trigger('focus');
	});


	/* Modals */
	$(document).on('click', '.modal [data-action="modalclose"]', function(e){
		$(this).parents('.modal').first().addClass('isHidden');
		e.preventDefault();
	});
	$(document).on('click', '.modal .modalOverlay', function(e){
		$(this).parents('.modal').first().addClass('isHidden');
		e.preventDefault();
	});


	/* Fieldsets */
	$(document).on('click', '.formFieldset [data-action="open"]', function(e){
		if (!$(this).hasClass('isDisabled')) {
			$(this).parents('.formFieldset').removeClass('isClosed');
		}
		e.preventDefault();
	});


});