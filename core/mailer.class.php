<?php

/**
 * Mailer class
 * This class extends PHPMailer to add SoapMailer support.
 */
class Mailer extends PHPMailer\PHPMailer\PHPMailer {

	public $SoapmailerServer = '';
	public $SoapmailerKey = '';

	protected function soapmailerSend($header, $body) {
		if ($this->SoapmailerServer) {
			if (substr($this->SoapmailerServer, -1, 1)!='/') {
				$this->SoapmailerServer = $this->SoapmailerServer.'/';
			}
			$opts = array(
				'ssl' => array(
					'verify_peer' => false, 
					'verify_peer_name' => false
				)
			);
			$client = new \SoapClient(NULL, array(
				'location' => $this->SoapmailerServer.'api.php',
				'uri'      => $this->SoapmailerServer,
				'stream_context' => stream_context_create($opts)
			));
			$mailData = array(
				'subject'=>$this->Subject,
			);
			/* From */
			if (is_array($this->From)) {
				$mailData['from'] = array('email'=>$this->From[0], 'name'=>$this->From[1]);
			} elseif ($this->FromName) {
				$mailData['from'] = array('email'=>$this->From, 'name'=>$this->FromName);
			} else {
				$mailData['from'] = $this->From;
			}
			/* To */
			$mailData['to'] = array();
			foreach($this->to as $foo) {
				if (is_array($foo)) {
					$mailData['to'][] = array('email'=>$foo[0], 'name'=>$foo[1]);
				} else {
					$mailData['to'][] = $foo;
				}
			}
			/* Cc */
			$mailData['cc'] = array();
			foreach($this->cc as $foo) {
				if (is_array($foo)) {
					$mailData['cc'][] = array('email'=>$foo[0], 'name'=>$foo[1]);
				} else {
					$mailData['cc'][] = $foo;
				}
			}
			if (count($mailData['cc'])==0) unset($mailData['cc']);
			/* Bcc */
			$mailData['bcc'] = array();
			foreach($this->bcc as $foo) {
				if (is_array($foo)) {
					$mailData['bcc'][] = array('email'=>$foo[0], 'name'=>$foo[1]);
				} else {
					$mailData['bcc'][] = $foo;
				}
			}
			if (count($mailData['bcc'])==0) unset($mailData['bcc']);
			/* Body */
			if ($this->ContentType!='text/plain') {
				$mailData['html'] = $this->Body;
				if ($this->AltBody) {
					$mailData['text'] = $this->AltBody;
					$mailData['type'] = 'both'; 
				} else {
					$mailData['type'] = 'html'; 
				}
			} else {
				$mailData['text'] = $this->Body;
				$mailData['type'] = 'text'; 
			}
			try {
				$result = $client->__call('send', array($this->SoapmailerKey, $mailData));
			}
			catch (\Exception $e) {
				throw new phpmailerException($result['message'], self::STOP_CONTINUE);
			}
			if ($result['ok']===true) {
				return true;
			} else {
				throw new phpmailerException($result['message'], self::STOP_CONTINUE);
			}
		} else {
			throw new phpmailerException('Soapmailer server URL not defined.', self::STOP_CRITICAL);
		}
		return false;
	}

}
	