<?php

class AI {


	public function fixText($text) {
		global $pico;

		$ollama = \ArdaGnsrn\Ollama\Ollama::client('http://192.168.178.29:11434');

/*
		$response = $ollama->chat()->create([
			'model' => 'llama3.1',
			'format' => 'json',
			'messages' => [
				['role' => 'system', 'content' => 'Tu es un spécialiste SEO francophone qui travaille dans une agence de communication web à Luxembourg. Tu réponds uniquement la réponse à la question, sans détail ou politesse. Si le texte contient de l\'HTML, fait attention de garder le code correct.'],
				['role' => 'system', 'content' => 'Retourne un tableau JSON avec le résultat dans la clé "page_content".'],
				['role' => 'user', 'content' => "Corriges les fautes d'orthographe dans ce texte :\n ".$text],
			],
		]);
*/
		$response = $ollama->chat()->create([
			'model' => 'llama3.1',
			'format' => 'json',
			'messages' => [
				['role' => 'system', 'content' => 'Tu es un spécialiste SEO francophone qui travaille dans une agence de communication web à Luxembourg nommée MICROMEGAS. Tu réponds uniquement la réponse à la question, sans détail ou politesse. Si le texte contient de l\'HTML, fait attention de garder le code correct.'],
				['role' => 'system', 'content' => "Retourne un tableau JSON avec le titre dans la clé 'title', la description dans la clé 'description' et les mots-clés dans la clé 'keywords'"],
				['role' => 'user', 'content' => "Ecris un titre, une description et des mots-clés pour les balises meta à partir du texte suivant. Essaye d'écrire un texte également agréable à lire pour les humains.\n\n ".$text],
			],
		]);

		return $response->message->content;


	}


}

