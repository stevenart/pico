<?php

class AIBotModel extends Model {


	public function chat($messages, $index=0) {
		global $pico;
		if ($this->fetched(0)) {
			$client = OpenAI::factory()
				->withApiKey($this->value('apikey', $index))
				->withBaseUri($this->value('server', $index))
				->make();
			$response = $client->chat()->create([
				'model' => $this->value('model', $index),
				'messages' => $messages,
			]);
			return $response->toArray();
		} else {
			return false;
		}
	}


	public function stream($messages, $index=0) {
		global $pico;
		if ($this->fetched(0)) {
			$client = OpenAI::factory()
				->withApiKey('ollama')
				->withBaseUri('http://192.168.178.29:11434/v1')
				->make();
			$stream = $client->chat()->createStreamed([
				'model' => $this->value('model', $index),
				'messages' => $messages,
			]);
			foreach($stream as $response) {
				yield $response;
			}
		} else {
			return false;
		}
	}


}