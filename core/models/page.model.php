<?php

class PageModel extends Model {
	

	public static function getTree($lang=NULL, $loadPluginChildren=false) {
		global $pico;
		if (is_null($lang)) {
			$lang = $pico->i18n->lang();
		}
		$page = Model::factory('Page');
		$page->fetch('`lang`="'.safer($lang, 'db').'" AND `__draft`=0', '`navigation` ASC, `parent` ASC, `__ordering` ASC, `id` ASC');
		$tree = array();
		foreach($page->field('navigation', 'options') as $navigation => $foo) {
			$tree[$navigation] = array();
		}
		foreach($page->values() as $p) {
			if (isset($tree[$p['navigation']])) {
				if (is_null($p['parent'])) {
					$tree[$p['navigation']][$p['id']] = $p;
					/* Load plugin children if class::_children exists */
					if ($loadPluginChildren && ($p['type']=='plugin')) {
						$pluginPath = explode('_', $p['plugin']);
						if (!isset($pluginPath[1])) {
							$pluginPath[1] = '_';
						}
						$pluginClass = 'Page_'.ucfirst($pluginPath[0]).'Controller';
						if (method_exists($pluginClass, '_children')) {
							$tree[$p['navigation']][$p['id']]['_children'] = $pluginClass::_children();
						}
					}
				} elseif (isset($tree[$p['navigation']][$p['parent']])) {
					if (!isset($tree[$p['navigation']][$p['parent']]['_children'])) {
						$tree[$p['navigation']][$p['parent']]['_children'] = array();
					}
					$tree[$p['navigation']][$p['parent']]['_children'][$p['id']] = $p;
				}
			}
		}
		return $tree;
	}


}