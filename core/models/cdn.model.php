<?php

class CdnModel extends Model {


	public function delete($primaryKey=NULL) {
		global $pico;
		if (!is_null($primaryKey)) {
			if (is_array($primaryKey)) {
				$deletions = $primaryKey;
			} else {
				$deletions = array($primaryKey);
			}
		} else {
			$deletions = array();
			foreach($this->values as $row) {
				$deletions[] = $row['id'];
			}	
		}
		foreach($deletions as $id) {
			array_map('unlink', glob(__ROOT.'/cdn/'.$id.'_*'));
		}
		return $pico->db->exec('DELETE FROM `'.$this->table.'` WHERE `id` in ('.implode(',', $deletions).')');			
	}


	public static function generateThumbnail($filename) {
		global $pico;
		$file = __ROOT.'/cdn/'.$filename;
		$thumbStart = strpos($filename, '_', strpos($filename, '_') + 1) + 1;
		$extLength = strlen($filename) - strrpos($filename, '.');
		$thumb = substr($filename, $thumbStart, strlen($filename) - $thumbStart - $extLength);
		$original = intval(substr($filename, 0, strpos($filename, '_')));
		if (!isset($pico->cdn[$original])) {
			$pico->cdnHref($original); // Try on-demand loading
		}
		if (isset($pico->cdn[$original])) {
			$originalFile = str_replace('_'.$thumb, '', $file);
			$originalFile = substr($originalFile, 0, strrpos($originalFile, '.')).'.'.$pico->cdn[$original]['extension'];
			if (file_exists($originalFile) && method_exists('Thumbnail', $thumb) && in_array($pico->cdn[$original]['extension'], Thumbnail::$driverExtensions)) {
				call_user_func('Thumbnail::'.$thumb, $originalFile, $file);
				return true;
			}
		}
		return false;
	}


}
