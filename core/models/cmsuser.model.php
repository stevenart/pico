<?php

class CMSUserModel extends Model {


	public function __construct($name=NULL) {
		global $pico;
		parent::__construct($name);
		$plugins = array();
		foreach(explode(',', $pico->config['cms']['plugins']) as $plugin) {
			if (substr($plugin, 0, 1)!='-') {
				$plugins[$plugin] = i18n('CMS.LABEL_PLUGIN_'.$plugin);
			}
		}
		$this->fields['plugins']['options'] = $plugins;
	}


	public static function logged() {
		return (new class { 
			use RestrictedControllerTrait;
			private $restrictedModel = 'CMSUser';
			private $restrictedI18nSection = 'CMS';
			private $restrictedPrefix = 'cms';
		})->_authorized();
	}


	public function restrictedLoginCallback() {
		global $pico;
		$pico->session('loggedCMSUserId', $this->value('id'));
		$this->value('logged', 0, date('Y-m-d H:i:s'));
		$this->save($this->value('id'));
	}


	public function save($primaryKey=NULL, $onlyInsert=false, $updateTimestamp=true) {
		global $pico;	
		foreach($this->values as &$row) {
			if (isset($row['twofactor']) && (!$row['twofactor'])) {
				$row['secret'] = NULL;
				$row['code'] = NULL;
			}
		}
		return parent::save($primaryKey, $onlyInsert, $updateTimestamp);
	}


}