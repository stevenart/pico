<?php

class I18nModel extends Model {


	public function delete($primaryKey=NULL) {
		global $pico;
		if (is_string($primaryKey)) {
			$slug = explode('.', $primaryKey);
			if (count($slug)==2) {
				$primaryKey = $slug;
			}
		}
		return parent::delete($primaryKey);
	}


	public function exists($primary) {
		global $pico;
		if (is_string($primary)) {
			$slug = explode('.', $primary);
			if (count($slug)==2) {
				return $pico->db->exists($this->table, '`section`="'.safer($slug[0], 'db').'" AND `key`="'.safer($slug[1], 'db').'"');
			}
		}
		return parent::exists($primary);
	}


	public function fetchPrimary($primary) {
		global $pico;
		if (is_string($primary)) {
			$slug = explode('.', $primary);
			if (count($slug)==2) {
				$rowCount = $this->fetch('`section`="'.safer($slug[0], 'db').'" AND `key`="'.safer($slug[1], 'db').'"', NULL, '0,1');
				return $rowCount;
			}
		}
		return parent::fetchPrimary($primary);
	}


	public function save($primaryKey=NULL, $onlyInsert=false, $updateTimestamp=true) {
		global $pico;
		if (is_string($primaryKey)) {
			$slug = explode('.', $primaryKey);
			if (count($slug)==2) {
				$primaryKey = $slug;
			}
		}
		return parent::save($primaryKey, $onlyInsert, $updateTimestamp);
	}


	public function values($fields=NULL, $joins=NULL, $counts=NULL, $start=0, $limit=9999, $values=NULL) {
		global $pico;
		$values = parent::values();
		foreach($values as &$row) {
			$row['id'] = $row['section'].'.'.$row['key'];
		} 
		return $values;
	}


}