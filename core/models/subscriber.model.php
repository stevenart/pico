<?php

class SubscriberModel extends Model {

	
	public function save($primaryKey=NULL, $onlyInsert=false, $updateTimestamp=true) {
		global $pico;
		if (is_null($primaryKey)) {
			foreach($this->values as $index => $row) {
				if (empty($this->values[$index]['secret'])) {
					$this->values[$index]['secret'] = Tools::generateSecret(8);
				}
				if (empty($this->values[$index]['lang'])) {
					$this->values[$index]['lang'] = $pico->i18n->config('default');
				}
			}
		}
		return parent::save($primaryKey, $onlyInsert, $updateTimestamp);
	}


}