<?php

class EmailRuleModel extends Model {


	public function applyAll() {
		global $pico;
		$condition = $this->getCondition();
		if ($condition) {
			switch ($this->value('action')) {

				case 'spam':
					$email = Model::factory('Email');
					$email->fetch($condition);
					if ($email->fetched()) {
						$email->setColumnValue('spam', 1);
						$email->setColumnValue('rule', $this->value('id'));
						return $email->save();
					} else {
						return 0;
					}
					break;

			}
		}
		return false;
	}


	private static function cleanValue($value) {
		$value = str_replace(array('"', "'"), '', $value);
		$value = str_replace('%', '¤', $value);
		$value = str_replace('*', '%', $value);
		$value = str_replace('¤', '%%', $value);
		return $value;
	}


	public function countAll($includeSpam=true) {
		global $pico;
		$condition = $this->getCondition();
		if ($condition) {
			$email = Model::factory('Email');
			if (!$includeSpam) {
				$condition.= ' AND `spam`=0';
			}
			return $email->count($condition);
		}
		return false;
	}


	public function getCondition($index=0) {
		global $pico;
		if ($this->fetched($index)) {
			$condition = '(1=1)';
			foreach (explode(',', $this->value('target', $index)) as $target) {
				switch ($target) {
					case 'recipient': 
						$condition.= ' AND (`recipientEmail` LIKE "' . self::cleanValue($this->value("recipient", $index)) . '")';
						break;
					case 'subject': 
						$condition.= ' AND (`subject` LIKE "%' . self::cleanValue($this->value("subject", $index)) . '%")';
						break;
					case 'message':
						$condition.= ' AND (`message` LIKE "%' . self::cleanValue($this->value("message", $index)) . '%")';
						break;
				}
			}
			return $condition;
		}
		return false;
	}


	public function verifyRule($mailData, $index=0) {
		global $pico;
		if ($this->fetched($index)) {
			foreach (explode(',', $this->value('target', $index)) as $target) {
				switch ($target) {
					case 'recipient':
						if (stripos($mailData['recipientEmail'], self::cleanValue($this->value("recipient", $index)))!==false) {
							return true;
						} 
						break;
					case 'subject': 
						if (stripos($mailData['subject'], self::cleanValue($this->value("subject", $index)))!==false) {
							return true;
						} 
						break;
					case 'message':
						if (stripos($mailData['message'], self::cleanValue($this->value("message", $index)))!==false) {
							return true;
						} 
						break;
				}
			}
		}
		return false;
	}

}