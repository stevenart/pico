<?php

class ModelFields {


	static private function fields() {
		$fields = [
			'address' => 	array(
								'escape' => true,
								'sql' => '`%name` JSON %null',
								'sql_default' => array('name'=>'address', 'null'=>'NULL'),
							),
			'blocks' => 	array(
								'escape' => true,
								'sql' => '`%name` MEDIUMTEXT %null',
								'sql_default' => array('name'=>'blocks', 'null'=>'NULL'),
							),
			'boolean' => 	array(
								'escape' => false,
								'options' => true,
								'sql' => '`%name` TINYINT(1) %null',
								'sql_default' => array('name'=>'boolean', 'null'=>'NOT NULL', 'default'=>0),
							),
			'cdn' => array(
								'escape' => true,
								'multiple' => true,
								'sql' => '`%name` VARCHAR(255) %null',
								'sql_default' => array('name'=>'cdn', 'null'=>'NULL'),
								'config' => array(
									'max' => 10,
								)
							),
			'choice' => 	array(
								'escape' => true,
								'options' => true,
								'sql' => '`%name` VARCHAR(%maxlength) %null',
								'sql_default' => array('name'=>'choice', 'null'=>'NULL', 'maxlength'=>'255'),
							),
			'choice_int' => array(
								'escape' => false,
								'options' => true,
								'sql' => '`%name` INTEGER(%maxlength) %null',
								'sql_default' => array('name'=>'choice_int', 'null'=>'NULL', 'maxlength'=>'9'),
								'template' => 'choice',
							),
			'choices' => array(
								'escape' => true,
								'options' => true,
								'implode' => true,
								'multiple' => true,
								'sql' => '`%name` TEXT %null',
								'sql_default' => array('name'=>'choices', 'null'=>'NULL'),
							),
			'color' => 		array(
								'escape' => true,
								'sql' => '`%name` VARCHAR(%maxlength) %null',
								'sql_default' => array('name'=>'color', 'null'=>'NULL', 'maxlength'=>'50'),
							),
			'code' => 		array(
								'escape' => true,
								'sql' => '`%name` %rawtype %null',
								'sql_default' => array('name'=>'text', 'null'=>'NULL', 'maxlength'=>'65535'),
							),
			'date' => 		array(
								'escape' => true,
								'sql' => '`%name` DATE %null',
								'sql_default' => array('name'=>'date', 'null'=>'NULL'),
							),
			'datetime' => 	array(
								'escape' => true,
								'sql' => '`%name` DATETIME %null',
								'sql_default' => array('name'=>'datetime', 'null'=>'NULL'),
							),
			'email' => 		array(
								'escape' => true,
								'sql' => '`%name` VARCHAR(%maxlength) %null',
								'sql_default' => array('name'=>'email', 'null'=>'NULL', 'maxlength'=>'150'),
							),
			'float' => 		array(
								'escape' => false,
								'sql' => '`%name` DECIMAL(%precision) %null',
								'sql_default' => array('name'=>'float', 'null'=>'NULL', 'precision'=>'16,8'),
							),
			'html' => 		array(
								'escape' => true,
								'sql' => '`%name` TEXT %null',
								'sql_default' => array('name'=>'text', 'null'=>'NULL'),
							),
			'hidden' => 	array(
								'escape' => true,
								'sql' => '`%name` VARCHAR(%maxlength) %null',
								'sql_default' => array('name'=>'hidden', 'null'=>'NULL', 'maxlength'=>'255'),
							),
			'integer' => 	array(
								'escape' => false,
								'sql' => '`%name` INTEGER(%maxlength) %null',
								'sql_default' => array('name'=>'integer', 'null'=>'NULL', 'maxlength'=>'9'),
							),
			'json' => 	array(
								'escape' => true,
								'sql' => '`%name` JSON %null',
								'sql_default' => array('name'=>'json', 'null'=>'NULL'),
								'template' => 'code',
							),
			'models' => array(
								'escape' => true,
								'sql' => '`%name` JSON %null',
								'sql_default' => array('name'=>'json', 'null'=>'NULL'),
								'config' => array(
									'max' => 10,
								)
							),
			'money' => 		array(
								'escape' => false,
								'sql' => '`%name` DECIMAL(%precision) %null',
								'sql_default' => array('name'=>'float', 'null'=>'NULL', 'precision'=>'8,2'),
							),
			'password' => 	array(
								'escape' => true,
								'sql' => '`%name` VARCHAR(255) %null',
								'sql_default' => array('name'=>'password', 'null'=>'NULL'),
							),
			'text' => 		array(
								'escape' => true,
								'sql' => '`%name` %rawtype %null',
								'sql_default' => array('name'=>'text', 'null'=>'NULL', 'maxlength'=>'65535'),
							),
			'time' => 		array(
								'escape' => true,
								'sql' => '`%name` TIME %null',
								'sql_default' => array('name'=>'time', 'null'=>'NULL'),
							),
			'timestamp' => 	array(
								'escape' => true,
								'sql' => '`%name` TIMESTAMP %null',
								'sql_default' => array('name'=>'timestamp', 'null'=>'NULL'),
							),
			'uri' => 		array(
								'escape' => true,
								'sql' => '`%name` VARCHAR(255) %null',
								'sql_default' => array('name'=>'uri', 'null'=>'NULL'),
							),
			'url' => 		array(
								'escape' => true,
								'sql' => '`%name` VARCHAR(255) %null',
								'sql_default' => array('name'=>'url', 'null'=>'NULL'),
							),
		];
		if (method_exists('Hooks', 'modelFields')) {
			Hooks::modelFields($fields);
		}
		return $fields;
	}

	static public function shouldEscape($type) {
		$escape = false;
		if (!self::exists($type)) {
			error_log('Cannot check escape value for an unknown field type ('.$type.').');
			return false;
		} else {
			if (isset(self::fields()[$type]['escape']) && (self::fields()[$type]['escape']===true)) {
				$escape = true;
			}
		}
		return $escape;
	}
	
	static public function shouldImplode($type) {
		$implode = false;
		if (!self::exists($type)) {
			error_log('Cannot check implode value for an unknown field type ('.$type.').');
			return false;
		} else {
			if (isset(self::fields()[$type]['implode']) && (self::fields()[$type]['implode']===true)) {
				$implode = true;
			}
		}
		return $implode;
	}

	static public function exists($type) {
		return isset(self::fields()[$type]);
	}

	static public function getDefaultConfig($type) {
		if (isset(self::fields()[$type]) && isset(self::fields()[$type]['config']) && is_array(self::fields()[$type]['config'])) {
			return self::fields()[$type]['config'];
		} else {
			return array();
		}
	}

	static public function getSQL($fieldDesc) {
		/* Checking field desc */
		if (!isset($fieldDesc['name'])) {
			error_log('Field has no name !');
			return '';
		} elseif (stripos($fieldDesc['name'], '_')) {
			error_log('Field name contains _ caracters !');
			return '';
		}
		if (!isset($fieldDesc['type'])) {
			error_log('Unknow field type ('.$fieldDesc['type'].') for field "'.$fieldDesc['name'].'"');
			return '';
		}
		$params = array_merge(self::fields()[$fieldDesc['type']]['sql_default'], $fieldDesc);
	
		/* Special operations for special fields */
		switch($params['type']) {
			case 'code':
			case 'text':
				if (intval($params['maxlength'])>65535) $params['rawtype'] = 'LONGTEXT';
				elseif (intval($params['maxlength'])<256) $params['rawtype'] = 'VARCHAR('.intval($params['maxlength']).')';
				else $params['rawtype'] = 'TEXT';
				break;
		} 
	
		/* Name and Null */
		$params['name'] = $fieldDesc['name'];
		if (isset($params['checks']) && in_array('required', $params['checks'])) $params['null'] = 'NOT NULL';
		
		/* Fetch SQL */
		$fieldSql = self::fields()[$params['type']]['sql'];
		foreach($params as $param => $value) {
			if (is_string($value) || is_int($value)) $fieldSql = str_replace("%$param", $value, $fieldSql);
		}

		/* Default */
		if (isset($fieldDesc['default'])) {
			if (self::shouldEscape($params['type'])) {
				$fieldSql.= " DEFAULT '".str_replace("'", "\'", $fieldDesc['default'])."'";
			} else {
				$fieldSql.= " DEFAULT ".$fieldDesc['default'];
			}
		}
		return $fieldSql;
	}

	static public function getTemplate($type) {
		if (isset(self::fields()[$type]) && isset(self::fields()[$type]['template'])) {
			return self::fields()[$type]['template'];
		} else {
			return $type;
		}
	}
	
	static public function isMultiple($type) {
		$multiple = false;
		if (!self::exists($type)) {
			error_log('Cannot check multiple for an unknown field type ('.$type.').');
		} else {
			if (isset(self::fields()[$type]['multiple']) && (self::fields()[$type]['multiple']===true)) {
				$multiple = true;
			}
		}
		return $multiple;
	}
	
	static public function needOptions($type) {
		return ( isset(self::fields()[$type]) && isset(self::fields()[$type]['options']) && self::fields()[$type]['options'] );
	}

}