<?php

/* UTF-8 encoding */
mb_internal_encoding('UTF-8');
mb_regex_encoding('UTF-8');


/* Timezone */
date_default_timezone_set('Europe/Paris');


/* Start session */
if (session_id()=='') session_start();


/* Paths */
if (!defined('__ROOT')) define("__ROOT", substr(dirname(__FILE__), 0, -5));
if (!isset($_SERVER["HTTP_HOST"])) $httpHost = 'localhost';
else $httpHost = $_SERVER["HTTP_HOST"];
if (!defined('__HTTPHOST')) define("__HTTPHOST", $httpHost);
if (!defined('__URL')) define("__URL", (isset($_SERVER['SERVER_PORT']) && (intval($_SERVER['SERVER_PORT'])==443)?'https://':'http://').$httpHost);
if (isset($_GET['__pico_root']) && intval($_GET['__pico_root'])>0) {
	if (!defined('__ROOT_UNSECURE')) define("__ROOT_UNSECURE", true);
} 


/* Composer */
require __ROOT.'/vendor/autoload.php';


/* Autoload */
spl_autoload_register(function($class) {
	$lowerClass = strtolower($class);
	
	/* Pico classes */
	$file = false;
	if (substr($class, -9) === 'CMSPlugin') {
		$file = '/cmsplugins/'.substr($lowerClass, 0, -9).'.cmsplugin.php';
	}	
	if (substr($class, -10) === 'Controller') {
		$file = '/controllers/'.substr($lowerClass, 0, -10).'.controller.php';
	}	
	if (substr($class, -5) === 'Model') {
		$file = '/models/'.substr($lowerClass, 0, -5).'.model.php';
	}
	if (substr($class, -5) === 'Block') {
		$file = '/blocks/'.substr($lowerClass, 0, -5).'.block.php';
	}
	if ($file) {
		if (file_exists(__ROOT.'/app'.$file)) {
			require __ROOT.'/app'.$file;
			return true;
		} elseif (file_exists(__ROOT.'/core'.$file)) {
			require __ROOT.'/core'.$file;
			return true;
		}
	}

	/* Other classes */
	$file = "/$lowerClass.class.php";
	if (file_exists(__ROOT.'/app'.$file)) {
		require __ROOT.'/app'.$file;
		return true;
	} elseif (file_exists(__ROOT.'/core'.$file)) {
		require __ROOT.'/core'.$file;
		return true;
	}
	
	return false;
});


function getMimes() {
	$mimes = array(
		'ai' => 'application/postscript',
		'bmp' => 'image/bmp',
		'doc' => 'application/msword',
		'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
		'eps' => 'application/postscript',
		'gif' => 'image/gif',
		'htm' => 'text/html',
		'html' => 'text/html',
		'ico' => 'image/x-icon',
		'jpeg' => 'image/jpeg',
		'jpg' => 'image/jpeg',
		'key' => 'application/keynote',
		'mid' => 'audio/mid',
		'mov' => 'video/quicktime',
		'mp3' => 'audio/mpeg',
		'mp4' => 'video/mpeg',
		'mpeg' => 'video/mpeg',
		'mpg' => 'video/mpeg',
		'pages' => 'application/pages',
		'pdf' => 'application/pdf',
		'php' => 'text/php',
		'png' => 'image/png',
		'ppt' => 'application/vnd.ms-powerpoint',
		'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
		'ps' => 'application/postscript',
		'rtf' => 'text/rtf',
		'svg' => 'image/svg+xml',
		'swf' => 'application/x-shockwave-flash',
		'tar' => 'application/x-tar',
		'tif' => 'image/tiff',
		'tiff' => 'image/tiff',
		'txt' => 'text/plain',
		'vcard' => 'text/vcard',
		'vcf' => 'text/vcard',
		'wav' => 'audio/x-wav',
		'webp' => 'image/webp',
		'webm' => 'video/webm',
		'xhtml' => 'text/html',
		'xls' => 'application/vnd.ms-excel',
		'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
		'zip' => 'application/zip',
	);
	return $mimes;
}


function appfile($path, $returnFullPath=true, $default=false) {
	if (substr($path, 0, 1)=='/') {
		$path = substr($path, 1);
	}
	if (file_exists(__ROOT."/app/".$path)) {
		if ($returnFullPath) {
			return __ROOT."/app/".$path;
		} else {
			return 'app';
		}
	} elseif (file_exists(__ROOT."/core/".$path)) {
		if ($returnFullPath) {
			return __ROOT."/core/".$path;
		} else {
			return 'core';
		}
	} else {
		return $default;
	}
}


function i18n($slug, $parameters=NULL, $lang=NULL, $create=true, $betterHtml=NULL) {
	global $pico;
	if (strpos($slug, '.')===false) {
		$section = 'GLOBAL';
		$key = $slug;
	} else {
		list($section,$key) = explode('.', $slug);
	}
	if (strpos($section, '|')!==false) {
		$sections = explode('|',  $section);
		foreach($sections as $section) {
			if ($pico->i18n->exists($section, $key)) {
				break;
			}
		}
	}
	if (is_null($betterHtml)) {
		$betterHtml = (substr($key, 0, 4)=='HTML');
	} 
	$text = $pico->i18n->text($section, $key, $parameters, $lang, $create);
	if ($betterHtml) {
		$text = str_replace(
			array(' :', ' !', ' ?', '« ', ' »'), 
			array('&nbsp;:', '&nbsp;!', '&nbsp;?', '«&nbsp;', '&nbsp;»'), 
			$text
		);
	}
	return $text;
}


function safer($value, $context) {
	switch($context) {
		case 'db':
			$value = str_replace('"', "'", $value);
			break;
		case 'dblike':
			$value = str_replace(array('"', '%', '_'), array("'", '\%', '\_'), $value);
			break;
	}
	return $value;
}


function vardie(...$vars) {
	@ini_set('zlib.output_compression', 0);
	@ini_set('implicit_flush', 1);
	@ob_end_clean();
	set_time_limit(0);
	echo '<pre>';
	$backtrace = debug_backtrace();
	varlog(__ROOT);
	echo '<span style="color:#999999;">__ '.substr($backtrace[0]['file'], strlen(__ROOT)).':'.$backtrace[0]['line'].' __</span>'."\n";
	foreach($vars as $var) {
		if (is_bool($var)) {
			$var = ($var ? 'true' : 'false').' (bool)';
		}
		echo print_r($var, true)."\n";

	}
	echo '</pre>';
	die();
}


function vardebug(...$vars) {
	global $pico;
	if ($pico->debugBar) {
		if (count($vars)==1) {
			$var = reset($vars);
			$pico->debugBar['messages']->addMessage($var);
		} else {
			$pico->debugBar['messages']->addMessage($vars);
		}
	}
}


function varlog(...$vars) {
	$backtrace = debug_backtrace();
	error_log('__ '.substr($backtrace[0]['file'], strlen(__ROOT)).':'.$backtrace[0]['line'].' __');
	$line = '';
	foreach($vars as $var) {
		if (is_bool($var)) {
			$var = ($var ? 'true' : 'false').' (bool)';
		}
		$line.= ($line!=='' ? ', ' : '').print_r($var, true);
	}
	error_log($line);

}
