<?php

abstract class CMSPlugin extends Controller {
	

	static public function getPluginConfiguration() {
		return array(
			'actions' 		=> array(
				'index' => array(
					'condition'	=> NULL,
					'counts'	=> NULL,
					'sections'	=> false,
					'item' 		=> false,
					'joins'		=> NULL,
					'order'		=> NULL,
					'pagination'=> 50,
					'parentable'=> false,
					'sections'  => false,
					'sortable' 	=> false,
					'star'		=> false,
					'tabs'		=> false,
					'tabsAll' 	=> false,
				),
				'create' => array(
					'css' 		=> 'isCreate isPrimary',
					'defaults'	=> array(),
					'form'		=> array(
						'actions'	=> array(
							'cancel' => array('type'=>'link','href'=>'_index','text'=>i18n('PICO.ACTION_CANCEL'),'css'=>'button'),
							'submit' => array('type'=>'submit', 'text'=>i18n('CMS.ACTION_SAVE'), 'css'=>'isCreate isPrimary'),
						),
						'css' 	=> 'isCreate',	
					),
					'icon' 		=> 'plus',
					'item' 		=> false,
					'text' 		=> i18n('CMS.ACTION_CREATE'), 
					'title' 	=> i18n('CMS.TITLE_CREATE'), 
				),
				'read'	 => array(
					'counts'	=> NULL,
					'fields' 	=> NULL,
					'icon' 		=> 'preview',
					'include'	=> true,
					'item' 		=> true,
					'joins'		=> NULL,
					'legends'	=> true,
					'text' 		=> i18n('CMS.ACTION_READ'), 
					'title' 	=> i18n('CMS.TITLE_READ'), 
				),
				'preview'	 => array(
					'icon' 		=> 'browser',
					'item' 		=> true,
					'target'	=> 'preview',
					'text' 		=> i18n('CMS.ACTION_PREVIEW'),
					'url'		=> ':',
				),
				'update' => array(
					'css' 		=> 'isUpdate',
					'form'		=> array(
						'actions'	=> array(
							'cancel' => array('type'=>'link','href'=>'_index','text'=>i18n('PICO.ACTION_CANCEL'),'css'=>'button'),
							'continue' => array('type'=>'link', 'text'=>i18n('CMS.ACTION_SAVE_CONTINUE'), 'css'=>'button isUpdate'),
							'submit' => array('type'=>'submit', 'text'=>i18n('CMS.ACTION_SAVE'), 'css'=>'isUpdate isPrimary'),
						),
						'css' 	=> 'isUpdate',	
					),
					'icon' 		=> 'pencil',
					'item' 		=> true,
					'text' 		=> i18n('CMS.ACTION_UPDATE'), 
					'title' 	=> i18n('CMS.TITLE_UPDATE'), 
				),
				'delete' => array(
					'css' 		=> 'isDelete',
					'form'		=> array(
						'actions'	=> array(
							'cancel' => array('type'=>'link','href'=>'_index','text'=>i18n('PICO.ACTION_CANCEL'),'css'=>'button'),	
							'submit' => array('type'=>'submit', 'text'=>i18n('CMS.ACTION_DELETE'), 'css'=>'isDelete isPrimary'),
						),
						'css' 	=> 'isDelete',
						'fieldsets' => false,
					),
					'icon' 		=> 'trash',
					'item' 		=> true,
					'text' 		=> i18n('CMS.ACTION_DELETE'), 
					'title' 	=> i18n('CMS.TITLE_DELETE'), 
				),
				'duplicate'	 => array(
					'css' 		=> 'isCreate',
					'icon' 		=> 'copy',
					'item' 		=> true,
					'text' 		=> i18n('CMS.ACTION_DUPLICATE'),
				),
			),
			'defaultAction' => 'update',
			'draft'			=> false,
			'icon'			=> 'rocket',
			'model' 		=> '',
			'notification' 	=> false,
			'search'		=> true,
			'shortcuts'		=> array(),
			'shortcutsPre'	=> '',
			'templateItem' 	=> NULL,
			'title'			=> '',
			'type'			=> 'plugin',
			'view'			=> 'cms/_cmsplugin.twig'
		);
	}


	public function __construct($name, array $urlPath=NULL, array $config=NULL) {
		if (is_array($config)) {
			$c = Tools::arrayMergeConfig(static::getPluginConfiguration(), $config);
		} else {
			$c = static::getPluginConfiguration();
		}
		if (!$c['templateItem'] && $c['model']) {
			$model = Model::factory($c['model']);
			$c['templateItem'] = $model->getTemplateItem();
		}
		parent::__construct($name, $urlPath, $c);

		global $pico;
		$pico->variable('_pico', array(
			'action' => (is_array($urlPath) && isset($urlPath[1])) ? $urlPath[1] : '_',
			'controller' => 'cms',
			'langs' => $pico->i18n->langs(NULL, false, true),
			'plugin' => $name,
			'pluginCss' => appfile("css/cms/$name.scss", false) ? "cms/$name" : false,
			'search' => (isset($c['search']) && ($c['search'])),
		), false, true);
		$pico->meta('title', $c['title'].' - '.CoreTwig::urlDomain(__URL).' CMS');
		$pico->meta('noindex', true);
	}


	public function _() {
		global $pico;
		$c = static::getPluginConfiguration();
		if (isset($c['actions']['index'])) {
			$pico->url('_index');
		}
	}


	public function ajax() {
		global $pico;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])=='xmlhttprequest') {
			if ($pico->debugBar) {
				$pico->debugBar['messages']->addMessage($_SERVER['REQUEST_URI'] ?? '?', 'request');
				$pico->debugBar->sendDataInHeaders();
			}
			$c = static::getPluginConfiguration();
			$result = ['success' => false];
			switch ($this->path(2)) {	

				case 'ordering':	
					if (isset($_POST['ordering'])) {
						if (is_array($_POST['ordering'])) {
							$model = Model::factory($c['model']);
							$sql = '';
							foreach($_POST['ordering'] as $index => $id) {
								$sql.= 'UPDATE `'.$model->table().'` SET `__ordering`='.intval($index).' WHERE `id`='.intval($id).'; ';
							}
						}
						$pico->db->exec($sql);
						$result['success'] = true;
					}
					break;

				case 'search':	
					if ($_POST['query'] && (strlen($_POST['query'])>2)) {
						$pluginConfig = static::getPluginConfiguration();
						if ($pluginConfig['search']) {
							$model = Model::factory($pluginConfig['model']);
							$model->search(
								is_array($pluginConfig['search']) ? $pluginConfig['search'] : NULL, 
								$_POST['query']
							);
							$result['success'] = true;
							$result['count'] = $model->fetched();
							if ($model->fetched()) {
								$result['html'] = $pico->twig->render('cms/_cmsplugin.items.twig', [
									'fields' => $model->getFields(),
									'items' => $model->values(NULL, $c['actions']['index']['joins'], $c['actions']['index']['counts']),
									'itemsTemplateItem' => $pluginConfig['templateItem'] ?: $model->getTemplateItem(),
									'sections' => false,
								]);
							}
						}
					} else {
						$result['title'] = $pluginConfig['title'];
					}
					$result['success'] = true;
					break;

				case 'star':	
					if (isset($_POST['id'])) {
						$id = intval($_POST['id']);
						$model = Model::factory($c['model']);
						if ($model->fetchPrimary($id)) {
							if (intval($model->value('__starred'))>0) {
								$sql = "UPDATE `{$model->table()}` SET `__starred`=0 WHERE `id`=$id;";
							} else {
								$sql = "UPDATE `{$model->table()}` SET `__starred`=1 WHERE `id`=$id;";
							}
							$pico->db->exec($sql);
							$result['success'] = true;
						}
					}
					break;
			}
			echo json_encode($result);
			die();
		} else {
			$pico->url(':');
		}
	}


	public function create() {
		global $pico;
		$c = static::getPluginConfiguration();
		if (isset($c['actions'][$this->path(1)])) {
			$model = Model::factory($c['model']);
			$canSave = $model->loadForm();
			if ($canSave===true) {
				$model->save();
				$pico->url('_index');
			}
			if ($canSave===false) {
				if (isset($_GET['duplicate'])) {
					$model->fetchPrimary($_GET['duplicate']);
				} else {
					$defaults = $c['actions'][$this->path(1)]['defaults'] ?? [];
					foreach(array_intersect(array_keys($_GET), array_keys($model->getFields())) as $field) {
						$defaults[$field] = $_GET[$field];
					}
					foreach($defaults as $field => $value) {
						$model->value($field, 0, $value);
					}
				}
			}
			if (!isset($c['actions'][$this->path(1)]['form'])) {
				$c['actions'][$this->path(1)]['form'] = array();
			}
			if (isset($c['draft']) && ($c['draft'])) {
				$c['actions'][$this->path(1)]['form']['internalRemove'] = '__draft';
			}
			$c['actions'][$this->path(1)]['form']['errors'] = $canSave;
			$this->variable('form', $model->form($c['actions'][$this->path(1)]['form']));

		} else {
			$pico->url('_index');
		}
	}


	public function delete() {
		global $pico;
		$c = static::getPluginConfiguration();
		if (isset($c['actions'][$this->path(1)])) {
			$model = Model::factory($c['model']);
			if (!is_null($this->path(2)) && $model->exists($this->path(2))) {
				$canDelete = $model->loadForm();
				if ($canDelete!==false) {
					$model->delete($this->path(2));
					$pico->url('_index');
				} else {
					$model->fetchPrimary($this->path(2));
				}
				if (!isset($c['actions'][$this->path(1)]['form'])) {
					$c['actions'][$this->path(1)]['form'] = array();
				}
				$c['actions'][$this->path(1)]['form']['disabled'] = true;
				$this->variable('_id', $this->path(2));
				if (!isset($c['actions'][$this->path(1)]['form']['fields'])) {
					$c['actions'][$this->path(1)]['form']['fields'] = array();
				}
				$this->variable('form', $model->form($c['actions'][$this->path(1)]['form']));

				$this->variable('item', $model->one(NULL, $c['actions']['index']['joins'], $c['actions']['index']['counts']));
			} else {
				$pico->url('_index');
			}
		} else {
			$pico->url('_index');
		}
	}


	public function duplicate() {
		global $pico;
		$c = static::getPluginConfiguration();
		if (isset($c['actions'][$this->path(1)])) {
			$model = Model::factory($c['model']);
			if (!is_null($this->path(2)) && $model->exists($this->path(2))) {
				$pico->url('_create?duplicate='.$this->path(2));
			} else {
				$pico->url('_index');
			}
		} else {
			$pico->url('_index');
		}
	}


	public function fetch() {
		global $pico;
		$c = static::getPluginConfiguration();
		return $pico->twig->render($c['view'], $this->variables);
	}


	public function index() {
		global $pico;
		$c = static::getPluginConfiguration();
		if ($c['model'] && isset($c['actions'][$this->path(1)])) {
			$model = Model::factory($c['model']);
			$fields = $model->getFields();
			$this->variable('fields', $fields);
			/* Default tab */
			$tabs = [
				'_all' => [
					'condition' => '(1=1)',
					'id' => '_all',
					'order' => $c['actions'][$this->path(1)]['order'],
					'pagination' => $c['actions'][$this->path(1)]['pagination'],
					'sections' => $c['actions'][$this->path(1)]['sections'],
					'shortcuts' => $c['shortcuts'],
					'shortcutsPre' => $c['shortcutsPre'],
					'templateItem' => $c['templateItem'] ?: $model->getTemplateItem(),
					'text' => i18n('CMS.LABEL_TAB_ALL'),
				]
			];
			/* Custom tabs */
			if (isset($c['actions'][$this->path(1)]['tabs'])) {
				if (is_string($c['actions'][$this->path(1)]['tabs'])) {
					$tf = $c['actions'][$this->path(1)]['tabs'];
					if (isset($fields[$tf])) {
						if (isset($fields[$tf]['options'])) {
							foreach($fields[$tf]['options'] as $oid => $otext) {
								$tabs[$oid] = Tools::arrayMergeConfig($tabs['_all'], [
									'condition' => '`'.$tf.'`="'.safer($oid, 'db').'"',
									'id' => $oid,
									'text' => $otext,
									'params' => $tf.'='.$oid,
								]);
							}
						} else {
							$options = $pico->db->column($model->table(), 'DISTINCT(`'.$tf.'`)', NULL, '`'.$tf.'`');
							foreach($options as $oid) {
								$tabs[$oid] = Tools::arrayMergeConfig($tabs['_all'], [
									'condition' => '`'.$tf.'`="'.safer($oid, 'db').'"',
									'id' => $oid,
									'text' => $oid,
								]);
							}
						}
					}
				} elseif (is_array($c['actions'][$this->path(1)]['tabs'])) {
					foreach($c['actions'][$this->path(1)]['tabs'] as $tabId => $tabData) {
						if (isset($tabData['id'])) {
							$c['actions'][$this->path(1)]['tabs'][$tabId] = Tools::arrayMergeConfig($tabs['_all'], $c['actions'][$this->path(1)]['tabs'][$tabId]);
						} else {
							unset($c['actions'][$this->path(1)]['tabs'][$tabId]);
						}
					}
					$tabs+= $c['actions'][$this->path(1)]['tabs'];
				}
				if (count($tabs)>1 && !($c['actions'][$this->path(1)]['tabsAll'])) {
					unset($tabs['_all']);
				}
			}
			/* Tab text length */
			$tabsTextLength = 0;
			foreach($tabs as $foo) {
				$tabsTextLength+= strlen($foo['text']);
			}
			/* Current tab */
			if (isset($_GET['tab']) && isset($tabs[$_GET['tab']])) {
				$pico->session($this->getName().'_tab', $_GET['tab']);
				$pico->session($this->getName().'_page', 1);
			}
			$tab = $pico->session($this->getName().'_tab');
			if (is_null($tab) || !isset($tabs[$tab])) {
				$tab = array_key_first($tabs);
			}
			/* Buidling tab parameters */
			if (is_null($c['actions'][$this->path(1)]['condition'])) {
				$c['actions'][$this->path(1)]['condition'].= $tabs[$tab]['condition'];
			} else {
				$c['actions'][$this->path(1)]['condition'].= ' AND ('.$tabs[$tab]['condition'].')';
			}
			$fetchOrder = $tabs[$tab]['order'];
			$fetchPagination = $tabs[$tab]['pagination'];
			$sections = $tabs[$tab]['sections'];
			$itemsTemplateItem = $tabs[$tab]['templateItem'];
			$itemsShortcuts = $tabs[$tab]['shortcuts'];
			$itemsShortcutsPre = $tabs[$tab]['shortcutsPre'];
			$this->variable('tab', $tab);
			$this->variable('tabs', $tabs);
			$this->variable('tabsTextLength', $tabsTextLength);
			/* Sortable */
			if ($c['actions'][$this->path(1)]['sortable']) {
				if (is_string($fetchOrder)) {
					$fetchOrder = '`__ordering` ASC, '.$fetchOrder;
				} else {
					$fetchOrder = '`__ordering` ASC';
				}
			}
			/* Parentable */
			if ($c['actions'][$this->path(1)]['parentable']) {
				if (!is_string($c['actions'][$this->path(1)]['parentable'])) {
					$c['actions'][$this->path(1)]['parentable'] = 'parent';
				}
				$this->variable('parentField', $c['actions'][$this->path(1)]['parentable']);
				if ($c['actions'][$this->path(1)]['sortable']) {
					$orderFormula = '(IF(`'.$c['actions'][$this->path(1)]['parentable'].'` is null, `__ordering`, (SELECT t2.`__ordering` FROM `'.$model->table().'` t2 WHERE t2.`id`=`'.$model->table().'`.`'.$c['actions'][$this->path(1)]['parentable'].'`))) ASC, (IF(`'.$c['actions'][$this->path(1)]['parentable'].'` is null, `id`*100000, `'.$c['actions'][$this->path(1)]['parentable'].'`*100000)) ASC, ISNULL(`parent`) DESC, `__ordering` ASC';
				} elseif (is_string($fetchOrder)) {
					$mainOrderField = explode(' ', $fetchOrder)[0];
					$orderFormula = '(IF(`'.$c['actions'][$this->path(1)]['parentable'].'` IS NULL, '.$mainOrderField.',CONCAT((SELECT t2.'.$mainOrderField.' FROM `'.$model->table().'` t2 WHERE t2.`id`=`'.$model->table().'`.'.$c['actions'][$this->path(1)]['parentable'].'), \'-\', '.$mainOrderField.'))) ASC, `'.$c['actions'][$this->path(1)]['parentable'].'` ASC, `__ordering` ASC';
				} else {
					$orderFormula = '(IF(`'.$c['actions'][$this->path(1)]['parentable'].'` is null, `id`*100000, `'.$c['actions'][$this->path(1)]['parentable'].'`*100000+`id`))';
				}
				$fetchOrder = $orderFormula;
			}
			/* Sections */
			if (isset($sections)) {
				if (is_string($sections)) {
					if (isset($fields[$sections]['options'])) {
						$sfOrder = '';
						foreach(array_keys($fields[$sections]['options']) as $foo) {
							$sfOrder.= ($sfOrder=='' ? "" : "','").$foo;
						}
						$fetchOrder = "FIELD(`$sections`,'$sfOrder') ASC, $fetchOrder";
					} else {
						$fetchOrder = '`'.$sections.'` ASC, '.$fetchOrder;
					}
					$this->variable('sections', $sections);
				}
			}
			/* Pagination */
			if ($fetchPagination) {
				if (isset($_GET['page'])) {
					$pico->session($this->getName().'_page', intval($_GET['page']));
				}
				$pagination = array(
					'count' => false,
					'current' => $pico->session($this->getName().'_page'),
					'items' => $model->count($c['actions'][$this->path(1)]['condition']),
					'next' => false,
					'prev' => false,
					'size' => is_numeric($fetchPagination) ? intval($fetchPagination) : 20,
				);
				$pagination['count'] = ceil($pagination['items'] / $pagination['size']);
				if (!$pagination['current'] || ($pagination['current']>$pagination['count'])) {
					$pagination['current'] = 1;
				}
				if ($pagination['current']>1) {
					$pagination['prev'] = ($pagination['current']-1);
				}
				if ($pagination['current']<$pagination['count']) {
					$pagination['next'] = ($pagination['current']+1);
				}
				$this->variable('pagination', $pagination);
				$c['actions'][$this->path(1)]['limit'] = (($pagination['current']-1)*$pagination['size']).','.$pagination['size'];
			}
			/* Fetching */
			$model->fetch($c['actions'][$this->path(1)]['condition'], $fetchOrder, $c['actions'][$this->path(1)]['limit']);

			$this->variable('items', $model->values(NULL, $c['actions'][$this->path(1)]['joins'], $c['actions'][$this->path(1)]['counts']));
			$this->variable('itemsTemplateItem', $itemsTemplateItem);
			$this->variable('itemsShortcuts', $itemsShortcuts);
			$this->variable('itemsShortcutsPre', $itemsShortcutsPre);
		}
	}


	public function preview() {
		global $pico;
		$c = static::getPluginConfiguration();
		if (isset($c['actions'][$this->path(1)])) {
			$model = Model::factory($c['model']);
			if (!is_null($this->path(2)) && $model->exists($this->path(2))) {
				$pico->url($c['actions'][$this->path(1)]['url'].'/'.$this->path(2));
			} else {
				$pico->url('_index');
			}
		} else {
			$pico->url('_index');
		}
	}


	public function read() {
		global $pico;
		$c = static::getPluginConfiguration();
		if (isset($c['actions'][$this->path(1)])) {
			$model = Model::factory($c['model']);
			if (!is_null($this->path(2)) && $model->exists($this->path(2))) {
				$fields = $model->getFields();
				if (!empty($c['actions'][$this->path(1)]['fields'])) {
					if ($c['actions'][$this->path(1)]['include']) {
						$fields = array_intersect_key($fields, array_flip($c['actions'][$this->path(1)]['fields']));
					} else {
						$fields = array_diff_key($fields, array_flip($c['actions'][$this->path(1)]['fields']));
					}
				}
				$this->variable('fields', $fields);
				$fieldsets = $model->getFieldsets();
				if (is_array($fieldsets)) {
					$fieldsetted = array();
					foreach($fieldsets as $foo) {
						$fieldsetted = array_merge($fieldsetted, $foo);
					}
					$fieldsets['_'] = array_diff(array_keys($fields), $fieldsetted);
				} else {
					$fieldsets = array('_' => array_keys($fields));
				}
				$fieldsets['__'] = array();
				foreach($fieldsets['_'] as $index => $field) {
					if ($field=='id' || $field=='uri' || $field=='secret' || substr($field, 0, 2)=='__') {
						$fieldsets['__'][] = $field;
						unset($fieldsets['_'][$index]);
					}
				}
				$this->variable('fieldsets', $fieldsets);
				$model->fetchPrimary($this->path(2));
				$this->variable('_id', $this->path(2));
				$this->variable('item', $model->one(array_keys($fields), $c['actions'][$this->path(1)]['joins'], $c['actions'][$this->path(1)]['counts']));
			} else {
				$pico->url('_index');
			}
		} else {
			$pico->url('_index');
		}
	}


	public function update() {
		global $pico;
		$c = static::getPluginConfiguration();
		if (isset($c['actions'][$this->path(1)])) {
			$model = Model::factory($c['model']);
			if (!is_null($this->path(2)) && $model->exists($this->path(2))) {
				$rootData = NULL;
				if ($model->primary()[0]=='id') {
					$rootData = array('id' => $this->path(2));
				}
				$canSave = $model->loadForm(NULL, true, $rootData);
				if ($canSave===true) {
					$model->save($this->path(2));
					if (isset($_GET['continue'])) {
						$pico->url('~');
					} else {
						$pico->url('_index');
					}
				} elseif ($canSave===false) {
					$model->fetchPrimary($this->path(2));
				}
				if (!isset($c['actions'][$this->path(1)]['form'])) {
					$c['actions'][$this->path(1)]['form'] = array();
				}
				if (isset($c['draft']) && ($c['draft'])) {
					$c['actions'][$this->path(1)]['form']['internalRemove'] = '__draft';
				}
				$c['actions'][$this->path(1)]['form']['errors'] = $canSave;
				$this->variable('_id', $this->path(2));
				$this->variable('form', $model->form($c['actions'][$this->path(1)]['form']));
			} else {
				$pico->url('_index');
			}
		} else {
			$pico->url('_index');
		}
	}



}