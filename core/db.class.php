<?php

/**
 * Db class extends PDO, so be sure to check http://php.net/manual/fr/book.pdo.php for more help.
 * 
 * @extends PDO
 */
class Db extends PDO {
	
	public $configuration;

	private $database;
	
	
	public function __construct(array $config) {
		global $pico;
		
		/* Loading and extending config */
		$this->configuration = array_merge(array(
			'constraints'=>1,
			'name'=>'pico',
			'password'=>'root',
			'port'=>'3306',
			'server'=>'localhost',
			'user'=>'root'
		), $config);
		$this->database = $config['name'];
		$this->configuration['constraints'] = (intval($this->configuration['constraints'])>0);
		
		/* Calling PDO with DSN */
		return parent::__construct(	
			'mysql:charset=utf8mb4;host='.$this->configuration['server'].';port='.$this->configuration['port'].';dbname='.$this->configuration['name'], 
			$this->configuration['user'], 
			$this->configuration['password'],
			array(
				PDO::ATTR_ERRMODE => PDO::ERRMODE_SILENT
				//PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING
			)
		);
	}
	
	
	/**
	 * The all function fetches data from a table.
	 * 
	 * @access public
	 * @param string $table Tablename
	 * @param mixed $fields Field(s) you want to fetch. If $fields is an array of fields, they will be escaped (not allowed: *, count, etc.). (default: '*')
	 * @param string $condition [SQL] If set, only rows matching this condition will be fetched (default: '')
	 * @param string $order [SQL] Id set, results will be ordered with given SQL order (default: '')
	 * @param string $limit [SQL] If set, results will be limited with given SQL limit (default: '')
	 * @return mixed array Array of associative arrays of fetched fields.
	 */
	public function all($table, $fields=NULL, $condition=NULL, $order=NULL, $limit=NULL) {
		if (is_null($fields)) $fields = '*';
		elseif (is_array($fields)) $fields = '`'.implode('`, `', $fields).'`';
		$sql = "SELECT $fields FROM `$table`" . (is_null($condition)?'':" WHERE $condition") . (is_null($order)?'':" ORDER BY $order") . (is_null($limit)?'':" LIMIT $limit");
		$result = $this->prepare($sql);
		$result->execute();
		return $result->fetchAll(PDO::FETCH_ASSOC);	
	}
	
	
	/**
	 * The bags function fetches data from a table with a field as the key of the array.
	 * 
	 * @access public
	 * @param string $table Tablename
	 * @param string $keyField Field used for key.
	 * @param string $valueField Field used for value.
	 * @param string [SQL] $condition If set, only rows matching this condition will be fetched (default: '')
	 * @param string [SQL] $order Id set, results will be ordered with given SQL order (default: '')
	 * @param string [SQL] $limit If set, results will be limited with given SQL limit (default: '')
	 * @return void
	 */
	public function bags($table, $keyField, $fields=NULL, $condition=NULL, $order=NULL, $limit=NULL) {
		$values = array();
		$keyField = str_replace('`', '', $keyField);
		if (is_null($fields)) $fields = '*';
		elseif (is_array($fields)) $fields = '`'.implode('`, `', $fields).'`';
		$sql = "SELECT $fields FROM `$table`" . (!is_null($condition)?" WHERE $condition":'') . (!is_null($order)?" ORDER BY $order":'') . (!is_null($limit)?" LIMIT $limit":'');
		$result = $this->prepare($sql);
		$result->execute();
		$rows = $result->fetchAll(PDO::FETCH_ASSOC);	
		foreach($rows as $row) {
			$values[$row[$keyField]] = $row;
		}
		return $values;
	}
	
	
	/**
	 * The column function fetches a column from a table.
	 * 
	 * @access public
	 * @param string $table Tablename
	 * @param string $column [SQL] Column you want to fetch.
	 * @param string $condition [SQL] If set, only rows matching this condition will be fetched (default: '')
	 * @param string $order [SQL] Id set, results will be ordered with given SQL order (default: '')
	 * @param string $limit [SQL] If set, results will be limited with given SQL limit (default: '')
	 * @return mixed array Array of fetched fields.
	 */
	public function column($table, $column, $condition=NULL, $order=NULL, $limit=NULL) {
		$sql = "SELECT $column FROM `$table`" . (!is_null($condition)?" WHERE $condition":'') . (!is_null($order)?" ORDER BY $order":'') . (!is_null($limit)?" LIMIT $limit":'');
		$result = $this->prepare($sql);
		$result->execute();
		return $result->fetchAll(PDO::FETCH_COLUMN, 0);	
	}
	
	
	/**
	 * The count function counts elements in a given table.
	 * 
	 * @access public
	 * @param string $table Tablename
	 * @param string $condition [SQL] If set will count only rows matching this SQL condition (default: '').
	 * @return void
	 */
	public function count($table, $condition=NULL) {
		return intval( $this->value($table, 'COUNT(*)', $condition) );
	}
	
	
	/**
	 * The exists function returns true if there are elements in a given table matching a given condition.
	 * 
	 * @access public
	 * @param string $table Tablename
	 * @param string $condition If set will see only rows matching this SQL condition (default: '').
	 * @return void
	 */
	public function exists($table, $condition=NULL) {
		return ($this->count($table, $condition) > 0);
	}
	

	/**
	 * The groups function fetches data from a table with a field as the non-unique key of the array.
	 * Same as bags but with non-unique key.
	 * 
	 * @access public
	 * @param string $table Tablename
	 * @param string $keyField Field used for key.
	 * @param string $valueField Field used for value.
	 * @param string [SQL] $condition If set, only rows matching this condition will be fetched (default: '')
	 * @param string [SQL] $order Id set, results will be ordered with given SQL order (default: '')
	 * @param string [SQL] $limit If set, results will be limited with given SQL limit (default: '')
	 * @return void
	 */
	public function groups($table, $keyField, $fields=NULL, $condition=NULL, $order=NULL, $limit=NULL) {
		$values = array();
		$keyField = str_replace('`', '', $keyField);
		if (is_null($fields)) $fields = '*';
		elseif (is_array($fields)) $fields = '`'.implode('`, `', $fields).'`';
		$sql = "SELECT $fields FROM `$table`" . (!is_null($condition)?" WHERE $condition":'') . (!is_null($order)?" ORDER BY $order":'') . (!is_null($limit)?" LIMIT $limit":'');
		$result = $this->prepare($sql);
		$result->execute();
		$rows = $result->fetchAll(PDO::FETCH_ASSOC);	
		foreach($rows as $row) {
			if (!isset($values[$row[$keyField]])) {
				$values[$row[$keyField]] = array();
			}
			$values[$row[$keyField]][] = $row;
		}
		return $values;
	}

	
	/**
	 * The info function return informations about a given table.
	 * 
	 * @access public
	 * @param string $table Tablename
	 * @return mixed array Associative array of fetched info.
	 */
	public function info($table) {
		$sql = 'SHOW TABLE STATUS WHERE `Name`="'.$table.'";';
		$result = $this->prepare( $sql );
		$result->execute();
		return $result->fetch(PDO::FETCH_ASSOC);
	}


	/**
	 * The nextId function will return the next available id for a key.
	 * This function automatically creates sequence table if needed.
	 * 
	 * @access public
	 * @param mixed $key Computer-friendly string used to define the key.
	 * @return int Id
	 */
	public function nextId($key) {
		$ok = $this->exec('INSERT INTO `' . $key . '_seq` (sequence) VALUES (NULL)');
		if ($ok === false) {
			$result = $this->exec('CREATE TABLE `' . $key . '_seq` ( sequence INT(9) UNSIGNED NOT NULL AUTO_INCREMENT, PRIMARY KEY (sequence) ) ENGINE=InnoDb DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci');
			return $this->nextId($key);
		} else {
			$id = $this->lastInsertId();
			if (is_numeric($id)) $this->exec('DELETE FROM `' . $key . '_seq` WHERE sequence < ' . $id);
			return $id;
		}
	}
	

	/**
	 * The pairs function fetches pairs (key/value) from a table.
	 * 
	 * @access public
	 * @param string $table Tablename
	 * @param string $keyField [SQL] Field used for key.
	 * @param string $valueField [SQL] Field used for value.
	 * @param string $condition [SQL] If set, only rows matching this condition will be fetched (default: '')
	 * @param string $order [SQL] Id set, results will be ordered with given SQL order (default: '')
	 * @param string $limit [SQL] If set, results will be limited with given SQL limit (default: '')
	 * @return void
	 */
	public function pairs($table, $keyField, $valueField, $condition=NULL, $order=NULL, $limit=NULL) {
		$values = array();
		$result = $this->query("SELECT $keyField, $valueField FROM `$table`" . (!is_null($condition)?" WHERE $condition":'') . (!is_null($order)?" ORDER BY $order":'') . (!is_null($limit)?" LIMIT $limit":''), PDO::FETCH_NUM);
		if (!$result) return array();
		foreach($result as $row) {
			$values[$row[0]] = $row[1];
		}
		return $values;
	}
	

	/**
	 * The row function fetches a row from a table.
	 * If more than one row are validates only the first one is fetched.
	 * 
	 * @access public
	 * @param string $table Tablename
	 * @param string $condition [SQL] If set, only rows matching this condition will be fetched (default: '')
	 * @param string $order [SQL] Id set, results will be ordered with given SQL order (default: '')
	 * @return mixed array Associative array of fetched fields.
	 */
	public function row($table, $condition=NULL, $order=NULL) {
		$sql = "SELECT * FROM `$table`" . (!is_null($condition)?" WHERE $condition":'') . (!is_null($order)?" ORDER BY $order":'') . ' LIMIT 0,1';
		$result = $this->prepare($sql);
		$result->execute();
		return $result->fetch(PDO::FETCH_ASSOC);	
	}
	

	/**
	 * The script function executes commands from a SQL file in /scripts/sql directory.
	 * 
	 * @access public
	 * @param string $scriptName Script name without path or extension.
	 * @return bool
	 */
	public function script($scriptName) {
		$filename = appfile('scripts/sql/'.$scriptName.'.sql');
		if (file_exists($filename)) {
			$statement = $this->prepare(file_get_contents($filename));
			if ($statement->execute()) {
				return $statement->rowCount();
			}
		}
		return false;
	}


	/**
	 * The sql function returns the result of a SQL query.
	 * 
	 * @access public
	 * @param string $sql SQL query.
	 * @return mixed array Array of associative arrays of fields.
	 */
	public function sql($sql) {
		$result = $this->prepare($sql);
		$result->execute();
		return $result->fetchAll(PDO::FETCH_ASSOC);	
	}
	

	/**
	 * The tableAlter function alter a table if needed to match a pico definition.
	 * 
	 * @access public
	 * @param mixed array $definition
	 * @param string $tableName If set, overwrite the table name defined in $definition (default: NULL)
	 */
	public function tableAlter(array $definition, $tableName=NULL) {
		global $pico;

		/* Checking definition */
		$definition = $this->validateTableDefinition($definition, $tableName);

		/* Fetching existing */
		$existing = $this->sql('DESC `'.$definition['table'].'`;');
		$existingFields = array();
		$existingPrimary = array();
		foreach ($existing as $row) {
			$existingFields[$row['Field']] = true;
			if ($row['Key']=='PRI') {
				$existingPrimary[] = $row['Field'];
			}
		}
		$deleteFields = $existingFields;
		
		/* Building fields SQL */
		$sql = '';
		foreach ($existing as $row) {
			$baseFieldName = $row['Field'];
			if ((stripos($row['Field'], '_')!==false) && (stripos($row['Field'], '_')>0)) {
				$baseFieldName = substr($row['Field'], 0, stripos($row['Field'], '_'));
				$existingFields[$baseFieldName] = true;
			}
			if (isset($definition['fields'][$baseFieldName])) {
				unset($deleteFields[$row['Field']]);
				$definition['fields'][$baseFieldName]['name'] = $baseFieldName;
				$fieldSQL = ModelFields::getSQL($definition['fields'][$baseFieldName]);
				if (stripos($fieldSQL, 'NOT NULL')!==false) {
					$fieldNotNull = true;
				} else {
					$fieldNotNull = false;
				}
				$fieldSQL = explode(' ', $fieldSQL);
				$fieldDefault = '';
				if (isset($definition['fields'][$baseFieldName]['default'])) {
					if (ModelFields::shouldEscape($definition['fields'][$baseFieldName]['type'])) {
						$fieldDefault = " DEFAULT '".str_replace("'", "\'", $definition['fields'][$baseFieldName]['default'])."'";
					} else {
						$fieldDefault = " DEFAULT ".$definition['fields'][$baseFieldName]['default'];
					}
				} else {
					$definition['fields'][$baseFieldName]['default'] = NULL;
				}
				if (
					(strtolower($fieldSQL[1])!=$row['Type']) || 
					($row['Default']!=$definition['fields'][$baseFieldName]['default']) || 
					($row['Null']=='NO' && $fieldNotNull===false) || 
					($row['Null']=='YES' && $fieldNotNull===true)
				) {
					$sql.= 'CHANGE `'.$row['Field'].'` `'.$row['Field'].'` '.$fieldSQL[1].($fieldNotNull?' NOT NULL':' NULL').$fieldDefault.', ';
				}
			}
		}
		foreach($deleteFields as $deleteField => $deleteFieldDummy) {
			$sql.= 'DROP `'.$deleteField.'`, ';
		}
		foreach($definition['fields'] as $field => $fieldDefinition) {
			if (!isset($existingFields[$field])) {
				$fieldDefinition['name']= $field;
				$sql.= 'ADD '.ModelFields::getSQL($fieldDefinition).', ';
			} 
		}

		/* Building constraints SQL */
		$indexes = $this->sql('SHOW INDEX FROM `'.$definition['table'].'` where `Key_name`<>"PRIMARY"');
		foreach ($indexes as $ind) {
			$sql.= 'DROP INDEX `'.$ind['Key_name'].'`, ';
		}
		$fkeys = $this->sql('SELECT CONSTRAINT_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE REFERENCED_TABLE_NAME IS NOT NULL AND CONSTRAINT_SCHEMA="'.$this->configuration['name'].'" AND TABLE_NAME = "'.$definition['table'].'"');
		foreach ($fkeys as $fkey) {
			$sql.= 'DROP FOREIGN KEY `'.$fkey['CONSTRAINT_NAME'].'`, ';
		}
		foreach($definition['fields'] as $field => $fieldDefinition) {
			$fieldRequired = false;
			if (isset($fieldDefinition['checks']) && is_array($fieldDefinition['checks'])) {
				foreach($fieldDefinition['checks'] as $fieldCheck) {
					if ($fieldCheck=='required') {
						$fieldRequired = true;
					} elseif ($fieldCheck=='unique' || substr($fieldCheck, 0, 7)=='unique(') {
						if ($this->configuration['constraints']) {
							$sql.= 'ADD UNIQUE KEY `'.$field.'_unique` (`'.$field.'`), ';
						}
					} elseif (substr($fieldCheck, 0, 11)=='references(') {
						if ($this->configuration['constraints']) {
							$references = explode(',', str_replace(['"',"'"], '', substr($fieldCheck, 11, -1)));
							array_walk($references, 'trim');
							if (count($references)>1) {
								$cTable = trim($references[0]);
								$cField = trim($references[1]);
								$cDelete = $references[2] ?? ($fieldRequired ? 'RESTRICT' : 'SET NULL');
								$cUpdate = $references[3] ?? 'CASCADE';
								$sql.= 'ADD FOREIGN KEY (`'.$field.'`) REFERENCES `'.$cTable.'` (`'.$cField.'`) ON DELETE '.$cDelete.' ON UPDATE '.$cUpdate.', ';
							}
						}
					}
				}
			}
		}

		/* Building primary key SQL + Closing */
		if ($sql!='' || ($definition['primary'] != $existingPrimary)) {
			if ($sql=='') $sqlEmpty = true;
			else $sqlEmpty = false;
			if (substr($sql, -2)==', ') $sql = substr($sql, 0, -2);
			$sql = 'ALTER TABLE `'. $definition['table'] .'` '.$sql;
			if ($definition['primary'] != $existingPrimary) {
				$sql.= ($sqlEmpty?'':', ').'DROP PRIMARY KEY, ADD PRIMARY KEY(`'.implode('`,`', $definition['primary']).'`)';
			}
			$sql.= ';';
		}
		
		/* Go! */
		if ($sql!='') {
			if ($this->exec($sql)===false) {
				/* Error */
				$pico->fatal($this->errorInfo());
				return false;
			} else {
				/* Success, let's create empty check file */
				if (!file_exists(__ROOT.'/cache/db')) {
  					@mkdir(__ROOT.'/cache/db', 0777, true);
				}
				$checkFile = __ROOT.'/cache/db/'.$definition['table'].'.check';
				if (file_exists($checkFile)) {
					unlink($checkFile);
				}
				fclose(fopen($checkFile, 'w'));
				/* Mark checked */
				$statement = $this->prepare('CHECK TABLE `'. $definition['table'] .'`;');
				$statement->execute();
			}
		}

		return true;
	}


	/**
	 * The tableCheck function checks if a database table matches a pico definition.
	 * NOTE: This does NOT mark the table checked in MySQL.
	 * 
	 * @access public
	 * @param mixed array $definition
	 * @param string $tableName If set, overwrite the table name defined in $definition (default: NULL)
	 * @param bool $create Should the table be created if it doesn't exists ?
	 * @param bool $alter Should the table be altered to match definition if it's not ?
	 * @param string $file Definition file fullname (if NULL, it will be guessed !)
	 * @return bool	 
	 */
	public function tableCheck(array $definition, $tableName=NULL, $create=false, $alter=false, $file=NULL) {
		global $pico;
		
		/* Checking definition */
		$definition = $this->validateTableDefinition($definition, $tableName);
		if (is_null($file)) {
			$file = appfile('models/'.strtolower($definition['name']).'.model.yaml');
		}

		/* Let's go ! */
		if ($this->tableExists($definition['table'])) {
			$checkFile = __ROOT.'/cache/db/'.$definition['table'].'.check';
			$checkTime = 0;
			if (file_exists($checkFile)) {
				$checkTime = filemtime($checkFile);
			}
			if ((filemtime($file)>$checkTime) || (filemtime(__ROOT.'/app/config.app.ini')>$checkTime)) {
				if ($alter) {
					return $this->tableAlter($definition);
				}
			} else {
				return true;
			}
		} else {
			if ($create) {
				return $this->tableCreate($definition);
			}
		}
		return false;
	}


	/**
	 * The tableCreate function creates a table from a pico definition.
	 * 
	 * @access public
	 * @param mixed array $definition
	 * @param bool $overwrite If true the function will drop any existing table with same name before creating (default: false)
	 * @param string $tableName If set, overwrite the table name defined in $definition (default: NULL)
	 * @return bool True if the table has been created, false otherwise (error or table already exists)
	 */
	public function tableCreate(array $definition, $overwrite=false, $tableName=NULL) {
		global $pico;
		
		/* Checking definition */
		$definition = $this->validateTableDefinition($definition, $tableName);
		
		/* Exists ? Overwrite ? */
		if ( $this->tableExists($definition['table']) ) {
			if ($overwrite) $this->exec('DROP TABLE IF EXISTS `'. $definition['table'] .'`;');
			else return false;
		}

		/* Building fields SQL */
		$sql = 'CREATE TABLE `'. $definition['table'] .'` (';
		foreach($definition['fields'] as $field => $desc) {
			$desc['name'] = $field;
			$sql.= ModelFields::getSQL($desc).', '; 
		}	
		
		/* Building constraints SQL */
		foreach($definition['fields'] as $field => $desc) {
			$fieldRequired = false;
			if (isset($desc['checks']) && is_array($desc['checks'])) {
				foreach($desc['checks'] as $fieldCheck) {
					if ($fieldCheck=='required') {
						$fieldRequired = true;
					} elseif ($fieldCheck=='unique' || substr($fieldCheck, 0, 7)=='unique(') {
						if ($this->configuration['constraints']) {
							$sql.= 'UNIQUE KEY `'.$field.'_unique` (`'.$field.'`), ';
						}
					} elseif (substr($fieldCheck, 0, 11)=='references(') {
						if ($this->configuration['constraints']) {
							$references = explode(',', str_replace(['"',"'"], '', substr($fieldCheck, 11, -1)));
							array_walk($references, 'trim');
							if (count($references)>1) {
								$cTable = trim($references[0]);
								$cField = trim($references[1]);
								$cDelete = $references[2] ?? ($fieldRequired ? 'RESTRICT' : 'SET NULL');
								$cUpdate = $references[3] ?? 'CASCADE';
								$sql.= 'FOREIGN KEY (`'.$field.'`) REFERENCES `'.$cTable.'` (`'.$cField.'`) ON DELETE '.$cDelete.' ON UPDATE '.$cUpdate.', ';
							}
						}
					}
				}
			}
		}

		/* Building primary key SQL + Closing */
		if ( is_array($definition['primary']) ) {
			$sql.= 'PRIMARY KEY(`' . implode('`,`', $definition['primary']) . '`)';
		} else {
			$sql.= 'PRIMARY KEY(`' . $definition['primary'] . '`)';
		}
		$sql.= ') ENGINE=InnoDb DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_unicode_ci;';
		
		/* Go! */
		if ($this->exec($sql)===false) {
			/* Error */
			$pico->fatal($this->errorInfo());
			return false;
		} else {
			/* Success, let's create empty check file */
			if (!file_exists(__ROOT.'/cache/db')) {
				@mkdir(__ROOT.'/cache/db', 0777, true);
			}
			$checkFile = __ROOT.'/cache/db/'.$definition['table'].'.check';
			if (file_exists($checkFile)) {
				unlink($checkFile);
			}
			fclose(fopen($checkFile, 'w'));
			/* Mark checked */
			$statement = $this->prepare('CHECK TABLE `'. $definition['table'] .'`;');
			$statement->execute();
			return true;
		}
	}
	
	
	/**
	 * The tableExists function returns true if a given table exists.
	 * 
	 * @access public
	 * @param string $name The table's name. Note that you can use anything you could use in a sql 'LIKE' statement.
	 * @return void
	 */
	public function tableExists($name) {
		$result = $this->prepare("SHOW TABLES LIKE '$name';");
		$result->execute();
		if (is_array($result->fetch(PDO::FETCH_ASSOC))) {
			return true;
		}
		return false;
	}
		

	private function validateTableDefinition(array $definition, $tableName=NULL) {
		global $pico;
		/* Checking definition */
		if ( is_string($tableName) ) {
			$definition['table'] = $tableName;
		} else {
			if ( !isset($definition['table']) ) {
				die(__FILE__.':'.__LINE__);
			}
		}
		if ( !isset($definition['fields']) || !is_array($definition['fields']) || count($definition['fields'])==0 ) {
			// Error
			die(__FILE__.':'.__LINE__);
		}
		if ( !isset($definition['primary']) ) {
			// Error
			die(__FILE__.':'.__LINE__);
		}
		if ( is_array($definition['primary']) ) {
			foreach($definition['primary'] as $primary) {
				if ( !isset($definition['fields'][$primary]) ) {
					// Error
					die(__FILE__.':'.__LINE__);
				}
			}
		} else {
			if ( !isset($definition['fields'][$definition['primary']]) ) {
				// Error
				die(__FILE__.':'.__LINE__);
			}
		}
		if ( isset($definition['unique']) && !is_array($definition['unique']) ) {
			$definition['unique'] = array($definition['unique']);
		}
		if ( isset($definition['index']) && !is_array($definition['index']) ) {
			$definition['index'] = array($definition['index']);
		}
		return $definition;
	}


	/**
	 * The value function fetches a single value from a table.
	 * 
	 * @access public
	 * @param string $table Tablename
	 * @param string $value [SQL] Value you want to fetch.
	 * @param string $condition [SQL] If set, only rows matching this condition will be fetched (default: '')
	 * @param string $order [SQL] Id set, results will be ordered with given SQL order (default: '')
	 * @param string $limit [SQL] If set, results will be limited with given SQL limit (default: '')
	 * @return mixed array Array of fetched fields.
	 */
	public function value($table, $value, $condition=NULL, $order=NULL, $limit=NULL) {
		$sql = "SELECT $value FROM `$table`" . (!is_null($condition)?" WHERE $condition":'') . (!is_null($order)?" ORDER BY $order":'') . (!is_null($limit)?" LIMIT $limit":'');
		$result = $this->prepare($sql);
		$result->execute();
		$data = $result->fetchAll(PDO::FETCH_COLUMN, 0);
		return reset($data);	
	}

	
}