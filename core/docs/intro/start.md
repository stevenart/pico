# Getting started


## Getting the code

### Using the helper script (easiest)


### Getting code from GitLab

If you don't want to use the helper script, you don't have too. You can export an archive of the current code from [GitLab](https://gitlab.com/stevenart/pico).

The one-liner to do that from your terminal in the current directory is:
```bash
git archive --format=tar --remote=git@gitlab.com:stevenart/pico.git master | tar -xf -
```

You can also download directly the code:
- [zip archive](https://gitlab.com/stevenart/pico/-/archive/master/pico-master.zip)
- [tar.gz archive](https://gitlab.com/stevenart/pico/-/archive/master/pico-master.tar.gz)
- [tar.bz2 archive](https://gitlab.com/stevenart/pico/-/archive/master/pico-master.tar.bz2)
- [tar archive](https://gitlab.com/stevenart/pico/-/archive/master/pico-master.tar)

Please note that if you get the code from GitLab, there are some files you probably won't need : 
- `docs/`: this documentation
- `.gitlab-ci.yml`: [docsify](https://docsify.js.org/) GitLab script to update documentation on push

You can delete thoses files without any impact on your app.

!> You could clone the GitLab repository and use `git pull` to update PicoPHP... but we don't recommend it! First because it will prevent you from using git. And also because merging could be tricky. If you want to update the code, you should do it manually or use the helper script.

---

## Server configuration

PicoPHP needs a \*AMP infrastructure: Apache, MySQL, PHP. 
- If you are on a **Linux** machine, you can probably use [the version packaged with your distribution](https://help.ubuntu.com/community/ApacheMySQLPHP) but you could also use a tool like [XAMPP](https://www.apachefriends.org/) if you prefer. 
- If you are on **macOS**, we recommend using [MAMP](https://www.mamp.info/en/mac/) (the free version does everything needed).
- If you are on **Windows**, we recommend using [MAMP](https://www.mamp.info/en/windows/) (the free version does everything needed) or [WAMP](https://www.wampserver.com/en/).

Those are recommendations but you can of course use whatever configuration you want. Here is what is needed:
- PHP 7.2 or superior (7.4 currently recommended)
- MySQL 5.5 or superior (8.0 currently recommended)
- Apache 2.2 or superior (2.4 currently recommended) with mod_rewrite and gd support

Those are super basic and classic stuff and you should be able to run PicoPHP on any hosting (managed, VPS or dedicated).

*Modify PicoPHP to run on other configurations (nginx, PostgreSQL, ...) should be relatively easy but is not currently supported by default.*


---

## First run

First, you need to specify the database name and credentials for your app in `app/config.app.ini`. For example, this will use the database named "pico" with user "root" (password "root").
```ini
[database]
name=pico
user=root
password=root
```

Since you're in `app/config.app.ini`, you should also specify the languages your app/website should be available in. For example this will configure your website as bilingual (english + french) :
```ini
[i18n]
; Supported languages are: de, en, fr, it, lb, nl, pt
languages=fr,en
cms_only=
disabled=
default=fr 
```

When this is done, **you can now start your server** using the `www/` folder as the DocumentRoot.

!> On some cases (like cheap managed hostings), you can't change the DocumentRoot and will be forced to use the root folder (`/`). This is slightly less secure but PicoPHP will also work with DocumentRoot as the project root.

**If your database user has the rights to create a database (usually in local environments),** 
you can access `/firstrun.php` in a browser to create the database. For exemple in a local MAMP configuration, that will be: [http://localhost:8888/firstrun.php](http://localhost:8888/firstrun.php)
And follow the instructions.

**If your database user does NOT have the rights to create a database,** 
you must create it yourself with a tool like the `mysql` client or phpMyAdmin (often packaged with MAMP). Once the database created, you must :
- Import `scripts/sql/_start.sql` to the new database.
- Create your `CMSUser` access if you want to access the CMS. 


---

## Done!

Once all that is done, you should be able to access your app/website on your browser at `/`. In a local MAMP configuration, that will be: 
[http://localhost:8888/](http://localhost:8888/)

The CMS is accessible at `/cms` ([http://localhost:8888/cms](http://localhost:8888/cms) in local MAMP) and you should be able to login with the user you've created.