# What is PicoPHP?

PicoPHP is a PHP micro-framework and CMS. It offers tools to quickly create a working website, webapp or any kind of tool. The goal of PicoPHP is to offer all the components needed to create a simple website while remaining simple to understand and keeping a small footprint.

The micro-framework part is really simple and offers a very basic [implicit routing system](arch/routing.md), a [kinda ORM](models/models.md) system (no SQL needed, except for reading) and a templating system ([Twig](views/views.md)).


## Who is PicoPHP for?

PicoPHP is built for developers who:
- are building small websites or projects
- want someting quick to build and simple to use
- want something super easy to tweak and adapt to their needs
- like to understand exactly what happens (fear/hate "magic" in code)

?> The perfect use-case for PicoPHP is a website for a small business or local organization, built by a web agency or a freelancer.


## Who is PicoPHP NOT for?

PicoPHP is not for developers who:
- need a million plugins pre-built (PicoPHP offers everything needed for a simple website, anything fancy is up to you)
- are building websites in a large team (if your team more than 5 concurrent developers on the same project, PicoPHP is not the right tool)


