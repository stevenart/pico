# PicoPHP Documentation

Welcome to the PicoPHP documentation.

This documentation is for people who want to use PicoPHP to build a website (developers), not people using PicoPHP CMS to update the content of their website (administrators).

This documentation serves as a starting point for developers willing to start building websites. It covers the basics of the framework. The code of PicoPHP was written to be easy to read so developers looking for more in-depth knowledge of how everything works should not be afraid to open the code.


### Bugs

If you find a bug, please report it on the [GitLab page](https://gitlab.com/stevenart/pico).

### Community

If you have any question, please ask to community on [stack overflow](https://stackoverflow.com/) with the tag "picophp". We try to keep an eye on that and your question may help others. 


