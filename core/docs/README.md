# PicoPHP Documentation

This documentation is built using [docsify](https://docsify.js.org).

You can run it locally using:
```bash
python -m http.server 3000
```
in the `docs` folder and opening a browser to [localhost:3000](http://localhost:3000/).
