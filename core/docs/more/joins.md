# Joining and counting

When accessing data, it can be usefull to join and or count data. If your code need true model operations, you should create a proper model and fetch it separately. But if what you need is just getting more data to assign to the view, keep on reading!


## Join on getting values

Once fetched, you can access fetched data with either the `values()` or the `one()` function (see [How do models work?](/models/models.md)). Both act the same way and accept a `$joins` parameter :
```php
Model::values($fields=NULL, $joins=NULL, $counts=NULL, $start=0, $limit=9999)
```

The joins parameter allows to do the equivalent of an `INNER JOIN` on a table, and replace a field with the result of the join. The `$joins` parameter is an array with the following structure:
```php
array(
	$field => array($joinTable, $joinField, $joinCondition="(1=1)", $joinOrder=NULL, $joinLimit=NULL),
	...
)
```
- `$field` is the field to replace.
- `$joinTable` is the table to join on.
- `$joinField` is the field to join on.
- `$joinCondition` is the condition to join. This is optional, no condition is used by default.
- `$joinOrder` is the join order. This is optional, by default joined values are unordered.
- `$joinLimit` is the join limit. This is optional, by default joined values are unlimited.

Here is an example of using join:
_The hypothetical app contains two models: a `Vehicle` model and a `Brand` model. The `Vehicle` model contains a `brand` field with the `id` of the brand. The `Brand` model contain the name of the brand and its creation year and is stored in the table `brands`._

```php
$vehicle = Model::factory('Vehicle');
$vehicle->fetch();
$vehicle->values(NULL, [
	'brand' => ['brands', 'id']
]);
```

In the returned data from the example above, the `brand` field will be replaced with an array containing all fields from the `brands` table where `id` is equal to the field value.




