# Building forms and saving data

Now that we have seen how [models are defined](/models/models.md) and how [controllers works](/controllers/controllers.md) let's dive into a complete example.

---

With our `vehicule` [example model](/models/models.md), let's create a controller/view structure that can create new vehicles:

```php
// THE CONTROLLER: /app/controllers/demo.controller.php

class DemoController extends Controller {

	public function _() {	
		
		global $pico;	
			// The global $pico variable should always be accessible.

		$vehicle = Model::factory('Vehicle');	
			// Creation of the Model object.

		$canSave = $vehicle->loadForm();	
			// Check if there is a valid vehicle form in the request and loads it if there is one.

		if ($canSave===true) {
			// The form is valid and has been loaded.
			if ($vehicle->save()) {
				// The data has been saved to the database
				$this->variable('success', true);
				// Let's assign a 'success' variable to the view.
				// The save() function has created an id automatically, let's assign it to the view.
				$this->variable('id', $vehicle->value('id'));
			} else {
				// The data couldn't be saved to the database
				$this->variable('error', true);
				// Let's assign an 'error' variable to the view.
			}

		} else {
			// The form is not present or not valid. Let's create the form and assign it to the view.

			$form = $vehicle->form(array(
				'actions' => array(
					'submit' => array('type'=>'submit', 'text'=>'Submit')
						// One button below the form: Submit
				),
				'errors' => $canSave,
					// Errors, if any.
				'fields' => array('id'),
				'include' => false,
					// Field not to include (id).
			));
			$this->variable('form', $form);
		}
	}
}
```

```twig
{# THE VIEW: /app/views/demo.twig #}

{% extends '_skeleton.twig' %}

{% block body %}
		
	{% if success %}	
		{# If the variable success is set, it means the data has been saved. #}
		<p>The form has been saved to the database with id={{ id }}.</p>
		
	{% elseif error %}
		{# If the variable error is set, it means that an error has occurred. #}
		<p>An error has occurred.</p>

	{% elseif form %}
		{# If the variable form is set, we can display it. #}
		{# We must use the 'raw' filter because 'form' is HTML and it should not be escaped. #}
		{{ form|raw }}

	{% endif %}

{% endblock %}
```

There is a lot of new information in the code above if you are new to PicoPHP but you should be able to understand the value of working with models:
- You can add or remove fields without worrying about the structure or your database (no `CREATE` or `ALTER TABLE` needed).
- You can work with simpler field types (`email` vs `VARCHAR(255)`). See [Field types](models/types.md) for extended documentation about available types.
- You can easily save data (no `INSERT` or `UPDATE` needed).
- You can easily access data.

---


That is quite a mouthful, so let's pause on each main action:

## Creating a Model object

To use the model you defined in the YAML file, you need an object. That object can be the basic `Model`, like this:
```php
$vehicle = Model::factory('Vehicle');
```

Since we don't need anything special for our example, we don't need to write a specific class.


## Building a form

The `Model` class can also build the code of your HTML form. To do so, you just need to call the `form` function. In our example, this looks like:
```php
$form = $vehicle->form(array(
	'actions' => array(
		'submit' => array('type'=>'submit', 'text'=>'Submit')
			// One button below the form: Submit
	),
	'errors' => $canSave,
		// Errors, if any.
	'fields' => array('id'),
	'include' => false,
		// Field not to include (id).
));
```

The first (and only) argument of the `form` function is an array of options for your form. The available options are:
- `actions`: An array of actions (buttons) displayed below your form. Each action is an array containing a `type` that can be `submit` (a submit input tag) or `link` (a link tag). Each action can have specific arguments, please check the template to see which ones. _Default: a single submit button_ 
- `action`: The `action` URL of your form. _Default: current URL_ 
- `captcha`: Should your form include an automatic captcha? _Default: false_ 
- `css`: Add a CSS class to your form? _Default: none_ 
- `disabled`: Array of disabled fields. _Default: none_ 
- `errors`: Array of erroneous fields. _Default: none_ 
- `errorsMessage`: Should an error message be displayed if your form has erroneous fields? _Default: false_ 
- `fields`: Array of fields to be displayed (or hidden, see `include`). _Default: false_ 
- `fieldsets`: Display fieldsets with groups defined in the model? _Default: false_ 
- `fsclosed`: Array of fieldsets that should be closed by default. _Default: none_ 
- `fsempty`: Display empty fieldsets? _Default: false_ 
- `internal`: Internal fields that should be excluded if not explicitly specified? _Default: 'id', 'uri', '__draft', '__ordering', '__starred', '__timestamp', '__created', '__user'_ 
- `internalAdd`: Array of fields that should be added to default `internal`. _Default: none_ 
- `internalRemove`: Array of fields that should be removed to default `internal`. _Default: none_ 
- `include`: `true` if `fields` are the fields that needs to be included, `false` if it is the fields that needs to be excluded. That way you can switch between "nothing except ..." or "everything but ...". _Default: true_ 
- `index`: Index to be loaded (see [Fetching](models/fetching.md)). _Default: 0_ 
- `legends`: Should fieldsets include a legend? _Default: true_ 
- `method`: Form method. _Default: 'post'_ 
- `name`: Form name. _Default: model name_ 
- `values`: Values to load. _Default: none_ 
- `variables`: Variables to assign to the view. _Default: none_ 
- `view`: The form view to use. _Default: 'forms/_form.twig'_ 

The form is build using a main view (`core/views/forms/_form.twig`) that can be read for more in-depth comprehension of how everything works. Like anything in PicoCMS, it can also be duplicated in `app/views/forms/_form.twig` if you need to alter it but a **better** way is to create a new form (ie. `app/views/forms/custom.twig`) if you need to.

The main view then calls blocks view for each field types (see [Field Types](models/types.md)).

The `form` function returns a HTML code that can then be passed to the view for display. In our example, that is done with the line:
```twig
{{ form|raw }}
```


## Loading a form

The `Model` class also provides methods to deal with the form once it has been submitted by the user. The base method is `Model::loadForm` and does a few things by calling other functions so you don't need to:

1. Looks for submitted data in `$data`, `$_POST` or `$_GET` (in that order) and that the data has a legit token (preventing [Cross-site request forgery](https://en.wikipedia.org/wiki/Cross-site_request_forgery)). This is done by the `Model::present` function.

2. If legit data is found, it checks the content of the data against the `checks` defined in your YAML file see [Checks](models/checks.md). This is done by the `Model::check` function.

3. Does some very basic sanitation of the data.

4. Check captcha if asked for in the form options.

The function `Model::loadForm` can return 3 things:
- `true` = Valid data have been found and checked, the code can proceed.
- `false` = No valid data have been found.
- `array()` = Valid data have been found but at least one field is erroneous. The array are the erroneous fields, the key is the field name and the value is the check that failed (see [Checks](models/checks.md)).

If we take a look at our code, it should be much easier to understand now:
```php
$canSave = $vehicle->loadForm();	
if ($canSave===true) {
	 //  Valid data have been found and checked, the code can proceed.
} else {
	// No valid data have been found or fields are erroneous.
	$form = $vehicle->form(array(
		//(...)
		'errors' => $canSave,
		//(...)
	));
}
```


## Saving a form

Once legitimate and valid data have been loaded, PicoCMS can help saving those data into the database with the function `save`:
```php
if ($vehicle->save()) {
	// Saved !
} else {
	// Failed !
}
```

The save function does a few things:
1. Checks that there is loaded data.
2. Checks that there is a primary key, and generates one if not. That means that the vehicle in our example will get an automatically generated id since we obviously didn't ask the user for one.
3. Generates URI fields, if presents (see [Field types](/models/fields.md)). 
4. Fill out PicoCMS internal fields (see [Internal fields](models/internal.md)).
5. Saves the data to the database.

If no primary key is provided or if the provided key doesn't exists, the `save` will insert a new row in the table.
If an existing primary key is provided, the `save` will update the existing row. 

?> In the case of a form, there is usually only one item (row) to be saved but if you load values from your code (ie. importing data from a CSV file) you can use `save` to update/insert all the loaded data in one call.


The `save` function will return:
- The number of rows impacted (`1` in case of a form)
- `false` if an error occurred

That is why in our example, we use `$vehicle->save()` as the condition of our success: if it returns 0 or false, something went wrong, otherwise the data was saved. 

