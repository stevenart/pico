# How do controllers work?

A controller is the object that is going to handle a request. The role of a controller is usually to read parameters from the request (URL, GET/POST parameters, etc.) to fetch the right data to the [view](/views/views.md). To do that, the controller usually uses one or more [models](/models/models.md).

We have already seen which controller is called for which URL (see [Routing](arch/routing.md)), now let's see how to build a controller.


## Structure

A basic controller is a PHP file, located in `/app/controllers`[^1]. Each controller must extends the abstract `Controller` class, and therefore implement at least one function : `_()`.

Here is an example of a very basic controller:
```php
<?php

class TestController extends Controller {

	public function _() {	
	}

}
```

This simple controller will be instanced when the URL `/[language]/test` is called. It will do real tasks but will automatically fetch the view of the same name (`/app/views/test.twig`) and serve it to the client. If no view file is present, Pico will output an error. 


## Assigning values to the view

To assign variables to the view, you can use the `Controller::variable()` function. For exemple, the following code will assign a variable called `brands` to the view :
```php
<?php

class TestController extends Controller {

	public function _() {	
		$brands = ['Fiat', 'Opel', 'Renault', 'VW'];
		$this->variable('brands', $brands);
	}

}
```

And in the view, you will be able to access it directly:
```twig
<ul>
	{% for brand in brands %}
		<li>{{ brand }}</li>
	{% endfor %}
</ul>
```


## Going further...

Controllers are where most of your logic will take place, so it is normal that they seems empty at first. In a the controller, you can:
- [fetch data from the database](/arch/db) 
- [read configuration](/arch/config)
- call an API
- or do any logic !

Please see [Building forms and saving data](/controllers/form) to see how to do some logic within a controller.


?> There are special controllers that inherit `PageController` and that are specific to the CMS concept of pages. See [Pages and PageController](/cms/pages)



---

[^1]\: ...or `/core/models`, see [Core vs App](arch/core.md).