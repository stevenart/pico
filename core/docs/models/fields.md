# Fields types

We've seen how models are mainly a data structure defined by fields. Those fields most defining element is their type. The type of a fields impacts how it will be stored in the database and how it will be presented to the user when building a form.

Here are the existing types in PicoCMS (some requires options, discussed below) :


- `boolean`: Simple boolean value (true/false). Boolean fields would requires options but default options (0=No, 1=Yes) are loaded if no options are present.

- `cdn`: PicoCMS provides a simple [CDN](/cms/cdn.md) and the `cdn` types is the equivalent of a 'files' or 'images' field. It contain CDN ids and offers the user the ability to upload new files. This type can be configured:
	- `max`: Maximum number of files that can be uploaded. _Default: 10_
	- `upload`: Can the user upload a file? _Default: 1 (yes)_
	- `upload_i18n`: The upload button I18N. _Default: 'PICO.ACTION_CDN_NEW'_

- `choice` (requires options): Choice between options where the key is a string.

- `choice_int` (requires options): Choice between options where the key is an integer.

- `choices` (requires options): Choice with multiple choices possible (checkboxes).

- `color`: Hexadecimal color code.

- `date`: Date.

- `datetime`: Date and time.

- `email`: Email address.

- `float`: Floating point number.
	- `precision`: Float precision. _Default: 16,8_

- `html`: HTML content. This also can be configured:
	- `rows`: Number of rows of the textarea. _Default: 10_
	- `tinymce_*`: Fields starting with `tinymce_` are passed to TinyMCE configuration.

- `hidden`: Hidden field.

- `integer`: Integer number.

- `money`: Alias for float with a precision of 8,2.

- `password`: Password.

- `text`: A text. Maxlength is important here because it will change how the user sees the input: it will be displayed as a `input[type=text]` if maxlength is smaller than 256 and as a `textarea` otherwise. 

- `time`: A time (without date).

- `timestamp`: A timestamp.

- `uri`: A unique URL part. This can be configured:
	- `auto_uri`: An array of fields concatenated to generate the uri. If provided, PicoCMS will keep this field up to date when one of the field is updated.

- `url`: A valid URL.


## Parameters for all field types

These parameters can be used on all (or most) field types:

- `checks`: Checks a field must pass to be valid. See [Checks](/models/checks.md). 
- `maxlength`: Maximum length for the value of a field. This is important when working with type `text`.
- `placeholder`: Placeholder text for the field.
- `placeholder_i18n`: Is the placeholder an I18N key? _Default: 0 (no)_
- `template`: Specify the template to use in forms if you don't want the default template. The default template is always the type in `/views/forms`.
- `text`: The field label text. By default, PicoCMS generates an I18N key named `[MODEL].FIELD_[FIELD]`.
- `value`: Default value.


## Options

Some field types requires options. Options are the list of valid values for that field (think of it as `option` in a `select` tag). 

You can specify options directly, with parameter `options`. Here is a simple example:
```yaml
category:
	type: choice
	options:
		'bike': 'I ride a bike'
		'car': 'I drive a car'
		'truck': 'I drive a truck'
```
The keys of the array are the value saved in the database and the values are the texts displayed to the user. The exemple would result in something like:
```html
<select id="vehicle_category" name="vehicle_category">
	<option value="bike">I ride a bike</option>
	<option value="car">I drive a car</option>
	<option value="truck">I drive a truck</option>
</select>
```

There are two limits with that syntax: internationalization and scaling.


### Internationalization

If my website is offered in English and French, I would want to display two different texts.

The first parameter available is `options_i18n` which specify that texts are in fact i18n keys. In our example that would be:
```yaml
category:
	type: choice
	options:
		'bike': 'APP.LABEL_BIKE'
		'car': 'APP.LABEL_CAR'
		'truck': 'APP.LABEL_TRUCK'
	options_i18n: 1
```
Specifying `options_i18n` will replace keys like `APP.LABEL_BIKE` with their proper values (see [I18n](/more/i18n.md)).

Another parameter is `options_auto_i18n` which automates even more by naming the i18n keys for you. For example:
```yaml
category:
	type: choice
	options: ['bike', 'car', 'truck']
	options_auto_i18n: 1
```
Specifying `options_auto_i18n` will automatically create keys named `[MODEL].FIELD_[FIELD]_[OPTION]` for each option and use that like with `options_i18n`.


### Scaling

This is neat when the field we have has a few static options but how do we link that to another table in the database? Let's say for our example that we have a table `categories` with two columns: `id` and `name`. Then our example become:
```yaml
category:
	type: choice
	options_db:	['categories', '`id`', '`name`']
```
Options will then be filled with values from table `categories`. The full `options_db` parameters can have 6 values:
1. Table name
2. Key field
3. Value field
4. (optional) SQL condition 
5. (optional) SQL order 
6. (optional) SQL limit

Another option available is `options_range` that will mimic [PHP `range()`](https://www.php.net/manual/en/function.range.php) function to create options sets. 
For example:
```yaml
category:
	type: choice_int
	options_range:	[0, 100, 10]
```
Will create 11 options, from 0 to 100 by increments of 10.


### Other options-related parameters

There are a few parameters related to options available:
- `options_auto`: Allows to provide a single non-associative array when keys are egal to values. _Default: 0 (no)_
- `options_auto_i18n`: Shortcut for options_auto + options_i18n. _Default: 0 (no)_
- `options_config`: Load options from [Config](/arch/config.md) (ie. `options_config: i18n.languages`).
- `options_i18n`: If texts are I18n keys. _Default: 0 (no)_
- `options_null`: Adds a NULL option before other options. _Default: 0 (no)_
- `options_sort`: Sorts options alphabetically by texts after fetching. _Default: 0 (no)_
- `options_reverse`: Reverse options order on display. _Default: 0 (no)_


## Creating new field types

Creating new field types is of course possible (and not hard) but should probably be avoided. Most of the "new field type" cravings can be satisfied with a template. For example, there is a `html_simple` template that can be use on `html` fields to limit the possibilities of the editor:
```yaml
description:
	type: html
	template: html_simple
```
That way, the html field will not be displayed with `views/form/html.twig` but with `views/form/html_simple.twig`.