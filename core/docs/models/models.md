# How do models work?

A model is an object that allows you to deal with data. In PicoPHP, a model helps you do 4 things:
- Define data structure
- Build forms
- Save data to the database
- Fetch data from the database


## Data structure

A basic model is a YAML[^1] file, located in `/app/models`[^2]. Each model has at least 2 main fields:
- `fields`: An array of fields needed in your structure.
- `primary`: The field used for primary key. 99% of the time, this should be an integer named "id".

There are also optional fields:
- `fieldsets`: Allows the creation of groups of fields to display fieldsets on forms.
- `i18n_prefixed`: List of fields that should be prefixed with the language code. For example, for a field `name` referenced in `i18n_prefixed` in a bilingual app/website, PicoPHP will create 2 fields: `en-name` and `fr-name` (if those are the two languages defined)
- `table`: The name of the table in which the data for this model are stored. If not specified, the table will be the name of the model.
- `require`: If the creation of this model depends on a previous model being created, include it here. This is only necessary to prevent errors on first instantiation of models with extenal `references` (see [Checks](/models/checks.md)).

The name of the model is the first part of the filename.


## Example model

Here is an example of a simple model `vehicle.model.yaml` representing vehicles:
```yaml
table: vehicles

primary: [id]

fields:

	id:
		type: integer
		checks: [required]

	category:
		type: choice
		options: ['bike', 'car', 'truck']
		options_auto_i18n: 1
		checks: [required]

	plate:
		type: text
		maxlength: 10	
		checks: [required]

	brand:
		type: text
		maxlength: 100				

	description:
		type: html

i18n_prefixed:		['description']

fieldsets:

	general:		['category', 'plate']

	optional:		['brand', 'description']
```

If we translate that into English, we have a model named `vehicle` (from the filename) with 5 fields:
- `id`: the unique identification number, specified by the `primary` directive.
- `category`: a choice between 3 options (bike, car or truck).
- `plate`: the license plate, a string of maximum 10 characters
- `brand`: the brand, a string of maximum 100 characters
- `description`: an HTML description of the vehicle 

The first 3 fields are required (specified with `checks: [required]`, see [Checks](models/checks.md)) and are placed inside a fieldset named "general". The other two fields are inside a fieldset named "optional".

You'll find more information on field types and options on the [Field Types](models/types.md) page. Let's see how we can use this model.


## The Model object

In your PHP code (usually in a controller), you must build a `Model` object to use the structure. To do so, you should use the `Model::factory` method with the name of your model. For example:
```php
$vehicle = Model::factory('Vehicle');
```

You could also create your own model class if you need some special action with your model. For example, you could create the class `/app/models/vehicle.model.php` (the same filename as your YAML file, but with a PHP extension):
```php
class VehicleModel extends Model {
	public function vroom() {
		global $pico;
		error_log('Vroom vroom');
	}	
}
```

Now, you can do:
```php
$vehicle->vroom();
```

?> Please note that even if you create your own model class, you should use Model::factory to create the object. That way you will use the builtin [factory design pattern](https://en.wikipedia.org/wiki/Factory_method_pattern) so that it will load the specific model if one exists and the default `Model` class if not.


## Fetching data

Fetching data from the database can be done with the `fetch` function.

The `fetch` function allows you to select data from the database and loading those data in your model object. It takes 3 optional parameters:
```php
Model::fetch($condition=NULL, $order=NULL, $limit=NULL)
```
- `condition`: Fetches rows matching that SQL condition. This is what would come after "WHERE" in a SQL statement. _Default: NULL (all rows are selected)_ 
- `order`: Sorts rows based on fields, this is what would come after "ORDER BY" in a SQL statement. _Default: NULL (no sorting)_ 
- `limit`: Limits the number of fetched rows. This is what would come after "LIMIT" a SQL statement. _Default: NULL (no limit)_ 

Lets look an example with our `$vehicle` object:
```php
$vehicle = Model::factory('Vehicle');

$vehicle->fetch(); 
	// Fetches all vehicles from the database

$vehicle->fetch('`brand` LIKE "VW"', '`plate` ASC', 3); 
	// Fetches the first 3 VW vehicles ordered by plate number.
```

As you can see, you are not writing full-fledge SQL statements but you are still using SQL (a language you probably already know the basics) for conditions and orders.

There are others functions that can help you fetch data. There are usually shortcuts for `fetch`:

- **`fetchOne($field, $value)`** 
Fetches the first row where `$field` is `$value`. This is usefull for fetching a single element based on e unique field (uri, etc.). 
_Example: ```$vehicle->fetchOne('plate', '737-BBQ');```_

- **`fetchPrimary($primary)`** 
Fetches the row where the primary key is `$primary`. This is the safest and fastest way of fetching an element when you know its id.
_Example: ```$vehicle->fetchPrimary(7);```_

?> All fetching functions returns the number of row fetched. The number of fetched rows in the model can always be tested later with the `fetched` function.

Once data are loaded, they are stored in the protected array $values with an index.


## Accessing data

Once data is loaded onto the model object, you can access them 3 ways :

**`value($name, $index=0, $value=NULL, $clear=false)`** 

This method allows to read/update/clear one field (column) of a fetched item. Its parameters are:
- `name`:  The name of the field.
- `index`: The index of the targeted item. _Default: 0_ 
- `value`: The new value you want for that field. _Default: NULL_ 
- `clear`: Do you want to clear that field (put NULL as its value)? _Default: false_ 

Lets see `value` in an example:
```php
$vehicle = Model::factory('Vehicle');

$vehicle->fetchPrimary(7); 
	// Fetches vehicle with id=7

if ($vehicle->value('brand')=='VW') {
	// If the brand is "VW"..

	$vehicle->value('brand', 0, 'Volkswagen');
	// Change it to "Volkswagen"...

	$vehicle->value('plate', 0, NULL, true);
	// And clear its plate
} 
```

There are also a powerful method that can help you get data for reading only (usually passing it to the view). That method is `values`.

**`values($fields=NULL, $joins=NULL, $counts=NULL, $start=0, $limit=9999)`** 

This method allows to retrieve an array of values from the fetched data. Its parameters are:
- `fields`: An array containing fieldnames you want to retrieve. _Default: NULL (all fields)_
- `joins`: An array containing fields you want to join. This can allow you to replace a field with an array, usually an id with values. _Default: NULL (no joins)_
- `counts`: An array containing new counts fields you need. _Default: NULL (no counts)_
- `start`: Starting offset. _Default: 0_
- `limit`: Maximum number of rows. _Default: 9999_ 

Lets see an example:
```php
$vehicle = Model::factory('Vehicle');

$vehicle->fetch('`brand` LIKE "VW"', '`plate` ASC', 3); 
	// Fetches the first 3 VW vehicles ordered by plate number.

$data = $vehicle->values();
	// Returns an array with all fetched vehicles.

$data = $vehicle->values(array('id', 'brand', 'plate'));
	// Returns an array with a subset of 3 fields (id, brand and plate) all fetched vehicles.
```

`joins` and `counts` are more advanced techniques we'll discuss later in [Joining and Counting](/more/joins.md).

## Forms

Building, loading and saving forms requires some controller knowledge and are discussed [here](/controllers/forms.md).


---

[^1]\: YAML is a human-friendly data serialization language for all programming languages. More on [Wikipedia](https://en.wikipedia.org/wiki/YAML).

[^2]\: ...or `/core/models`, see [Core vs App](arch/core.md).