# Checks

Checks can be specified for each field to verify its validity before saving. Invalid values will not be loaded in model and will return errors on `Model::loadForm` (see [Building forms and saving data](/controllers/forms.md)).



## Using checks

Checks are simple function that return `true` if the value is valid and `false` if not. Here is a simple check used to verify that an email is valid:
```php
static public function email($value) {
	return (preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]+)$/i', $value)>0);
}
```
Checks can get more complicated but are usually pretty basic.

A field can have more check, for example a field `plate` defined as:
```yaml
plate:
	type: text
	checks: [required, longer(3)]
```
Would be required and longer than 3 characters.

Few remarks on checks:
- If there is a check with the same name as the field type, it is implicitly added to that field. For example a `date` field will automatically be checked against the `Check::date` function, whether it is explicitly defined or not. 



## Available core checks

Here are the available core checks, defined in `/core/check.class.php`:

- `after`: checks if a date is after another. Usage: `checks: [after("2020-01-01")]`

- `before`: checks if a date is before another. Usage: `checks: [before("2020-01-01")]`

- `computerfriendly`: checks if a text doesn't contain any special characters. Usage: `checks: [computerfriendly]`

- `date`: checks if value is a valid date. Usage: `checks: [date]`

- `datetime`: checks if value is a valid datetime. Usage: `checks: [datetime]`

- `different`: checks if value is different than something. Usage: `checks: [different("TEST")]`

- `email`: checks if value is a valid email address. Usage: `checks: [email]`

- `equal`: checks if value is a number equal to another. Usage: `checks: [equal(7)]`

- `extensions`: checks if file extension is in selection. Usage: `checks: [extensions("docx,xlsx")]`

- `float`: checks if value is a float. Usage: `checks: [float]`

- `future`: checks if date is in the future. Usage: `checks: [future]`

- `integer`: checks if value is an integer. Usage: `checks: [integer]`

- `images`: checks if file is an image. Usage: `checks: [images]`

- `length`: checks text length. Usage: `checks: [length(8)]`

- `length`: checks text length. Usage: `checks: [length(8)]`

- `longer`: checks if text length is longer than something. Usage: `checks: [longer(8)]`

- `max`: checks if value is below something. Usage: `checks: [max(8)]`

- `min`: checks if value is above something. Usage: `checks: [min(3)]`

- `none`: checks if value not present in db. Usage: `checks: [none("vehicles,id,7")]` = checks if there is no vehicle with id=7

- `number`: checks if value is numeric. Usage: `checks: [number]`

- `past`:  checks if date is in the past. Usage: `checks: [past]`

- `references`: checks if value references a row in the database. Usage: `checks: [references("pages,if")]` = checks if the value is a page id. If the value is being checked against another model, it should be defined in `require` to avoid first instantiation errors (see [How do models work?](/models/models.md)).

- `required`: checks if a value is specified. Usage: `checks: [required]`

- `requiredif`: conditionally checks if a value is specified. Usage: `checks: [requiredif('$brand,VW')]` = field is required if field "brand" is equal to "VW". (Parameters starting with $ will be replaced with proper field value if present).

- `requiredifnot`: conditionally checks if a value is specified. Usage: `checks: [requiredifnot('$brand,VW')]` = field is required if field "brand" is not equal to "VW".

- `requiredoptions`: checks if an option is selected from a choices field. Usage: `checks: [requiredoptions(terms)]` = checks that option "terms" is checked.

- `same`: checks if a value is the same as something. Usage: `checks: [same("TEST")]`

- `shorter`: checks if text length is shorter than something. Usage: `checks: [shorter(8)]`

- `time`: checks if value is a time. Usage: `checks: [time]`

- `unique`: checks if value is unique in the database table. Usage: `checks: [unique("vehicles,plate")]` = checks if the plate number doesn't already exists in the database.

- `url`: checks if value is an url. Usage: `checks: [url]`

- `vatnumber`: checks if value is a valid European VAT number. Usage: `checks: [vatnumber]`

?> Some checks will change the database structure depending on the database type, driver and configuration.


## Adding app-specific checks

You can add your app-specific checks in `/app/appcheck.class.php`. You can also redefine existing core checks you would like to overwrite as this fils will have priority over `/core/check.class.php`.
