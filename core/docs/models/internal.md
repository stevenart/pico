# Internal fields

PicoCMS adds 6 fields to every model. These are called "Internal fields" and cannot be deleted or renamed, their names begins with `__` so it will never conflicts with explicitly defined fields (since those should not start with `_`).

The fields are:

- `__draft`: a boolean specifying if this element is in draft state or not.

- `__ordering`: an integer used when user can manually sort elements.

- `__starred`: a boolean specifying if this element is starred or not.

- `__timestamp`: a timestamp of the last update of this element. This value is updated automatically by PicoCMS.

- `__created`: a timestamp of the creation date of this element. This value is set automatically by PicoCMS.

- `__user`: an integer specifying the id of the `cmsuser` logged when this element was last updated (if any). This value is set automatically by PicoCMS.


These fields can be read/updated from anywhere if needed. The [CMS](/cms/cms.md) uses those fields but each app can/should handle them the way its wants.

For example: 
- a drafted element (`__draft`=1) will be displayed if the controller doesn't include the condition "\`__draft\`==0" when fetching
- user-sorting will not be done if the controller doesn't include "\`__ordering\` ASC" as order when fetching

You can also ignore theses fields altogether if you'd like, they just shouldn't be deleted.