# Configuration files

The configuration of PicoPHP is very basic. It consists of two files: `config.core.ini` and `config.app.ini`. If you read [the Core/App page](arch/core.md), you've guessed it... `config.app.ini` extends `config.core.ini`.

That means that all the default configuration is defined in `config.core.ini` and can (or should or must, depending on the directive) be modified in `config.app.ini` (again, you should never modify `config.core.ini`).


## Structure and access

There are \~70 directives split in 5 sections in `config.core.ini`. All these directives are described [below](arch/config.md?id=core-directives). You can modify them in `config.app.ini` but you can also create new ones if your app/website requires it.

The configuration can be accessed anywhere from your PHP code with the global object $pico. For example, here is how you print the `plugins` directive in the `cms` section: 
```php
global $pico;
echo $pico->config['cms']['plugins'];
```

For security reasons you can't access configuration directly from a view but you can assign what you need in your controller (see [Controllers](controllers/controllers.md) and [Views](views/views.md)).


## Environments

You can manage multiple environments with the `Hooks::env()` function. This function returns a string that is the environment key. Pico will automatically look for `config.[env].ini` and load it, overwriting `config.core.ini` and `config.app.ini`.
An example of this can be found by default with "local" environment. 



## Core directives

Here are the configuration directives you'll find in `config.core.ini` and what they are used for:

- `cms`: Directives related to the CMS interface.

	- `plugins`: The lists of cmsplugins that should be accessible in the CMS sidebar. The value is a list of plugins, separated by a comma. Each entry in the list should be the first part of the cmsplugin file (ex.: "posts" for `app/cmsplugins/posts.cmsplugin.php`). If the entry start with a dash ("-"), it will include a separator (a simple "-" is a blank separator and something with a text like "-group1" will be a named separator, here "group1").

	- `root_user_domain`: CMS users with email from the domain are considered "root" and have special privileges such as an access to the `database` CMS plugin.


- `database`: Directives related to the database. (Currently, PicoPHP only supports MySQL.)
	
	- `name`: The name of the database.

	- `port`: The port of the database server.

	- `user`: The user used to connect to the database server.

	- `password`: The password for that user.


- `i18n`: Directives related to the internationalization. By default, PicoPHP configuration supports these languages: `de`, `en`, `fr`, `it`, `lb`, `nl`, `pt`. You can add new one easily by adding directives in `config.app.ini`. For each language, here are the directives needed (`**` being the language code, `en` for example). Note: The CMS interface is translated in French and English by default.[^1]

	- `languages`: List of the languages supported by your website/app, comma-separated.

	- `cms_only`: If you website/app doesn't support French or English, you should add `en` or `fr` (or both) to this directive if you want to keep an access to the CMS interface (which is only available in those languages).

	- `disabled`: List of languages visible in the CMS interface but not accessible on the app/website. This is useful when content is being translated in one language but the app/website is already used in another language.

	- `default`: The default language used if there is not one specified in the URL and if `default_browser` is set to `0` or if the browser language is not in `languages`.

	- `default_browser`: Overwrites the `default` with the language of the user browser if possible.

	- `**` (language specific): The locale used for that language (for more information on locales, check out [the setlocale documentation](https://www.php.net/manual/en/function.setlocale.php)).

	- `**_date` (language specific): The default "short" date format for that language (the same format as [strftime](https://www.php.net/manual/en/function.strftime.php)).

	- `**_date_long` (language specific): The default "long" date format for that language (the same format as [strftime](https://www.php.net/manual/en/function.strftime.php)).

	- `**_date_time` (language specific): The default date format with time for that language (the same format as [strftime](https://www.php.net/manual/en/function.strftime.php)).

	- `**_filesize` (language specific): The filesize units, decimals being separated by a space.


- `mail`: Directives related to the sending of emails.

	- `from_email`: Default sender email.

	- `from_name`: Default sender name.

	- `replace_all_email`: If set, all recipients of emails sent will be replaced by this email.

	- `method`: Sending method, can be:
		- `mail`: The default PHP mail() function.
		- `smtp`: SMTP configured with `smtp_*` directives below.
		- `soapmailer`: SoapMailer server configured with `soapmailer_*` directives below.

	- `method_fallback`: Alternative sending method if `method` fails. Options are the same as `method`.

	- `smtp_password`: SMTP password.

	- `smtp_port`: SMTP port (usually `25` or `587`).

	- `smtp_secure`: SMTP encryption type (default: '').

	- `smtp_server`: SMTP host.

	- `smtp_username`: SMTP user.

	- `soapmailer_apikey`: SoapMailer authentication key.

	- `soapmailer_server`: SoapMailer host.

	- `to_email`: Default recipient email.

	- `to_name`: Default recipient name.


- `site`: Directives related to the app/website (website oriented).
	
	- `gdpr`: Should the GDPR cookies acceptation box be displayed? (`0`=no, `1`=yes) For more information on GDPR, please check [GDPR](more/gdpr.md).

	- `load_children_widgets`: If your website is displaying children pages on the same parent page, you should set this to `1` so that widgets data from children pages are loaded with parent.

	- `noindex_pattern`: Automatically add a `<meta name="robots" content="noindex,noarchive">` on the page if the url contains this string. Useful for beta environments.

	- `root_controller`: The default controller, its name will be removed from the URL. For exemple, if set to `page` (default), a page will be accessible at : `/en/test-page_1` instead of `/en/page/test-page_1`.

	- `uri_id`: If set to `1` (default) the generated URIs will contain the element ID. The upside is no 404 when the URI changes and the downside is slightly uglier URI (`/en/test-page_1` instead of `/en/test-page`).

&nbsp;

?> TIP: check out `config.core.ini` for examples and default values!


---

[^1]\: For more info on internationalization, check out [I18n](more/i18n.md). 