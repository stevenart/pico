# Database

There is some [object–relational mapping](https://en.wikipedia.org/wiki/Object%E2%80%93relational_mapping) in PicoPHP and some direct access to the database.

All database operations in PicoPHP (explicit or not) are done with the class `Db` (`core/db.class.php`) which extends `PDO`[^1]\. PicoPHP currently only supports MySQL but implementing support for any database server with a [PDO driver](https://www.php.net/manual/en/pdo.drivers.php) available should be trivial. 


## Creation

By default, one PicoPHP project is linked to one database. The database (and tables) should have the following properties: 
- Charset: `utf8mb4`
- Collation: `utf8mb4_unicode_ci` 
- Engine: `InnoDB`

The database can be created automatically on the [first run](/intro/start.md?id=first-run) in a local environment (if the user has the proper privileges). Otherwise (when deploying on another environment or if the user doesn't have the right privileges), the database should be created manually with a MySQL client [^2].


## Structure (creating/altering tables)

In PicoPHP, there is a one-to-one relation between models and tables. This means that each table is linked to a model and each model is linked to a table. Tables are automatically created and altered by PicoPHP when you create or update a model (see [Models](/models/models.md)).

!> You should never create or alter a table yourself. Create your models and let PicoPHP deal with the database management.


## Fetching (reading data from tables)

There are two ways of fetching data from the database in PicoPHP. Both require some SQL knowledge.

1. **With models**: You can use the `fetch` function in `Model` to read data from the database with a SQL condition. For example, if I created a `Test` model with a field `firstname`, I could do:
```php
$test = Model::factory('Test');
$test->fetch('`firstname` LIKE "John"');
```
(see [Models](/models/models.md) for more information and examples)

2. **Directly, through `Db`**: You should always prefer accessing data via a model but sometimes accessing directly is simpler. For example, you can fetch the same data as in the previous example with the following:
```php
global pico;
$pico->db->all('tests', NULL, '`firstname` LIKE "John"');
```
Keep in mind that `$pico->db` is a PDO object, so you can use any method provided by [PDO](https://www.php.net/manual/en/intro.pdo.php). PicoPHP also provide helpers, which are listed below.


## Helpers

Helpers are methods provided by PicoPHP on top of PDO. You can use them if you like them, and ignore them if you don't. Here are a list of the most used ones, please check the code (`core/db.class.php`) if you want to know more.

&nbsp;

- `all($table, $fields=NULL, $condition=NULL, $order=NULL, $limit=NULL)`: Fetches all matching fields and returns a PHP array with the results.
	Parameters:
	- `$table`: Table name, automatically escaped.
	- `$fields`: Columns to fetch. This can be an array of field names or a compatible SQL string (ex.: '*'). The default is `NULL` which means all the fields.
	- `$condition`: String containing SQL string. Fields are not automatically escaped in this. The default is `NULL` which means no condition (all rows).
	- `$order`: String containing SQL order by. Fields are not automatically escaped in this. The default is `NULL` which means no order.
	- `$limit`: String containing SQL limit, or integer which will fetch the first N rows. The default is `NULL` which means no limit.

	Example:
	```php
	// Fetches the first 3 rows from table "tests" where column "firstname" matches "John" ordered by the column "lastname".
	$pico->db->all('tests', NULL, '`firstname` LIKE "John"', '`lastname` ASC', 3);
	```
	Returns:
	```php
		[
			{id:1, firstname:'John', lastname:'Abbot'},
			{id:2, firstname:'John', lastname:'Birch'},
			{id:3, firstname:'John', lastname:'Cold'},
		]
	```
	&nbsp;

- `bags($table, $keyField, $fields=NULL, $condition=NULL, $order=NULL, $limit=NULL)`: Fetches all matching fields the same way `Db::all()` does but returns a PHP associative array on `$keyField` with the results. If the `$keyField` is not unique, the keyField of the last row will overwrite the others. (see `Db::groups()` if you want non-unique `$keyField`).
	Parameters:
	- `$table`: Table name, automatically escaped.
	- `$keyField`: Field used as the key of the associative array, not automatically escaped.
	- `$fields`, `$condition`, `$order` and `$limit`: see `Db::all()` above.

	Example:
	```php
	// Fetches the first 3 rows from table "tests" where column "firstname" matches "John" ordered by the column "lastname", as an associative array based on "lastname".
	$pico->db->bags('tests', 'lastname', NULL, '`firstname` LIKE "John"', '`lastname` ASC', 3);
	```
	Returns:
	```php
		[
			'Abbot' => {id:1, firstname:'John', lastname:'Abbot'},
			'Birch' => {id:2, firstname:'John', lastname:'Birch'},
			'Cold' => {id:3, firstname:'John', lastname:'Cold'},
		]
	```
	&nbsp;


- `column($table, $column, $condition=NULL, $order=NULL, $limit=NULL)`: Fetches a single column (field) from a table.
	Parameters:
	- `$table`: Table name, automatically escaped.
	- `$column`: Column name to fetch, not automatically escaped.
	- `$condition`, `$order` and `$limit`: see `Db::all()` above.

	Example:
	```php
	// Fetches the column "lastname" for the first 3 rows from table "tests" where column "firstname" matches "John" ordered by the column "lastname".
	$pico->db->column('tests', 'lastname', '`firstname` LIKE "John"', '`lastname` ASC', 3);
	```
	Returns:
	```php
		[
			'Abbot',
			'Birch',
			'Cold',
		]
	```
	&nbsp;


- `count($table, $condition)`: Count the number of rows in a table.
	Parameters:
	- `$table`: Table name, automatically escaped.
	- `$condition`: see `Db::all()` above.

	Example:
	```php
	// Counts the number of rows in the table "tests" where "firstname" is matching "John".
	$pico->db->count('tests', '`firstname` LIKE "John"');
	```
	Returns:
	```php
		(int) 18
	```
	&nbsp;


- `groups($table, $keyField, $fields=NULL, $condition=NULL, $order=NULL, $limit=NULL)`: Same as `Db::bags()` but with non-unique `$keyField`.
	Parameters:
	- `$table` and `$keyField`: see `Db::bags()` above.
	- `$fields`, `$condition`, `$order` and `$limit`: see `Db::all()` above.

	Example:
	```php
	// Fetches the first 4 rows from table "tests" where column "firstname" matches "John" ordered by the column "lastname", as an associative array based on "lastname".
	$pico->db->bags('tests', 'lastname', NULL, '`firstname` LIKE "John"', '`lastname` ASC', 4);
	```
	Returns:
	```php
		[
			'Abbot' => [
				{id:1, firstname:'John', lastname:'Abbot'}
			],
			'Birch' => [
				{id:2, firstname:'John', lastname:'Birch'}
			],
			'Cold' => [
				{id:3, firstname:'John', lastname:'Cold'},
				{id:4, firstname:'John', lastname:'Cold'},
			],
		]
	```
	&nbsp;


- `pairs($table, $keyField, $valueField, $condition=NULL, $order=NULL, $limit=NULL)`: Fetches a key/value pair with matching rows.
	Parameters:
	- `$keyField`: Field used as the key of the associative array, not automatically escaped.
	- `$valueField`: Field used as the value of the associative array, not automatically escaped.
	- `$condition`, `$order` and `$limit`: see `Db::all()` above.

	Example:
	```php
	// Fetches the first 3 pairs from table "tests" where column "firstname" matches "John" ordered by the column "lastname", as an associative array based on "id".
	$pico->db->bags('tests', '`id`', 'CONCAT(`firstname`," ",`lastname`)', '`firstname` LIKE "John"', '`lastname` ASC', 3);
	```
	Returns:
	```php
		[
			1 => 'John Abbot',
			2 => 'John Birch',
			3 => 'John Cold',
		]
	```
	&nbsp;


- `row($table, $condition=NULL, $order=NULL)`: Fetches the first matching row from a table.
	Parameters:
	- `$table`: Table name, automatically escaped.
	- `$condition`, `$order`: see `Db::all()` above.

	Example:
	```php
	// Fetches the first row from table "tests" where column "firstname" matches "John" ordered by the column "lastname".
	$pico->db->row('tests', '`firstname` LIKE "John"', '`lastname` ASC');
	```
	Returns:
	```php
		[
			{id:1, firstname:'John', lastname:'Abbot'},
		]
	```
	&nbsp;


- `script($scriptName)`: Executes a script present in `/sql`.
	Parameters:
	- `$scriptName`: The script name without extension. 

	Example:
	```php
	// Executes scripts/sql/auto_i18n_fr.sql
	$pico->db->script('auto_i18n_fr');
	```
	Returns the number of rows altered by the last command on the script.

	&nbsp;


- `sql($sql)`: Executes a command and returns the results as a PHP array. This is useful when SQL joining or grouping is needed.
	Parameters:
	- `$sql`: The SQL command.

	Example:
	```php
	// Executes scripts/sql/auto_i18n_fr.sql
	$pico->db->sql('SELECT * FROM `tests` WHERE `firstname` LIKE "John" LIMIT 0,3');
	```
	Returns:
	```php
		[
			{id:1, firstname:'John', lastname:'Abbot'},
			{id:2, firstname:'John', lastname:'Birch'},
			{id:3, firstname:'John', lastname:'Cold'},
		]
	```
	&nbsp;


- `value($table, $value, $condition=NULL, $order=NULL, $limit=NULL)`: Fetches the first matching single field from a table.
	Parameters:
	- `$table`: Table name, automatically escaped.
	- `$value`: The field (column) name.
	- `$condition`, `$order` and `$limit`: see `Db::all()` above.

	Example:
	```php
	// Fetches the first "lastname" of the first 3 rows from table "tests" where column "firstname" matches "John" ordered by the column "lastname".
	$pico->db->row('tests', '`firstname` LIKE "John"', '`lastname` ASC');
	```
	Returns:
	```php
		(string) 'Abbot'
	```
	&nbsp;


---

[^1]\: PDO is [the default interface that ships with PHP](https://www.php.net/manual/en/intro.pdo.php). 

[^2]\: [phpMyAdmin](https://www.phpmyadmin.net/) is the most well-known and available on various hosts.