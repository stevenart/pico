# Core vs App

Folders `core` and `app` have the same structure (see [Files arborescence](arch/arch?id=files-arborescence))... let's see why.


## The core folder

The `core` folder contains almost everything needed for PicoPHP to run by default. It contains basic controllers, cmsplugins, models and views. You should never update anything in this folder. All the code for your app/website is in the `app` folder.


## The app folder

By default, the `app` folder contains the bare bones of a website. You should erase/modify the files as you see fit. 


## Why both?

You should think of this structure as inheritance: the `app` folder extends the `core` folder. If a PHP script factors a new `CMSUserModel` object, the autoloader will look for the file in `app/models` then, if it doesn't find it there, it will look in `core/models`. 

This systems allows for a simple update of the code: 

- In theory, you could replace the `core` folder of any app/website by the `core` folder from the latest PicoPHP and enjoy patches and fixes without breaking anything. [^1]

- You can always overwrite a `core` file by creating a file by the same name in `app/`.

---

[^1]\: In practice, you should use the update script when possible as it will provide more information.