# Routing

In a web CMS, "routing" is the process used to parse the URL and find the proper controller to deal with that request. This process is usually based on a file containing rules to match URLs with controllers, that is why the routing is called "explicit".

In PicoPHP, there is no such file: the routing is "implicit". PicoPHP will find the proper controller based on the file structure of your project alone. Let's see how that work...

A complete PicoPHP URL is as follow :
```
https://domain.tld/language/controller/method/more?parameter
```

## The protocol and domain

Protocol and domain plays no part in the routing in PicoPHP. Protocol should always be `https` (`http` in few cases like local) and domain is irrelevant. You can run the same app/website on `localhost`, `beta.domain.tld` or `domain.tld` without any difference for your app.

For environments, see [How do controllers work?](arch/config.md).


## The language

The first part of the URL is the language. It is a two character code from the list defined in `config.app.ini` under the `languages` directive. If the URL doesn't contain it, PicoPHP will automatically redirect to the best available language. 

For example if the URL is:
```
https://mywebsite.com/test
```

It will automatically be redirected to: [^1] 
```
https://mywebsite.com/en/test
```



## The controller

The second part of the URL is simply the name of the controller.

For example if the URL is:
```
https://mywebsite.com/en/test
```

The controller called to deal with the request is `test` and the file will be `app/controllers/test.controller.php`.[^2] 


## The method

If there is a third part, this will indicate the method of that controller to use. For example if the URL is :
```
https://mywebsite.com/en/test/something
```
The method `TestController::something()` will be called to deal with the request. If that method doesn't exists or isn't publicly accessible or if the URL doesn't specify a third part, the default method `TestController::_()` will be called.


## The rest

The following parts of the URL will be for the method to deal with. Each method can use them the way the developer chooses. This is also true for GET/POST parameters.

&nbsp;

?> **Wait, there is more!** Check out [How do controllers work?](controllers/controllers.md) to learn more about controllers.

---

[^1]\: If `en` is the best available language, depending on your computer and the [configuration](arch/config.md), it can change.

[^2]\: ...or `core/controllers/test.controller.php`, see [Core vs App](arch/core.md).