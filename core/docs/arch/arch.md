# General Architecture

Like most web frameworks, PicoPHP is based on the [MVC](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller) architectural pattern. Also like most frameworks, it has a specific flavor of the pattern... 

## Handling requests

Here are the basics of how PicoPHP handles a request:

1. Every request[^1] is "redirected" to `www/index.php` via apache's mod_rewrite and the `www/.htaccess` file.[^2]


2. The file `www/index.php` creates a global variable named `$pico` that is an object of the Pico class (`core/pico.class.php`). This object is used to parse the request (via the `Pico::start()` function) and will also be used inside the app (that is why it is created as a global variable using the [singleton pattern](https://en.wikipedia.org/wiki/Singleton_pattern)).


3. The main goal of the `Pico::start()` function is to set the right language and find the right controller to deal with the request (see [Routing](arch/routing.md)).


4. If the controller needs to fetch data from the database or build a form, it will probably create a `Model` object to do so (see [Models](models/models.md)).


5. The controller should return HTML[^3] and to do so, it will use a view. In PicoPHP, the view is simply a Twig file (see [Views](views/views.md)).


## Files arborescence

Here is a list of folders found in PicoPHP and what they mean:

- `app`: This folder contains all __*your*__ code. Ideally, you shouldn't write anything elsewhere [^4].

	- `cmsplugins`: CMS plugins created for your app/website (see [Plugins](cms/plugins.md)), or overwritten from `core`.

	- `controllers`: Controllers created for your app/website (see [Controllers](controllers/controllers.md)), or overwritten from `core`.

	- `css`: The [SCSS](https://sass-lang.com/) files needed by your app/website. These files will be accessed via `www/css.php` (see [CSS/JS/images](views/cssjsimg.md). If you want to use regular CSS, you can do so in a SCSS file since CSS is valid SCSS.

	- `img`: Images needed by your app/website. These files will be accessed via `www/img.php` (see [CSS/JS/images](views/cssjsimg.md)).

	- `js`: Javascript needed by your app/website. These files will be accessed via `www/js.php` (see [CSS/JS/images](views/cssjsimg.md)). 

- `cache`: This folder is internal caching by PicoPHP. It should be ignored, can be deleted at any moment and will be recreated automatically.

- `cdn`: This folder contains every file uploaded via the CMS or any `Model` using a file-type field (see [CDN](cms/cdn.md)). 

- `core`: This folder has the same structure as `app`... It contains all the basics cmsplugins, controllers, models, etc. that make PicoPHP. You should never modify this folder. (see [Core vs App](arch/core.md)). 

- `docs`: This documentation, built using [docsify](https://docsify.js.org). It can be removed unless you want to keep a copy of this documentation bundled with your project. 

- `sql`: SQL files used by PicoPHP. If you have your own SQL scripts, you can add them here.

- `vendor`: Third-party dependencies managed by [Composer](https://getcomposer.org/). Dependencies used by PicoPHP are listed in `composer.core.json` and included in `composer.json` via the [composer-merge plugin](https://github.com/wikimedia/composer-merge-plugin). This leaves the `composer.json` file to your app (just don't remove the `composer-merge-plugin` config).

- `www`: This is the DocumentRoot, it contains files that should be accessible directly.

	- `cdn.php`: CDN access point (see [CDN](cms/cdn.md)). 

	- `css.php`, `img.php` and `js.php`: CSS, images and JavaScript access points (see [CSS/JS/images](views/cssjsimg.md)). 

	- `firstrun.php`: First run script for local installation.

	- `index.php`: Request handler.

	- `robots.php`: `robots.txt` generator. You can update this file to match your app/website.

	- `sitemap.php`: `sitemap.xml` generator. You can update this file to match your app/website.

- `.gitignore`: This contains the files that are ignored in Git by default. You can update it if you need/want to.

- `.gitlab-ci.yml`: This is the GitLab script to update documentation on push. It should be removed from your app/website if present.

- `.htaccess`: The sole purpose of this file is to allow the framework to work if `www/` cannot be set as the DocumentRoot. The main .htaccess file is `www/.htaccess`.

- `autoload.php`: This is the default autoloader.


---

[^1]\: Some exceptions exist, like existing files. See `www/.htaccess` for details.

[^2]\: The main .htaccess file is `www/.htaccess`, the one in `/.htaccess` is only there to allow the framework to work if `www/` cannot be set as the DocumentRoot.

[^3]\: Unless it is used for an API or any other specific use.

[^4]\: There are a exceptions of course (public resources in `resources`, etc.) but 99% of the code you write should be here.