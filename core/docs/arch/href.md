# Links

Writing links in a webapp or website can be tricky. Ideally you want your links to be:
- **environment-proof**: working on `localhost` and `beta.domain1.tld` and `domain2.tld` all the same.
- **DocumentRoot-proof**: working when placed in root or in a folder.
- **CMS-proof**: working even when the page URIs changes.

To do that, PicoPHP allows you to build links based on a single character code that will be compiled into a valid URL. 


## Introducing *href*

*href* is the name of the function called that will transform the code into a URL. The first character of the *href* string indicates the type of link and the rest is the standard URL.

For exemple, a leading `;` indicates "the root of the app/website". So the *href* `;cms` built on `https://domain.tld` will be compiled into `https://domain.tld/cms`. And if compiled on `http://localhost:8888` it will be `http://localhost:888/cms`.

With that, links are simpler to write and domain/protocol agnostic. Great! Now let's see what the possible first characters are.


## The first "code" character of an *href*

Here are the possible first characters of an *href* and what they mean. Each example is as if compiled on `https://domain.tld`.

- `;`: The root domain without language. Examples:
	- `;` → `https://domain.tld`
	- `;test` → `https://domain.tld/test`
	- `;test/something?param=yay` → `https://domain.tld/test/something?param=yay`

- `:`: The root domain with current language. Examples (if user current language is English):
	- `:` → `https://domain.tld/en/`
	- `:test` → `https://domain.tld/en/test`
	- `:test/something?param=yay` → `https://domain.tld/en/test/something?param=yay`

- `~`: The current URL without GET parameters. Examples:
	- `~` on `https://domain.tld/en/test?param=yay` → `https://domain.tld/en/test`
	- `~something` on `https://domain.tld/en/test?param=yay` → `https://domain.tld/en/test/something`

- `#`: Keeping the current only changing GET parameters. Examples:
	- `#param=yay` on `https://domain.tld/en/test` → `https://domain.tld/en/test?param=yay`
	- `#param=yay&param2=great` on `https://domain.tld/en/test?param=nope` → `https://domain.tld/en/test?param=yay&param2=great`

- `*`: Getting the first page managed by a `PageController`[^1]\. Example on a website where the page managed by the `Page_PostsController` ("posts") is named "blog":  
	- `*posts` → `https://domain.tld/en/blog`

- `^`: Getting the first page containing a specific widget[^2]\. Example on a website where the `contactForm` widget is activated on the page named "Contact":  
	- `^contactForm` → `https://domain.tld/en/page/contact_1`

- `$`: Getting an image from the `/app/img` folder (not the [CDN](cms/cdn.md)). Example:
	- `$test.png` → `https://domain.tld/img.php?file=test.png` (the src for image `/app/img/test.png`)
	- `$folder/test.png` → `https://domain.tld/img.php?file=folder/test.png` (the src for image `/app/img/folder/test.png`)

- `|`: Changing the language. Examples (user current language is English):
	- `|fr` on `https://domain.tld/en/` → `https://domain.tld/fr/`
	- `|fr` on `https://domain.tld/en/test?param=yay` → `https://domain.tld/fr/test?param=yay`


## *href* from a controller (PHP)

You can build an *href* anywhere you have access to the global object `$pico` with the method `href($href='', $includeHost=true, $disableCache=false)`:
```php
global $pico;
echo $pico->href(':test'); 	// https://domain.tld/en/test 
```

The method has 2 optional parameters: 
- `$includeHost`: If set to false the URL will be relative. For example: `https://domain.tld/en/test` whould be `/en/test `
- `$disableCache`: Automatically adds a GET parameter to prevent browser caching. It can be useful but shouldn't be used in production.


## *href* from a view (Twig)

There is also a Twig filter to build links in views (the most common use). Examples:
```twig
<a href="{{ ':'|href }}" class="brand">Brand</a>

<a href="{{ ':test'|href }}">Test</a>

<img src="{{ '$test.png'|href }}"/>
```


## *href* from CSS

No, *href* is NOT accessible from CSS but you can access images in `/app/img` in you SCSS files without manually linking to `img.php`. To do so, use ":" as the first character in the `url()`. Example:
```css
body {
	background: #ffffff url(:test.png) center center no-repeat;
}
```
`:test.png` will automatically be converted to `https://domain.tld/img.php?file=test.png` before sending the CSS to the browser.


## Idempotence

*href* are [idempotent](https://en.wikipedia.org/wiki/Idempotence) which means that you can call the PHP `href` function or the Twig `href` filter multiple times on the same string without breaking. 

Why? Because if the *href* doesn't start with a character described above, it will return the string passed in parameter. And since no valid URL starts with any of the codes, there is no chance of recursively changing the URL.

Why is that of any importance? Because that makes *href* "retro-compatible" with links, meaning you can use href on any string without knowing if it is an URL or an *href*.

For example, if I'm not sure if the variable $url is a URL or an *href*, I can always compile it:
```php
global $pico;
echo $pico->href($url); 	
/* 
If $url is a valid href like ":test", the URL will be https://domain.tld/en/test.
If $url is a full URL like "https://php.net", il will be kept "https://php.net".
*/
```


---

[^1]\: To learn more about `PageController`, please read [How do controllers work?](controllers/controllers.md). 

[^2]\: To learn more about widgets and pages, please read [Pages](cms/pages.md).