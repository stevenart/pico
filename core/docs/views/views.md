# How do views work?

A view is a Twig file that is used to serve HTML to the client. It is the presentation layer as all the data should be defined in models and logic should be done in controllers. Views receives data from the controller and displays it how it need to be displayed. There is no view without a controller: a view is always the result of a controller function.

?> Views are built using [Twig](https://twig.symfony.com/doc/), a very popular and well documented templating system used for example in the Symfony framework. 


## Structure

A basic controller is a Twig file, located in `/app/views`[^1]. 

Usually, views extends the `_skeleton.twig` witch contains the doctype, the head and everything needed for Pico to work (CSS, JS, etc.). This is not a technical requirement, you could start from scratch or use another base view.


## Skeleton

Here is a very simple view using `_skeleton.twig`:
```twig
{% extends '_skeleton.twig' %}

{% block body %}
	<h1>Hello, World!</h1>
{% endblock %}
```

When extending a view, you can only write content inside blocks. `_skeleton.twig` has multiple blocks you can extends:

- `headStart` : Right at the start of the `head`, usefull to add tracking scripts like GTM. (If you really need to.)

- `head` : If you need to add something in the head section, this is where it's at.

- `bodyStart` : Right at the start of the `body` tag. Usually, you should use `body` instead.

- `body` : This is where your content go. It will be encapsulated inside a `#global` div.

- `js` : This is where your in-page JavaScript goes. It is loaded after the content and you will have access to external libraries at this point.

- `bodyEnd` : Right at the end of the `body` tag. Usually, you should use `body` instead.


---

[^1]\: ...or `/core/views`, see [Core vs App](arch/core.md).