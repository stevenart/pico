# Twig extensions

Twig allows to create extensions that can contains filters, functions and tests for your application. By default, Pico loads 2 extensions: `CoreTwig` and `AppTwig`.

?> Be sure to checkout [the official documentation](https://twig.symfony.com/doc/3.x/advanced.html) to learn more about extending Twig.


## CoreTwig

`CoreTwig` is an extension loaded with Twig in Pico and contains some usefull filters, functions and tests that you can use in your application. Like any file in `/core`, it is not supposed to by modified. You should use `AppTwig` for your custom needs.

### Filters


### Functions

- `cdnGallery` = Creates an HTML gallery from a CDN ids set.

- `cssLoad` = Loads one or more CSS files.

- `jsLoad` = Loads one or more CSS files.

- `token` = Creates a token and store it in session.

- `uploadMaxSize` = Gets upload max size from server configuration.


### Tests

- `external` = Return true if an URL is not pointing on the current domain.

- `numeric` = Return true if the value is a numeric value.



## AppTwig

`AppTwig` (located in `/app/apptwig.class.php`) is an empty shell, ready to receive your custom filters etc. It is automatically loaded with Twig in Pico.

