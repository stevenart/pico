# CSS, JS and images

Design resources and javascript are store outside the DocumentRoot. How does it work?


## CSS

CSS files are stored in `app/css/`. Files must be .scss files because Pico automatically compiles SCSS in CSS. Since CSS files are valid SCSS files, you don't have to change anything except the extension if you wish to use plain CSS.

The base `_skeleton.twig` view loads SCSS files defined in the `css` Twig variable. So if you add:
```twig
{% set css = ['pico', 'app', 'fonts']|merge(css|default([])) %}
```
before extending `_skeleton.twig`, it will load `app.scss` and `fonts.scss`. Using the `merge` prevent erasing parent styles (for exmaple `_skeleton.twig` loads `pico.scss`).

With this technique, Pico automatically generates one minified CSS file that is served with only one request. With this simple function, most cases won't need extenal tooling like Webpack.

You can also load a single file with the `cssLoad` Twig function and enjoy minification and SCSS compilation:
```twig
{{ cssLoad('my_scss_file') }}
```


## JS

JS files are stored in `app/js/`. And works almost excatly like SCSS.

The base `_skeleton.twig` view loads JS files defined in the `js` Twig variable that can be populated the same way:
```twig
{% set js = ['pickadate']|merge(js|default([])) %}
```

Same way that CSS are automatically minified and concatenated, Pico minifies JS that are not and load a single bundle with all files. No external tools required.

You can also load a single file with the `jsLoad` Twig function and enjoy minification:
```twig
{{ jsLoad('my_js_file') }}
```


### Images

Images files are stored in `app/img/` and can be accessed in two ways:

- in Twig files, with the `href` filter (see [Links](/arch/href.md)). Example:
```twig
{{ '$logo.png'|href }}
```

- in SCSS files, with the special char `:` that will be compiled in the correct URL. Example:
```css
.logo {
	background-image: url(:logo.png);
}
```


?> Make sure you understand the difference between images (used for website design for example) and CDN files which are files uploaded by an authorized used (that can be images). Check [CDN](cms/cdn.md) for more details.














