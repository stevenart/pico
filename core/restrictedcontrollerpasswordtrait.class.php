<?php


trait RestrictedControllerPasswordTrait {


	public function restrictedCookie($name, $value=NULL, $clear=false, $lifetime='30days') {
		global $pico;
		return $pico->cookie($this->name.'Restricted'.ucfirst($name), $value, $clear, $lifetime);
	}


	private function restrictedSession($name, $value=NULL, $clear=false) {
		global $pico;
		return $pico->session($this->name.'Restricted'.ucfirst($name), $value, $clear);
	}


	public function _authorized() {
		global $pico;
		if (!$this->restrictedSession('password') && $this->restrictedCookie('password')) {
			$this->restrictedSession('password', $this->restrictedCookie('password'));
		}
		return ($this->restrictedSession('password')==$this->restrictedPassword);
	}


	public function _login() {	
		global $pico;
		$view = property_exists($this, 'restrictedView') ? $this->restrictedView : '_restricted.twig';
		if (CoreTwig::viewExists($view)) {
			$this->variable('restrictedType', 'password');
			if (isset($_POST['password'])) {
				if (md5($_POST['password'])==$this->restrictedPassword) {
					$this->restrictedSession('password', $this->restrictedPassword);
					$this->restrictedCookie('password', $this->password);
					$pico->url('#');
				} else {
					$this->variable('restrictedControllerError', 'password');
				}
			}
			$this->view = $view;
		} else {
			$pico->_401();
		}
	}


	public function _logout() {
		global $pico;
		$this->restrictedSession('password', NULL, true);
		$this->restrictedCookie('password', NULL, true);
		$pico->url('-');
	}	


}