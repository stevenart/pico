<?php

class Pico {

	private $blockPages;
	private static $instance;
	private $instanceDb;
	private $instanceCDN;
	private $instanceI18n;
	private $instancePluginPages;
	private $instanceWidgetPages;
	private $twigLoader;
	private $variables;
	
	public $debugBar;
	public $config;
	public $env;
	public $outputed;
	public $twig;
	public $urlString;
	public $urlParts;
	
	
	private function __construct() {
		/* Initializing session */
		if (!isset($_SESSION['__pico']) || (!is_array($_SESSION['__pico']))) {
			$_SESSION['__pico'] = array();
			$_SESSION['__pico']['data'] = array();
			$_SESSION['__pico']['id'] = session_id();
		}

		/* Environment */
		if (method_exists('Hooks', 'env')) {
			$this->env = Hooks::env();
		} else {
			$this->env = '';
		}

		/* Loading config */
		$this->config = parse_ini_file(__ROOT.'/core/config.core.ini', true);
		if (file_exists(__ROOT.'/app/config.app.ini')) {
			$appConfig = parse_ini_file(__ROOT.'/app/config.app.ini', true);
			foreach($appConfig as $key => $values) {
				if (isset($appConfig[$key])) {
					$this->config[$key] = array_merge(
						(isset($this->config[$key]) ? $this->config[$key] : array()), 
						(array)$appConfig[$key]
					);
				}
			}
		}
		if ($this->env) {
			if (file_exists(__ROOT.'/app/config.'.$this->env.'.ini')) {
				$envConfig = parse_ini_file(__ROOT.'/app/config.'.$this->env.'.ini', true);
				foreach($envConfig as $key => $values) {
					if (isset($envConfig[$key])) {
						$this->config[$key] = array_merge(
							(isset($this->config[$key]) ? $this->config[$key] : array()), 
							(array)$envConfig[$key]
						);
					}
				}
			}
		}

		/* DebugBar */
		$this->debugBar = false;
		if (isset($this->config['site']['debugbar']) && (intval($this->config['site']['debugbar'])>0)) {
			$this->debugBar = new DebugBar\DebugBar();
			$this->debugBar->addCollector(new DebugBar\DataCollector\PhpInfoCollector());
			$this->debugBar->addCollector(new DebugBar\DataCollector\MessagesCollector());
			$this->debugBar->addCollector(new DebugBar\DataCollector\RequestDataCollector());
			$this->debugBar->addCollector(new DebugBar\DataCollector\PDO\PDOCollector($this->db));
			$this->debugBar->addCollector(new DebugBar\DataCollector\TimeDataCollector());
			$this->debugBar->addCollector(new DebugBar\DataCollector\MemoryCollector());
			$this->debugBar->addCollector(new DebugBar\DataCollector\ExceptionsCollector());
			$this->debugBar->addCollector(new DebugBar\DataCollector\ConfigCollector($this->config));
			$this->debugBar->addCollector(new PicoCollector());
		}
		
		$this->blockPages = array();

   		/* Loading Twig */
  		if ( !file_exists(__ROOT.'/cache/twig') ) {
  			@mkdir(__ROOT.'/cache/twig', 0777, true);
		}
		try {
			$this->twigLoader = new \Twig\Loader\FilesystemLoader([
				__ROOT.'/app/views', 
				__ROOT.'/core/views',
				__ROOT.'/www/resources/tinymce',
			]);
			$this->twig = new \Twig\Environment($this->twigLoader, array( 
				'cache' => __ROOT.'/cache/twig',
				'auto_reload' => true,
				'trim_blocks' => true
			));
			$this->twig->addExtension(new CoreTwig());
			$this->twig->addExtension(new AppTwig());
			if ($this->debugBar) {
				$this->twig->addExtension(new DebugBar\Bridge\Twig\MeasureTwigExtension($this->debugBar['time']));
				$this->twig->addExtension(new DebugBar\Bridge\Twig\DebugTwigExtension($this->debugBar['messages']));
				$this->twig->addExtension(new DebugBar\Bridge\Twig\DumpTwigExtension());
				$twigProfile = new Twig\Profiler\Profile();
				$this->twig->addExtension(new Twig\Extension\ProfilerExtension($twigProfile));
				$this->debugBar->addCollector(new DebugBar\Bridge\NamespacedTwigProfileCollector($twigProfile));
				$this->twig->enableDebug();
			}
		}
		catch (TypeError $e) {
			$this->fatal('Could not load Twig (TypeError). Is PHP version >= 7.4?');
		}
		catch (Exception $e) {
			$this->fatal('Could not load Twig, logs should help.');
		}
		$this->variables = array();
	}
	
	
	public function __get($name) {
		switch($name) {

			case 'cdn':
				if (!isset($this->instanceCDN) || (!is_array($this->instanceCDN))) {
					if ($this->db->count('cdn')<2000) {
						$this->instanceCDN = $this->db->bags('cdn', 'id', '*');
					} else {
						$this->instanceCDN = [];
					}
				}
				return $this->instanceCDN;
				break;

			case 'db':
				if (!isset($this->instanceDb) || (!is_a($this->instanceDb, "Db"))) {
					try {
						$this->instanceDb = new Db($this->config["database"]);
					}
					catch (PDOException $e) {
						if ($this->config['database']['firstrun'] && in_array($e->getCode(), array(1049, 1044, 1045))) {
							$this->url(';firstrun.php');
						} else {
							$this->fatal('Database failure.');
						}
					}
				}
				return $this->instanceDb;
				break;

			case 'i18n':
				if (!isset($this->instanceI18n) || (!is_a($this->instanceI18n, "I18n"))) {
					$this->instanceI18n = new I18n(NULL, $this->config["i18n"]);
				}
				return $this->instanceI18n;
				break;	

			case 'pluginPages':
				if (!isset($this->instancePluginPages) || (!is_array($this->instancePluginPages))) {
					$this->instancePluginPages = $this->db->pairs('pages', '`plugin`', '`uri`', '`type`="plugin" AND `lang`="'.$this->i18n->lang().'" AND `__draft`=0');
				}
				return $this->instancePluginPages;
				break;

			case 'widgetPages':
				if (!isset($this->instanceWidgetPages) || (!is_array($this->instanceWidgetPages))) {
					$this->instanceWidgetPages = array();
					foreach($this->db->pairs('pages', '`widgets`', '`uri`', '`widgets` IS NOT NULL AND `lang`="'.$this->i18n->lang().'" AND `__draft`=0') as $widgets => $uri) {
						foreach(explode(',', $widgets) as $widget) {
							$this->instanceWidgetPages[$widget] = $uri;
						}
					}
				}
				return $this->instanceWidgetPages;
				break;

		}
		return NULL;
		
	}


	public function _401() {
		global $pico;
		if (!headers_sent()) {
			header('HTTP/1.0 401 Authorization Required'); 
		}
		$pico->variable('_pico', [
			'action' => '_401',
			'basepath' => __URL,
			'controller' => '',
			'gdpr' => $pico->config['site']['gdpr'] ? $pico->cookie('gdpr') : true,
			'lang' => $this->i18n->lang(),
			'langs' => $this->i18n->config('languages'),
			'locale' => $this->i18n->locale(),
			'now' => time(),
			'session' => session_id(),
			'url' => $this->urlPartss,
		], false, true);
		echo $pico->twig->render('_401.twig', []);
		die();
	}


	public function _404() {
		global $pico;
		if (!headers_sent()) {
			header('HTTP/1.0 404 Not Found');
		}
		$pico->variable('_pico', [
			'action' => '_404',
			'basepath' => __URL,
			'controller' => '',
			'gdpr' => $pico->config['site']['gdpr'] ? $pico->cookie('gdpr') : true,
			'lang' => $this->i18n->lang(),
			'langs' => $this->i18n->config('languages'),
			'locale' => $this->i18n->locale(),
			'now' => time(),
			'session' => session_id(),
			'url' => $this->urlPartss,
		], false, true);
		echo $pico->twig->render('_404.twig', []);
		die();
	}


	public function cdnHref($cdn, $thumb='', $download=false, $relative=false) {
		global $pico;
		if (!is_array($cdn)) {
			$cdn = intval($cdn);
			if (!isset($pico->cdn[$cdn])) {
				$this->instanceCDN[$cdn] = $pico->db->row('cdn', '`id`='.$cdn);
			}
			$cdn = $pico->cdn[$cdn];
		}
		if (!is_array($cdn)) {
			return false;
		}
		if ($thumb) {
			if (strpos($thumb, '_')) {
				$cdn['extension'] = explode('_', $thumb)[1];
				$cdn['name'] = substr($cdn['name'], 0, strrpos($cdn['name'], '.')).'.'.$cdn['extension'];
			}
		}
		if (intval($pico->config['cdn']['rewrite_href'])>0) {
			if ($pico->config['cdn']['url_encode']) {
				$cdn['name'] = urlencode($cdn['name']);
			}
			$url = ($relative ? '' : __URL).'/cdn/'.$cdn['id'].'_'.$cdn['key'].($thumb ? '_'.$thumb : '').'/'.$cdn['name'];
			if ($download) {
				$url.= '?download';
			}
		} else {
			$url = ($relative ? '' : __URL).'/cdn.php?file='.$cdn['id'].'_'.$cdn['key'].($thumb ? '_'.$thumb : '').'.'.$cdn['extension'];
			if ($download) {
				$url.= '&download';
			}
		}
		return $url;
	}

	
	public function cookie($name, $value=NULL, $clear=false, $lifetime='30days') {
		if ($clear) {
			setrawcookie($name, "", 1, "/");
		}
		if (!is_null($value)) {
			if (is_string($lifetime)) {
				$lifetime = strtotime($lifetime)-time();
			}
			setrawcookie($name, $value, time()+$lifetime, '/');
		}
		return isset($_COOKIE[$name]) ? $_COOKIE[$name] : NULL;
	}


	public function fatal($log=NULL) {
		if (!headers_sent()) {
			header('HTTP/1.0 503 Service Unavailable', true, 503);
			header('Retry-After: 3600');
		}
		echo file_get_contents(appfile('views/_fatal.html'));
		varlog($log, 'FATAL ERROR ('.date('d/m/Y H:i:s').')');
		die();
	}


	public function href($href='', $includeHost=true, $disableCache=false) {
		global $pico;
		$host = $includeHost ? __URL.'/' : '/';
		if (empty($href)) return $host;
		$url = '';
		if (!empty($href)) {
			switch( substr($href, 0, 1) ) {
				case ';': // Root without lang
					$url = $host . substr($href, 1);
					break;
				case ':': // Root with lang
					$url = $host . $this->i18n->lang() . '/' . substr($href, 1);
					break;
				case '|': // Full URL with GET, only changes lang part
					if (isset($this->urlParts) && isset($this->urlParts[1]) && ($this->urlParts[1]!='page')) {
						$url = $host . substr($href, 1, 2) . '/' . $this->url() . Tools::paramsArrayToString($_GET);
					} else {
						$url = $host . substr($href, 1, 2) . Tools::paramsArrayToString($_GET);
					}
					break;
				case '~': // Full URL without GET
					$url = $host . $this->i18n->lang() . '/' . $this->url() . '/' . substr($href, 1);
					break;
				case '#': // Full URL with GET, only changes provided parameters (no leading ? needed)
	 				$params = array_merge($_GET, Tools::paramsStringToArray(substr($href, 1)));
					$url = $host . $this->i18n->lang() . '/' . $this->url() . Tools::paramsArrayToString($params);
					break;
				case '-': // The first part of the URL (usually the controller)
					$position = stripos($this->url(), '/');
					if ($position===false) {
						$url = $host . $this->i18n->lang() . '/' . $this->url() . '/' . substr($href, 1);
					} else {
						$url = $host . $this->i18n->lang() . '/' . substr($this->url(), 0, $position) . '/' . substr($href, 1);
					}
					break;
				case '_': // The first 2 parts of the URL (usually the controller and the action/cmsplugin)
					$position = stripos($this->url(), '/', stripos($this->url(), '/')+1);
					if ($position===false) {
						$url = $host . $this->i18n->lang() . '/' . $this->url() . '/' . substr($href, 1);
					} else {
						$url = $host . $this->i18n->lang() . '/' . substr($this->url(), 0, $position) . '/' . substr($href, 1);
					}
					break;
				case '*': // First page managed by plugin X
					$position = stripos(substr($href, 1), '/');
					$url = '#';
					if ($position===false) {
						$plugin = substr($href, 1);
						if ($plugin && isset($this->pluginPages[$plugin])) {
							$url = $host . $this->i18n->lang() . '/' . $this->pluginPages[$plugin];
						}
					} else {
						$plugin = substr(substr($href, 1), 0, $position);
						if ($plugin && isset($this->pluginPages[$plugin])) {
							$url = $host . $this->i18n->lang() . '/' . $this->pluginPages[$plugin] . substr(substr($href, 1), $position);
						}
					}
					break;
				case '^': // First page with widget X
					$position = stripos(substr($href, 1), '/');
					$url = '#';
					if ($position===false) {
						$widget = substr($href, 1);
						if ($widget && isset($this->widgetPages[$widget])) {
							$url = $host . $this->i18n->lang() . '/' . $this->widgetPages[$widget];
						}
					} else {
						$widget = substr(substr($href, 1), 0, $position);
						if ($widget && isset($this->widgetPages[$widget])) {
							$url = $host . $this->i18n->lang() . '/' . $this->widgetPages[$widget] . substr(substr($href, 1), $position);
						}
					}
					break;
				case ']': // First page containing a X block
					$position = stripos(substr($href, 1), '/');
					$url = '#';
					$block = ($position===false) ? substr($href, 1) : substr(substr($href, 1), 0, $position);
					if ($block) {
						if (!isset($this->blockPages[$block])) {
							$this->blockPages[$block] = $pico->db->value('pages', '`uri`', '`type`="blocks" AND `blocks` LIKE "%%22_block%22%3A%22'.safer($block, 'db').'%22%"');	
						}
						if ($this->blockPages[$block]) {
							$url = $host . $this->i18n->lang() . '/' .$this->blockPages[$block];
							if ($position!==false) {
								$url.= substr(substr($href, 1), $position);
							}
						}
					}
					break;
				case '%': // CDN
					$cdn = explode('.', substr($href, 1), 3);
					$url = $pico->cdnHref(intval($cdn[0]), $cdn[1] ?? '', $cdn[2] ?? false);
					break;
				case '$': // Non-CDN resource (in /img)
					$url = $host.'img.php?file='.substr($href, 1);
					break;
				default:
					$url = $href;
			}
		}
		$url = preg_replace("/([^:])\/\//", "$1/", stripslashes($url));
		if ($disableCache) $url.= (strpos($url, '?') ? '&' : '?').'__pico_cache='.time();
		if (method_exists('Hooks', 'picoHref')) {
			Hooks::picoHref($url, $href, $includeHost, $disableCache);
		}
		return $url;
	}


	public function meta($name, $value=NULL, $clear=false) {
		if (is_null($value)) {
			$metas = $this->variable('metas');
			return empty($metas[$name]) ? NULL : $metas[$name];
		} else {
			return $this->variable('metas', array($name=>$value), $clear, true);
		}
	}

		
	public function session($name, $value=NULL, $clear=false, $merge=false) {
		if ($clear) {
			unset($_SESSION['_pico']['data'][$name]);
		}
		if (!is_null($value)) {
			if (isset($_SESSION['_pico']['data'][$name]) && is_array($_SESSION['_pico']['data'][$name]) && is_array($value) && $merge) {
				$_SESSION['_pico']['data'][$name] = array_merge($_SESSION['_pico']['data'][$name], $value);
			} else {
				$_SESSION['_pico']['data'][$name] = $value;
			}
		}
		return isset($_SESSION['_pico']['data'][$name]) ? $_SESSION['_pico']['data'][$name] : NULL;
	}
	
	
	public static function singleton() {
		if (!isset(self::$instance)) {
			$class = __CLASS__;
			self::$instance = new $class;
		}
		return self::$instance;
	}


	public function start($url) {
		global $pico;

		/* Cleaning url */
		if (substr($url, -1)=='/') {
			$this->urlString = substr($url, 0, -1);
		} else {
			$this->urlString = $url;
		}
		$this->urlString = strtolower($this->urlString);
		$this->urlParts = explode('/', $this->urlString);

		/* Languages */
		if (in_array($this->urlParts[0], $this->i18n->langs(NULL, true, false))) {
			if ($this->urlParts[0]!==$this->i18n->lang()) {
				$this->i18n->lang($this->urlParts[0]);
			}
			$this->urlString = substr($this->urlString, strlen($this->urlParts[0])+1);
		} else {
			$this->url(':'.$this->urlString);
		}

		/* Starting defer if not ajax */
		if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])!='xmlhttprequest') {
			$_SESSION['pico__defer'] = true;
			$_SESSION['pico__deferred'] = array();
		}
		
		/* Find the appopriate controller/action to deal with url */
		$i = 0;
		$context = array();
		$name = $this->urlString;
		if (strpos($name,'/') !== false) {
			$name = substr($name, 0, strpos($name,'/'));
		} else {
			$name = ucwords($this->urlString);
		}
		$name = str_replace(' ', '', ucwords(str_replace('_', ' ', $name)));
		if ($name==ucfirst($pico->config['site']['root_controller'])) {
			$this->url(':'.substr($this->urlString, strlen($name)+1).Tools::paramsArrayToString($_GET));
		}
		if (empty($name) || !class_exists(ucfirst($name).'Controller')) {
			$name = ucfirst($pico->config['site']['root_controller']);
			array_shift($this->urlParts);
			array_unshift($this->urlParts, $this->i18n->lang(), strtolower($name));
		}
		$is404 = false;
		/* Check if language is not CMS-only */
		if ((strtolower($name)!="cms") && (in_array($this->i18n->lang(), $this->i18n->config('cms_only')))) {
			$this->url(';'.$this->i18n->config('default').'/'.$this->urlString);
		}
		/* Continue dealing with url */
		if (class_exists(ucfirst($name).'Controller')) {
			$this->outputed = false;
			$class = $name . 'Controller';
			$object = new $class($name, array_slice($this->urlParts, 1));
			if (isset($object) && is_object($object) && (is_subclass_of($object, 'Controller'))) {
				if (!in_array('_authorized', get_class_methods($object)) || ($object->_authorized())) {
					if (isset($this->urlParts[2])) {
						if (in_array($this->urlParts[2], get_class_methods($object))) {
							$action = $this->urlParts[2];
							$pico->variable('_pico', array('action' => $action), false, true);
							$object->$action();
						} elseif (in_array('_404', get_class_methods($object))) {
							$pico->variable('_pico', array('action' => $action), false, true);
							$object->_404($this->urlParts[2]);
						} else {
							$object->_();
						}
					} else { 
						$object->_();
					}
				} elseif (in_array('_login', get_class_methods($object))) {
					$object->_login();
				} elseif (in_array('_401', get_class_methods($object))) {
					$object->_401();
				} else {
					$this->_401();
				}
				if (!$this->outputed) {
					$object->output();
				}
			} else {
				$this->_404();
			}
		} else {
			$this->_404();
		}
	}
	
	
	public function url($href=NULL, $redirect='302') {
		if (!is_null($href)) {
			$this->urlString = $this->href($href);
			if ($redirect==='302') {
				header("HTTP/1.1 302 Moved Temporarily", true, 302);
				header("Location: ".$this->urlString);
				die();
			} elseif ($redirect==='301') {
				header("HTTP/1.1 301 Moved Permanently", true, 301);
				header("Location: ".$this->urlString);
				die();
			} elseif ($redirect==='307') {
				header("HTTP/1.1 307 Temorary Redirect", true, 307);
				header("Location: ".$this->urlString);
				die();
			} elseif ($redirect==='meta') {
				die('<html><head><meta http-equiv="Refresh" content="0; url='.$this->urlString.'"></head><body>
					<noscript><p>Please follow <a href="'.$url.'">this link</a>!</p></noscript>
					<script>window.location.href='.json_encode($this->urlString).';</script></body></html>');
			} elseif ($redirect==='die') {
				die($this->urlString);
			}
		}
		return $this->urlString;
	}
	
	
	public function variable($name, $value=NULL, $clear=false, $merge=false, $mergeConfig=false) {
		if ($clear) {
			$this->twig->addGlobal($name, NULL);
			unset($this->variables[$name]);
		}
		if (!is_null($value)) {
			if ($merge) {
				if (!isset($this->variables[$name])) $this->variables[$name] = array();
				if (!is_array($this->variables[$name])) $this->variables[$name] = array($this->variables[$name]);
				if (is_array($value)) {
					if ($mergeConfig) {
						$this->variables[$name] = Tools::arrayMergeConfig($this->variables[$name], $value);
					} else {
						$this->variables[$name] = array_merge($this->variables[$name], $value);
					}
				} else {
					$this->variables[$name][] = $value;
				}
			} else {
				$this->variables[$name] = $value;
			}
			$this->twig->addGlobal($name, $this->variables[$name]);
		}
		return isset($this->variables[$name]) ? $this->variables[$name] : NULL;
	}


}