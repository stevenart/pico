<?php

if (method_exists('Hooks', 'cdnAccess')) {
	Hooks::cdnAccess();
}

$maxImageWidth = 2560;
$maxImageHeight = 2560;
$extractColor = true;


/**
 * Function used for devices with byte-ranges support (iOS) 
 * Source: https://mobiforge.com/design-development/content-delivery-mobile-devices
 */
function rangeDownload($file) {
	$fp = @fopen($file, 'rb');
	$size = filesize($file);
	$length = $size;
	$start = 0;
	$end = $size - 1;
	header("Accept-Ranges: 0-$length");
	if (isset($_SERVER['HTTP_RANGE'])) {
		$c_start = $start;
		$c_end   = $end;
		list(, $range) = explode('=', $_SERVER['HTTP_RANGE'], 2);
		if (strpos($range, ',') !== false) {
			header('HTTP/1.1 416 Requested Range Not Satisfiable');
			header("Content-Range: bytes $start-$end/$size");
			exit;
		}
		if ($range[0] == '-') {
			$c_start = $size - substr($range, 1);
		} else {
			$range  = explode('-', $range);
			$c_start = $range[0];
			$c_end   = (isset($range[1]) && is_numeric($range[1])) ? $range[1] : $size;
		}
		$c_end = ($c_end > $end) ? $end : $c_end;
		if ($c_start > $c_end || $c_start > $size - 1 || $c_end >= $size) {
			header('HTTP/1.1 416 Requested Range Not Satisfiable');
			header("Content-Range: bytes $start-$end/$size");
			exit;
		}
		$start  = $c_start;
		$end    = $c_end;
		$length = $end - $start + 1;
		fseek($fp, $start);
		header('HTTP/1.1 206 Partial Content');
	}
	header("Content-Range: bytes $start-$end/$size");
	header("Content-Length: $length");
 	$buffer = 1024 * 8;
	while(!feof($fp) && ($p = ftell($fp)) <= $end) {
		if ($p + $buffer > $end) {
			$buffer = $end - $p + 1;
		}
		set_time_limit(0);
		echo fread($fp, $buffer);
		flush();
	}
	fclose($fp);                 
}


if (isset($_SERVER['CONTENT_TYPE']) && (substr($_SERVER['CONTENT_TYPE'], 0, 20)=='multipart/form-data;')) {
	/**
	 * Uploading a file.
	 */

	if (!empty($_FILES) && is_array($_FILES) && isset($_POST['token'])) {
	
		global $pico;
		$pico = Pico::singleton();
		$result = array('error'=>false,'items'=>array());

		$tokens = $pico->session('tokens');
		if (isset($tokens['upload']) && ($tokens['upload']==$_POST['token'])) {
			if (!file_exists(__ROOT.'/cdn')) {
				@mkdir(__ROOT.'/cdn', 0777, true);
			}
			$cdn = new Model('cdn');
			$files = reset($_FILES);
			foreach($files['name'] as $index => $name) {
				$key = substr(md5_file($files['tmp_name'][$index]), 0, 10);
				$ext = strtolower(substr($name, strrpos($name, '.')+1));
				$size = filesize($files['tmp_name'][$index]);
				if ($pico->config['cdn']['avoid_duplicates'] && $cdn->fetch("`name`='$name' AND `key`='$key' AND `extension`='$ext' AND `size`=$size", '`id` DESC')) {
					$result['items'][] = $cdn->one();
				} else {
					$id = $cdn->nextId();
					$target = __ROOT."/cdn/${id}_${key}.${ext}";
					if (file_exists($target)) {
						unlink($target);
					}
					if (move_uploaded_file($files['tmp_name'][$index], $target)) {
						if (in_array($ext, Thumbnail::$driverExtensions)) {
							$imageSize = function_exists('getimagesize') ? getimagesize($target) : false;
							if (is_array($imageSize)) {
								$width = $imageSize[0];
								$height = $imageSize[1];
								$rotate = 0;
								$exif = @exif_read_data($target);
								if (isset($exif['Orientation'])) {
									switch ($exif['Orientation']) {
										case 3:
											$rotate = 180;
											break;
										case 6:
											$rotate = 270;
											break;
										case 8:
											$rotate = 90;
											break;
									}
								}
								if ($width>$maxImageWidth || $height>$maxImageHeight || $rotate) {
									Intervention\Image\ImageManager::{Thumbnail::$driver}()
										->read($target)
										->rotate($rotate)
										->scaleDown($maxImageWidth, $maxImageHeight)
										->save($target);
									$imageSize = getimagesize($target);
									$width = $imageSize[0];
									$height = $imageSize[1];
								}
								$ratio = $width / $height;
								if ($extractColor) {
									if ($ecOriginalImage = @imagecreatefromstring(@file_get_contents($target))) {
										$ecPixelImage = imagecreatetruecolor(1, 1);
										imagecopyresampled($ecPixelImage, $ecOriginalImage, 0, 0, 0, 0, 1, 1, imagesx($ecOriginalImage), imagesy($ecOriginalImage));
										$color = '#'.dechex(imagecolorat($ecPixelImage, 0, 0));
										$colorR = hexdec($color[1].$color[2]);
										$colorG = hexdec($color[3].$color[4]);
										$colorB = hexdec($color[5].$color[6]);
										$lightness = ((max($colorR, $colorG, $colorB) + min($colorR, $colorG, $colorB)) / 510.0) * 100;
									}
								}
							}
						} elseif (in_array($ext, array('svg'))) {
							$fileContent = file_get_contents($target);
							preg_match("/viewbox=[\"']\d* \d* (\d*+(\.?+\d*)) (\d*+(\.?+\d*))/i", $fileContent, $fileViewBox);
							if (is_array($fileViewBox) && (count($fileViewBox)>3)) {
								$width = intval($fileViewBox[1]);
								$height = intval($fileViewBox[3]);
							}
							if ($extractColor) {
								preg_match_all('/#[A-Fa-f0-9]{6}/', $fileContent, $fileColors);
								if (is_array($fileColors) && (count($fileColors)>0)) {
									$fileColors = array_count_values($fileColors[0]);
									arsort($fileColors);
									$fileColors = array_keys($fileColors);
									$color = reset($fileColors);
									if (strlen($color)>3) {
										if ($color>6) {
											$colorR = hexdec($color[1].$color[2]);
											$colorG = hexdec($color[3].$color[4]);
											$colorB = hexdec($color[5].$color[6]);
										} else {
											$colorR = hexdec($color[1].$color[2]);
											$colorG = hexdec($color[2].$color[2]);
											$colorB = hexdec($color[3].$color[3]);
										}
										$lightness = ((max($colorR, $colorG, $colorB) + min($colorR, $colorG, $colorB)) / 510.0) * 100;
									}
								}
							}
						}					
						$cdn->load(array(
							'id' => $id,
							'key' => $key,
							'name' => $name,
							'extension' => $ext,
							'size' => $size,
							'width' => isset($width) ? $width : NULL,
							'height' => isset($height) ? $height : NULL,
							'ratio' => isset($ratio) ? $ratio : NULL,
							'color' => isset($color) ? $color : NULL,
							'lightness' => isset($lightness) ? $lightness : NULL,
						), false, true);
						$cdn->save($id);
						$result['items'][] = $cdn->one();
					} else {
						$result['error'] = true;
						$result['error_type'] = 'rights';
					}
				}
			}
		} else {
			$result['error'] = true;
		}
	} else {
		/* Nothing in POST, probably Content-Length error */
		$result['error'] = true;
		$result['error_type'] = 'length';
		$result['error_maxuploadsize'] = CoreTwig::bytesize(Tools::getUploadMaxSize(), false);
	}
	echo json_encode($result);
	die();

} elseif (isset($_GET['file']) && (strpos($_GET['file'], '/')===false)) {

	/**
	 * Requesting a file.
	 */

	$file = __ROOT.'/cdn/'.$_GET['file'];
	if (strpos($_GET['file'], '.')===false) $extension = '';
	else $extension = strtolower(substr($_GET['file'], strrpos($_GET['file'], '.')+1));
	$file = substr($file, 0, strlen($file)-strlen($extension)).$extension; // Extension should be lowercased

	if (!file_exists($file) && (substr_count($_GET['file'], '_')>1)) {
		/* Generating thumbnail */
		global $pico;
		$pico = Pico::singleton();
		CdnModel::generateThumbnail($_GET['file']);
	}

	if (file_exists($file)) {
		/* Output file */
		if (connection_aborted()) die();
		$headers = apache_request_headers();
		if (isset($headers['If-Modified-Since']) && (strtotime($headers['If-Modified-Since']) == filemtime($file))) {
			header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($file)).' GMT', true, 304);
		} else {
			header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($file)).' GMT', true, 200);
			header("Expires: ".gmdate('D, d M Y H:i:s \G\M\T', time()+(60*60*24*30)));
			$mimes = getMimes();
			$mimeType = substr($mimes[$extension], 0, strpos($mimes[$extension], '/'));
			if (isset($mimes[$extension])) {
				header("Content-Type: ".$mimes[$extension]);
			}
			if (isset($_GET['download'])) {
				header('Content-Description: File Transfer');
				header('Content-Disposition: attachment;');
			} else {
				header('Content-Disposition: inline;');
			}
			if (isset($_SERVER['HTTP_RANGE'])) {
				rangeDownload($file);
			} else {
				if (in_array($mimeType, array('audio', 'video'))) {
					header("Accept-Ranges: bytes");
					header("Content-Length: ".filesize($file));
				}
				flush();
				session_write_close();
				readfile($file);
			}
		}
	} else {
		header('HTTP/1.0 404 Not Found');
	}
} else {
	header('HTTP/1.0 404 Not Found');
}