<?php

global $pico;
$pico = Pico::singleton();

if (isset($_GET['file']) && $pico->debugBar) {

	$path = __ROOT.'/vendor/php-debugbar/php-debugbar/src/DebugBar/Resources';

	if (file_exists($path.$_GET['file'])) {
		if (substr($_GET['file'], -4)=='.css') {
			header("Content-Type: text/css");
		} elseif (substr($_GET['file'], -3)=='.js') {
			header("Content-Type: application/x-javascript");
		}
		header("Expires: ".gmdate('D, d M Y H:i:s \G\M\T', time()+(60*60)));
		echo file_get_contents($path.$_GET['file']);
	}

}