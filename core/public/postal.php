<?php

global $pico;
$pico = Pico::singleton();

$request = file_get_contents('php://input');
$payload = json_decode($request, true);

if (isset($payload['payload'])) {

	$payload = $payload['payload'];
	if (isset($payload['message']['message_id']) && isset($payload['status'])) {

		// Message status
		$email = Model::factory('Email');
		if ($email->fetch('`messageId`="<'.safer($payload['message']['message_id'], 'db').'>"')) {
			$emailId = $email->value('id');
			if ($payload['status']=='Sent') {
				$email->value('messageStatus', 0, 'sent');
			} elseif ($payload['status']=='DeliveryFailed') {
				$email->value('messageStatus', 0, 'failed');
			} else {
				$email->value('messageStatus', 0, 'pending');
			}
			$email->value('messageDetails', 0, json_encode($payload));
			$email->save($emailId);
		} else {
			http_response_code(404);
			die();
		}
	
	} elseif (isset($payload['original_message']['message_id']) && isset($payload['bounce'])) {

		// Bounce
		$email = Model::factory('Email');
		if ($email->fetch('`messageId`="<'.safer($payload['original_message']['message_id'], 'db').'>"')) {
			$emailId = $email->value('id');
			$email->value('messageStatus', 0, 'failed');
			$email->value('messageDetails', 0, json_encode($payload));
			$email->save($emailId);
		} else {
			http_response_code(404);
			die();
		}

	} else {

		// Untreated event
		http_response_code(406);

	}
	http_response_code(200);
	die();

} else {

	$pico->_404();

}