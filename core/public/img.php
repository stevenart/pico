<?php

if (isset($_GET['file'])) {
	$file = appfile('img/'.urldecode($_GET['file']));
	if (isset($_GET['default']) && !$file) {
		$file = urldecode($_GET['default']);
	}
	if ($file) {
		$headers = apache_request_headers();
		if (isset($headers['If-Modified-Since']) && (strtotime($headers['If-Modified-Since']) == filemtime($file))) {
			header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($file)).' GMT', true, 304);
		} else {
			header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($file)).' GMT', true, 200);
			header("Expires: ".gmdate('D, d M Y H:i:s \G\M\T', time()+(60*60*24*30)));
			$mimes = getMimes();
			if (strpos($file, '.')===false) $extension = '';
			else $extension = strtolower(substr($file, strrpos($file, '.')+1));
			if (isset($mimes[$extension])) {
				header("Content-Type: ".$mimes[$extension]);
			}
			echo file_get_contents($file);
		}
	} else {
		header('HTTP/1.0 404 Not Found');
	}
} else {
	header('HTTP/1.0 404 Not Found');
}