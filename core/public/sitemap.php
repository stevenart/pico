<?php 


/* Initialization */
header('Content-Type: application/xml; charset=utf-8');
echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";
echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";
$pico = Pico::singleton();
$urls = array();


/**
 * Build url array for sitemap.xml from single row from pages table.
 *
 * @param array $page The pages table's row
 *
 * @return array URL parameters for sitemap.xml
 */
function urlFromPage(array $page, string $lang): array {	
	global $pico;
	return [
		'loc' => $pico->href(';'.$lang.'/'.$page['uri']),
		'changefreq' => ($page['type']=='plugin' ? 'daily' : 'weekly'),
		'lastmod' => ($page['type']=='plugin' ? date('Y-m-d') : substr($page['__timestamp'], 0, 10)),
		'priority' => (
			$page['navigation']=='main' 
			? ($page['parent'] ? 0.6 : 0.8)  
			: ($page['parent'] ? 0.2 : 0.4)  
		),
	];
}


/* Building URLs */
if (isset($_GET['lang'])) {
	$langs = array($_GET['lang']);
} else {
	$langs = $pico->i18n->langs();
}
foreach($langs as $lang) {
	$urls[] = array(
		'loc' => $pico->href(';'.$lang),
		'changefreq' => 'daily',
		'lastmod' => date('Y-m-d'),
		'priority' => '1'
	);
	/* Pages */
	$pages = PageModel::getTree($lang);
	foreach(array('main', 'footer') as $nav) {
		foreach($pages[$nav] as $page) {
			if (intval($page['metaRobots'])>0) {
				$urls[] = urlFromPage($page, $lang);
				if (!empty($page['_children'])) {
					foreach($page['_children'] as $child) {
						$urls[] = urlFromPage($child, $lang);
						if ($child['type']=='plugin') {
							$controllerClass = 'Page_'.ucfirst($child['plugin']).'Controller';
							if (is_callable([$controllerClass, '_sitemap'])) {
								$urls = array_merge($urls, $controllerClass::_sitemap($child['uri'], $lang));
							}
						}
					}
				}
				if ($page['type']=='plugin') {
					$controllerClass = 'Page_'.ucfirst($page['plugin']).'Controller';
					if (is_callable([$controllerClass, '_sitemap'])) {
						$urls = array_merge($urls, $controllerClass::_sitemap($page['uri'], $lang));
					}
				}
			}
		}
	}
	if (method_exists('Hooks', 'sitemap')) {
		Hooks::sitemap($urls, $lang);
	}
}

/* Generation */
foreach($urls as $url) {
	echo '<url>'."\n";
	foreach($url as $key => $value) echo '<'.$key.'>'.$value.'</'.$key.'>'."\n";
	echo '</url>'."\n";
}	
echo '</urlset>'."\n";