<?php

global $pico;
$pico = Pico::singleton();
$error = false;
$errorDetail = '';
$errors = array(
	'unknown' => 'An undocumented error has occured, please check logs.',
	'dbserver' => 'Cannot connect to the database server. Please check information provided in <code>/app/config.app.ini</code>.',
	'dbuser' => 'The user provided in <code>/app/config.app.ini</code> cannot connect to the database server. Please check the user and password.',
	'dbcreate' => 'Could not create the database with the user provided in <code>/app/config.app.ini</code>. Please check user privileges or create the empty database yourself and refresh this page.',
	'populate' => 'Could not populate the database with i18n texts. Please check logs.',
);

/** 
 * For security reason, firstrun must be activated in config (usually on local/dev environments).
 */
if ($pico->config['database']['firstrun']) {

	/**
	 * Trying to connect to the database specified in config and if to created it if the database doesn't exists.
	 * @see /app/config.app.ini
	 */
	try {
		$db = new Db($pico->config["database"]);
	}
	catch (PDOException $e) {		
		if ($e->getCode()==1049) {
			if (!file_exists(__ROOT.'/cache/db')) {
				@mkdir(__ROOT.'/cache/db', 0777, true);
			}
			$createDb = new PDO('mysql:charset=utf8mb4;host=localhost;port=8889;', $pico->config['database']['user'], $pico->config['database']['password']);
			$createDb->exec("SET NAMES utf8; SET time_zone = '+00:00'; SET foreign_key_checks = 0; SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO'; SET NAMES utf8mb4;");
			$statement = $createDb->prepare("CREATE DATABASE `".$pico->config['database']['name']."` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;");
			if ($statement->execute()) {
				$pico->url(';firstrun.php');
			} else {
				$error = 'dbcreate';
				$errorDetail = $createDb->errorInfo();
			}
		} elseif (in_array($e->getCode(), array(1044, 1045))) {
			$error = 'dbserver';
			$errorDetail = $e->getMessage(); 
		} else {
			$error = 'unknown';
			$errorDetail = $e->getMessage(); 
		}	
	}


	if (!$error) {
		/**
		 * Once the connection to the database is made, this script will populate the database with sql/cms_i18n_*.sql script.
		 */
		$i18nCount = $db->prepare('SELECT COUNT(*) FROM `_i18n`');
		$i18nCount->execute();
		$hasI18n = intval($i18nCount->fetchAll(PDO::FETCH_COLUMN, 0));
		if (!$hasI18n) {
			$i18nModel = Model::factory('I18n');
			foreach($pico->i18n->langs() as $lang) {
				$filename = appfile('scripts/sql/cms_i18n_'.$lang.'.sql');
				if (file_exists($filename)) {
					$statement = $db->prepare(file_get_contents($filename));
					if (!$statement->execute()) {
						$error = 'populate';
						$errorDetail = $db->errorInfo();
					}
				}
			}
			$pico->url(';firstrun.php');
		}
	}

	if (!$error) {
		/**
		 * Once the database is populated, this script will allow the user to create a first CMSUSer.
		 */
		$model = Model::factory('CMSUser');
		$canSave = $model->loadForm();
		if ($canSave===true) {
			$model->save();
			$pico->session('firstrun_user_created', $model->value('email'));
		} else {
			$form = $model->form(array(
				'action' => $pico->href(';firstrun.php'),
				'actions' => array(
					'submit' => array('type'=>'submit', 'text'=>i18n('CMS.ACTION_CREATE'), 'css'=>'add'),
				),
				'errors' => $canSave,
				'fields' => array('name', 'email', 'password', 'lang'),
				'fieldsets' => false,
			));
		}
	}

} else {
	$pico->fatal('First run is not accessible with this configuration. (env='.$pico->env.')';
}

?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>First run</title>
	<meta name="robots" content="noindex,nofollow,noarchive">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo $pico->href(';'); ?>css.php?files=%5B%22pico%22%2C%22dripicons%22%2C%22cms%5C%2F_cms%22%5D">
	<style type="text/css">
		#global {
			align-items: center;
			display: flex;
			justify-content: center;
		}
		.singlePageContent {
			border: 1px solid var(--colorBorder);
			border-radius: var(--radius);
			margin: 1rem;
			max-width: calc(100vw - 2rem);
			width: 30rem;
		}
		.intro {
			padding: 1rem;
			border-bottom: 1px solid var(--colorBorder);
		}
			.intro .logo {
			}
			.intro .logo img {
				width: 3rem;
				border-radius: var(--radius);
				margin: 0 auto;
			}
		.pico-form {
			padding: 0;
			margin: 0;
		}
			.pico-form .pico-form-field {
				padding: .5rem 1rem;
			}
			.pico-form .pico-form-actions, .cmsActions {
				border-bottom: none;
				margin-bottom: 0;
				padding: 1rem;
			}
		code {
			font-family: monospace;
			background-color: var(--colorLight);
			color: var(--colorText);
			border-radius: var(--radius);
			padding: .2em;
		}
		blockquote {
			color: var(--colorTextLight);
			font-family: monospace;
			margin: 1rem 0 0 0;
			padding: 1rem 0 0 0;
			border-top: 1px solid var(--colorBorder);
		}
		p.error {
			color: var(--colorError);
		}
		p.success {
			color: var(--colorSuccess);
			margin: 2rem 0 0 0;
			padding: 1rem;
		}
	</style>
</head>
<body>
	<div id="global">
		<div class="singlePageContent">
			<div class="intro">
				<div class="logo">
					<img src="/apple-touch-icon.png" alt="Logo" />
				</div>

				<?php 
					if ($error) {
				?>
					<p class="error">
						<?php echo (isset($errors[$error]) ? $errors[$error] : reset($errors)); ?>
					</p>
					<?php 
						if ($errorDetail) {
					?>
						<blockquote>
							<?php echo (is_array($errorDetail) ? print_r($errorDetail, true) : $errorDetail); ?>
						</blockquote>
					<?php
						} 
					?>
				<?php
					} else {
				?>
					<p>
						<?php echo nl2br(i18n('CMS.MESSAGE_FIRSTRUN')); ?>
					</p>
				<?php 
					}
				?>
			</div>
			<?php 
				if (!$error) {
					if ($pico->session('firstrun_user_created')) {
			?>
				<p class="success">
					<?php echo nl2br(i18n('CMS.MESSAGE_FIRSTRUN_SUCCESS', array(
						$pico->session('firstrun_user_created'),
						$pico->href(':cms'),
						$pico->href(':cms'),
					))); ?>
				</p>
				<div class="cmsActions">
					<a href="<?php echo $pico->href(';'); ?>" class="button">
						<?php echo i18n('CMS.ACTION_WEBSITE'); ?>
					</a>
					<a href="<?php echo $pico->href(';cms'); ?>" class="button">
						<?php echo i18n('CMS.ACTION_LOGIN'); ?>
					</a>
				</div>
			<?php
					} else {
						echo $form;
					}
				} 
			?>
		</div>
	</div>
</body>
</html>