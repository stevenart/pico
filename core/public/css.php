<?php


function cssLoad($files, $cacheDir, $clearCache=true) {
	$newer = 0;
	$id = '';
	foreach($files as $index => $file) {
		$id.=$file;
		$files[$index] = appfile('css/'.$file.'.scss');
		if (filemtime($files[$index])>$newer) {
			$newer = filemtime($files[$index]);
		}
	}
	$files = array_filter($files);
	$id = md5($id);
	if (!file_exists($cacheDir)) {
		@mkdir($cacheDir, 0777, true);
	}
	if ($clearCache && file_exists($cacheDir.'/'.$id.'_'.$newer.'.css')) {
		unlink($cacheDir.'/'.$id.'_'.$newer.'.css');
	}
	if (!file_exists($cacheDir.'/'.$id.'_'.$newer.'.css')) {
		/* Cleaning old files */
		$old = glob($cacheDir.'/'.$id.'_*.css');
		foreach($old as $oldcache) unlink($oldcache);
		/* Creating cache */
		$cache = '';
		foreach($files as $file) $cache.= file_get_contents($file)."\n";
		/* Replacing ":"-starting URLs with correct img.php link */ 
		$cache = preg_replace('/url\([\'"]?:{1}([^)]+?)[\'"]?\)/', 'url(img.php?file=$1) ', $cache);
		/* 
			Creating _R, _G and _B suffixed variables for css hexadecimal variables starting with "--color".
		*/
		$cache = preg_replace_callback(
			'/\-\-color([0-9A-Za-z_\-]*)\:\ (#[0-9A-Za-z]{6})\;/', 
			function ($matches)  {	
				$rgb = sscanf($matches[2], "#%02x%02x%02x");
				return 
					'--color'.$matches[1].': '.$matches[2]."; \n".
					'--color'.$matches[1].'_R: '.$rgb[0]."; \n".
					'--color'.$matches[1].'_G: '.$rgb[1]."; \n".
					'--color'.$matches[1].'_B: '.$rgb[2]."; \n";
			},
			$cache
		);
		/* Compiling cache */
		$scss = new ScssPhp\ScssPhp\Compiler();
		$scss->setFormatter('ScssPhp\ScssPhp\Formatter\Compressed');
		$scss->setImportPaths(array(
			__ROOT.'/app/css',
			__ROOT.'/core/css',
		));
		$cache = $scss->compile($cache);
		/* Writing file */
		@file_put_contents($cacheDir.'/'.$id.'_'.$newer.'.css', $cache);
	}
	return file_get_contents($cacheDir.'/'.$id.'_'.$newer.'.css');
}

header("Content-Type: text/css");
header("Expires: ".gmdate('D, d M Y H:i:s \G\M\T', time()+3600));
if (isset($_GET['files'])) {
	$files = json_decode(stripslashes(urldecode($_GET['files'])), true);
	if (is_array($files)) {
		echo cssLoad($files, __ROOT.'/cache/css', (isset($_GET['clearcache']) && (intval($_GET['clearcache'])==1)));
	} else {
		echo "/* Files parameter is not a properly JSON-encoded array. */";
	}
} else {
	echo "/* All your base are belong to us. */";
}