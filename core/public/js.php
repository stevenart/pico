<?php

function jsLoad($files, $cacheDir, $clearCache=true) {
	$newer = 0;
	$id = '';
	foreach($files as $index => $file) {
		$id.=$file;
		$files[$index] = appfile('js/'.$file.'.js');
		if (filemtime($files[$index])>$newer) {
			$newer = filemtime($files[$index]);
		}
	}
	$files = array_filter($files);
	$id = md5($id);
	if (!file_exists($cacheDir)) {
		@mkdir($cacheDir, 0777, true);
	}
	if ($clearCache && file_exists($cacheDir.'/'.$id.'_'.$newer.'.js')) {
		unlink($cacheDir.'/'.$id.'_'.$newer.'.js');
	}
	if (!file_exists($cacheDir.'/'.$id.'_'.$newer.'.js')) {
		/* Cleaning old files */
		$old = glob($cacheDir.'/'.$id.'_*.js');
		foreach($old as $oldcache) unlink($oldcache);
		/* Creating file */
		$cache = '';
		foreach($files as $file) {
			if (stripos($file, '.min.js')) {
				$cache.= file_get_contents($file);
			} else {
				$cache.= \JShrink\Minifier::minify(file_get_contents($file));
			}
		}
		$cache.= "\n";
		@file_put_contents($cacheDir.'/'.$id.'_'.$newer.'.js', $cache);
	}
	return file_get_contents($cacheDir.'/'.$id.'_'.$newer.'.js');
}

header("Content-Type: application/x-javascript");
header("Expires: ".gmdate('D, d M Y H:i:s \G\M\T', time()+(60*60)));
if (isset($_GET['files'])) {
	$files = json_decode(stripslashes(urldecode($_GET['files'])), true);
	if (is_array($files)) {
		echo jsLoad($files, __ROOT.'/cache/js');
	} else {
		echo "/* Files parameter is not a properly JSON-encoded array. */";
	}
} else {
	echo "/* All your base are belong to us. */";
}