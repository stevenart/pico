<?php


class Mail {
	
	protected $config;
	protected $mailer;
	protected $debugLogs = '';
	

	public function __construct($config=NULL) {
		global $pico;

		/* Loading configuration */
		if (is_null($config)) {
			$configModel = Model::factory('EmailConfig');
			if ($configModel->fetchCurrent()) {
				$config = $configModel->one();
			} else {
				$config = [
					'method' => $pico->config['mail']['method'],
					'smtpServer' => $pico->config['mail']['smtp_server'],
					'smtpPort' => $pico->config['mail']['smtp_port'],
					'smtpDkimPrivate' => $pico->config['mail']['smtp_dkim_private'],
					'smtpDkimSelector' => $pico->config['mail']['smtp_dkim_selector'],
					'smtpDkimPassphrase' => $pico->config['mail']['smtp_dkim_passphrase'],
					'smtpUsername' => $pico->config['mail']['smtp_username'],
					'smtpPassword' => $pico->config['mail']['smtp_password'],
					'smtpSecure' => in_array($pico->config['mail']['smtp_secure'], ['tls', 'ssl']) ? $pico->config['mail']['smtp_secure'] : 'auto',
					'fromEmail' => $pico->config['mail']['from_email'],
					'fromName' => $pico->config['mail']['from_name'],
					'toEmail' => $pico->config['mail']['to_email'],
					'toName' => $pico->config['mail']['to_name'],
				];
			}
		}
		$this->config = $config;

		/* Configuring PHPMailer */
		$this->mailer = new Mailer();
		$this->mailer->CharSet = "UTF-8";
		switch($this->config['method']) {
			case 'mail':	
				$this->mailer->isMail();
				break;
			case 'smtp':	
				$this->mailer->isSMTP();
				$this->mailer->Timeout = 10;
				$this->mailer->Host = $this->config['smtpServer'];
				if ($this->config['smtpServer']=='127.0.0.1') {
					if (!empty($config['smtpDkimPrivate'])) {
						$this->mailer->DKIM_private = $config['smtpDkimPrivate'];
					}
					if (!empty($config['smtpDkimSelector'])) {
						$this->mailer->DKIM_selector = $config['smtpDkimSelector'];
					}
					if (!empty($config['smtpDkimPassphrase'])) {
						$this->mailer->DKIM_passphrase = $config['smtpDkimPassphrase'];
					}
				}
				$this->mailer->Port = $this->config['smtpPort']; 
				if ($this->config['smtpSecure']!='auto') {
					$this->mailer->SMTPSecure = $this->config['smtpSecure'];
				}
				$this->mailer->SMTPAuth = true;
				$this->mailer->Username = $this->config['smtpUsername'];  
				$this->mailer->Password = $this->config['smtpPassword']; 
				break;
		}
		if ($this->config['fromEmail']) {
			$this->setFrom([
				'email' => $this->config['fromEmail'],
				'name' => $this->config['fromName'] ?? $this->config['fromEmail'],
			]);
		} else {
			$domain = CoreTwig::urlDomain($pico->href(';'));
			$this->setFrom([
				'email' => 'noreply@'.$domain,
				'name' => $domain,
			]);
		}
		if ($this->config['toEmail']) {
			$this->setFrom([
				'email' => $this->config['fromEmail'],
				'name' => $this->config['fromName'] ?? $this->config['fromEmail'],
			]);
		}
	}


	public function addAttachment($file, $name='') {
		$this->mailer->addAttachment($file, $name);
	}
	
	
	private function addAddress($addresses, $to=false, $cc=false, $bcc=false, $replyto=false, $defaultToName=false) {
		global $pico;
		if (is_string($addresses) || (is_array($addresses) && isset($addresses['email']))) $addresses = array($addresses);
		$count = 0;
		foreach ($addresses as $address) {
			if (is_array($address)) {
				$address['email'] = trim($address['email']);
			} else {
				$address = array('email'=>trim($address));
			}
			if ($defaultToName && $to && !isset($address['name'])) {
				$address['name'] = $this->config['toName'];
			}
			if ($pico->config['mail']['replace_all_email']) {
				// Replacing all external addresses
				$address['email'] = $pico->config['mail']['replace_all_email'];
			}
			if (Check::email($address['email'])!==true) continue;
			if (!isset($address['name'])) $address['name'] = '';
			if ($to) {
				$this->mailer->addAddress($address['email'], $address['name']);
			}
			if ($cc) {
				$this->mailer->addCC($address['email'], $address['name']);
			}
			if ($bcc) {
				$this->mailer->addBCC($address['email'], $address['name']);
			}
			if ($replyto) {
				$this->mailer->addReplyTo($address['email'], $address['name']);
			}
			$count++;
		}
		return $count;
	}
	
	
	/**
	 * The addTo function adds a recipient for the email.
	 * 
	 * Ideal recipient format is :
	 * 	array('email'=>'name@domain.tld', 'name'=>'Recipient Name')
	 *
	 * Note : $to can also be multiple, like : 
	 *	array(
	 *		array('email'=>'name@domain.tld', 'name'=>'Recipient Name'),
	 *		array('email'=>'name@domain.tld', 'name'=>'Recipient Name')
	 *	)
	 *
	 * @access public
	 * @param mixed $to Recipient
	 * @return int Number of recipients added.
	 */
	public function addTo($to, $defaultToName=false) {
		return $this->addAddress($to, true, false, false, false, $defaultToName);
	}
	
	
	/**
	 * The addCC function adds a recipient for the email in carbon copy.
	 * Recipient format is the same as Mail::addTo.
	 *
	 * @access public
	 * @param mixed $cc Recipient cc
	 * @return int Number of recipients cc added.
	 */
	public function addCC($cc) {
		return $this->addAddress($cc, false, true);
	}


	/**
	 * The addReplyTo function adds a replyto for the email.
	 * Recipient format is the same as Mail::addTo.
	 *
	 * @access public
	 * @param mixed $bcc Recipient bcc
	 * @return int Number of recipients bcc added.
	 */
	public function addReplyTo($replyto) {
		return $this->addAddress($replyto, false, false, false, true);
	}
	
	
	
	/**
	 * The addBCC function adds a recipient for the email in blank carbon copy.
	 * Recipient format is the same as Mail::addBCC.
	 *
	 * @access public
	 * @param mixed $bcc Recipient bcc
	 * @return int Number of recipients bcc added.
	 */
	public function addBCC($bcc) {
		return $this->addAddress($bcc, false, false, true);
	}


	public function debugLog($str=NULL, $level=NULL, $clear=false) {
		if (!is_null($str)) {
			$this->debugLogs.= $str."\n";
		} elseif ($clear) {
			$this->debugLogs = '';
		}
		return $this->debugLogs;
	}

	
	/**
	 * The renderHtml static function fetches a twig template and renders.
	 * 
	 * @access public
	 * @param mixed $templateName Name of the template. The name part of the file is : views/_emails/<name>.email.html
	 * @param mixed $parameters Associative array of parameters to pass to the template.
	 * @return void
	 */
	public static function renderHtml($template, $parameters) {
		global $pico;
		$html = $pico->twig->render($template, $parameters);
		$html = preg_replace('/src="[^(http)]/i', 'src="'.$pico->href(';'), $html); // Fix "non-absolute src" images
		return $html;
	}


	/**
	 * The setBodyHtml function sets the body of the email with a given html.
	 * 
	 * @access public
	 * @param string $html Content
	 * @param bool $buildTextFromHtml If set to false, no text alternative will be generated (default: true).
	 * @return void
	 */
	public function setBodyHtml($html, $setAlternativeText=false) {
		$this->mailer->isHTML(true);
		$this->mailer->Body = $html;
		if ($setAlternativeText) {
			$this->setBodyText(strip_tags($html), NULL, false);
		}
		return $html;
	}
	

	/**
	 * The setBodyTemplate function fetches a twig template, renders it and sets the result as the body of the email.
	 * 
	 * @access public
	 * @param mixed $templateName Name of the template. The name part of the file is : views/_emails/<name>.email.html
	 * @param mixed $parameters Associative array of parameters to pass to the template.
	 * @param bool $buildTextFromHtml If set to false, no text alternative will be generated (default: true).
	 * @return void
	 */
	public function setBodyTemplate($template, $parameters, $setAlternativeText=false) {
		$html = Mail::renderHtml($template, $parameters);
		$this->setBodyHtml($html, $setAlternativeText);
		if (isset($parameters['unsubscribe']) && Check::url($parameters['unsubscribe'])) {
			$this->mailer->addCustomHeader('List-Unsubscribe', '<'.$parameters['unsubscribe'].'>');
			$this->mailer->addCustomHeader('List-Unsubscribe-Post', 'List-Unsubscribe=One-Click');
		}
		return $html;
	}
	

	/**
	 * The setBodyText function sets the body of the email with a given text.
	 * Variables will be replaced with parameters.
	 * 
	 * @access public
	 * @param string $text Content
	 * @param mixed $parameters Associative array of parameters to pass to the text (placeholders are :%key).
	 * @param bool $mainBody If false, sets the body as alternative (default: true).
	 * @return void
	 */
	public function setBodyText($text, $parameters=NULL, $mainBody=true) {
		if (is_array($parameters)) {
			foreach ($parameters as $key=>$value) {
				$text = str_replace(':%'.$key, $value, $text);
			}
		}	
		if ($mainBody) {
			$this->mailer->isHTML(false);
			$this->mailer->Body = $text;
		} else {
			$this->mailer->AltBody = $text;
		}
	}
	
	
  	/**
  	 * The setFrom function sets the sender of the email.
  	 *
  	 * Sender format is the same as recipients :
	 * 	array('email'=>'name@domain.tld', 'name'=>'Sender Name')
  	 *
  	 * @access public
  	 * @param mixed $from Sender
  	 * @param bool $autoReplyTo Automatically sets the Reply-To (default: true)
  	 * @return void
  	 */
  	public function setFrom($from, $autoReplyTo=true) {  	
		if (is_array($from)) {
			$from['email'] = trim($from['email']);
		} else {
			$from = array('email'=>trim($from));
		}
		if (Check::email($from['email'])!==true) return false;
		if (!isset($from['name'])) $from['name'] = '';
		$this->mailer->setFrom($from['email'], $from['name'], ($autoReplyTo?1:0));
		if ($this->config['smtpServer']=='127.0.0.1' && !empty($this->config['smtpDkimPrivate'])) {
			$this->mailer->DKIM_domain = substr($from['email'], stripos($from['email'], '@')+1);
			$mail->DKIM_identity = $mail->From;
		}
		return true;
	}
	
  	
  	/**
  	 * The setSubject function sets the subject of the email.
  	 * 
  	 * @access public
  	 * @param string $subject
  	 * @return void
  	 */
  	public function setSubject($subject) {
  		$this->mailer->Subject = $subject;
  	}
  	
  	
	/**
	 * The send function sends the email.
	 * 
	 * @access public
	 * @return void
	 */
	public function send($save=true, $saveData=NULL, $antiSpam=true, $returnEmailId=false, $debug=false) {
		global $pico;

		/* Debug */
		if ($debug) {
			$this->mailer->SMTPDebug = 1; // SMTP::DEBUG_CLIENT
			$this->mailer->Debugoutput = array($this, 'debugLog');
		}

		/* Email data */
		$emailData = array();
		if (is_array($this->mailer->From)) {
			$emailData['senderEmail'] = $this->mailer->From[0]; 
			$emailData['senderName'] = $this->mailer->From[1];
		} elseif ($this->mailer->FromName) {
			$emailData['senderEmail'] = $this->mailer->From; 
			$emailData['senderName'] = $this->mailer->FromName;
		} else {
			$emailData['senderEmail'] = $this->mailer->From;
		}
		$toAddresses = $this->mailer->getToAddresses();
		if (is_array($toAddresses)) {
			if (count($toAddresses)==0) {
				$this->addTo([
					'email' => $this->config['toEmail'],
					'name' => $this->config['toName'] ?? $this->config['toEmail'],
				]);
				$toAddresses = $this->mailer->getToAddresses();
			}
			if (count($toAddresses)>1) {
				$emailData['recipientName'] = count($toAddresses).' recipients';
			} elseif (count($toAddresses)>0) {
				$emailData['recipientEmail'] = $toAddresses[0][0]; 
				$emailData['recipientName'] = $toAddresses[0][1];
			}
		}
		$emailData['subject'] = $this->mailer->Subject;
		$emailData['message'] = $this->mailer->Body;
		$emailData['sent'] = 0;
		$emailData['spam'] = 0;
		$emailData['rule'] = NULL;
		if (isset($this->config['id'])) {
			$emailData['config'] = $this->config['id'];
		}
		if (is_array($saveData)) {
			$emailData = array_merge($emailData, $saveData);
		}
	
		/* Checking if spam */
		$spam = false;
		if ($antiSpam) {
			$emailRule = Model::factory('EmailRule');
			$skipSpam = false;
			$emailRule->fetch('`__draft`=0 AND `action`="neverspam"', '`__ordering` ASC, `__timestamp` DESC');
			for ($index=0; $index<$emailRule->fetched(); $index++) {
				if ($emailRule->verifyRule($emailData, $index)) {
					$spam = false;
					$skipSpam = true;
					$emailData['spam'] = 0;
					$emailData['rule'] = $emailRule->value('id');
				}
			}
			if (!$skipSpam) {
				$emailRule->fetch('`__draft`=0 AND `action`="spam"', '`__ordering` ASC, `__timestamp` DESC');
				for ($index=0; $index<$emailRule->fetched(); $index++) {
					if ($emailRule->verifyRule($emailData, $index)) {
						$spam = true;
						$emailData['spam'] = 1;
						$emailData['rule'] = $emailRule->value('id');
					}
				}
				if (!$spam) {
					$emailData['spamDetails'] = "";
					$emailData['spamScore'] = Tools::spamScore($emailData['message'], $emailData['spamDetails']);
					if ($emailData['spamScore']>$pico->config['mail']['spamscore']) {
						$spam = true;
						$emailData['spam'] = 1;
					}  
				}
			}
		}

		/* Sending */
		$sent = false;
		if (!$spam) {
			$sent = $this->mailer->send();
			if ($this->mailer->ErrorInfo) {
				$emailData['errorMessage'] = $this->mailer->ErrorInfo;
			}
			if ($sent) {
				$emailData['sent'] = 1;
				$emailData['messageId'] = $this->mailer->getLastMessageID();
			}
		}

		/* Debug */
		if ($debug && !$sent) {
			varlog($this->debugLog(), 'MAIL DEBUG');
		}

		/* Save */
		if ($save) {
			$email = Model::factory('Email');
			$email->load($emailData);
			if ($email->save()) {
				if ($returnEmailId) {
					return $email->value('id');
				}
			}
		}
	
		return $sent;
	}
	
}