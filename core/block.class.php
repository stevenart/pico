<?php

/**
 * Block class : this class is the class every pico block must extends.
 */
class Block extends Model {


	private $description;
	private $icon;
	private $modalClass;
	private $title;


	public function __construct($name=NULL, $detached=true, $classType='Block') {
		if (is_null($name)) {
			$name = substr(get_called_class(), 0, -5);
		}

		parent::__construct($name, $detached, $classType);

		/* Description */
		if (isset($this->definition['description'])) {
			if (isset($this->definition['description_i18n'])) {
				$this->description = i18n($this->definition['description']);
			} else {
				$this->description = $this->definition['description'];
			}
		} else {
			$this->description = i18n('PICO.MESSAGE_BLOCK_'.$name);
		}

		/* Icon */
		if (isset($this->definition['icon'])) {
			$this->icon = $this->definition['icon'];
		} else {
			$this->icon = 'media-stop';
		}

		/* Modal class */
		$this->modalClass = isset($this->definition['modalClass']) ? $this->definition['modalClass'] : '';

		/* Title */
		if (isset($this->definition['title'])) {
			if (isset($this->definition['title_i18n'])) {
				$this->title = i18n($this->definition['title']);
			} else {
				$this->title = $this->definition['title'];
			}
		} else {
			$this->title = i18n('PICO.TITLE_BLOCK_'.$name);
		}
	}


	public static function factory($name) {
		$classname = $name.'Block';
		if (class_exists($classname)) {
			return new $classname();
		} else {
			return new Block($name);
		}
		return false;
	}


	public function getDescription() {
		return $this->description;
	}


	public function getIcon() {
		return $this->icon;
	}


	public function getModalClass() {
		return $this->modalClass;
	}


	public function getTitle() {
		return $this->title;
	}


	public function render(int $blockNo=0) {
		global $pico;
		$vars = [
			'blockNo' => $blockNo,
			'blockType' => $this->name,
			'fields' => $this->getFields(),
			'item' => $this->one(),
			'unique' => Tools::generateSecret(10),
		];
		$this->variables($vars);
		return $pico->twig->render('blocks/'.$this->name.'.twig', $vars);
	}


	protected function variables(&$vars) {
		// Update $vars in child block with this function.
	}


}