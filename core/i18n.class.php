<?php

class I18n {
	

	public $locale;
	
	private $configuration;
	private $default;
	private $language;
	private $loaded;
	private $table;
	

	public function __construct(string $lang=NULL, array $config) {
		global $pico;
		
		/* Loading and extending config */
		if (isset($config['languages'])) {
			$config['languages'] = explode(',', $config['languages']);
		}
		if (isset($config['disabled'])) {
			$config['disabled'] = explode(',', $config['disabled']);
		}
		if (isset($config['cms_only'])) {
			$config['cms_only'] = explode(',', $config['cms_only']);
		}
		$this->configuration = array_merge(array(
			'cms_only'=>array(),
			'default'=>'fr',
			'languages'=>array('fr')
		), $config);
		if (!in_array($this->configuration['default'], array_diff($this->configuration['languages'], $this->configuration['cms_only']))) {
			$this->configuration['default'] = reset($config['languages']);
		}	
		$this->default = $this->configuration['default'];
		if ($this->configuration['default_browser']) {
			if (isset($_SERVER["HTTP_ACCEPT_LANGUAGE"])) {
				$httpLang = substr($_SERVER["HTTP_ACCEPT_LANGUAGE"], 0, 2);
				if (in_array($httpLang, array_diff($this->configuration['languages'], $this->configuration['disabled'], $this->configuration['cms_only']))) {
					$this->default = $httpLang;
				}
			}
		}

		/* Preparing to load keys */
		$this->loaded = array();
		foreach ($this->configuration['languages'] as $foo) {
			$this->loaded[$foo] = array();
		}
		$this->table = '_i18n';
			
		/* Setting current lang */
		$this->language = in_array($lang, $this->configuration['languages']) ? $lang : $this->default;
		return $this->lang($this->language, true);
	}


	public function cmsLangs() {
		return array_intersect($this->langs(), array('en', 'fr'));
	}


	public function config($name, $value=NULL, $clear=false) {
		global $pico;
		if ($clear) {
			unset($this->configuration[$name]);
		}
		if (!is_null($value)) {
			$this->configuration[$name] = $value;
		}
		return $this->configuration[$name];
	}
	
	
	public function exists($section, $key) {
		global $pico;
		$section = strtoupper($section);
		$key = strtoupper($key);
		return $pico->db->exists($this->table, "`section`='$section' AND `key`='$key'");
	}


	public function lang($lang=NULL, $force=false) {
		global $pico;
		if (!is_null($lang)) {
			if (($this->language!=$lang) || $force) {
				$this->language = $lang;
				if (isset($this->configuration[$this->language])) {
					setlocale(LC_ALL, explode(',', $this->configuration[$this->language]));
					$localeInfo = localeconv();
					if (!is_array($localeInfo)) {
						$localeInfo = array(
							'decimal_point' => ',',
							'thousands_sep' => ' ',
							'int_curr_symbol' => 'EUR',
							'currency_symbol' => 'Eu',
							'mon_decimal_point' => ',',
							'mon_thousands_sep' => ' ',
							'positive_sign' => '',
							'negative_sign' => '-',
							'int_frac_digits' => 2,
							'frac_digits' => 2,
							'p_cs_precedes' => 0,
							'p_sep_by_space' => 1,
							'n_cs_precedes' => 0,
							'n_sep_by_space' => 1,
							'p_sign_posn' => 1,
							'negative_sign' => 2,
							'n_sign_posn' => ',',
							'grouping' => array(127),
							'mon_grouping' => array(3, 3)
						);
					}
					$this->locale = $localeInfo;
				}
				return true;
			}
		}
		return $this->language;
	}


	public function langs($value=NULL, $includeCmsOnly=true, $includeDisabled=true) {
		global $pico;
		if (!is_null($value)) {
			$this->config('languages', $value);
		} 
		$langs = $this->config('languages');
		if (!$includeDisabled && $this->config('disabled'))  {
			$langs = array_diff($langs, $this->config('disabled'));
		}
		if (!$includeCmsOnly) {
			$langs = array_diff($langs, $this->config('cms_only'));
		}
		return $langs;
	}


	public function key($section, $key, $text=NULL, $update=true) {
		global $pico;
		$section = strtoupper(trim($section));
		$key = strtoupper(trim($key));
		if (preg_match('/[^A-Z0-9_\-]/u', $section.$key)) {
			return false;
		}
		if (is_null($text)) {
			$pico->db->exec("INSERT INTO `".$this->table."`(`section`,`key`) VALUES('$section', '$key') ON DUPLICATE KEY UPDATE `key`=`key`;");
			return true;
		} elseif (is_string($text)) {
			$pico->db->exec("INSERT INTO `".$this->table."`(`section`,`key`,`".$this->lang()."-text`) VALUES('$section', '$key', '$text') ON DUPLICATE KEY UPDATE `".$this->lang()."-text`=VALUES(`".$this->lang()."-text`);");
			return true;
		} elseif (is_array($text)) {
			$fields = array('section'=>$section, 'key'=>$key);
			foreach($this->configuration['languages'] as $lang) {
				if (isset($text[$lang])) {
					$fields[$lang.'-text'] = $text[$lang];
				}
			}
			$sql = "INSERT INTO `".$this->table."`(`".implode('`,`', array_keys($fields))."`) VALUES('".implode("','", array_values($fields))."') ON DUPLICATE KEY UPDATE ";
			foreach($fields as $fieldName => $fieldValue) {
				$sql.= "`$fieldName`=VALUES(`$fieldName`), ";
			}
			$sql = substr($sql, 0, -2).';';
			$pico->db->exec($sql);
			return true;
		}
		return false;
	}


	/**
	 * The load function preload an i18n section in the memory.
	 * 
	 * @access public
	 * @param mixed $section Section to load.
	 * @param mixed $lang Language, if null the current language is used (default: NULL)
	 * @return void
	 */
	public function load($section, $lang=NULL, $reload=false) {
		global $pico;
		$section = strtoupper($section);
		if (!is_string($lang) || !in_array($lang, $this->configuration['languages'])) $lang = $this->lang();
		if (isset($this->loaded[$lang][$section]) && !$reload) return false;
		$this->loaded[$lang][$section] = $pico->db->pairs($this->table, '`key`', '`'.$lang.'-text`', "`section`='$section'");
		return true;
	}


	public function locale($name=NULL, $value=NULL, $clear=false) {
		if ($clear) {
			unset($this->locale[$name]);
		}
		if (is_null($name)) {
			return $this->locale;
		} else {
			if (!is_null($value) || $clear) {
				$this->locale[$name] = $value;
			}
			return $this->locale[$name];
		}
	}


	public function text($section, $key, $parameters=NULL, $lang=NULL, $create=true, $autoload=true) {
		global $pico;
		if (!is_string($lang) || !in_array($lang, $this->configuration['languages'])) $lang = $this->lang();
		$section = strtoupper(trim($section));
		$key = strtoupper(trim($key));
		if (preg_match('/[^A-Z0-9_\-]/u', $section.$key)) {
			return '';
		}
		if ($autoload && !isset($this->loaded[$lang][$section])) {
			$this->load($section, $lang);
		}
		if (isset($this->loaded[$lang][$section])) {
			if (empty($this->loaded[$lang][$section][$key])) {
				$text = '';
				if ($create) {
					$this->key($section, $key);
				}
			} else {
				$text = $this->loaded[$lang][$section][$key];
			}
			if (is_null($text) || $text==='') {
				if ($pico->config['i18n']['print_missing']) {
					$text = $section.'.'.$key;
				} else {
					$text = '';
				}
			}
		} else {
			$text = $pico->db->getValue($this->table, '`'.$lang.'-text`', "`section`='$section' AND `key`='$key'");
			if ($trans===false) {
				if ($pico->config['i18n']['create_missing']) {
					$this->key($section, $key);
				}
				$text = '';
			}
			if (is_null($text) || $text==='') {
				if ($pico->config['i18n']['print_missing']) {
					$text = $section.'.'.$key;
				} else {
					$text = '';
				}
			}
		}
		if (is_array($parameters) && count($parameters)>0) {
			if (array_keys($parameters) === range(0, count($parameters) - 1)) { // array_is_list substitute
				$text = vsprintf($text, $parameters);
			} else {
				$text = str_replace(
					array_map(function($value) { return "%$value%"; }, array_keys($parameters)), 
					array_values($parameters), 
					$text
				);
			}
		}
		return $text;
	}	


}