<?php 

class PicoCollector extends DebugBar\DataCollector\DataCollector implements DebugBar\DataCollector\Renderable {


	public function collect() {
		global $pico;
		return ['env' => strtoupper($pico->env)];
	}


	public function getName() {
		return 'picocollector';
	}


	public function getWidgets() {
		global $pico;
		return [
			'picocollector' => [
				'icon' => 'leaf',
				'tooltip' => 'Current environment',
				'map' => 'env',
				"default" => "'".strtoupper($pico->env)."'",
			]
		];
	}


}