update `_i18n` set `de-text` = "Letzte Aktualisierung" where `key` = "FIELD___TIMESTAMP" and  `de-text` is null;
update `_i18n` set `de-text` = "Sortierung" where `key` = "FIELD___ORDERING" and  `de-text` is null;
update `_i18n` set `de-text` = "Liebling" where `key` = "FIELD___STARRED" and  `de-text` is null;
update `_i18n` set `de-text` = "Erstellung" where `key` = "FIELD___CREATED" and  `de-text` is null;
update `_i18n` set `de-text` = "Entwurf" where `key` = "FIELD___DRAFT" and  `de-text` is null;
update `_i18n` set `de-text` = "Letzter Updater" where `key` = "FIELD___USER" and  `de-text` is null;
update `_i18n` set `de-text` = "ID" where `key` = "FIELD_ID" and  `de-text` is null;
update `_i18n` set `de-text` = "Name" where `key` = "FIELD_NAME" and  `de-text` is null;
update `_i18n` set `de-text` = "Email" where `key` = "FIELD_EMAIL" and  `de-text` is null;
update `_i18n` set `de-text` = "URI" where `key` = "FIELD_URI" and  `de-text` is null;
update `_i18n` set `de-text` = "Stadt" where `key` = "FIELD_CITY" and  `de-text` is null;
update `_i18n` set `de-text` = "Postleitzahl" where `key` = "FIELD_ZIPCODE" and  `de-text` is null;
update `_i18n` set `de-text` = "Land" where `key` = "FIELD_COUNTRY" and  `de-text` is null;
update `_i18n` set `de-text` = "Sprache" where `key` = "FIELD_LANG" and  `de-text` is null;
update `_i18n` set `de-text` = "Titel" where `key` = "FIELD_TITLE" and  `de-text` is null;
update `_i18n` set `de-text` = "Vorname" where `key` = "FIELD_FIRSTNAME" and  `de-text` is null;
update `_i18n` set `de-text` = "Nachname" where `key` = "FIELD_LASTNAME" and  `de-text` is null;
update `_i18n` set `de-text` = "Beschreibung" where `key` = "FIELD_DESCRIPTION" and  `de-text` is null;
update `_i18n` set `de-text` = "Downloads" where `key` = "FIELD_ATTACHMENTS" and  `de-text` is null;
update `_i18n` set `de-text` = "Sichtbarkeit in Suchmaschinen" where `key` = "FIELD_METAROBOTS" and  `de-text` is null;
update `_i18n` set `de-text` = "Titel in Suchmaschinen" where `key` = "FIELD_METATITLE" and  `de-text` is null;
update `_i18n` set `de-text` = "Beschreibung" where `key` = "FIELD_METADESCRIPTION" and  `de-text` is null;
update `_i18n` set `de-text` = "Schlüsselwörter (durch Komma getrennt)" where `key` = "FIELD_METAKEYWORDS" and  `de-text` is null;
update `_i18n` set `de-text` = "Suchmaschinenoptimierung" where `key` = "LEGEND_SEO" and  `de-text` is null;
update `_i18n` set `de-text` = "Allgemeine Informationen" where `key` = "LEGEND_GENERAL" and  `de-text` is null;
update `_i18n` set `de-text` = "Link" where `key` = "FIELD_LINK" and  `de-text` is null;
update `_i18n` set `de-text` = "Tags" where `key` = "FIELD_TAGS" and  `de-text` is null;
update `_i18n` set `de-text` = "Logo" where `key` = "FIELD_LOGO" and  `de-text` is null;
update `_i18n` set `de-text` = "Bild" where `key` = "FIELD_PICTURE" and  `de-text` is null;
update `_i18n` set `de-text` = "Galerie" where `key` = "FIELD_GALLERY" and  `de-text` is null;
update `_i18n` set `de-text` = "Straße und Hausnummer" where `key` = "FIELD_STREET" and  `de-text` is null;
update `_i18n` set `de-text` = "Belgien" where `key` = "FIELD_COUNTRY_BE" and  `de-text` is null;
update `_i18n` set `de-text` = "Deutschland" where `key` = "FIELD_COUNTRY_DE" and  `de-text` is null;
update `_i18n` set `de-text` = "Frankreich" where `key` = "FIELD_COUNTRY_FR" and  `de-text` is null;
update `_i18n` set `de-text` = "Luxemburg" where `key` = "FIELD_COUNTRY_LU" and  `de-text` is null;
update `_i18n` set `de-text` = "Breitengrad" where `key` = "FIELD_LAT" and  `de-text` is null;
update `_i18n` set `de-text` = "Längengrad" where `key` = "FIELD_LNG" and  `de-text` is null;
update `_i18n` set `de-text` = "Referenz" where `key` = "FIELD_REFERENCE" and  `de-text` is null;
update `_i18n` set `de-text` = "Firma" where `key` = "FIELD_COMPANY" and  `de-text` is null;
update `_i18n` set `de-text` = "Nachricht" where `key` = "FIELD_MESSAGE" and  `de-text` is null;
update `_i18n` set `de-text` = "Telefon" where `key` = "FIELD_PHONE" and  `de-text` is null;
update `_i18n` set `de-text` = "Inhalt (frei)" where `key` = "FIELD_CONTENT" and  `de-text` is null;
update `_i18n` set `de-text` = "Inhalt (Blöcke)" where `key` = "FIELD_BLOCKS" and  `de-text` is null;
update `_i18n` set `de-text` = "Titelbild" where `key` = "FIELD_COVER" and  `de-text` is null;
update `_i18n` set `de-text` = "Art von Inhalt" where `key` = "FIELD_CONTENTTYPE" and  `de-text` is null;
update `_i18n` set `de-text` = "Passwort" where `key` = "FIELD_PASSWORD" and  `de-text` is null;
update `_i18n` set `de-text` = "Postanschrift" where `key` = "FIELD_ADDRESS" and  `de-text` is null;
update `_i18n` set `de-text` = "Bemerkungen" where `key` = "FIELD_REMARKS" and  `de-text` is null;
update `_i18n` set `de-text` = "MwSt.-Nummer" where `key` = "FIELD_TVA" and  `de-text` is null;
update `_i18n` set `de-text` = "MwSt.-Nummer" where `key` = "FIELD_VAT" and  `de-text` is null;
update `_i18n` set `de-text` = "Website" where `key` = "FIELD_INTERNET" and  `de-text` is null;