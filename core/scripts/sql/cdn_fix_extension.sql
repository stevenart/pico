UPDATE `cdn` SET `extension`=LOWER(SUBSTRING_INDEX(`name`, '.', -1)) WHERE `extension` IS NULL OR `extension` = '';
UPDATE `cdn` SET `name`=CONCAT(SUBSTRING(`name`, 1, LENGTH(`name`) - LENGTH(SUBSTRING_INDEX(`name`, '.', -1))), `extension`);
