<?php

$cachePath = dirname(__FILE__).'/../../../cache';
$cdnPath = dirname(__FILE__).'/../../../cdn';

echo "Clearing ORM cache... ";
$files = glob($cachePath."/db/*.check");
array_map('unlink', $files);
echo "DONE!\n\n";

echo "Clearing CSS cache... ";
$files = glob($cachePath."/css/*.css");
array_map('unlink', $files);
echo "DONE!\n\n";

echo "Clearing JS cache... ";
$files = glob($cachePath."/js/*.js");
array_map('unlink', $files);
echo "DONE!\n\n";

echo "Clearing Twig cache... ";
$files = new RecursiveIteratorIterator(
    new RecursiveDirectoryIterator($cachePath."/twig", RecursiveDirectoryIterator::SKIP_DOTS),
    RecursiveIteratorIterator::CHILD_FIRST
);
foreach ($files as $fileinfo) {
	if ($fileinfo->isDir()) rmdir($fileinfo->getRealPath());
	else unlink($fileinfo->getRealPath());
}
echo "DONE!\n\n";

echo "Clearing thumbnails... ";
$files = glob($cdnPath."/*_*_*.*");
array_map('unlink', $files);
echo "DONE!\n\n";