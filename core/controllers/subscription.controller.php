<?php

class SubscriptionController extends PageController {


	public function _() {	
		global $pico;
		if (in_array('subscribers', explode(',', $pico->config['cms']['plugins']))) {
			$subscriber = Model::factory('Subscriber');
			$subscriber->checks('email', 'unique', false);
			$canSave = $subscriber->loadForm();
			if ($canSave===true) {
				$subscriberId = $pico->db->value('subscribers', '`id`', '`email`="'.$subscriber->value('email').'"');
				if ($subscriberId) {
					/* Existing user */
					if ($subscriber->value('subscribed')==1) {
						/* Updating existing user */
						$subscriber->value('lang', 0, $pico->i18n->lang());
						$this->variable('subscription', $subscriber->save($subscriberId));
					} else {
						/* Send unsubscribe link to existing user */
						$subscriber->fetchPrimary($subscriberId);
						$link = $pico->href(':subscription/unsubscribe?id='.$subscriber->value('id').'&key='.$subscriber->value('secret'));
						$domain = CoreTwig::urlDomain($pico->href(';'));
						$mail = new Mail();
						$mail->addTo(array(
							'name' => $subscriber->value('name'),
							'email' => $subscriber->value('email')
						));
						$subject = i18n('SUBSCRIBER.TITLE_MAIL_UNSUBSCRIBE', NULL, $subscriber->value('lang'));
						$mail->setSubject($subject);
						$mail->setBodyTemplate('emails/_email.twig', array(
							'subject' => '<span style="color:#00afe8;font-weight:normal;">'.$domain.'</span><br />'.$subject,
							'content' => nl2br(i18n('SUBSCRIBER.MESSAGE_MAIL_UNSUBSCRIBE', array(
								$subscriber->value('name'),
								$domain, 
								$subscriber->value('email'),
								$link,
								$link,
							), $subscriber->value('lang'))),
							'signature' => i18n('PICO.MESSAGE_MAIL_SIGNATURE', array($domain), $subscriber->value('lang')),
						));
						$this->variable('unsubscription', $mail->send(true, NULL, false));
					}
				} else {
					/* New subscription */
					if ($subscriber->value('subscribed')==1) {
						$subscriber->value('lang', 0, $pico->i18n->lang());
						$this->variable('subscription', $subscriber->save());
					} else {
						$this->variable('not_subscribed', $subscriber->value('email'));
					}
				}
			} else {
				$this->variable('form', $subscriber->form(array(
					'actions'	=> array(
						'submit' => array('type'=>'submit', 'text'=>i18n('APP.ACTION_SUBSCRIPTION_SUBMIT'))
					),
					'errors' => $canSave,
					'fields' => array('name', 'email', 'subscribed'), 
					'fieldsets' => false,
					'include' => true,
				)));
			}
		} else {
			$pico->_404();
		}
	}


	public function unsubscribe() {
		global $pico;
		$unsubscribed = false;
		if (isset($_GET['id']) && isset($_GET['key'])) {
			$subscriber = Model::factory('Subscriber');
			if ($subscriber->fetch('`id`='.intval($_GET['id']).' AND `secret`="'.safer($_GET['key'], 'db').'"')) {
				$subscriber->value('subscribed', 0, 0);
				$unsubscribed = $subscriber->save(intval($_GET['id']));
			}
		}
		$this->variable('unsubscribed', $unsubscribed);
	}	


}