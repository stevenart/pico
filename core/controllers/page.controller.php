<?php

class PageController extends Controller {

	protected $page = false;
	protected $pages = false;
	protected $widgets = NULL;


	public function __construct($name, array $urlPath=NULL, array $config=NULL) {
		parent::__construct($name, $urlPath, $config);
		
		global $pico;
		$this->pages = PageModel::getTree(NULL, true);
		$pico->variable('_pages', $this->pages);
		$this->page = Model::factory('Page');
		if (PagesCMSPlugin::getPluginConfiguration()['actions']['index']['star']) {
			$this->page->fetch('`__draft`=0 AND `lang`="'.$pico->i18n->lang().'" AND `__starred`=1');
			$pico->variable('_starred', $this->page->values());
			$this->page->clear();
		}
		if (is_numeric($this->path(1)) && $this->page->exists($this->path(1))) {
			$fetched = $this->page->fetchPrimary($this->path(1));
			if (!CMSController::isLogged()) {
				/* Not preview */
				if ($this->page->value('__draft')) {
					$pico->_404();
				} else {
					$pico->url(';'.$this->page->value('lang').'/'.$this->page->value('uri'), '301');
				}
			}
		} else {
			$fetched = $this->page->fetch('`__draft`=0 AND `lang`="'.$pico->i18n->lang().'" AND `uri`="'.$this->path(1).'"');
		}
		if ($fetched) {
			$pico->variable('_page', $this->page->one());
			$this->_loadWidgets();
			/* Trackers */
			if (!isset($this->widgets['gdpr'])) {
				$trackers = Model::factory('Tracker');
				$trackers->fetch('`__draft`=0');
				$pico->variable('_trackers', $trackers->values());
			}
			/* Metas */
			if (!$this->page->value('metaRobots')) {
				$pico->meta('noindex', true);
			}
			if (!$this->page->value('metaTitle')) {
				$pico->meta('title', $this->page->value('name').' - '.i18n('APP.TITLE'));
			} else {
				$pico->meta('title', $this->page->value('metaTitle'));
			}
			if ($this->page->value('metaCanonical')) {
				if ($this->page->value('metaCanonical')=='yes') {
					$pico->meta('canonical', $pico->href('~'));
				} elseif ($this->page->value('metaCanonical')=='no') {
					$pico->meta('canonical', NULL, true);
				} elseif ($this->page->value('metaCanonical')=='manual') {
					$pico->meta('canonical', $pico->href($this->page->value('metaCanonicalManual')));
				}
			}
			if (!$this->page->value('metaDescription')) {
				$pico->meta('description', CoreTwig::summary($this->page->value('content'), 200, '...'));
			} else {
				$pico->meta('description', $this->page->value('metaDescription'));
			}
			if (!$this->page->value('metaKeywords')) {
				$pico->meta('keywords', $this->page->value('metaKeywords'));
			}
		}
	}


	protected function _loadWidgets() {
		global $pico;
		if (!is_array($this->widgets)) {
			$this->widgets = array();
			/* 
				It is safe to register the _widgets global before because Twig 
				can't register globals once it has been initialized.
			*/
			$pico->variable('_widgets', $this->widgets); 
		}
		$pageWidgets = explode(',', $this->page->value('widgets'));
		if ($pico->config['site']['load_children_widgets'] && isset($this->pages[$this->page->value('navigation')][$this->page->value('id')]['_children'])) {
			foreach($this->pages[$this->page->value('navigation')][$this->page->value('id')]['_children'] as $foo) {
				$pageWidgets = array_merge($pageWidgets, explode(',', $foo['widgets']));
			}
		}
		/* Implicit widgets */
		$implicits = array_filter(
			get_class_methods('PageWidgets'), 
			function($method) { 
				return (substr($method, 0, 1)=='_'); 
			}
		);
		$pageWidgets = array_merge($implicits, $pageWidgets);
		/* Executing widgets */
		foreach($pageWidgets as $widget) {
			if ($widget) {
				$widgetData = true;
				if (method_exists('PageWidgets', $widget)) {
					$this->widgets[$widget] = PageWidgets::$widget($this->page);
				} else {
					$this->widgets[$widget] = $widgetData;
				}
			}
		}
		$pico->variable('_widgets', $this->widgets); 
	}	


	public function _() {	
		global $pico;
		if ($this->page->fetched()) {
			switch($this->page->value('type')) {

				case 'blocks':
					break;

				case 'content':
					break;

				case 'plugin':
					$pluginPath = explode('_', $this->page->value('plugin'));
					if (!isset($pluginPath[1])) {
						$pluginPath[1] = '_';
					}
					$pluginClass = 'Page_'.ucfirst($pluginPath[0]).'Controller';
					if (class_exists($pluginClass)) {
						$plugin = new $pluginClass(strtolower($pluginClass), $pluginPath);
						if (is_object($plugin) && (is_subclass_of($plugin, 'Controller'))) {
							if (!in_array('_authorized', get_class_methods($plugin)) || ($plugin->_authorized())) {
								if (in_array($pluginPath[1], get_class_methods($plugin))) {
									$action = $pluginPath[1];
									$pico->variable('_pico', array('action' => $action), false, true);
									$plugin->$action();
								} elseif (in_array('_404', get_class_methods($plugin))) {
									$pico->variable('_pico', array('action' => $action), false, true);
									$plugin->_404($this->urlParts[2]);
								} else {
									$plugin->_();
								}
							} elseif (in_array('_login', get_class_methods($plugin))) {
								$plugin->_login();
							} elseif (in_array('_401', get_class_methods($plugin))) {
								$plugin->_401();
							} else {
								$pico->_401();
							}
							$plugin->output();
							$pico->outputed = true;
						} else {
							$pico->_404();
						}
					} else {
						$pico->_404();
					}

					break;

				case 'redirect':
					$pico->url($this->page->value('redirect'));
					break;

			}
		} else {
			if ($this->path(1)) {
				$redirectPage = NULL;
				if ($pico->config['site']['uri_id']) {
					if (strpos($this->path(1), '_')!==false) {
						$redirectId = intval(substr($this->path(1), strrpos($this->path(1), '_')+1));
						$redirectPage = $pico->db->row('pages', '`__draft`=0 AND `id`='.$redirectId);
					} else {
						$redirectPage = $pico->db->row('pages', '`__draft`=0 AND `uri` LIKE "'.$this->path(1).'\_%"');
					}
				}
				if ($redirectPage) {
					$pico->url(';'.$redirectPage['lang'].'/page/'.$redirectPage['uri'].$this->pathUri(2), '301');
				} else {
					$pico->_404();
				}
			} else {
				if (count($this->pages)>0) {
					$foo = reset($this->pages);
					if (count($foo)>0) {
						$bar = reset($foo);
						$pico->url(':'.$bar['uri']);
					}
				} else {
					$pico->_404();
				}
			}
		}
	}
	
}