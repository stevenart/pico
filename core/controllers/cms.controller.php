<?php

class CMSController extends Controller {
	
	use RestrictedControllerTrait;
	private $restrictedModel = 'CMSUser';
	private $restrictedI18nSection = 'CMS';
	private $restrictedView = 'cms/_restricted.twig';


	public $configuration;
	public $core;
	public $plugins;


	public function __construct($name, array $urlPath=NULL, array $config=NULL) {
		global $pico;
		parent::__construct($name, $urlPath, $config);
		$pico->twig->addExtension(new \Twig\Extension\StringLoaderExtension()); // Extension needed to use template_from_string function in Twig templates
		
		define('__PICO_DNT', true);
		$pico->meta('title', CoreTwig::urlDomain(__URL).' CMS');
		$pico->meta('noindex', true);
	}


	public function _() {
		global $pico;

		/* Redirect to correct language */
		$cmsLangs = $pico->i18n->cmsLangs();
		if (in_array($this->restrictedAuthorized->value('lang'), $cmsLangs) && ($this->restrictedAuthorized->value('lang')!=$pico->i18n->lang())) {
			$pico->url('|'.$this->restrictedAuthorized->value('lang'));
		} else {
			if (!in_array($pico->i18n->lang(), $cmsLangs)) {
				$pico->url('|'.reset($cmsLangs));
			}
		}
	
		/* Bulding configuration */
		$this->configuration = array(
			'plugins' => ''
		);
		if (isset($pico->config['cms'])) {
			$this->configuration = array_merge($this->configuration, $pico->config['cms']); 
		}
		$this->plugins = array();
		$pluginsChildren = array();
		$spaced = false;
		$group = '';
		foreach(explode(',', $this->configuration['plugins']) as $plugin) {
			if (substr($plugin, 0, 1)=='-') {
				$spaced = strlen($plugin)>1 ? substr($plugin, 1) : true;
				$group = strlen($plugin)>1 ? substr($plugin, 1) : '';
			} else {
				$pluginClass = ucfirst($plugin).'CMSPlugin';
				$this->plugins[$plugin] = array(
					'class'		=> $pluginClass,
					'config' 	=> $pluginClass::getPluginConfiguration(),
					'name' 		=> $plugin,
					'spaced'	=> $spaced,
					'group'		=> $group,
				);
				if (isset($this->plugins[$plugin]['config']['parentPlugin'])) {
					if (!isset($pluginsChildren[$this->plugins[$plugin]['config']['parentPlugin']])) {
						$pluginsChildren[$this->plugins[$plugin]['config']['parentPlugin']] = array();
					}
					$pluginsChildren[$this->plugins[$plugin]['config']['parentPlugin']][] = $plugin;
				}
				$spaced = false;
			}
		}
		if ($pico->config['cms']['ai']) {
			$pluginClass = 'AibotsCMSPlugin';
			$this->plugins['aibots'] = array(
				'class'		=> $pluginClass,
				'config' 	=> $pluginClass::getPluginConfiguration(),
				'spaced'	=> false,
			);
		}
		if ($pico->config['plausible']['auth']) {
			$pluginClass = 'PlausibleCMSPlugin';
			$this->plugins['plausible'] = array(
				'class'		=> $pluginClass,
				'config' 	=> $pluginClass::getPluginConfiguration(),
				'spaced'	=> false,
			);
		}
		if (substr($this->restrictedAuthorized->value('email'), -1 * (strlen($pico->config['cms']['root_user_domain'])+1))=='@'.$pico->config['cms']['root_user_domain']) {
			$pluginClass = 'DevCMSPlugin';
			$this->plugins['dev'] = array(
				'class'		=> $pluginClass,
				'config' 	=> $pluginClass::getPluginConfiguration(),
				'spaced'	=> false,
			);
		}
		if ($this->restrictedAuthorized->value('restricted')) {
			$this->plugins = array_intersect_key($this->plugins, array_flip(explode(',', $this->restrictedAuthorized->value('plugins'))));
		}
		$this->variable('plugins', $this->plugins);
		$this->variable('pluginsChildren', $pluginsChildren);

		/* View */
		$this->view = 'cms/_cms.twig';
		$pico->variable('_cms', array(
			'ai'=> $pico->config['cms']['ai'],
			'logged' => $this->restrictedAuthorized->one(),
			'toggleCore' => $pico->config['cms']['plugins_toggle_core'],
		));
		$pico->variable('i18nCmsSections', I18nCMSPlugin::$cmsSections);

		/* Current plugin */
		if ($this->path(1)) {
			if ($this->path(1, array_keys($this->plugins))) {
				$pluginConfig = array();
				if (isset($pluginsChildren[$this->path(1)])) {
					$pluginConfig['pluginChildren'] = array();
					foreach ($pluginsChildren[$this->path(1)] as $pluginChild) {
						if (isset($this->plugins[$pluginChild])) {
							$pluginConfig['pluginChildren'][$pluginChild] = $this->plugins[$pluginChild];
						}
					}
				}
				if (isset($this->plugins[$this->path(1)]['config']['parentPlugin'])) {
					$pluginConfig['parentPlugin'] = $this->plugins[$this->plugins[$this->path(1)]['config']['parentPlugin']];
				}
				$plugin = new $this->plugins[$this->path(1)]['class']($this->path(1), array_slice($this->path(), 1), $pluginConfig);
				call_user_func(array(
					$plugin, 
					$this->path(2, get_class_methods($plugin)) ? $this->path(2) : '_', 
				));
				$this->variable('content', $plugin->fetch());
			} else {
				$pico->url(':cms');
			}
		} elseif (count($this->plugins)>0) {
			$pico->url(':cms/'.array_keys($this->plugins)[0]);
		}
	}


	public function ajax() {
		global $pico;
		if ($this->path(2) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])=='xmlhttprequest') {
			if ($pico->debugBar) {
				$pico->debugBar['messages']->addMessage($_SERVER['REQUEST_URI'] ?? '?', 'request');
				$pico->debugBar->sendDataInHeaders();
			}
			$result = array('success'=>false);
			switch ($this->path(2)) {

				case 'blocks':
					if (isset($_POST['action'])) {
						switch ($_POST['action']) {
							case 'list':
								$blocksList = explode(',', $pico->config['cms']['blocks']);
								if ($_POST['fieldblocks']) {
									$blocksList = array_intersect($blocksList, explode(',', $_POST['fieldblocks']));
								}
								$result['list'] = array();
								foreach($blocksList as $blockType) {
									$block = Block::factory($blockType);
									if (is_a($block, 'Block')) {
										$result['list'][] = array(
											'type' => $blockType,
											'description' => $block->getDescription(),
											'icon' => $block->getIcon(),
											'modalClass' => $block->getModalClass(),
											'title' => $block->getTitle(),
										);
									}
								}
								$result['success'] = (count($result['list']) > 0);
								break;
							case 'form':
								if (isset($_POST['block']) && isset($_POST['field'])) {
									$formName = $_POST['field'].'_blockForm';
									$block = Block::factory($_POST['block']);
									$result['block'] = array(
										'type' => $_POST['block'],
										'description' => $block->getDescription(),
										'icon' => $block->getIcon(),
										'modalClass' => $block->getModalClass(),
										'title' => $block->getTitle(),
									);
									$canSave = $block->loadForm(NULL, true, NULL, false, false, $formName);
									if ($canSave===true) {
										$result['success'] = true;
										$result['value'] = $block->one();
										$result['value']['_block'] = $_POST['block'];
									} else {
										if ($canSave===false) {
											$result['success'] = true;
											if ($_POST['load']) {
												$block->load(json_decode($_POST['load'], true));
											}
										}
										$result['form'] = $block->form(array(
											'actions' => array(
												'cancel' => array(
													'css' => 'button',
													'text' => i18n('CMS.ACTION_CANCEL'),
												),
												'save' => array(
													'css' => 'button isUpdate isPrimary',
													'text' => i18n('CMS.ACTION_SAVE'),
												),
											),
											'ajax' => true,
											'errors' => $canSave,
											'name' => $formName,
											'variables' => [
												'_cms' => ['ai' => $pico->config['cms']['ai']]
											],
										));
									}
								}
								break;
							case 'refresh':
								if (isset($_POST['items'])) {
									$result['html'] = ''; 
									$items = json_decode($_POST['items'], true);
									$i = 0;
									$result['countBlocks'] = count($items);
									foreach ($items as $item) {
										$block = Block::factory($item['_block']);
										if (is_a($block, 'Block')) {
											$block->load($item);
											$result['html'].= ' '.$pico->twig->render('forms/blocks.item.twig', array(
												'blockType' => $item['_block'],
												'description' => $block->getDescription(),
												'fields' => $block->getFields(),
												'icon' => $block->getIcon(),
												'index' => $i++,
												'item' => $block->one(),
												'modalClass' => $block->getModalClass(),
												'templateItem' => $block->getTemplateItem(),
												'title' => $block->getTitle(),
											));
										}
									}
								}
								break;
						}
					}
					break;

				case 'models':
					if (isset($_POST['action'])) {
						switch ($_POST['action']) {
							case 'form':
								if (isset($_POST['model']) && isset($_POST['field'])) {
									$formName = $_POST['field'].'_modelForm';
									$model = Model::factory($_POST['model']);
									$canSave = $model->loadForm(NULL, true, NULL, false, false, $formName);
									if ($canSave===true) {
										$result['success'] = true;
										$result['value'] = $model->one();
									} else {
										if ($canSave===false) {
											$result['success'] = true;
											if ($_POST['load']) {
												$model->load(json_decode($_POST['load'], true));
											}
										}
										$_POST['formconfig'] = json_decode($_POST['formconfig'], true);
										if (!is_array($_POST['formconfig'])) {
											$_POST['formconfig'] = [];
										}
										$formConfig = Tools::arrayMergeConfig(array(
											'actions' => array(
												'cancel' => array(
													'css' => 'button',
													'text' => i18n('CMS.ACTION_CANCEL'),
												),
												'save' => array(
													'css' => 'button isUpdate isPrimary',
													'text' => i18n('CMS.ACTION_SAVE'),
												),
											),
											'ajax' => true,
											'errors' => $canSave,
											'fieldsets' => false,
											'name' => $formName,
											'variables' => [
												'_cms' => ['ai' => $pico->config['cms']['ai']]
											],
										), $_POST['formconfig']);
										$result['form'] = $model->form($formConfig);
									}
								}
								break;
							case 'refresh':
								if (isset($_POST['items']) && isset($_POST['model'])) {
									$model = Model::factory($_POST['model']);
									$result['html'] = ''; 
									$items = json_decode($_POST['items'], true);
									if (!is_array($items)) {
										$items = [];
									}
									$i = 0;
									$result['countModels'] = count($items);
									foreach ($items as $item) {
										$model->load($item, false, true);
										$result['html'].= ' '.$pico->twig->render('forms/models.item.twig', array(
											'fields' => $model->getFields(),
											'index' => $i++,
											'item' => $model->one(),
											'templateItem' => $model->getTemplateItem()
										));
									}
								}
								break;
						}
					}
					break;

				case 'references':	
					if (isset($_POST['action']) && isset($_POST['model'])) {
						switch ($_POST['action']) {
							case 'browser':
								if (isset($_POST['query'])) {
									$model = Model::factory($_POST['model']);						
									$condition = '(1=1)';
									if (isset($_POST['condition']) && !empty($_POST['condition'])) {
										$condition = $_POST['condition'];
									}
									$resultsData = array(
										'model' => $_POST['model'],
										'fields' => $model->getFields(),
										'templateItem' => $model->getTemplateItem(),
									);
									if (strlen($_POST['query'])>2) {
										$model->search(NULL, $_POST['query'], $condition);
										$resultsData['results'] = $model->values();
									} elseif (strlen($_POST['query'])>0) {
										$resultsData['help'] = true;
									} else {
										$model->fetch($condition, '`__timestamp` DESC', 50);
										$resultsData['results'] = $model->values();
									}
									$result['results'] = $pico->twig->render('forms/references.browser.twig', $resultsData);
									$result['success'] = true;
								}
								break;
							case 'form':
								if (isset($_POST['model']) && isset($_POST['field'])) {
									$formName = $_POST['field'].'_createModal_form';
									$formConfig = [];
									if (isset($_POST['config']) && isset($_POST['config'])) {
										$formConfig = json_decode($_POST['config'], true);
									}
									$model = Model::factory($_POST['model']);
									$canSave = $model->loadForm(NULL, true, NULL, false, false, $formName);
									if ($canSave===true) {
										if ($model->save()) {
											$result['success'] = true;
											$result['value'] = $model->value('id');
										}
									} else {
										$formConfig = array_merge($formConfig, [
											'actions' => [
												'cancel' => ['css' => 'button', 'text' => i18n('CMS.ACTION_CANCEL')],
												'submit' => ['type'=>'submit', 'css'=>'button update', 'text'=>i18n('CMS.ACTION_SAVE')],
											],
											'ajax' => true,
											'errors' => $canSave,
											'fieldsets' => false,
											'name' => $formName,
											'variables' => [
												'_cms' => ['ai' => $pico->config['cms']['ai']]
											],
										]);
										$result['form'] = $model->form($formConfig);
									}
								}
								break;
							case 'refresh':
								if (isset($_POST['items'])) {
									$result['html'] = ''; 
									$model = Model::factory($_POST['model']);
									$ids = explode(',', $_POST['items']);
									array_walk($ids, 'intval');
									$model->fetch('`id` IN ('.implode(',', $ids).')');
									$result['count'] = $model->fetched();
									$i = 0;
									foreach ($model->values() as $row) {
										$result['html'].= ' '.$pico->twig->render('forms/references.item.twig', array(
											'model' => $_POST['model'],
											'fields' => $model->getFields(),
											'index' => $i++,
											'item' => $row,
											'templateItem' => $model->getTemplateItem()
										));
									}
									$result['success'] = true;
								}
								break;
						}
					}
					break;

			}

			echo json_encode($result);
			die();
		} else {
			$pico->url(':cms');
		}
	}			


	public static function isLogged() {
		global $pico;
		$dummy = new CMSController('Cms');
		return $dummy->_authorized();
	}
	

}