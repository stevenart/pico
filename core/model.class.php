<?php

/**
 * Model class : this class is the class every pico model must extends.
 */
class Model {

	public $lastSave;
	
	protected $definition;
	protected $detached;
	protected $fields;
	protected $fieldsets;
	protected $i18nPrefixed;
	protected $name;
	protected $primary;
	protected $searchable;
	protected $table;
	protected $templateItem;
	protected $values;


	public function __construct($name=NULL, $detached=NULL, $classType='Model') {
		global $pico;

		/* Loading definition */
		if (get_class($this)==$classType) {
			if (is_null($name)) {
				die('No name provided for this '.get_class($this).'.');
			} elseif (is_array($name) && isset($name['name'])) {
				$this->name = $name['name'];
				$definition = $name;
			} else {
				$this->name = strtolower($name);
			}
		} else {
			$this->name = strtolower(substr(get_class($this), 0, -1*strlen($classType)));
		}

		$file = false;
		switch ($classType) {
			case 'Block':
				$file = appfile('blocks/'.strtolower($this->name).'.block.yaml');
				break;
			case 'Model':
				$file = appfile('models/'.strtolower($this->name).'.model.yaml');
				break;
		}
		if ($file && file_exists($file)) {
			$definition = Spyc::YAMLLoad($file);
		} elseif (!isset($definition)) {
			die('No definition file found !');
		}

		/* Detached */
		if (is_null($detached)) {
			$detached = isset($definition['detached']) ? $definition['detached'] : false;
		}
		$this->detached = ($detached ? true : false);
		if (!$this->detached) {
			$this->table = isset($definition['table']) ? $definition['table'] : strtolower($this->name);
			if (isset($definition['primary'])) {
				$this->primary = is_array($definition['primary']) ? $definition['primary'] : array($definition['primary']);
			} elseif (isset($definition['fields']['id'])) {
				$definition['primary'] = array('id');
				$this->primary = array('id');
			} else {
				die('Undetached models must have a primary key !');
			}
		}

		/* Definition hook */
		if (method_exists('Hooks', 'modelDefinition')) {
			Hooks::modelDefinition($this->name, $definition);
		}

		/* Fieldsets */
		if (isset($definition['fieldsets']) && (is_array($definition['fieldsets']))) {
			$this->fieldsets = $definition['fieldsets'];
		} else {
			$this->fieldsets = array();
		}

		/* Searchable */
		if (isset($definition['searchable']) && (is_array($definition['searchable']))) {
			$this->searchable = $definition['searchable'];
		} elseif (is_array($definition['fields'])) {
			$this->searchable = array_keys($definition['fields']);
		} else {
			$this->searchable = [];
		}

		/* Requires */
		if (isset($definition['require'])) {
			if (!is_array($definition['require'])) {
				$definition['require'] = array($definition['require']);
			}
			foreach($definition['require'] as $requireModel) {
				Model::factory($requireModel);
			}
		} 

		/* i18n prefixed fields */
		if (isset($definition['i18n_prefixed'])) {
			if (!is_array($definition['i18n_prefixed'])) {
				$definition['i18n_prefixed'] = array($definition['i18n_prefixed']);
			}
			if (isset($definition['i18n_prefixed_all']) && ($definition['i18n_prefixed_all'])) {
				$i18nLanguages = $pico->i18n->langs();
			} else {
				$i18nLanguages = $pico->i18n->langs(NULL, false);
			}
			foreach($definition['i18n_prefixed'] as $i18nPrefixed) {
				if (isset($definition['fields'][$i18nPrefixed])) {
					$this->i18nPrefixed[$i18nPrefixed] = array();
					foreach($i18nLanguages as $i18nLanguage) {
						$definition['fields'][$i18nPrefixed]['_unprefixed'] = $i18nPrefixed;
						$definition['fields'] = Tools::arrayInsertBefore($definition['fields'], $i18nPrefixed, $i18nLanguage.'-'.$i18nPrefixed, $definition['fields'][$i18nPrefixed]);
						$this->i18nPrefixed[$i18nPrefixed][] = $i18nLanguage.'-'.$i18nPrefixed;
						/* i18n prefixed fields in auto_uri */
						if (isset($definition['fields'][$i18nLanguage.'-'.$i18nPrefixed]['auto_uri'])) {
							foreach($definition['fields'][$i18nLanguage.'-'.$i18nPrefixed]['auto_uri'] as $autoUriField) {
								if (in_array($autoUriField, $definition['i18n_prefixed'])) {
									$foundInUri = array_search($autoUriField, $definition['i18n_prefixed']);
									if (is_integer($foundInUri)) {
										array_splice(
											$definition['fields'][$i18nLanguage.'-'.$i18nPrefixed]['auto_uri'], 
											$foundInUri-1, 
											1, 
											$i18nLanguage.'-'.$autoUriField
										);
									}
								}
							}
						}	
						/* i18n prefixed fields in fieldsets */
						foreach($this->fieldsets as $fsName => $fsFields) {
							$foundInFieldset = array_search($i18nPrefixed, $fsFields);
							if (is_integer($foundInFieldset)) {
								array_splice(
									$this->fieldsets[$fsName], 
									$foundInFieldset, 
									0, 
									$i18nLanguage.'-'.$i18nPrefixed
								);
							}
						}
						/* i18n prefixed fields in searchable */
						$foundInSearchable = array_search($i18nPrefixed, $this->searchable);
						if (is_integer($foundInSearchable)) {
							array_splice(
								$this->searchable, 
								$foundInSearchable, 
								0, 
								$i18nLanguage.'-'.$i18nPrefixed
							);
						}
					}
					unset($definition['fields'][$i18nPrefixed]);
					unset($definition['searchable'][$i18nPrefixed]);
				}
			}
		}

		/* Adding default fields */
		$definition['fields']['__draft'] = array('name'=>'__draft','type'=>'boolean');
		$definition['fields']['__ordering'] = array('name'=>'__ordering','type'=>'integer');
		$definition['fields']['__starred'] = array('name'=>'__starred','type'=>'boolean');
		$definition['fields']['__timestamp'] = array('name'=>'__timestamp','type'=>'timestamp');
		$definition['fields']['__created'] = array('name'=>'__created','type'=>'timestamp');
		$definition['fields']['__user'] = array('name'=>'__user','type'=>'choice_int','options_db'=>array('cmsusers','id','email'));

		/* Checking table structure in database */
		if (!$this->detached) {
			if (!is_array($name)) {
				if (!$pico->db->tableCheck($definition, $this->table, true, true, $file)) {
					die('Error checking table for model "'.$this->name.'. Logs should help more.');
				}
			}
		}

		/* Loading fields */
		$this->fields = $definition['fields'];
		foreach($this->fields as $field => $desc) {
			if (ModelFields::exists($desc['type'])) {

				/* Default from ModelFields */
				$descDefault = ModelFields::getDefaultConfig($desc['type']);
				if ($descDefault) {
					$this->fields[$field] = Tools::arrayMergeConfig($descDefault, $desc);
					$desc = $this->fields[$field];
				}

				/* Checks */
				if (!isset($this->fields[$field]['checks']) || !is_array($this->fields[$field]['checks'])) {
					$this->fields[$field]['checks'] = array();
				}
				foreach($this->fields[$field]['checks'] as $checkRule) {
					$checkRuleParams = stripos($checkRule, '(');
					if ($checkRuleParams!==false) {
						$checkRule = substr($checkRule, 0, $checkRuleParams);
					}
					if (!method_exists('Check', $checkRule) && !method_exists('AppCheck', $checkRule)) {
						die('Checks rule "'.$checkRule.'" does not exists for "'.$this->name.':'.$field.'".');
					}
				}
				if (method_exists('Check', $desc['type']) && ($desc['type']!='references') && !in_array($desc['type'], $this->fields[$field]['checks'])) {
					$this->fields[$field]['checks'][] = $desc['type'];
				}

				/* Text (label), placeholder, helper and template */
				if (!isset($this->fields[$field]['text'])) {
					if (isset($this->fields[$field]['_unprefixed'])) {
						$this->fields[$field]['text'] = i18n($this->name.'.FIELD_'.$this->fields[$field]['_unprefixed']).' ('.i18n('CONFIG.FIELD_LANG_'.substr($field, 0, strpos($field, '-'))).')';
					} else {
						$this->fields[$field]['text'] = i18n($this->name.'.FIELD_'.$field);
					}
				} elseif (isset($this->fields[$field]['text_i18n'])) {
					$this->fields[$field]['text'] = i18n($this->fields[$field]['text']);
				}
				if (isset($this->fields[$field]['placeholder']) && isset($this->fields[$field]['placeholder_i18n'])) {
					$this->fields[$field]['placeholder'] = i18n($this->fields[$field]['placeholder']);
				} elseif (isset($this->fields[$field]['placeholder_auto_i18n'])) {
					if (isset($this->fields[$field]['_unprefixed'])) {
						$this->fields[$field]['placeholder'] = i18n($this->name.'.FIELD_'.$this->fields[$field]['_unprefixed'].'__PLACEHOLDER');
					} else {
						$this->fields[$field]['placeholder'] = i18n($this->name.'.FIELD_'.$field.'__PLACEHOLDER');
					}
				}
				if (isset($this->fields[$field]['helper']) && isset($this->fields[$field]['helper_i18n'])) {
					$this->fields[$field]['helper'] = i18n($this->fields[$field]['helper']);
				} elseif (isset($this->fields[$field]['helper_auto_i18n'])) {
					if (isset($this->fields[$field]['_unprefixed'])) {
						$this->fields[$field]['helper'] = i18n($this->name.'.FIELD_'.$this->fields[$field]['_unprefixed'].'__HELPER');
					} else {
						$this->fields[$field]['helper'] = i18n($this->name.'.FIELD_'.$field.'__HELPER');
					}
				}
				if (isset($this->fields[$field]['checks_helper']) && is_array($this->fields[$field]['checks_helper']) && isset($this->fields[$field]['checks_helper_i18n'])) {
					foreach($this->fields[$field]['checks_helper'] as &$checkHelper) {
						$checkHelper = i18n($checkHelper);
					}
				} elseif (isset($this->fields[$field]['checks_helper_auto_i18n']) && is_array($this->fields[$field]['checks_helper'])) {
					foreach($this->fields[$field]['checks_helper'] as $index => $checkHelperRule) {
						if (isset($this->fields[$field]['_unprefixed'])) {
							$this->fields[$field]['checks_helper'][$checkHelperRule] = i18n($this->name.'.FIELD_'.$this->fields[$field]['_unprefixed'].'__ERROR_'.$checkHelperRule);
						} else {
							$this->fields[$field]['checks_helper'][$checkHelperRule] = i18n($this->name.'.FIELD_'.$field.'__ERROR_'.$checkHelperRule);
						}
						unset($this->fields[$field]['checks_helper'][$index]);
					}
				}
				if (!isset($this->fields[$field]['template'])) {
					$this->fields[$field]['template'] = ModelFields::getTemplate($desc['type']);
				}

				/* Allowpaste */
				if (isset($pico->config['cms']['allowpaste']) && ($pico->config['cms']['allowpaste']) && (in_array($desc['type'], array('html')))) {
					if (!isset($this->fields[$field]['allowpaste'])) {
						$this->fields[$field]['allowpaste'] = true;
					}
				} 

				/* Visibility */
				if (isset($this->fields[$field]['visibility'])) {
					if (is_array($this->fields[$field]['visibility']) && (count($this->fields[$field]['visibility'])<3) && (isset($this->fields[$this->fields[$field]['visibility'][0]]))) {
						if (count($this->fields[$field]['visibility'])==2) {
							if (!is_array($this->fields[$field]['visibility'][1])) {
								$this->fields[$field]['visibility'][1] = array($this->fields[$field]['visibility'][1]);
							}
						}
					} else {
						die('Visibility is incorrectly defined for "'.$this->name.':'.$field.'".');
					}	
				}

				/* Options */
				if (ModelFields::needOptions($desc['type'])) {
					if (!isset($desc['options'])) {
						if ($desc['type']=='boolean') {
							$this->fields[$field]['options'] = array(
								0=>'PICO.LABEL_NO',
								1=>'PICO.LABEL_YES',
							);
							$desc['options_i18n'] = true;
						} elseif (isset($desc['options_db']) && is_array($desc['options_db']) && count($desc['options_db'])>2) {
							$this->fields[$field]['options'] = $pico->db->pairs( 
								$desc['options_db'][0], 
								$desc['options_db'][1], 
								$desc['options_db'][2], 
								isset($desc['options_db'][3])?$desc['options_db'][3]:NULL, 
								isset($desc['options_db'][4])?$desc['options_db'][4]:$desc['options_db'][2].' ASC', 
								isset($desc['options_db'][5])?$desc['options_db'][5]:NULL);
						} elseif (isset($desc['options_config'])) {
							$this->fields[$field]['options']  = array();
							if ($desc['options_config']=='i18n.languages') {
								$configOptions = $pico->i18n->langs(NULL, false);
							} elseif ($desc['options_config']=='i18n.languages_all') {
								$configOptions = $pico->i18n->langs();
							} elseif ($desc['options_config']=='i18n.languages_site') {
								$configOptions = $pico->i18n->langs(NULL, false, false);
							} else {
								list($configSection, $configKey) = explode('.', $desc['options_config']);
								$configOptions = explode(',', $pico->config[$configSection][$configKey]);
							}
							foreach($configOptions as $option) {
								$this->fields[$field]['options'][$option] = isset($desc['options_i18n']) ? ('CONFIG.FIELD_'.$field.'_'.$option) : $option;
							}
						} elseif (isset($desc['options_range'])) {
							$this->fields[$field]['options'] = array();
							if (is_numeric($desc['options_range'])) {
								$desc['options_range'] = array(1, intval($desc['options_range']));
							}
							if (is_array($desc['options_range']) && count($desc['options_range'])>1) {
								foreach(range(
									$desc['options_range'][0], 
									$desc['options_range'][1], 
									isset($desc['options_range'][2]) ? $desc['options_range'][2] : 1 
								) as $option) {
									$this->fields[$field]['options'][$option] = $option;
								}
							}
						}
					}
					if (!is_array($this->fields[$field]['options'])) {
						die('Invalid options for '.$this->name.'.'.$field);
					}
					if (isset($desc['options_auto'])) {
						$this->fields[$field]['options'] = array();
						foreach($desc['options'] as $foo) {
							$this->fields[$field]['options'][$foo] = $foo;
						}
					} elseif (isset($desc['options_auto_i18n'])) {
						$this->fields[$field]['options'] = array();
						foreach($desc['options'] as $foo) {
							if (isset($this->fields[$field]['_unprefixed'])) {
								$this->fields[$field]['options'][$foo] = i18n($this->name.'.FIELD_'.$this->fields[$field]['_unprefixed'].'_'.$foo);
							} else {
								$this->fields[$field]['options'][$foo] = i18n($this->name.'.FIELD_'.$field.'_'.$foo);
							}
						}
					} elseif (isset($desc['options_i18n'])) {
						foreach($this->fields[$field]['options'] as $key => $value) {
							$this->fields[$field]['options'][$key] = i18n($value);
						}
					}
					if (isset($desc['options_sort'])) {
						asort($this->fields[$field]['options'], SORT_NATURAL | SORT_FLAG_CASE);
					}
					if (isset($desc['options_null'])) {
						if (isset($this->fields[$field]['_unprefixed'])) {
							$this->fields[$field]['options'] = array(''=>i18n($this->name.'.FIELD_'.$this->fields[$field]['_unprefixed'].'_NULL')) + $this->fields[$field]['options'];
						} else {
							$this->fields[$field]['options'] = array(''=>i18n($this->name.'.FIELD_'.$field.'_NULL')) + $this->fields[$field]['options'];
						}
					}
				}
			} else {
				die('Unknown field type ("'.$desc['type'].'") for "'.$this->name.':'.$field.'".');
			}	
		}

		/* Saving definition */
		$this->definition = $definition;

		/* Template item */
		if (isset($this->definition['templateItem'])) {
			$this->templateItem = $this->definition['templateItem'];
		} else {
			$this->templateItem = 
				'<small>{{ title }}</small>'.
				'<strong>#{{ item.id }}</strong>';
		}

		/* Default values */
		$this->values = array();
	}
	
	
	/**
	 * The check function checks the validity of loaded values based on rules set in checks.
	 * 
	 * @access public
	 * @param mixed $fields Fields to check. If NULL, all fields are checked. (default: NULL)
	 * @param int $index Values' index to check. (default: 0)
	 * @return bool
	 */
	public function check($fields=NULL, $index=0) {
		$check = array();
		foreach($this->fields as $fieldName => $field) {
			if (is_array($fields) && !in_array($fieldName, $fields)) continue;
			if (isset($field['checks'])) {		
				foreach($field['checks'] as $rule) {
					if (!Check::checkValue(
							(isset($this->values[$index][$fieldName])?$this->values[$index][$fieldName]:NULL), 
							Check::parseRule($rule, isset($this->values[$index])?$this->values[$index]:NULL)
						)) {
						$check[$fieldName] = (strripos($rule,'(')===false) ? $rule : substr($rule, 0, strripos($rule,'('));
					}
				}
			}
		}
		if (count($check)>0) return $check;
		else return true;
	}



	/**
	 * The checks function allows to access one field checks after model load.
	 * 
	 * @access public
	 * @param string $field Field name
	 * @param mixed $checks Checks to add or remove. If null, doesn't add any checks if $adding=true OR remove all checks if $adding=false. (default: NULL)
	 * @param bool $adding If false, remove specified checks instead of adding them. (default: true)
	 * @return array
	 */
	public function checks($field, $checks=NULL, $adding=true) {
		if (isset($this->fields[$field])) {
			if (is_null($checks)) {
				if ($adding===false) {
					$this->fields[$field]['checks'] = array();
				}
			} else {
				if (!is_array($checks)) {
					$checks = array($checks);
				}
				if ($adding===false) {
					foreach($this->fields[$field]['checks'] as $index => $fieldCheck) {
						if (in_array($fieldCheck, $checks)) {
							unset($this->fields[$field]['checks'][$index]);
						} elseif (stripos($fieldCheck, '(')) {
							$fieldCheck = substr($fieldCheck, 0, stripos($fieldCheck, '('));
							if (in_array($fieldCheck, $checks)) {
								unset($this->fields[$field]['checks'][$index]);
							}
						}
					}
					$this->fields[$field]['checks'] = array_diff($this->fields[$field]['checks'], $checks);
				} else {
					$this->fields[$field]['checks'] = array_merge($this->fields[$field]['checks'], $checks);
				}
			}
		} else {
			die("Can't access checks for $field the field '$field' does not exists.");
		}
		return $this->fields[$field]['checks'];
	}
	

	public function clear($index=NULL) {
		if (is_null($index)) {
			$this->values = array();
		} elseif (is_array($this->values) && isset($this->values[$index])) {
			unset($this->values[$index]);
		}
	}


	/**
	 * The count function counts matching sets of values store in database for a given condition.
	 * 
	 * @access public
	 * @param string $condition. (default: '')
	 * @return void
	 */
	public function count($condition='') {
		global $pico;
		if ($this->detached) return 0;
		return $pico->db->count($this->table, $condition);
	}

	
	/**
	 * Perform external counts on a given set of values.
	 * 
	 * @param array $values Set of values.
	 * @param array $counts External counts wanted. Format is : array('fieldname' => array('external_table', 'external_field')) 
	 * @access public
	 * @return void
	 */
	private function countValues($values, $counts) {
		global $pico;
		if (!is_array($values)) return array();
		if (is_array($counts)) {
			foreach($counts as $countFieldName => $count) {
				$valueField = $count[0];
				$countTable = $count[1];
				$countField = $count[2];
				if (isset($count[3])) $countCondition = '('.$count[3].')';
				else $countCondition = '(1=1)';
				if (isset($count[4])) $countOrder = $count[4];
				else $countOrder = NULL;
				if (isset($count[5])) $countLimit = '('.$count[5].')';
				else $countLimit = NULL;
				$joined = $pico->db->pairs($countTable, '`'.$countField.'`', 'COUNT(*)', $countCondition.' GROUP BY `'.$countField.'`', $countOrder, $countLimit);
				$countMultiple = ModelFields::isMultiple($this->fields[$valueField]['type']);
				foreach($values as $index => $row) {
					if ($countMultiple) {
						/*
							@todo Find a **good** way to do this.
						*/
					} else {
						$values[$index][$countFieldName] = isset($joined[$row[$valueField]]) ? $joined[$row[$valueField]] : 0;
					}
				}
			}
		}
		return $values;
	}
	
	
	/**
	 * The delete function deletes the loaded record from the database.
	 * 
	 * @access public
	 * @param mixed $primaryKey If sets, changes primary key before delete. (default: NULL)
	 * @return int Number of delete rows deleted or FALSE if error.
	 */
	public function delete($primaryKey=NULL) {
		global $pico;
		if ($this->detached) return false;
		if ( !is_null($primaryKey) ) {
			$this->primary($primaryKey);
		}
		$conditions = array();
		foreach( $this->values as $values) {	
			$condition = array();
			foreach( $this->primary as $primary ) {
				$escapeChar = ModelFields::shouldEscape($this->fields[$primary]['type'])? '"' : '';
				$condition[] = ' `'.$primary.'` = '.$escapeChar.$values[$primary].$escapeChar.' ';
			}
			$conditions[] = implode(' AND ', $condition);
		}
		return $pico->db->exec( 'DELETE FROM `'.$this->table.'` WHERE ('.implode(') OR (', $conditions).')' );	
	}
	
	
	/**
	 * The exists function checks the presence of a primary key exists in the database.
	 * 
	 * @access public
	 * @param mixed $primary
	 * @return bool
	 */
	public function exists($primary) {
		global $pico;
		if ($this->detached) return false;
		if (!is_array($primary)) $primary = array($primary);
		$conditions = array();
		foreach($primary as $index => $value) {
			$escape = ModelFields::shouldEscape($this->fields[$this->primary[$index]]['type']);
			$conditions[] = ' `' . $this->primary[$index] . '`= ' . ($escape?'"':'') . $value . ($escape?'"':'') . ' ';
		}
		return $pico->db->exists($this->table, '('.implode(' AND ', $conditions).')');
	}
	

	/**
	 * The existsCondition function checks the presence of a row in the database for the given condition.
	 * 
	 * @access public
	 * @param string $condition If set will see only rows matching this SQL condition (default: '').
	 * @return bool
	 */
	public function existsCondition($condition='') {
		global $pico;
		if ($this->detached) return false;
		return $pico->db->exists($this->table, $condition);
	}
	
	
	/**
	 * The existsField function checks the presence of a row in the database for the given field/value.
	 * 
	 * @access public
	 * @param string $field Field's name
	 * @param mixed $value Field's value
	 * @return bool
	 */
	public function existsField($field, $value) {
		global $pico;
		if ($this->detached) return false;
		if (!isset($this->fields[$field])) {
			die("Can't check if there is a row with $field='$value' because the field '$field' does not exists.");
			return false;
		}
		$escape = ModelFields::shouldEscape($this->fields[$field]['type']);
		$condition = ' `' . $field . '`= ' . ($escape?'"':'') . $value . ($escape?'"':'') . ' ';
		return $pico->db->exists($this->table, $condition);
	}
	

	public static function factory($name) {
		$classname = $name.'Model';
		if (class_exists($classname)) {
			return new $classname();
		} else {
			return new Model($name);
		}
		return false;
	}

	
	/**
	 * The fetch function is called to fetch data from the database to this model.
	 * 
	 * @access public
	 * @param string $condition. (default: '')
	 * @param string $order. (default: '')
	 * @param string $limit. (default: '')
	 * @return int Number of rows fetched.
	 */
	public function fetch($condition=NULL, $order=NULL, $limit=NULL) {
		global $pico;		
		if ($this->detached) return false;
		$this->values = $pico->db->all($this->table, '*', $condition, $order, $limit);
		return $this->fetched();
	}
	
	
	public function fetchOne($field, $value) {
		global $pico;
		if ($this->detached) return false;
		return $this->fetch('`'.$field.'`="'.safer($value,'db').'"', NULL, '0,1');
	}

	
	public function fetched($index=NULL) {
		global $pico;
		if (is_null($index)) {
			return count($this->values);
		} else {
			return isset($this->values[intval($index)]);
		}
	}
	
	
	/**
	 * The fetchPrimary function fetches a value set (row) for a given primary key value. 
	 * 
	 * @access public
	 * @param mixed array $primary Primary key to load.
	 * @return int Number of rows fetched (1 or 0).
	 */
	public function fetchPrimary($primary) {
		global $pico;
		if ($this->detached) return false;
		if (!is_array($primary)) $primary = array($primary);
		$conditions = array();
		foreach( $primary as $index => $value ) {
			$escape = ModelFields::shouldEscape($this->fields[$this->primary[$index]]['type']);
			$conditions[] = ' `' . $this->primary[$index] . '`= ' . ($escape?'"':'') . $value . ($escape?'"':'') . ' ';
		}
		$rowCount = $this->fetch('('.implode(' AND ', $conditions).')', NULL, '0,1');
		return $rowCount;
	}

	
	public function field($name, $key=NULL, $value=NULL, $clear=false) {
		if (!is_null($value)) {
			if (is_null($key)) {
				$this->fields[$name] = $value;
			} else {
				$this->fields[$name][$key] = $value;
			}
		}
		if ($clear) {
			if (is_null($key)) {
				unset($this->fields[$name]);
			} else {
				unset($this->fields[$name][$key]);
			}
		} else {
			if (is_null($key)) {
				if (isset($this->fields[$name])) {
					return $this->fields[$name];
				}
			} else {
				if (isset($this->fields[$name][$key])) {
					return $this->fields[$name][$key];
				}
			}
		}
		return NULL;
	}
	
	
	/**
	 * fieldInSet can return the fieldset a field is in, or change the fieldset
	 * from a field.
	 *
	 * @param string $fieldName The field name
	 * @param null|string $newFieldset The new fieldset (if change is wanted)
	 *
	 * @return bool|string The fieldset name or false if field/fieldset not found.
	 */
	public function fieldInSet(string $fieldName, ?string $newFieldset = NULL) {
		if (isset($this->fields[$fieldName])) {
			if (!is_null($newFieldset) && !isset($this->fieldsets[$newFieldset])) {
				$this->fieldsets[$newFieldset] = array();
			}
			foreach ($this->fieldsets as $fieldset => $fields) {
				if (in_array($fieldName, $fields)) {
					if (is_null($newFieldset)) {
						return $fieldset;
					} else {
						$this->fieldsets[$fieldset] = array_diff($this->fieldsets[$fieldset], array($fieldName));
						$this->fieldsets[$newFieldset][] = $fieldName;
						return $newFieldset;
					}
				}
			}
		}
		return false;
	}

	/**
	 * { function_description }
	 *
	 * @param array $parameters The parameters
	 *
	 * @return <type> ( description_of_the_return_value )
	 */
	public function form($parameters=array()) {
		global $pico;

		$internal = array_filter(array_keys($this->fields), function($foo){
			return (substr($foo, 0, 2)==='__');
		});
		$internal = array_merge(['id', 'uri'], $internal);
		foreach(explode(',', $pico->config['i18n']['languages']) as $lang) {
			$internal[] = $lang.'-uri';
		}
		if (isset($parameters['internalAdd'])) {
			if (!is_array($parameters['internalAdd'])) {
				$parameters['internalAdd'] = array($parameters['internalAdd']);
			}
			$internal = array_merge($internal, $parameters['internalAdd']);
		}
		if (isset($parameters['internalRemove'])) {
			if (!is_array($parameters['internalRemove'])) {
				$parameters['internalRemove'] = array($parameters['internalRemove']);
			}
			$internal = array_diff($internal, $parameters['internalRemove']);
		}
		$p = array_merge(array(
			'actions'	=> array(
				'submit' => array('type'=>'submit', 'text'=>'Submit')
			),
			'action' 		=> $pico->href('#'),
			'ajax'			=> false,
			'captcha'		=> false, 
			'css'			=> NULL,
			'botDetect'		=> NULL,
			'disabled'		=> NULL, 
			'errors'		=> array(), 
			'errorsMessage' => false,
			'fields'		=> NULL,
			'fieldsets'		=> $this->fieldsets,
			'fsclosed'		=> array(),
			'fsempty'		=> false,
			'internal' 		=> $internal,
			'include'		=> true,
			'index'			=> 0,
			'legends'		=> true,
			'method'		=> 'post',
			'model' 		=> $this->name,
			'name'			=> $this->name,
			'units'			=> true,
			'values' 		=> array(),
			'variables'		=> array(),
			'view'			=> 'forms/_form.twig',
		), $parameters);

		if (is_null($p['botDetect'])) {
			$p['botDetect'] = ($p['captcha'] ? true : false);
		}

		if (!is_null($p['index']) && isset($this->values[$p['index']])) {
			$p['values'] = array_merge($this->values[$p['index']], $p['values']);
		}

		$p['fields'] = $this->fields;
		if (isset($parameters['fields'])) {
			if ($p['include']) {
				$p['fields'] = array_intersect_key($p['fields'], array_flip($parameters['fields']));
			} else {
				$p['fields'] = array_diff_key($p['fields'], array_flip($parameters['fields']));
			}
		}
		$p['fields'] = array_diff_key($p['fields'], array_flip($p['internal']));

		if ($p['disabled']===true) {
			$p['disabled'] = array_keys($p['fields']);
		}

		if ($p['captcha']===true) {
			switch (rand(1, 2)) {
				case 1:
					$foo = rand(1,5);
					$bar = rand(1,5);
					$p['captcha'] = array(
						'answer' => $foo + $bar,
						'question' => i18n('PICO.MESSAGE_CAPTCHA_QUESTION', array($foo, $bar)),
					);
					unset($foo);
					unset($bar);
					break;
				case 2:
					$letters = ['b', 'c', 'd', 'h', 'l', 'p', 's', 't'];
					$foo = $letters[array_rand($letters)];
					$words = ['hello', 'thank you', 'love', 'house', 'eat', 'make', 'go', 'time', 'work', 'day', 'night', 'laugh', 'cry', 'car', 'school', 'cat', 'dog', 'book', 'drink', 'sleep'];
					if ($pico->i18n->lang()=="fr") {
						$words = ['bonjour', 'merci', 'amour', 'maison', 'manger', 'faire', 'aller', 'temps', 'travailler', 'jour', 'nuit', 'rire', 'pleurer', 'voiture', 'école', 'chat', 'chien', 'livre', 'boire', 'dormir'];
					}
					$bar = $words[array_rand($words)];
					$p['captcha'] = array(
						'answer' => substr_count($bar, $foo),
						'question' => i18n('PICO.MESSAGE_CAPTCHA_QUESTION2', array(strtoupper($foo), $bar)),
					);
					unset($foo);
					unset($bar);
					break;
			}				
		}
		if (is_array($p['captcha']) && isset($p['captcha']['answer'])) {
			$pico->session($p['name'].'_captcha', $p['captcha']['answer']);
		}

		if (is_array($p['errors'])) {
			$p['errors'] = array_intersect_key(
				$p['errors'], 
				array_merge($p['fields'], array('_captcha' => 'captcha'))
			);
		} else {
			$p['errors'] = array();
		}

		if (is_array($p['fieldsets'])) {
			$fieldsetsFields = array();
			foreach($p['fieldsets'] as $foo) {
				$fieldsetsFields = array_merge($fieldsetsFields, $foo);
			}
			$p['fieldsets']['_'] = array_diff(array_keys($p['fields']), $fieldsetsFields);
		} else {
			$p['fieldsets'] = array('_' => array_keys($p['fields']));
		}
		return $pico->twig->render($p['view'], array_merge(
			array('_form' => array_diff_key($p, array('variables'=>0))),  
			$p['variables']
		));
	}	


	protected function generateURI($index=0) {
		global $pico;
		if ($this->detached) return false;
		$generated = array();
		if (isset($this->fields['id'])) {
			foreach($this->fields as $fieldName => $field) {
				if (isset($field['auto_uri'])) {
					if (!is_array($field['auto_uri'])) {
						$field['auto_uri'] = explode(',', $field['auto_uri']);
					}
					if (isset($this->values[$index]) && isset($this->values[$index]['id'])) {
						$uri = '';
						foreach($field['auto_uri'] as $foo) {
							if (isset($this->values[$index][$foo])) {
								if (isset($this->fields[$foo]['options']) && isset($this->fields[$foo]['options'][$this->values[$index][$foo]])) {
									$uri.= $this->fields[$foo]['options'][$this->values[$index][$foo]].'-';
								} else {
									$uri.= $this->values[$index][$foo].'-';
								}
							}
						}
						$uri = Tools::generateURI($uri);
						if ($pico->config['site']['uri_id']) {
							$uri = $uri.'_'.intval($this->values[$index]['id']);
						} else {	
							$foo = $uri;
							$i = 1;
							while ($pico->db->exists($this->table, "`id`<>".intval($this->values[$index]['id'])." AND `$fieldName`='$foo'")) {
								$foo = $uri.'_'.$i++;
							}
							$uri = $foo;
						}
						$this->values[$index][$fieldName] = $uri;
						$generated[] = $fieldName;
					}
				}
			}
		}
		return (count($generated)>0 ? $generated : false);
	}


	public function getFields() {
		return $this->fields;
	}


	public function getFieldsets() {
		return $this->fieldsets;
	}


	public function getI18nPrefixed($field=NULL, $searchSiblings=false) {
		if (is_null($field)) {
			return $this->i18nPrefixed;
		} else {
			if ($searchSiblings) {
				if (isset($this->fields[$field]) && isset($this->fields[$field]['_unprefixed'])) {
					$field = $this->fields[$field]['_unprefixed'];
				}
			}
			if (isset($this->i18nPrefixed[$field])) {
				return $this->i18nPrefixed[$field];
			}
		}
		return array();
	}


	public function getSearchable() {
		return $this->searchable;
	}


	public function getTemplateItem() {
		return $this->templateItem;
	}


	/**
	 * Perform external joins on a given set of values.
	 * 
	 * @param array $values Set of values.
	 * @param array $joins External joins wanted. Format is : array('fieldname' => array('join_table', 'join_field') 
	 * @access public
	 * @return void
	 */
	private function joinValues($values, $joins) {
		global $pico;
		if (!is_array($values)) return array();
		if (is_array($joins)) {
			foreach($joins as $field => $join) {
				$joinTable = $join[0];
				$joinField = $join[1];
				$joinCondition = isset($join[2]) ? $join[2] : '(1=1)';
				$joinOrder = isset($join[3]) ? $join[3] : NULL;
				$joinLimit = isset($join[4]) ? $join[4] : NULL;
				$joinMultiple = ModelFields::isMultiple($this->fields[$field]['type']);
				foreach($values as $index => $row) {
					if (!isset($row[$field])) continue;
					$escape = ModelFields::shouldEscape($this->fields[$field]['type']);
					if ($joinMultiple) {
						if ($escape) $row[$field] = '"'.str_replace(',','","',$row[$field]).'"';
						$condition = $joinCondition.' AND (`'.$joinField.'` IN ('.$row[$field].'))';
						if (is_null($joinOrder)) {
							$joinOrder = 'field(`'.$joinField.'`, '.$row[$field].')';	
						}
						$values[$index][$field] = $pico->db->all($joinTable, '*', $condition, $joinOrder, $joinLimit);	
					} else {
						$condition = $joinCondition.' AND (`'.$joinField.'`='.($escape?'"':'').$row[$field].($escape?'"':'').')';
						$values[$index][$field] = $pico->db->row($joinTable, $condition, $joinOrder);	
					}
				}
			}
		}
		return $values;
	}
	
		
	public function load(array $values, $multiple=false, $clear=false, $mergeIndex=false) {
		if ($clear) $this->values = array();
		if ($multiple) {
			foreach($values as $value) $this->values[] = $value;
		} else {
			if ($mergeIndex!==false) {	
				$mergeIndex = intval($mergeIndex);
				if (is_array($this->values[$mergeIndex])) {
					$this->values[$mergeIndex] = array_merge($this->values[$mergeIndex], $values);
				} else {
					$this->values[$mergeIndex] = $values;
				}
			} else {
				$this->values[] = $values;
			}
		}
	}
	
	
	/**
	 * The loadForm function loads values submitted by a form in $data. 
	 * If $data is NULL, the function search $_POST then $_GET to find values.
	 * 
	 * @access public
	 * @param mixed $data Data to load form from. Autodetect by default. (default: NULL)
	 * @param bool $check Check after load ? (default: true)
	 * @param array $rootData Thoses datas will be used as values for fields, no mather what's in $data and will not raise errors (default: NULL).
	 * @return void
	 */
	public function loadForm($data=NULL, $check=true, $rootData=NULL, $mergeIndex=false, $forceCaptchaCheck=false, $formName=NULL) {
		global $pico;
		if (is_null($formName)) {
			$formName = $this->name;
		} 
		$present = $this->present($data, true, $formName);
		if ($present===false) {
			return false;
		} else {
			if ($present=='post') $data = $_POST;
			elseif ($present=='get') $data = $_GET;
		}
		if (is_array($rootData)) {
			foreach($rootData as $rootDataKey => $rootDataValue) {
				$data[$formName.'_'.$rootDataKey] = $rootDataValue;
			}
		}
		$clean = array();
		$overwriteAfterCheck = array();
		foreach($this->fields as $fieldName => $field) {
			if (isset($data[$formName.'_'.$fieldName]) || isset($data[$formName.'_'.$fieldName.'_present'])) {
				if (ModelFields::shouldImplode($field['type'])===true) {
					if (!isset($data[$formName.'_'.$fieldName])) $data[$formName.'_'.$fieldName] = array();
					$clean[$fieldName] = $this->validate($field['type'], implode(',', $data[$formName.'_'.$fieldName]));
				} else {		
					if (!isset($data[$formName.'_'.$fieldName])) {
						$data[$formName.'_'.$fieldName] = '';
						$clean[$fieldName] = '';
					} else {
						$clean[$fieldName] = $this->validate($field['type'], $data[$formName.'_'.$fieldName]);
						if ($field['type']=='password' && (trim($clean[$fieldName])!='')) {
							$overwriteAfterCheck[$fieldName] = md5($clean[$fieldName]);
						}
					}					
				}
			}	
		}
		if ($mergeIndex!==false) {
			$this->load($clean, false, false, $mergeIndex);
		} else {
			$this->load($clean, false, true);
		}
		$checked = false;
		if ($check) {
			$checked = $this->check(array_keys($clean));
			if (is_array($rootData)) {
				if (!is_array($checked)) {
					$checked = array();
				}
				foreach($rootData as $rootDataKey => $rootDataValue) {
					if (isset($checked[$rootDataKey])) unset($checked[$rootDataKey]);
				}
				if (!is_array($checked) || (count($checked)==0)) $checked = true;
			}
			if (isset($data[$formName.'__captcha_present']) || $forceCaptchaCheck) {
				if (intval($data[$formName.'__captcha'])!=intval($pico->session($formName.'_captcha'))) {
					if (!is_array($checked)) $checked = array();
					$checked['_captcha'] = 'captcha';
				} 
			}
		} else {
			$checked = true;
		}
		foreach($overwriteAfterCheck as $fieldName => $overwrite) {
			if (!is_array($checked) || !isset($checked[$fieldName])) {
				$this->value($fieldName, 0, $overwrite);
			}
		}
		return $checked;
	}


	/**
	 * The nextId function returns the next available id for this table.
	 * 
	 * @access public
	 * @return void
	 */
	public function nextId() {
		global $pico;
		if ($this->detached) return false;
		return $pico->db->nextId($this->table);
	}

	
	public function one($fields=NULL, $joins=NULL, $counts=NULL, $index=0) {
		$first = $this->values($fields, $joins, $counts, $index, 1);
		return reset($first);
	}

	
	/**
	 * The present function checks if a form for this model is present in $data.
	 * If $data is null, the function checks $_POST then $_GET for values.
	 * 
	 * @access public
	 * @param mixed $data Array to check from. If NULL, $_POST and $_GET are used (default: NULL).
	 * @param bool $explicit If set to true, the function will return a string indicating where the data was found ('data','post' or 'get'). Otherwise the function will return a boolean (default: false).
	 * @return mixed Boolean or string (if $sexplicit set to true).
	 */
	public function present($data=NULL, $explicit=false, $formName=NULL) {
		global $pico;
		if (is_null($formName)) {
			$formName = $this->name;
		} 
		$tokens = $pico->session('tokens');
		if (!is_array($tokens) || !isset($tokens['form'])) {
			return false;
		} 
		if (is_array($data)) {
			if (isset($data[$formName.'_present']) && ($data[$formName.'_present']==$tokens['form'])) {
				if ($explicit) return 'data';
				else return true;
			}
		} else {
			if (isset($_POST[$formName.'_present']) && ($_POST[$formName.'_present']==$tokens['form'])) {
				if ($explicit) return 'post';
				else return true;
			} elseif (isset($_GET[$formName.'_present']) && ($_GET[$formName.'_present']==$tokens['form'])) {
				if ($explicit) return 'get';
				else return true;
			}
		}
		return false;
	}


	/**
	 * The primary function returns the primary key field(s) for this model.
	 * 
	 * @access public
	 * @return array
	 */
	public function primary($value=NULL, $index=0) {
		if ($this->detached) return false;
		if (!is_null($value)) {
			if (!is_array($value)) $value = array($value);
			if (count($value)==count($this->primary)) {
				for ($i=0; $i<count($value); $i++) {
					$this->value($this->primary[$i], $index, $value[$i]);
				}
			}
		}
		return $this->primary;
	}


	public function reloadOptions() {
		global $pico;
		foreach($this->fields as $fieldName => $fieldData) {
			if (isset($fieldData['options_i18n_raw']) && isset($fieldData['options'])) {
				foreach($fieldData['options_i18n_raw'] as $rawKey => $rawValue) {
					$this->fields[$fieldName]['options'][$rawKey] = i18n($rawValue);
				}
				if (isset($fieldData['options_sort'])) {
					asort($this->fields[$fieldName]['options'], SORT_NATURAL | SORT_FLAG_CASE);
				}
			}
		}
	}


	/**
	 * This function saves the loaded/fetched values in the database. 
	 * Inserts if there is no record for the primary key, update otherwise.
	 * 
	 * @access public
	 * @param mixed $primaryKey Primary key value. If set, this key will used. (default: NULL)
	 * @param bool $onlyInsert If set to true, the function will not update any row and just insert missing one. (default: false)
	 * @return void
	 */
	public function save($primaryKey=NULL, $onlyInsert=false, $updateTimestamp=true) {
		global $pico;	
		if ($this->detached) return false;

		/* Can we try to save ? */		
		if (empty($this->values)) {
			die('Trying to save model '.$this->name.' without values loaded or fetched.');
			return false;
		}
		if (!is_null($primaryKey)) {
			$this->primary($primaryKey);
		}

		/* Saving ... */
		$this->lastSave = array();
		$rowCount = 0;
		foreach($this->values as $index => $row) {
			foreach($this->primary as $primary) {
				if (!isset($row[$primary])) {
					if ( $this->fields[$primary]['type']=='integer' ) {
						if ($pico->session($this->name.'_generated_model_id')) {
							$generatedPrimaryKey = $pico->session($this->name.'_generated_model_id');
						} else {
							$generatedPrimaryKey = $pico->db->nextId($this->table);
						}
						$pico->session($this->name.'_generated_model_id', NULL);
						$this->values[$index][$primary] = $generatedPrimaryKey;
						$row[$primary] = $generatedPrimaryKey;
					} else {
						die('Cannot save model "'.$this->name.'" if the primary key is not set.');
						return false;
					}
				}
			}
			$uris = $this->generateURI($index);
			if ($uris!==false) {
				foreach($uris as $uriField) {
					$row[$uriField] = $this->values[$index][$uriField];
				}
			}
			$sqlFields = array();
			foreach($row as $field => $value) {
				$sqlFields[$field] = ($value==='') ? NULL : $value;
			}
			if (isset($this->fields['__user']) && !isset($sqlFields['__user'])) {
				$sqlFields['__user'] = $pico->session('loggedCMSUserId');
			}
			if (isset($this->fields['__timestamp']) && $updateTimestamp) {
				$sqlFields['__timestamp'] = date('Y-m-d H:i:s');
			}
			if (isset($this->fields['__created']) && !isset($sqlFields['__created'])) {
				$sqlFields['__created'] = date('Y-m-d H:i:s');
			}
			$query = 'INSERT '.( $onlyInsert ? 'IGNORE ' : '' ).'INTO `'.$this->table.'`(`';
			$query.= implode('`,`', array_keys($sqlFields));
			$query.= '`) VALUES (';
			for($i=0; $i<count($sqlFields); $i++) $query.= (($i==0)?'?':',?');
			if ( $onlyInsert ) {
				$query.= ')';
			} else {
				$query.= ') ON DUPLICATE KEY UPDATE';
				foreach($sqlFields as $fieldName => $fieldValue) {
					$doNotUpdateFields = array_merge($this->primary, array('__created'));
					if (!in_array($fieldName, $doNotUpdateFields)) $query.= ' `' . $fieldName . '`=VALUES(`' . $fieldName . '`),';
				}
				$query = substr($query, 0, -1);
			}
			$this->lastSave = array('query'=>$query, 'values'=>array_values($sqlFields));
			$statement = $pico->db->prepare($query);
			if (!$statement) {
				$this->lastSave['error'] = $pico->db->errorInfo();
			} else {
				if ($statement->execute(array_values($sqlFields))) {
					$rowCount++;
				} else {
					$this->lastSave['error'] = $statement->errorInfo();
				}
			}
		}
		if ($rowCount>0) return $rowCount;
		else return false; 
	}
	

	public function search($fields=NULL, $query, $condition='(1=1)', $order='`__timestamp` DESC', $limit=50) {
		global $pico;		
		if ($this->detached) return false;

		if (is_null($fields)) {
			$fields = $this->searchable;
		}
		if (!is_array($fields) || (count($fields)==0)) {
			return false;
		}
		$prefixedFields = array();
		foreach ($fields as $field) {
			if (isset($this->i18nPrefixed[$field])) {
				$prefixedFields = array_merge($prefixedFields, $this->i18nPrefixed[$field]);
			} else {
				$prefixedFields[] = $field;
			}
		}
		$query = mb_strtolower(trim($query));
		if (strlen($query)>2) {
			$words = explode(' ', $query);
			foreach($words as &$word) {
				$word = str_replace(
					array('"', "'"), 
					array('_', '_'), 
					$word
				);
			}
			unset($word);
			$condition.= ' AND (';
			$i = 0;
			foreach($prefixedFields as $sf) {
				$condition.= ($i>0 ? ' OR ': '')."`$sf` LIKE '%".implode('%', $words)."%' ";
				$i++;
			}
			$condition.= ') ';
		} else {
			return false;
		}
		$this->values = $pico->db->all($this->table, '*', $condition, $order, $limit);
		return $this->fetched();
	}

	
	/**
	 * The setColumnValue function sets the value of a field in all loaded/fetched row.
	 * 
	 * @access public
	 * @param string $name Name of the field.
	 * @param mixed $value Value of the field.
	 * @return void
	 */
	public function setColumnValue($name, $value) {
		$ok = true;
		foreach( $this->values as $index => $row ) {
			$ok = $ok && $this->value($name, $index, $value);
		}
		return $ok;
	}
	

	/**
	 * The table function returns the table for this model.
	 * 
	 * @access public
	 * @return void
	 */
	public function table() {
		return $this->table;
	}

	
	public function validate($type, $value) {
		global $pico;
		if (is_string($value)) {
			$value = trim($value);
			if ($value=='') return NULL; 
			if (ini_get('magic_quotes_gpc')) {
				$value = stripslashes($value);
			}
		}
		switch($type) {
			case 'float':
			case 'money':
				if ($pico->i18n->locale("mon_thousands_sep") && ($pico->i18n->locale("mon_thousands_sep")!=$pico->i18n->locale("mon_decimal_point")) && ($pico->i18n->locale("mon_thousands_sep")!='.')) {
					$value = str_replace($pico->i18n->locale("mon_thousands_sep"), "", $value);
				}
				if ($pico->i18n->locale("mon_decimal_point")) {
					$value = str_replace($pico->i18n->locale("mon_decimal_point"), ".", $value);
				}
				if (is_numeric($value)) $value = floatval($value);
				break;
			case 'integer':
				if ($pico->i18n->locale("mon_thousands_sep")) {
					$value = str_replace($pico->i18n->locale("mon_thousands_sep"), "", $value);
				}
				if (is_numeric($value)) $value = intval($value);
				break;
		}
		return $value;
	}
	
	
	public function value($name, $index=0, $value=NULL, $clear=false) {
		if (!is_null($value)) {
			if (!isset($this->values[$index]) || !is_array($this->values[$index])) {
				$this->values[$index] = array();
			}
			$this->values[$index][$name] = $value;
		}
		if ($clear) {
			/* Here we need to set to NULL and not unset in case of future save() */
			$this->values[$index][$name] = NULL;
		} else {
			if (isset($this->values[$index][$name])) {
				return $this->values[$index][$name];
			}
		}
		return NULL;
	}
	
	
	public function values($fields=NULL, $joins=NULL, $counts=NULL, $start=0, $limit=9999, $values=NULL) {
		if (!is_null($values)) {
			$this->values = $values;
		}
		if ($this->fetched()) {
			if (is_null($fields)) {
				$values = array_slice($this->values, $start, $limit);
			} else {
				$values = array();
				foreach(array_slice($this->values, $start, $limit) as $index => $row) {
					$values[] = array_intersect_key($row, array_flip($fields));
				}
			}
			if (!is_null($counts)) $values = $this->countValues($values, $counts);
			return $this->joinValues($values, $joins);
		}
		return $this->values;
	}

}