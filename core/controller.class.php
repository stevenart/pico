<?php

/**
 * Abstract Controller class : this class is the class every pico controller must extends.
 * 
 * There are a few methods that are not implemented in the parent that are reserved and can be implemented if needed :
 *	_401() : called if authorized() is present, returns false and _login() is not present.
 *	_404() : called if the url is pointing to a method that does not exists (overwrites optional 404 controller).
 *	_authorized() : if the controller needs authentification, this function returns true if the user is logged and false otherwise.
 *	_login() : called if authorized() is present and returns false.
 *
 * @abstract
 */
abstract class Controller {

	protected $configuration;
	protected $name;
	protected $urlPath;
	protected $variables;
	protected $view;
	

	public function __construct($name, array $urlPath=NULL, array $config=NULL) {
		global $pico;
		
		/* Loading properties */
		$this->name = strtolower($name);
		$this->configuration = is_null($config) ? array() : $config;
		$this->urlPath = is_null($urlPath) ? array() : $urlPath;
		$this->variables = array();

		/* Filling default variables */
		$picoVariable = array(
			'action' => '_',
			'basepath' => __URL,
			'cdnDriverExtensions' => Thumbnail::$driverExtensions,
			'cmsLangs' => $pico->i18n->cmsLangs(),
			'config' => $this->configuration,
			'controller' => $this->name,
			'env' => $pico->env,
			'envData' => $pico->config['env'],
			'filesize_units' => $pico->config['i18n'][$pico->i18n->lang().'_filesize'],
			'gdpr' => $pico->config['site']['gdpr'],
			'gdprConsent' => $pico->config['site']['gdpr'] ? $pico->cookie('gdpr') : true,
			'lang' => $pico->i18n->lang(),
			'langs' => $pico->i18n->langs(NULL, false, false),
			'langsAll' => $pico->i18n->langs(),
			'locale' => $pico->i18n->locale(),
			'plausible' => $pico->config['plausible'],
			'now' => time(),
			'services' => $pico->config['services'],
			'session' => session_id(),
			'url' => $this->urlPath
		);
		if (method_exists('Hooks', 'controllerPicoVariable')) {
			Hooks::controllerPicoVariable($picoVariable);
		}
		$pico->variable('_pico', $picoVariable);
		if (is_null($pico->meta('title'))) {
			$pico->meta('title', ucfirst($this->name).' - '.CoreTwig::urlDomain(__URL));
		}
		if ((stripos(__URL, '/localhost')!==false) || (stripos(__URL, $pico->config['site']['noindex_pattern'])!==false)) {
			$pico->meta('noindex', true);
		}

		/* DebugBar */
		if ($pico->debugBar) {
			$pico->debugBar->addCollector(new DebugBar\DataCollector\ConfigCollector($picoVariable, get_class($this)));
			$pico->variable('_debugbar', $pico->debugBar->getJavascriptRenderer($pico->href(';debugbar.php?file=')));
		}
	}

	
	/**
	 * The index function is the default action called on a controller.
	 * 
	 * @access public
	 * @abstract
	 * @return void
	 */
	abstract public function _();


	/**
	 * The fetch function fetches the view with setted variables and return the html code.
	 * 
	 * @access public
	 * @return void
	 */
	public function fetch() {
		global $pico;
		if (!isset($this->view) || (empty($this->view))) {
			$this->view = strtolower(substr(get_class($this), 0, -10)).'.twig';
		}
		return $pico->twig->render($this->view, $this->variables);
	}
	
	
	/**
	 * The output function simply print the result of $this->fetch() to standard output.
	 * 
	 * @access public
	 * @return void
	 */
	public function output() {
		echo $this->fetch();
	}


	public function getName() {
		return $this->name;
	}
	

	public function path($index=NULL, $in=false) {
		$part = NULL;
		if (is_null($index)) {
			$part = $this->urlPath;
		} elseif (isset($this->urlPath[$index])) {
			$part = $this->urlPath[$index];
		}
		if ($in===false) {
			return $part;
		} elseif (is_null($part)) {
			return false;
		} else {
			if (!is_array($in)) $in = array($in);
			return in_array($part, $in);
		}
	}


	public function pathUri($start=0, $get=true) {
		$uri = '';
		if (is_array($this->urlPath)) {
			for ($i=$start; $i<count($this->urlPath); $i++) {
				$uri.= '/'.$this->urlPath[$i];
			}
		}
		if ($get && isset($_GET) && (count($_GET)>0)) {
			$uri.= Tools::paramsArrayToString($_GET);
		}
		return $uri;
	}


	public function variable($name, $value=NULL, $clear=false, $merge=false) {
		if ($clear) {
			unset($this->variables[$name]);
		}
		if (!is_null($value)) {
			if (isset($this->variables[$name]) && is_array($this->variables[$name]) && is_array($value) && $merge) {
				$this->variables[$name] = array_merge($this->variables[$name], $value);
			} else {
				$this->variables[$name] = $value;
			}
		}
		return isset($this->variables[$name]) ? $this->variables[$name] : NULL;
	}

}