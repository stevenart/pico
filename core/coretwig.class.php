<?php

class CoreTwig extends \Twig\Extension\AbstractExtension {


	/* Extension filters */
	public function getFilters() {
		return array(
			new \Twig\TwigFilter('blocks', 'CoreTwig::blocks', array('is_safe'=>array('html'))),
			new \Twig\TwigFilter('bytesize', 'CoreTwig::bytesize', array('is_safe'=>array('html'))),
			new \Twig\TwigFilter('cdn', 'CoreTwig::cdn'),
			new \Twig\TwigFilter('cdnImg', 'CoreTwig::cdnImg', array('is_safe'=>array('html'))),
			new \Twig\TwigFilter('cdnJoin', 'CoreTwig::cdnJoin'),
			new \Twig\TwigFilter('cdnRaw', 'CoreTwig::cdn', array('is_safe'=>array('all'))),
			new \Twig\TwigFilter('ceil', 'CoreTwig::ceil', array('is_safe'=>array('all'))),
			new \Twig\TwigFilter('cleanHtml', 'CoreTwig::cleanHtml', array('is_safe'=>array('html'))),
			new \Twig\TwigFilter('colorize', 'CoreTwig::colorize', array('is_safe'=>array('all'))),
			new \Twig\TwigFilter('cut', 'CoreTwig::cut'),
			new \Twig\TwigFilter('dateI18n', 'CoreTwig::dateI18n', array('is_safe'=>array('html'))),
			new \Twig\TwigFilter('debug', 'CoreTwig::debug', array('pre_escape'=>'html', 'is_safe'=>array('html'))),
			new \Twig\TwigFilter('defer', 'CoreTwig::defer'),
			new \Twig\TwigFilter('firstTag', 'CoreTwig::firstTag', array('is_safe'=>array('all'))),
			new \Twig\TwigFilter('float', 'CoreTwig::float', array('is_safe'=>array('all'))),
			new \Twig\TwigFilter('floor', 'CoreTwig::floor', array('is_safe'=>array('all'))),
			new \Twig\TwigFilter('href', 'CoreTwig::href', array('is_safe'=>array('all'))),
			new \Twig\TwigFilter('i18n', 'CoreTwig::i18n', array('is_safe'=>array('html'))),
			new \Twig\TwigFilter('i18np', 'CoreTwig::i18np', array('is_safe'=>array('html'))),
			new \Twig\TwigFilter('i18nRaw', 'CoreTwig::i18n', array('is_safe'=>array('all'))),
			new \Twig\TwigFilter('ids', 'CoreTwig::ids'),
			new \Twig\TwigFilter('intersect', 'CoreTwig::intersect'),
			new \Twig\TwigFilter('inValues', 'CoreTwig::inValues'),
			new \Twig\TwigFilter('jsFile', 'CoreTwig::jsFile', array('pre_escape'=>'html', 'is_safe'=>array('html'))),
			new \Twig\TwigFilter('jsHref', 'CoreTwig::jsHref', array('is_safe'=>array('all'))),
			new \Twig\TwigFilter('jsonDecode', 'CoreTwig::jsonDecode'),
			new \Twig\TwigFilter('lowerFirst', 'CoreTwig::lowerFirst'),
			new \Twig\TwigFilter('money', 'CoreTwig::money', array('is_safe'=>array('html'))),
			new \Twig\TwigFilter('nl2p', 'CoreTwig::nl2p', array('pre_escape'=>'html', 'is_safe'=>array('html'))),
			new \Twig\TwigFilter('nl2h2p', 'CoreTwig::nl2h2p', array('pre_escape'=>'html', 'is_safe'=>array('html'))),
			new \Twig\TwigFilter('obfuscate', 'CoreTwig::obfuscate', array('is_safe'=>array('html'))),
			new \Twig\TwigFilter('shuffle', 'CoreTwig::shuffle'),
			new \Twig\TwigFilter('summary', 'CoreTwig::summary'),
			new \Twig\TwigFilter('tel', 'CoreTwig::tel'),
			new \Twig\TwigFilter('unset', 'CoreTwig::unset_twig', array('is_safe'=>array('all'))),
			new \Twig\TwigFilter('urlDomain', 'CoreTwig::urlDomain'),
			new \Twig\TwigFilter('values', 'CoreTwig::values'),
			new \Twig\TwigFilter('viewExists', 'CoreTwig::viewExists'),
			new \Twig\TwigFilter('wysiwyg', 'CoreTwig::wysiwyg', array('is_safe'=>array('html'))),
		);
	}
	
	/* Extension functions */
	public function getFunctions() {
		return array(
			new \Twig\TwigFunction('cdnClearMissing', 'CoreTwig::cdnClearMissing', array('pre_escape'=>'html', 'is_safe'=>array('html'))),
			new \Twig\TwigFunction('cdnGallery', 'CoreTwig::cdnGallery', array('pre_escape'=>'html', 'is_safe'=>array('html'))),
			new \Twig\TwigFunction('cssLoad', 'CoreTwig::cssLoad', array('pre_escape'=>'html', 'is_safe'=>array('html'))),
			new \Twig\TwigFunction('deferred', 'CoreTwig::deferred', array()),
			new \Twig\TwigFunction('deferFlush', 'CoreTwig::deferFlush', array('pre_escape'=>'html', 'is_safe'=>array('html'))),
			new \Twig\TwigFunction('jsLoad', 'CoreTwig::jsLoad', array('pre_escape'=>'html', 'is_safe'=>array('html'))),
			new \Twig\TwigFunction('token', 'CoreTwig::token', array('pre_escape'=>'html', 'is_safe'=>array('html'))),
			new \Twig\TwigFunction('uploadMaxSize', 'CoreTwig::uploadMaxSize', array('pre_escape'=>'html', 'is_safe'=>array('html'))),
		);
	}

	/* Extension tests */
	public function getTests() {
		return array(
			new \Twig\TwigTest('external', 'CoreTwig::testExternal'),
			new \Twig\TwigTest('numeric', 'CoreTwig::testNumeric'),
		);
	}


	static public function blocks($blocks, int $blockNoStart=0) {
		$blocks = json_decode(rawurldecode($blocks), true);
		$html = '';
		$blockNo = $blockNoStart;
		if (is_array($blocks)) {
			foreach ($blocks as $blockData) {
				if (isset($blockData['_block'])) {
					$block = Block::factory($blockData['_block']);
					if (is_a($block, 'Block')) {
						$block->load($blockData);
						$html.= ' '.$block->render($blockNo++);
					}
				}
			}
		}
		return $html;
	}


	static public function bytesize($size, $i18n=true) {
		global $pico;
		$mod = 1024;
		$size = intval($size);
		$units = explode(' ','B KB MB GB TB PB');
		if ($i18n) {
		    if (isset($pico->config['i18n'][$pico->i18n->lang().'_filesize'])) {
		    	$units = explode(' ',$pico->config['i18n'][$pico->i18n->lang().'_filesize']);
		    }
		}
	    for ($i = 0; $size > $mod; $i++) {
	    	 $size /= $mod;
	    }
	    return round($size, 2) . ' ' . $units[$i];
	}


	static public function cdn($id, $thumb='', $download=false) {
		global $pico;
		if (is_array($id)) {
			if (isset($id['id']) && isset($id['key']) && isset($id['extension'])) {
				return $pico->cdnHref($id, $thumb, $download);
			} else {
				$id = reset($id);
			}
		}
		if (!isset($pico->cdn[$id])) {
			$pico->cdnHref($id); // Try on-demand loading
		}
		if (isset($pico->cdn[$id])) {
			if (!in_array($pico->cdn[$id]['extension'], Thumbnail::$driverExtensions)) {
				$thumb = '';
			}
			return $pico->cdnHref($id, $thumb, $download);
		}
		return '#';
	}


	static public function cdnImg($id, $thumb='', $cssClass='', $lang=NULL, $data=NULL) {
		global $pico;
		if (is_array($id)) {
			if (!isset($id['id'])) {
				$id = intval($id['id']);
			} else {
				$id = reset($id);
			}
		}
		if (is_null($lang) || !in_array($lang, $pico->i18n->langs())) {
			$lang = $pico->i18n->lang();
		}
		if (!isset($pico->cdn[$id])) {
			$pico->cdnHref($id); // Try on-demand loading
		}
		$width = $pico->cdn[$id]['width'];
		$height = $pico->cdn[$id]['height'];
		$ratio = $pico->cdn[$id]['ratio'];
		if (!in_array($pico->cdn[$id]['extension'], Thumbnail::$driverExtensions)) {
			$thumb = '';
		}
		if ($thumb) {
			if (strpos($thumb, '_')) {
				$extension = explode('_', $thumb)[1];
			} else {
				$extension = $pico->cdn[$id]['extension'];
			}
			$thumbFile = __ROOT.'/cdn/'.$id.'_'.$pico->cdn[$id]['key'].'_'.$thumb.'.'.$extension;
			if (!file_exists($thumbFile)) {
				CdnModel::generateThumbnail($id.'_'.$pico->cdn[$id]['key'].'_'.$thumb.'.'.$extension);
			}
			if (file_exists($thumbFile)) {
				$imageSize = getimagesize($thumbFile);
				$width = $imageSize[0];
				$height = $imageSize[1];
				$ratio = $imageSize[0] / $imageSize[1];
			}
		}
		if (isset($pico->cdn[$id])) {
			$url = $pico->cdnHref($id, $thumb);
			$dataAttr = '';
			if (is_array($data)) {
				foreach($data as $key => $value) {
					$dataAttr.= ' data-'.$key.'="'.str_replace('"', '', $value).'" ';
				}
			}
			return '<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" class="'.$cssClass.'" data-lazyload="'.$url.'" data-ratio="'.$ratio.'" alt="'.str_replace('"', '', $pico->cdn[$id][$lang.'-alt']).'" width="'.$width.'" height="'.$height.'"'.$dataAttr.' style="--colorImg:'.$pico->cdn[$id]['color'].';">';
		}
		return '';
	}
	

	static public function cdnJoin($ids) {
		global $pico;
		if (!is_array($ids)) {
			$ids = explode(',', $ids);
		}
		$values = array();
		foreach($ids as $id) {
			if (!isset($pico->cdn[$id])) {
				$pico->cdnHref($id); // Try on-demand loading
			}
			if (isset($pico->cdn[$id])) {
				$values[$id] = $pico->cdn[$id];
			}
		}
		return $values;
	}


	static public function cdnClearMissing($ids) {
		global $pico;
		$implode = false;
		if (!is_array($ids)) {
			$implode = true;
			$ids = explode(',', $ids);
		}
		array_walk($ids, 'intval');
		$existing = $pico->db->column('cdn', '`id`', '`id` IN ('.implode(',', $ids).')', 'FIELD(`id`,'.implode(',', $ids).')');
		return $implode ? implode(',', $existing) : $existing;
	}


	static public function ceil($value) {
		return ceil($value);
	}


	static public function cleanHtml($value, $allowed='<a><blockquote><br><h1><h2><h3><img><p><table><tr><td>') {
		$value = preg_replace('#<(head|script)(?:[^>]+)?>.*?</\1>#s', '', $value);
		$value = strip_tags($value, $allowed);
		return $value;
	}
	

	static public function cdnGallery($ids) {
		global $pico;
		if (!is_array($ids)) {
			$ids = explode(',', $ids);
		}
		array_walk($ids, 'intval');
		$cdn = new Model('cdn');
		$cdn->fetch('`id` IN ('.implode(',', $ids).')', 'FIELD(`id`,'.implode(',', $ids).')');
		return $cdn->values();
	}


	static public function colorize($string, $opacity=NULL) {
		$string = preg_replace('/[^0-9a-f]/u', '0', $string);
		while(((strlen($string)%3)!=0) || (strlen($string)<6)) {
			$string.= '0';
		}
		$third = (strlen($string)/3);
		$color = array(
			substr($string, 0, $third),
			substr($string, $third, $third),
			substr($string, 2*$third, $third)
		);
		if ($third>2) {
			foreach($color as &$foo) {
				$foo = substr(ltrim($foo, '0'), 0, 2);
			}	
			unset($foo);
		}
		if (!is_null($opacity)) {
			return 'rgba('.hexdec($color[0]).','.hexdec($color[1]).','.hexdec($color[2]).','.$opacity.')';
		} else {
			return '#'.$color[0].$color[1].$color[2];
		}
	}
	

	/**
	 * The cssLoad function generates a link tag to be integrated in the head part of your html from files.
	 * 
	 * @access public
	 * @static
	 * @param array $files
	 * @param string $files
	 * @return void
	 */
	static public function cssLoad($files, $media='all') {
		global $pico;
		if (!is_array($files)) {
			$files = [$files];
		}
		$cacheInvalider = filemtime(__ROOT.'/app/css/app.scss'); 
		if (isset($pico->config['site']['cache_css']) && (!$pico->config['site']['cache_css'])) {
			$cacheInvalider.= '&clearcache=1';
		}
		return '<link rel="stylesheet" type="text/css" media="'.$media.'" href="'.$pico->href(';css.php?cache='.$cacheInvalider.'&files='.urlencode(json_encode($files))).'">';
	}


	static public function cut($string, $at, $cutSymbol=NULL) {
		$originalLength = strlen($string);
		if (is_array($at)) {
			$position = $originalLength;
			foreach($at as $foo) {
				$bar = stripos($string, $foo);
				if (($bar!==false) && ($bar<$position)) {
					$position = $bar;
				}
			}
			$at = $position;
		} elseif (is_string($at)) {
			$at = stripos($string, $at);
		}
		if (is_integer($at)) {
			$string = substr($string, 0, $at);
		}
		if (is_string($cutSymbol) && ($originalLength>strlen($string))) {
			$string.= $cutSymbol;
		}
		return $string;
	}
	
	
	static public function dateI18n($time, $format='', $formatConfig=true) {
		global $pico;
		if ($formatConfig) {
			if (!empty($format)) {
				$format = '_'.$format;
			}
			if (isset($pico->config['i18n'][$pico->i18n->lang().'_date'.$format])) {
				$format = $pico->config['i18n'][$pico->i18n->lang().'_date'.$format];
			} else {
				$format = '%x';
			}
		}
		return trim(strftime($format, is_int($time) ? $time : strtotime($time)));
	}


	/**
	 * The debug filter.
	 * 
	 * @access public
	 * @static
	 * @param mixed $var Variable to print
	 * @param string $name Variable name (optional)
	 * @param bool $escapeHTML 
	 * @return string HTML version of $var
	 */
	static public function debug($var, $name=false, $escapeHTML=true) {
		global $pico;

		/* Escaping HTML */
		if ($escapeHTML) {
			if (!function_exists('__debug_escape')) {
				function __debug_escape(&$v) {
					if (is_array($v)) {
						foreach($v as &$vv) __debug_escape($vv);
					} elseif (is_string($v)) {
						$v = str_replace(array("\r", "\n"), array('', '\n'), htmlentities($v));
					}
				}
			}
			__debug_escape($var);
		}

		return '<pre>'.($name?'<strong>'.$name.' = </strong>':'').print_r($var,true).'</pre>';
	}



	/**
	 * The defer filter.
	 * 
	 * @access public
	 * @static
	 * @param string $code
	 * @return string 
	 */
	static public function defer($code, $name=NULL) {
		global $pico;	
		if (!isset($_SESSION['pico__defer'])) {
			$_SESSION['pico__defer'] = true;
			$_SESSION['pico__deferred'] = array();
		}
		if ($_SESSION['pico__defer']) {
			if (is_null($name)) {
				$_SESSION['pico__deferred'][] = $code;
			} else {
				$_SESSION['pico__deferred'][$name] = $code;
			}
		} else {
			if (is_null($name) || !isset($_SESSION['pico__deferred'][$name])) {
				return $code;
			}
		}
		return '';
	}


	static public function deferred() {
		if (!isset($_SESSION['pico__defer'])) {
			return true;
		} else {
			return $_SESSION['pico__defer'];
		}
	}


	static public function deferFlush() {
		$output = '';
		if (isset($_SESSION['pico__deferred'])) {
			foreach ($_SESSION['pico__deferred'] as $name => $code) {
				$output.= $code." \n";
			}
		}
		$_SESSION['pico__deferred'] = array();
		$_SESSION['pico__defer'] = false;
		return $output;
	}


	static public function firstTag($html, $tags) {
		global $pico;
		if (!is_array($tags)) {
			$tags = array($tags);
		}
		$html = strip_tags($html, '<'.implode('><', $tags).'>');
		foreach ($tags as $tag) {
			preg_match("/<$tag ?.*>(.*)<\/$tag>/", $html, $matches);
			if (isset($matches[1])) {
				return $matches[1];
			}
		}
		return '';
	}


	static public function float($value) {
		return $value + 0;
	}


	static public function floor($value) {
		return floor($value);
	}


	/**
	 * The href filter.
	 * 
	 * @access public
	 * @static
	 * @param string $href
	 * @param bool $includeHost
	 * @param bool $disableCache
	 * @return string URL
	 */
	static public function href($href, $includeHost=true, $disableCache=false) {
		global $pico;
		return $pico->href($href, $includeHost, $disableCache);
	}
	

	/**
	 * The i18n filter.
	 * 
	 * @access public
	 * @static
	 * @param string $slug
	 * @param array $parameters
	 * @param string $lang
	 * @param bool $create
	 * @return string URL
	 */
	static public function i18n($slug, $parameters=NULL, $lang=NULL, $create=true, $betterHtml=false) {
		global $pico;
		if (!is_null($parameters) && !is_array($parameters)) $parameters = array($parameters);
		$text = i18n($slug, $parameters, $lang, $create, $betterHtml);
		if (strpos($slug, '.MESSAGE_')) {
			$text = nl2br($text);
		}
		return $text;
	}


	static public function i18np($fieldName, $lang=NULL, $includeCmsOnly=true) {
		global $pico;
		if (is_null($lang) || !in_array($lang, $pico->i18n->langs(NULL, $includeCmsOnly))) {
			$lang = $pico->i18n->lang();
		}
		return $lang.'-'.$fieldName;
	}


	static public function ids($value, $count=NULL, $offset=0) {
		$ids = array_filter(explode(',', $value));
		array_walk($ids, 'intval');
		if (is_null($count)) {
			return $ids;
		} else {
			return array_slice($ids, $offset, $count);
		}
	}

	
	static public function intersect($array1, $array2) {
		if (is_array($array1) && !is_array($array2)) return $array1;
		if (!is_array($array1) && is_array($array2)) return $array2;
		if (!is_array($array1) && !is_array($array2)) return false;
		return array_intersect($array1, $array2);
	}


	/**
	 * The inValues filter returns true if a value is find in a string or array.
	 * 
	 * @access public
	 * @static
	 * @param string $value
	 * @param string $values
	 * @return bool
	 */
	static public function inValues($value, $values) {
		if (!is_array($values)) $values = explode(',', $values);
		return in_array($value, $values);
	}


	/**
	 * The jsFile filter automatically creates a link tag to include a js file with an href given.
	 * 
	 * @access public
	 * @static
	 * @param mixed $href
	 * @return void
	 */
	static public function jsFile($href, $includeHost=true, $disableCache=false) {
		global $pico;
		return '<script type="text/javascript" src="'.$pico->href($href, $includeHost, $disableCache).'"></script>';
	}



	/**
	 * The jsonDecode filter decodes json string (objects are converted into a associative arrays).
	 *
	 * @param string $json The json string
	 *
	 * @return mixed Decoded value (objects are converted into a associative arrays).
	 */
	static public function jsonDecode($json) {
		return json_decode($json, true);
	}


	/**
	 * The jsHref function generates a link to a js file.
	 * 
	 * @access public
	 * @static
	 * @param array $files
	 * @param string $files
	 * @return void
	 */
	static public function jsHref($files) {
		global $pico;
		if (!is_array($files)) {
			$files = [$files];
		}
		return $pico->href(';js.php?files='.urlencode(json_encode($files)));
	}


	/**
	 * The jsLoad function generates a link tag to be integrated in the head part of your html from files.
	 * 
	 * @access public
	 * @static
	 * @param array $files
	 * @param string $files
	 * @return void
	 */
	static public function jsLoad($files) {
		global $pico;
		if (!is_array($files)) {
			$files = [$files];
		}
		$cacheInvalider = '';
		if (isset($pico->config['site']['cache_js']) && (!$pico->config['site']['cache_js'])) {
			$cacheInvalider.= '&clearcache=1';
		}
		return '<script type="text/javascript" src="'.$pico->href(';js.php?'.$cacheInvalider.'&files='.urlencode(json_encode($files))).'"></script>';
	}
	

	static public function lowerFirst($value) {
		global $pico;
		return strtolower(substr($value, 0, 1)).substr($value, 1); 
	}


	static public function money($value, $float=NULL, $currency='&nbsp;€') {
		global $pico;
		if (is_null($float)) {
			$float = (floor($value)!=$value);
		}
		if ($float===true) {
			$float = 2;
		}
		return number_format($value, ($float ? $float : 0), $pico->i18n->locale['decimal_point'], $pico->i18n->locale['mon_thousands_sep'])."$currency"; 
	}


	static public function nl2p($value) {
		global $pico;
		return '<p>'.preg_replace('#(<br\s*?/?>\s*?){2,}#', '</p><p>', nl2br($value)).'</p>'; 
	}


	static public function nl2h2p($value) {
		global $pico;
		$html = '<p>'.preg_replace('#(<br\s*?/?>\s*?){2,}#', '</p><p>', nl2br($value)).'</p>'; 
		$html = preg_replace('/<p>/', '<h2>', $html, 1);
		$html = preg_replace('/<\/p>/', '</h2>', $html, 1);
		return '<div class="nl2h2p">'.$html.'</div>';
	}


	static public function obfuscate($value) {
		global $pico;
		$out = '';
		if (is_string($value)) {
			for ($i=0; $i<mb_strlen($value); $i++) {
				$code = mb_ord($value[$i]);
				if ($code>31 && $code<127) {
					$out.= "&#$code;";
				} else {
					$out.= $value[$i];
				}
			}
		} else {
			return $value;
		}
		return $out;
	}


	static public function shuffle($values, $assoc=false, $delimiter=',') {
		if (!is_array($values)) {
			$values = explode($delimiter, $values);
		}
		if ($assoc) {
			$return = array();
			$keys = array_keys($values);
			shuffle($keys);
			foreach ($keys as $key) {
				$return[$key] = $values[$key];
			}
			return $return;
		} else {
			shuffle($values);
			return $values;
		}
	}


	static public function summary($text, $maxLength=200, $cutSymbol=NULL, $stripHTML=true, $inline=true) {
		if ($stripHTML) {
			$text = preg_replace('/<br(.?)(\/?)>/i', ' ', $text);
			$text = preg_replace('/<\/p>/i', ' ', $text);
			$text = preg_replace('/<\/h[1-9]>/i', ' ', $text);
			$text = preg_replace('/<\/td>/i', ' | ', $text);
			$text = strip_tags($text);
		}
		if ($inline) {
			$text = str_replace(array("\n", "\r"), ' ', $text);
		}
		$newString = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
		$length = mb_strlen($newString);
		if ($length>$maxLength) {
			$newString = mb_substr($newString, 0, $maxLength);
			$cutPosition = mb_strrpos($newString, ' ');
			if ($cutPosition===false || (mb_substr($newString, $maxLength, 1)==' ')) {
				$newString = mb_substr($newString, 0, $maxLength).$cutSymbol;
			} else {
				$newString = mb_substr($newString, 0, $cutPosition).$cutSymbol;
			}
		}
		return $newString;
	}


	static public function tel($value) {
		return preg_replace('/[^0-9]/u', '', str_replace('+', '00', $value));
 	}
	

	static public function testExternal($value) {
		if ((substr($value, 0, 1)=='/') || (substr($value, 0, strlen(__URL))==__URL)) {
			return false;	
		}
		return true;
	}


	static public function testNumeric($value) {
		return is_numeric($value);
	}


	static public function token($type='form') {
		global $pico;
		$tokens = $pico->session('tokens');
		if (!is_array($tokens)) {
			$tokens = array();
		}
		if (!isset($tokens[$type])) {
			$tokens[$type] = Tools::generateSecret();
		}
		$pico->session('tokens', $tokens);
		return $tokens[$type];
	}


	/**
	 * The unset filter.
	 * 
	 * @access public
	 * @static
	 * @param 
	 * @return
	 */
	static public function unset_twig($array, $keys) {
		global $pico;
		if (!is_array($array)) return $array;
		if (!is_array($keys)) $keys = array($keys);
		return array_diff_key($array, array_flip($keys));
	}


	static public function uploadMaxSize() {
		global $pico;
		return Tools::getUploadMaxSize();
	}

	
	/**
	 * The urlDomain filters return the domain name of a given url.
	 * 
	 * @access public
	 * @static
	 * @param mixed $url
	 * @return void
	 */
	static public function urlDomain($url) {
		return str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));
	}


	/**
	 * The viewExists filters check the existence of a view.
	 * 
	 * @access public
	 * @static
	 * @param mixed $url
	 * @return void
	 */
	static public function viewExists($view) {
		return (appfile('views/'.$view) ? true : false);
	}


	static public function wysiwyg($html, $cssClass='') {
		$html = str_replace(
			array(' :', ' !', ' ?', '« ', ' »', '&laquo; ', '&raquo; '), 
			array('&nbsp;:', '&nbsp;!', '&nbsp;?', '«&nbsp;', '&nbsp;»', '&laquo;&nbsp;', '&nbsp;&raquo;'), 
			$html
		);
		$html = preg_replace(
			'/<img([^>]*) src="([^\"]*)" ([^>]*)data-lazyload([^>]*)>/',
			'<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" $1 $3 data-lazyload$4>',
			$html
		);
		return '<div class="wysiwyg '.$cssClass.'">'.$html.'</div>';
	}

}	